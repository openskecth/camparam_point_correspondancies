function sketch_out = keepOnlyNonRemovedStrokes(sketch)
% Input:
%   sketch: struct with fields:
% 
%       canvas: struct with fields:
%            height: sketch height
%            pen_width: 1.5000
%            width: sketch width
%         
%       strokes: struct with fields:
%           is_removed
%           points

% Output:
%   sketch: struct with fields:
% 
%       canvas: struct with fields:
%            height: sketch height
%            pen_width: 1.5000
%            width: sketch width
%         
%       strokes: struct with fields:
%           points

    sketch_out.canvas = sketch.canvas;
    
    num_strokes_in = length(sketch.strokes);
    num_strokes_out = 0;
    
    
    for i=1:num_strokes_in
        if (iscell(sketch.strokes))
            stroke = sketch.strokes{i};
        else
            stroke = sketch.strokes(i);
        end
        
        if (~stroke.is_removed && isfield(stroke, 'points'))
            num_strokes_out = num_strokes_out+1;
            sketch_out.strokes(num_strokes_out).points = ...
                stroke.points;
        end
    end 
end