function filepath_line_types = getFilepathLineLabels(folder_designer, designer, object_name, view)

  obj_dir_path = fullfile(folder_designer, designer,...
        object_name, ['drawings_' object_name]);
   
  filepath_line_types = fullfile( obj_dir_path, ['semantic_' view '_concept'], 'strokes_lines_types.json');       
end