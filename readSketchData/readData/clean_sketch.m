function clean_sketch( filepath, filepath_out)
   fid = fopen(filepath);
   raw = fread(fid,inf);
   str = char(raw');
   fclose(fid);
   sketch = jsondecode(str);
   sketch = keepOnlyNonEmptyStrokes(sketch);
   str = jsonencode(sketch);
   fid = fopen(filepath_out, 'w');
   fwrite(fid, str);
   fclose(fid);
   fprintf('read file: %s\n', filepath);
end


