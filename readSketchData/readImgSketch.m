% function img = readImgSketch(folder_designer, designer, object_name, view)
% 
% Returns an image of a sketch.
% Input:
%   folder_designer -- path to the folder with subfolders of designers
%   designer        -- the name of designer, e.g. 'student1',
%                       'Professional1'.
%   object_name     -- the name of the object, e.g. 'vacuum cleaner' 
%   view            -- 'view1' or 'view2'
%   drawing_type    -- 'concept' or 'presentation', default 'concept'
% 
% Output:
%   img    -- an image of a designer sketch.
function img = readImgSketch(folder_designer, designer, object_name, view, drawing_type)
    if ~exist('drawing_type', 'var')
        drawing_type = 'concept';
    end
    
    obj_dir_path = fullfile(folder_designer, designer,...
        object_name, ['drawings_' object_name]);
    
    %Read the opaque version if exists:
    if exist(fullfile(obj_dir_path, [view '_' drawing_type '_opaque.png']), 'file')
        img = imread(fullfile(obj_dir_path, [view '_' drawing_type '_opaque.png']));   
    else
    %Convert transparent version to opaque and save the opaque version:
        [~,~,img] = imread(fullfile(obj_dir_path, [view '_' drawing_type '.png']));   
        img = 255-img;
        imwrite(img, fullfile(obj_dir_path, [view '_' drawing_type '_opaque.png']));   
    end
end