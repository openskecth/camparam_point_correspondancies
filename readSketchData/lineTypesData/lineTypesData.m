% function [label, line_types_names, line_types_colors, labels_order] = ...
%   lineTypesData()


function [label, line_types_names, line_types_colors, labels_order, line_types_ids] = lineTypesData()

label.occluding_contour = 0;
label.ridges_visible = 1;
label.vallleys_visible = 2;
label.ridges_occluded = 3;
label.valleys_occluded = 4;
label.cross_sections = 5;

label.context_axis_and_grids = 6;
label.context_scaffolds = 7; 
label.context_scaffolds_lines_to_vp = 8;
label.context_scaffold_square = 9;
label.context_scaffold_circle = 10;

label.surfacing_cross_section = 11;
label.surfacing_cross_section_scaffold = 12;
label.surfacing_mirroring = 13;
label.surfacing_temporary_plane = 14;
label.surfacing_projection_lines = 15;

label.propotions_div_rectangle2 = 16;
label.propotions_div_rectangle3 = 17;
label.propotions_div_ellipse = 18; 
label.propotions_mult_rectangle = 19;
label.hinging_and_rotating_elements = 20;

label.ticks = 21;

label.hatching = 22;
label.text = 23;
label.outline = 24;
label.shadow_construction = 25;
label.background = 26;
label.removed = 27;


labels_order = ...
[
   label.ridges_occluded;
   label.valleys_occluded;
   label.cross_sections;
   label.context_axis_and_grids;
   label.context_scaffolds;
   label.context_scaffolds_lines_to_vp;
   label.context_scaffold_square;
   label.context_scaffold_circle;
   label.propotions_div_rectangle2;
   label.propotions_div_rectangle3;
   label.propotions_div_ellipse;
   label.propotions_mult_rectangle;
   label.hinging_and_rotating_elements;
   label.surfacing_projection_lines;
   label.surfacing_cross_section;
   label.surfacing_mirroring;
   label.surfacing_temporary_plane;
   label.ticks;
   label.hatching
];



line_types_ids = ...
{
'smooth_contours', ...
'ridges_visible', ...
'valleys_visible', ...
'ridges_occluded',...
'valleys_occluded',...
'discriptive_cross_section',...
'axis_and_grids',...
'scaffolds',... 
'scaffolds_lines_to_vp',...
'scaffolds_square',...
'scaffolds_circle_tangents',...
'surfacing_cross_section',...
'cross_section_scaffold',...
'mirroring',...
'temporary_plane',...
'projection_lines',...
'divide_rectangle2',...
'divide_rectangle3',...
'divide_ellipse',... 
'mult_rectangle',...
'hinging_and_rotating_elements',...
'ticks',...
'hatching',...
'text',...
'rough_outline',...
'shadow_construction',...
'background',...
'removed'};

line_types_names = ...
{
'silhouette smooth', ...
'ridges visible', ...
'vallleys visible', ...
'ridges occluded',...
'valleys occluded',...
'discriptive cross sections',...
'axis and grids',...
'scaffolds',... 
'scaffolds: lines to VP',...
'scaffolds: square for an ellipse',...
'scaffolds: tangents',...
'surfacing: cross section',...
'surfacing: cross section scaffold',...
'surfacing: mirroring',...
'surfacing: temporary plane',...
'surfacing: projection lines',...
'proportions div. rectangle2',...
'proportions div. rectangle3',...
'proportions div. ellipse',... 
'proportions mult. rectangle',...
'hinging and rotating elements',...
'ticks',...
'hatching',...
'text',...
'outline',...
'shadow construction',...
'background',...
'removed'};


line_types_colors = [44, 108,  0;%'silhouette smooth', ...
           0,  40, 140;%'ridges visible', ...
           220,20,60;
            0, 149, 255;
            255, 184,  29;
            170,   0, 255;
            127,138, 55;
            0,250,154;
            255, 109,  16;
            255,212,0;
            0,234,255;
            0, 128, 128;
            132,215,187;
            150,149,211;
            221,160,221;
            106,255,0;
            255,0,170;
            191,255,0;
            143,106,35;
            75,0,130;
            64,224,208;
            143,35,35;
            249,255,94;
            0,0,0;
            100,100,100;
            249,255,94]/255.0;