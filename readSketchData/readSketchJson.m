function [ sketch ] = readSketchJson( filepath )
   fprintf('readSketchJson(): read file: %s\n', filepath);   
   fid = fopen(filepath);
   raw = fread(fid,inf);
   str = char(raw');
   fclose(fid);
   sketch = jsondecode(str);
   fprintf('readSketchJson(): read file: %s\n', filepath);
end


