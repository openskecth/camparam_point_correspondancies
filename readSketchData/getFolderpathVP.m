% function getFolderpathVP(folder_designer, designer, object_name, view)
% 
% Returns the filepath of the json sketch representation.
% Input:
%   designer        -- the name of designer, e.g. 'student1',
%                       'Professional1'.
%   object_name     -- the name of the object, e.g. 'vacuum cleaner' 
%   view            -- 'view1' or 'view2'
%   drawing_type    -- 'concept' or 'presentation', default 'concept'
% 
% Output:
%   folderpath    -- folderpath to a folder with VP data
function folderpath = getFolderpathVP(designer, object_name, view, drawing_type)
    folder_vp = 'Z:\WiresProject\EstimatedVP';    
    %   folder_vp -- path to the folder where information about vanishing
    %               points is stored.
    if ~exist('drawing_type', 'var')
        drawing_type = 'concept';
    end
    
    folderpath = fullfile(folder_vp, designer,...
        object_name, [view '_' drawing_type]);
    
    if ~exist(folderpath, 'dir')
        mkdir(folderpath);
    end    
end