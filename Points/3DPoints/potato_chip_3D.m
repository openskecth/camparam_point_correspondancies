% y is up -z is front
function [H, folder_data] = potato_chip_3D()
    H(1,:) = [2.24995 1.12505 0.00012 1.0000]; %front
    H(2,:) = [-2.25005 1.12505 0.00012 1.0000];%back
    H(3,:) = [-0.00005 -1.12495 -1.87488 1.0000]; %right
    H(4,:) = [-0.00007 -1.12495 1.87513 1.0000];%left
    H(5,:) = [-0.00004 0.00005 0.00012 1.0000]; %center


    H(6,:) = [1.12495 -0.56252 -1.62367 1.0000];%rigth
    H(7,:) = [-1.12508 -0.5625 -1.62366 1.0000];%rigth

    H(8,:) = [-1.12507 -0.5625 1.62391 1.0000];%left
    H(9,:) = [1.12494 -0.56253 1.62392 1.0000];%left

    H(10,:) = [1.13631 0.28704 -0.01882 1.0000];
    H(11,:) = [-1.13642 0.28704 0.01906 1.0000];
    folder_data = fileparts(which(mfilename)); 
end