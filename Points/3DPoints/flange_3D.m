% y is up z is front
function [H, folder_data] = flange_3D()
    H(1, :) = [  -1.33265 -0.12149 0.02717  1.0];
    H(2, :) = [  -0.67113 -0.14532 1.1606  1.0];
    H(3, :) = [  0.672 -0.13956 1.14645  1.0];
    H(4, :) = [  1.32322 -0.13169 -0.01801  1.0];

    H(5, :) = [  -0.00568 -0.499779 1.39718 1.0];
    H(6, :) = [  -0.01023 -0.21897 1.04744  1.0];
    H(7, :) = [  0.00796 0.42035 0.76988  1.0];
    H(8, :) = [  -0.00263 0.95081 0.54661  1.0];

    H(9, :) = [  -0.01008 0.93417 -0.55325  1.0];
    H(10, :) = [  -0.00226 0.41402 -0.76973  1.0];
    H(11, :) = [  -0.00963 -0.2396 -1.06623  1.0];
    H(12, :) = [  0.01129 -0.4894 -1.40682  1.0];


    H(13, :) = [  -1.40585 -0.48188 0.0025  1.0];
    H(14, :) = [  -0.77036 0.41797 -0.01608  1.0];
    H(15, :) = [  -0.53629 0.95469 -0.00035  1.0];

    H(16, :) = [  0.55873 0.93873 0.00187  1.0];
    H(17, :) = [ 0.76586 0.4189 0.00302  1.0];
    H(18, :) = [  1.39448 -0.51251 0.0007  1.0];

    indices_all = [1:length(H)];

    folder_data = fileparts(which(mfilename)); 
end
% 
% global obj_files_folder;
% file_object = fullfile(obj_files_folder, [object_id '.obj']);
% obj = read_obj(file_object);
% vertices = obj.v;
% min_corner = min(vertices, [], 1);
% max_corner = max(vertices, [], 1);
% centroid = [0.5*(min_corner + max_corner) 0.0];
% scale = 2.0 / max(abs((max_corner-min_corner)));
% 
% H = H - repmat(centroid, size(H,1), 1);
% H(:,1:3) = H(:,1:3)*scale;
% 
% 
% 
% same(fullfile(folder_flange_data, [object_id '.mat']), 'scale', 'centroid');