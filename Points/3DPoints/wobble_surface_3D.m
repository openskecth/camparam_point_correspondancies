% y is up -z is front

function [H, folder_data] = wobble_surface_3D()
object_id = 'wobble_surface';
H(1, :) = [  -1.035250 -0.054031 1.410039  1.0];
H(2, :) = [  -0.517639 -0.054031 -1.350570  1.0];
H(3, :) = [  0.517588 -0.054031 -1.350570  1.0];
H(4, :) = [   1.035202 -0.054031 1.410039  1.0];
H(5, :) = [  0.694395 -0.039197 1.409958  1.0];
H(6, :) = [-0.00002 0.2109 1.40987  1.0];
H(7, :) = [ -0.699826 -0.041084 1.409963  1.0];
H(8, :) = [0.000510 -0.050413 0.027378  1.0];
H(9, :) = [ -0.000026 -0.058056 -0.273297  1.0];
H(10, :) = [   -0.007661 -0.056827 -1.01033  1.0]; %-0.008471 -0.054031 -1.047443

H(11,:) = [-1.035250 -0.05403 -1.350570 1.0]; %scaffold back
H(12,:) = [1.035202  -0.05403 -1.350570 1.0]; %scaffold back

H(13,:) = [-0.51764 -0.08723 -1.35057 1.0]; %bottom back
H(14,:) = [ 0.51759 -0.08723 -1.35057 1.0]; %bottom back

H(15,:) = [1.0352 -0.08724  1.41004 1.0]; %bottom front
H(16,:) = [-1.03525 -0.08724 1.41004 1.0];%bottom front

H(17,:) = [-0.00003 0.17775 1.4102 1.0];%bottom front
folder_data = fileparts(which(mfilename)); 
end
% H(17,:) = [-0.38715 -0.05403 -0.66042 1.0];
% H(18,:) = [0.38715 -0.05403 -0.66886 1.0];
%H(1, :) = -H(1,:);
% T = H;
% H(:,1) = -T(:,1);
% H(:,2) = T(:,3);
% H(:,3) = -T(:,3);

