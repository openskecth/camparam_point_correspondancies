
addpath('./Points');
addpath('./Points/3DPoints/');
addpath('./Points/2DPoints/');

object_id = 'waffle_iron';
[H, folder_data] = waffle_iron_3D();

filepath_mat = fullfile(folder_data, [object_id '.mat']);
centroid = [0.0 0.0 0.0 0.0];
scale = 1.0;
save(filepath_mat, 'scale', 'centroid');


scripts_launch = [
    {'student3_waffle_iron_v1_points'}, ... 
    {'student3_waffle_iron_v2_points'}, ...    
    {'professional1_waffle_iron_v1_points'}, ... 
    {'professional1_waffle_iron_v2_points'}, ...    
    {'professional2_waffle_iron_v1_points'}, ... 
    {'professional2_waffle_iron_v2_points'}, ...    
    {'professional3_waffle_iron_v1_points'}, ... 
    {'professional3_waffle_iron_v2_points'}, ... 
    {'professional4_waffle_iron_v1_points'}, ... 
    {'professional4_waffle_iron_v2_points'}, ...
    {'professional5_waffle_iron_v1_points'}, ... 
    {'professional5_waffle_iron_v2_points'}, ...
    {'professional6_waffle_iron_v1_points'}, ... 
    {'professional6_waffle_iron_v2_points'}, ...
    ];

for i = 1:length(scripts_launch)
    close all;
    clearvars -except scripts_launch i object_id H folder_centered_sketches folder_sketches
    eval(scripts_launch{i});
    
    
    folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
    folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
    [img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
    height  = size(img,2);
    width   = size(img,1);
    
    
    [img, scale, bottom, left, dx, dy] = scale_center_sketch(folder, view, img);
    imwrite(img, fullfile(folder_centered_sketches, [designer_id '_' object_id '_' view '.png']));
    x_m(indices_opt,1) = (x_m(indices_opt,1) - double(left))*scale + double(dx);
    x_m(indices_opt,2) = (x_m(indices_opt,2) - double(bottom))*scale + double(dy);
    
    estimate_camera_parameters_from_3D_to_2D_correspondancies;
end
