addpath('./Points');
addpath('./Points/3DPoints/');
addpath('./Points/2DPoints/');

object_id = 'bumps';

[H, folder_data] = bumps_3D();

H = scale_center_mesh(H, folder_data, object_id);
% test_rendering_bumps_v1_points


scripts_launch = [
    {'student2_bumps_v1_points'}, ... 
    {'student2_bumps_v2_points'}, ...
    {'student9_bumps_v1_points'}, ... 
    {'student9_bumps_v2_points'}, ...
    {'professional1_bumps_v1_points'}, ... 
    {'professional1_bumps_v2_points'}, ...    
    {'professional2_bumps_v1_points'}, ... 
    {'professional2_bumps_v2_points'}, ...    
    {'professional3_bumps_v1_points'}, ... 
    {'professional3_bumps_v2_points'}, ...
    {'professional4_bumps_v1_points'}, ... 
    {'professional4_bumps_v2_points'}, ...
    {'professional5_bumps_v1_points'}, ... 
    {'professional5_bumps_v2_points'}, ...
    {'professional6_bumps_v1_points'}, ... 
    {'professional6_bumps_v2_points'}, ...
    ];


for i = 1:length(scripts_launch)
    close all;
    clearvars -except scripts_launch i object_id H folder_centered_sketches
    eval(scripts_launch{i});
    figure(1);
    imshow(img)
    hold on;
    x =  x_m(indices_opt,1);
    y =  x_m(indices_opt,2);
    plot(x,y, 'o-');
    a = [indices_opt]'; b = num2str(a); c = cellstr(b);
    clear text; t = text(x, y, c);

    [img, scale, bottom, left, dx, dy] = scale_center_sketch(folder, view, img);
    imwrite(img, fullfile(folder_centered_sketches, [designer_id '_' object_id '_' view '.png']));
    x_m(indices_opt,1) = (x_m(indices_opt,1) - double(left))*scale + double(dx);
    x_m(indices_opt,2) = (x_m(indices_opt,2) - double(bottom))*scale + double(dy);

    figure(2);
    imshow(img)
    hold on;
    x =  x_m(indices_opt,1);
    y =  x_m(indices_opt,2);
    plot(x,y, 'o-');
    a = [indices_opt]'; b = num2str(a); c = cellstr(b);
    clear text; t = text(x, y, c);

    estimate_camera_parameters_from_3D_to_2D_correspondancies;
end
