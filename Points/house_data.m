

% house3D_points_blender;
% house_3D;
[H, folder_data] = house_3D();
% H = scale_center_mesh(H, folder_data, object_id);
filepath_mat = fullfile(folder_data, [object_id '.mat']);
centroid = [0.0 0.0 0.0 0.0];
scale = 1.0;
save(filepath_mat, 'scale', 'centroid');
% Check code
% ground_truth_test_code

% house v1_00

scripts_launch = [
    {'student1_house_v1_points'}, ... 
    {'student1_house_v2_points'}, ...
    {'student2_house_v1_points'}, ... 
    {'student2_house_v2_points'}, ...
    {'student3_house_v1_points'}, ... 
    {'student3_house_v2_points'}, ...
    {'student4_house_v1_points'}, ... 
    {'student4_house_v2_points'}, ...
    {'student5_house_v1_points'}, ...
    {'student5_house_v2_points'}, ...
    {'student6_house_v1_points'}, ...
    {'student8_house_v1_points'}, ...
    {'student8_house_v2_points'}, ...
    {'student9_house_v1_points'}, ...
    {'student9_house_v2_points'}, ...
    {'professional1_house_v1_points'}, ... 
    {'professional1_house_v2_points'}, ...    
    {'professional2_house_v1_points'}, ... 
    {'professional2_house_v2_points'}, ...    
    {'professional3_house_v1_points'}, ... 
    {'professional3_house_v2_points'}, ...
    {'professional4_house_v1_points'}, ... 
    {'professional4_house_v2_points'}, ...
    {'professional5_house_v1_points'}, ... 
    {'professional5_house_v2_points'}, ...
    {'professional6_house_v1_points'}, ... 
    {'professional6_house_v2_points'}, ...
    ];

for i = 1:length(scripts_launch)
    close all;
    clearvars -except scripts_launch i object_id H folder_centered_sketches
    eval(scripts_launch{i});
    [img, scale, bottom, left, dx, dy] = scale_center_sketch(folder, view, img);
    imwrite(img, fullfile(folder_centered_sketches, [designer_id '_' object_id '_' view '.png']));
    x_m(indices_opt,1) = (x_m(indices_opt,1) - double(left))*scale + double(dx);
    x_m(indices_opt,2) = (x_m(indices_opt,2) - double(bottom))*scale + double(dy);

    estimate_camera_parameters_from_3D_to_2D_correspondancies;
end



% clearvars;
% close all;
% student1_house_v1_points; %student1
% estimate_camera_parameters_from_3D_to_2D_correspondancies

% clearvars;
% close all;
% student1_house_v2_points
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student2_house_v1_points; %student2
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student2_house_v2_points
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student3_house_v1_points; %student3
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student3_house_v2_points
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student4_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student4_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student5_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student5_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student6_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student8_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student8_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student9_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% student9_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% %% Professionals
% clearvars;
% close all;
% professional1_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional1_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% 
% professional2_house_v1_points;
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional2_house_v2_points;
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional3_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional3_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional4_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional4_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional5_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional5_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional6_house_v1_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
% clearvars;
% close all;
% professional6_house_v2_points; 
% estimate_camera_parameters_from_3D_to_2D_correspondancies
% 
