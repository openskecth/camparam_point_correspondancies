mouse_3D;
object_id = 'mouse';

figure(3);
designer_type = 'professionals';
designer_id ='Professional1';

view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [189.96 440.91 1.00];
x_m(2,:) = [260.34 464.37 1.00];
x_m(3,:) = [648.49 417.45 1.00];
x_m(4,:) = [334.27 325.74 1.00];
x_m(5,:) = [374.08 256.07 1.00];
x_m(6,:) = [298.01 332.14 1.00];
x_m(7,:) = [406.78 316.50 1.00];
x_m(8,:) = [274.55 283.80 1.00];
x_m(9,:) = [221.00 452.00 1.00];
x_m(10,:) = [487.00 364.00 1.00];
x_m(11,:) = [528.00 271.00 1.00];
x_m(12,:) = [284.00 212.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










