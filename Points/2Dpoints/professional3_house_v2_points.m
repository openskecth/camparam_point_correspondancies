designer_type = 'professionals';
designer_id ='Professional3';
object_id = 'house';
view = 'view2';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(length(x_m));

x_m(1,:) = [373.00 190.00 1.00];
x_m(2,:) = [551.00 280.00 1.00];
x_m(3,:) = [386.00 442.00 1.00];
x_m(4,:) = [384.00 536.00 1.00];
x_m(5,:) = [455.00 566.00 1.00];
x_m(6,:) = [393.00 615.00 1.00];
x_m(7,:) = [391.00 807.00 1.00];
x_m(8,:) = [457.00 836.00 1.00];
x_m(9,:) = [603.00 796.00 1.00];
x_m(10,:) = [678.00 734.00 1.00];
x_m(11,:) = [678.00 572.00 1.00];
x_m(12,:) = [598.00 538.00 1.00];
x_m(13,:) = [682.00 499.00 1.00];
x_m(14,:) = [680.00 398.00 1.00];
x_m(15,:) = [492.00 325.00 1.00];
x_m(16,:) = [491.00 418.00 1.00];
x_m(17,:) = [444.00 448.00 1.00];
x_m(18,:) = [490.00 481.00 1.00];
x_m(19,:) = [487.00 621.00 1.00];
x_m(20,:) = [457.00 665.00 1.00];
x_m(21,:) = [287.00 696.00 1.00];
x_m(22,:) = [228.00 666.00 1.00];
x_m(23,:) = [222.00 504.00 1.00];
x_m(24,:) = [277.00 465.00 1.00];
x_m(25,:) = [218.00 429.00 1.00];
x_m(26,:) = [220.00 351.00 1.00];
x_m(27,:) = [377.00 312.00 1.00];
x_m(28,:) = [395.00 852.00 1.00];
x_m(29,:) = [683.00 774.00 1.00];
x_m(30,:) = [487.00 660.00 1.00];
x_m(31,:) = [498.00 175.00 1.00];
x_m(32,:) = [684.00 275.00 1.00];
x_m(33,:) = [212.00 216.00 1.00];
x_m(34,:) = [237.00 705.00 1.00];



indices_all = [1:26 ];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);