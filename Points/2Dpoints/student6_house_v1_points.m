figure(3);

designer_type = 'students';
designer_id ='student6';
object_id = 'house';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);

x_m(1,:) = [421 139  1];
x_m(2,:) = [264 203 1];
x_m(3,:) = [182 298  1];
x_m(4,:) = [182 380  1];
x_m(5,:) = [221 421  1];
x_m(6,:) = [182 429  1];
% x_m(7,:) = [371 835  1];
% x_m(8,:) = [422 864  1];
% x_m(9,:) = [619 778  1];
% x_m(10,:) = [651 724  1];

x_m(11,:) = [367 503  1];
x_m(12,:) = [311 453  1];
x_m(13,:) = [367 447  1];
x_m(14,:) = [371 342  1];
x_m(15,:) = [522 264  1];
x_m(16,:) = [519 352  1];

x_m(17,:) = [458 378  1];
x_m(18,:) = [516 418  1];
% x_m(19,:) = [491 602  1];
% x_m(20,:) = [469 638  1];
% x_m(21,:) = [297 691  1];
% x_m(22,:) = [242 667  1];

% x_m(23,:) = [245 446  1];
x_m(25,:) = [331 308  1];
x_m(26,:) = [335 231  1];

x_m(27,:) = [177 188  1];
x_m(28,:) = [181 625  1];
x_m(29,:) = [366 716  1];
x_m(30,:) = [509 601  1];
x_m(31,:) = [527 163  1];
x_m(32,:) = [371 232  1];
x_m(33,:) = [338 135  1];

% x_m(34,:) = [444 589  1];

indices_all = [1:6 11:18 25 26 27:33];
% indices_opt = [27:33];
indices_opt  = indices_all;


x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')
