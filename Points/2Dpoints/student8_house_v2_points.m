figure(3);

designer_type = 'students';
designer_id ='student8';
object_id = 'house';
view = 'view2';


global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);

% x_m(:,1:2) = ginput(length(x_m));

x_m(1,:) = [429.00 273.00 1.00];
x_m(2,:) = [600.00 224.00 1.00];
x_m(3,:) = [504.00 305.00 1.00];
x_m(4,:) = [503.00 393.00 1.00];
x_m(5,:) = [553.00 422.00 1.00];
x_m(6,:) = [502.00 433.00 1.00];
x_m(7,:) = [505.00 636.00 1.00];
x_m(8,:) = [558.00 656.00 1.00];
x_m(9,:) = [642.00 653.00 1.00];
x_m(10,:) = [688.00 638.00 1.00];
x_m(11,:) = [685.00 463.00 1.00];
x_m(12,:) = [637.00 439.00 1.00];
x_m(13,:) = [686.00 426.00 1.00];
x_m(14,:) = [686.00 354.00 1.00];

x_m(15,:) = [nan nan 1.00];
x_m(16,:) =  [nan nan 1.00];
x_m(17,:) = [nan nan 1.00];
x_m(18,:) =  [nan nan 1.00];
x_m(19,:) =  [nan nan 1.00];
x_m(20,:) =  [nan nan 1.00];

x_m(21,:) = [378.00 656.00 1.00];
x_m(22,:) = [332.00 637.00 1.00];
x_m(23,:) = [329.00 453.00 1.00];
x_m(24,:) = [370.00 431.00 1.00];
x_m(25,:) = [329.00 412.00 1.00];
x_m(26,:) = [327.00 336.00 1.00];
x_m(27,:) = [510.00 185.00 1.00];
x_m(28,:) = [504.00 658.00 1.00];
x_m(29,:) = [688.00 653.00 1.00];
x_m(30,:) = [527.00 656.00 1.00];
x_m(31,:) = [527.00 296.00 1.00];
x_m(32,:) = [688.00 268.00 1.00];
x_m(33,:) = [316.00 248.00 1.00];
x_m(34,:) = [336.00 659.00 1.00];

indices_all = [1:14 21:26];
% indices_opt = [27:33];
indices_opt  = indices_all;


x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')