mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
object_id = 'mouse';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [514.55 638.34 1.00];
x_m(2,:) = [597.08 554.67 1.00];
x_m(3,:) = [448.08 334.61 1.00];
x_m(4,:) = [85.89 566.13 1.00];
x_m(5,:) = [415.98 317.41 1.00];
x_m(6,:) = [479.02 406.82 1.00];
x_m(7,:) = [483.61 335.75 1.00];
x_m(8,:) = [405.67 389.62 1.00];
x_m(9,:) = [561.55 589.06 1.00];
x_m(10,:) = [307.10 426.30 1.00];
x_m(11,:) = [437.00 264.00 1.00];
x_m(12,:) = [277.00 349.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










