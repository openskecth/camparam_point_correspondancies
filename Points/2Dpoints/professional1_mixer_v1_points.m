mixer_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional1';
object_id = 'mixer';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [148.00 420.00 1.00];
x_m(2,:) = [340.00 358.00 1.00];
x_m(3,:) = [375.00 420.00 1.00];
x_m(4,:) = [162.00 360.00 1.00];
x_m(5,:) = [206.00 550.00 1.00];
x_m(6,:) = [327.00 597.00 1.00];
x_m(7,:) = [321.00 550.00 1.00];
x_m(8,:) = [200.00 597.00 1.00];
x_m(9,:) = [172.00 606.00 1.00];
x_m(10,:) = [168.00 628.00 1.00];
x_m(11,:) = [530.00 473.00 1.00];
x_m(12,:) = [493.00 117.00 1.00];
x_m(13,:) = [253.00 180.00 1.00];
x_m(14,:) = [180.00 242.00 1.00];
x_m(15,:) = [179.00 282.00 1.00];
x_m(16,:) = [199.00 267.00 1.00];
x_m(17,:) = [169.00 258.00 1.00];
x_m(18,:) = [200.00 314.00 1.00];
x_m(19,:) = [311.00 341.00 1.00];
x_m(20,:) = [549.00 172.00 1.00];
x_m(21,:) = [536.00 550.00 1.00];
x_m(22,:) = [349.00 493.00 1.00];
x_m(23,:) = [386.00 635.00 1.00];
x_m(24,:) = [187.00 552.00 1.00];

indices_all = [1:24];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










