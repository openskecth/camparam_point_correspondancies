
figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
view = 'view1';

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m(1,:) = [700.66 634.39 1.00];
x_m(2,:) = [700.66 530.07 1.00];
x_m(3,:) = [696.98 514.11 1.00];
x_m(4,:) = [635.61 457.66 1.00];
x_m(5,:) = [602.48 428.20 1.00];
x_m(6,:) = [457.66 663.84 1.00];
x_m(7,:) = [764.48 525.16 1.00];
x_m(8,:) = [409.80 255.16 1.00];
x_m(9,:) = [138.57 304.25 1.00];
x_m(10,:) = [658.93 354.57 1.00];
x_m(11,:) = [359.48 426.98 1.00];
x_m(12,:) = [137.34 339.84 1.00];
x_m(13,:) = [154.00 514.00 1.00];
x_m(14,:) = [440.48 277.25 1.00];
x_m(15,:) = [448.00 426.02 1.00];
x_m(16,:) = [115.25 446.61 1.00];
x_m(17,:) = [413.48 350.89 1.00];
x_m(18,:) = [357.00 604.00 1.00];
x_m(19,:) = [691.00 479.00 1.00];

% x =  x_m(:,1);
% y =  x_m(:,2);
% plot(x,y, 'o-');
% a = [1:num_points]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c);


indices_all = [1:19];
% indices_all = [1:12 14 16 18:19];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);



