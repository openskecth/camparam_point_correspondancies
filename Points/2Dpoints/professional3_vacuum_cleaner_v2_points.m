
figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
view = 'view2';

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m(1,:) = [260.00 555.00 1.00];
x_m(2,:) = [259.00 462.00 1.00];
x_m(3,:) = [260.00 444.00 1.00];
x_m(4,:) = [305.00 404.00 1.00];
x_m(5,:) = [346.00 369.00 1.00];
x_m(6,:) = [195.00 467.00 1.00];
x_m(7,:) = [515.00 567.00 1.00];
x_m(8,:) = [791.00 303.00 1.00];
x_m(9,:) = [520.00 268.00 1.00];
x_m(10,:) = [583.00 348.00 1.00];
x_m(11,:) = [304.00 304.00 1.00];
x_m(12,:) = [495.00 282.00 1.00];
x_m(13,:) = [489.00 432.00 1.00];
x_m(14,:) = [782.00 322.00 1.00];
x_m(15,:) = [776.00 504.00 1.00];
x_m(16,:) = [505.00 389.00 1.00];
x_m(17,:) = [801.00 436.00 1.00];
x_m(18,:) = [280.00 444.00 1.00];
x_m(19,:) = [588.00 541.00 1.00];


% x =  x_m(:,1);
% y =  x_m(:,2);
% plot(x,y, 'o-');
% a = [1:num_points]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c);


indices_all = [1:16 18:19];
% indices_all = [1:12 14 16 18:19];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);



