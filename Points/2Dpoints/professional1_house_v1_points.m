designer_type = 'professionals';
designer_id ='Professional1';
object_id = 'house';
view = 'view1';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);



x_m(1,:) = [432 134  1];
x_m(2,:) = [230 189 1];
x_m(3,:) = [144 300  1];
x_m(4,:) = [146 367  1];
x_m(5,:) = [187 407  1];
x_m(6,:) = [144 417  1];
x_m(7,:) = [141 560  1];
x_m(8,:) = [nan nan  1];
x_m(9,:) = [nan nan  1];
x_m(10,:) = [315 641  1];
x_m(11,:) = [317 490  1];
x_m(12,:) = [270 444  1];
x_m(13,:) = [314 430  1];
x_m(14,:) = [322 359  1];
x_m(15,:) = [521 294  1];
x_m(16,:) = [522 362  1];
x_m(17,:) = [481 390  1];
x_m(18,:) = [521 420  1];
x_m(19,:) = [523 530  1];
x_m(20,:) = [nan nan  1];
x_m(21,:) = [nan nan  1];
x_m(22,:) = [356 473  1];
x_m(23,:) = [nan nan  1];
x_m(24,:) = [nan nan  1];
x_m(25,:) = [nan nan  1];
x_m(26,:) = [347 235  1];

x_m(27,:) = [nan nan  1];
x_m(28,:) = [144 592  1];
x_m(29,:) = [314 691  1];
x_m(30,:) = [516 577  1];
x_m(31,:) = [nan nan  1];
x_m(32,:) = [nan nan  1];
x_m(33,:) = [nan nan  1];

% x_m(34,:) = [444 589  1];

indices_all = [1:7 10:19 22 26 28:30];
% indices_opt = [1 2 3 10 11 14 15 22 26];
indices_opt  = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

