
designer_type = 'students';
designer_id ='student2';
object_id = 'bumps';
view = 'view2';

figure(3);
global folder_sketches;
folder_designers = fullfile(folder_sketches, 'students_clean\');
folder = fullfile(folder_designers, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, 'view2_concept_opaque.png'));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;




x_m = ones(size(H,1), 3);

% [x_m(:,1),x_m(:,2)] = ginput(length(x_m));
x_m(1,:) = [479.00 629.00 1.00];
x_m(2,:) = [479.00 638.00 1.00];
x_m(3,:) = [196.00 658.00 1.00];
x_m(4,:) = [195.00 649.00 1.00];
x_m(5,:) = [371.00 659.00 1.00];
x_m(6,:) = [371.00 669.00 1.00];
x_m(7,:) = [534.00 657.00 1.00];
x_m(8,:) = [534.00 648.00 1.00];
x_m(9,:) = [579.00 686.00 1.00];
x_m(10,:) = [627.00 751.00 1.00];
x_m(11,:) = [627.00 764.00 1.00];
x_m(12,:) = [285.00 769.00 1.00];
x_m(13,:) = [288.00 751.00 1.00];
x_m(14,:) = [303.00 678.00 1.00];
x_m(15,:) = [141.00 767.00 1.00];
x_m(16,:) = [143.00 760.00 1.00];
x_m(17,:) = [167.00 714.00 1.00];

indices_all = [1:4 7:17];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

