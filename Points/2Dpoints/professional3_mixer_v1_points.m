figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
object_id = 'mixer';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [203.00 413.00 1.00];
x_m(2,:) = [427.00 326.00 1.00];
x_m(3,:) = [458.00 411.00 1.00];
x_m(4,:) = [181.00 341.00 1.00];
x_m(5,:) = [266.00 611.00 1.00];
x_m(6,:) = [397.00 654.00 1.00];
x_m(7,:) = [390.00 604.00 1.00];
x_m(8,:) = [292.00 655.00 1.00];
x_m(9,:) = [208.00 700.00 1.00];
x_m(10,:) = [198.00 727.00 1.00];
x_m(11,:) = [646.00 475.00 1.00];
x_m(12,:) = [533.00 57.00 1.00];
x_m(13,:) = [339.00 98.00 1.00];
x_m(14,:) = [231.00 210.00 1.00];
x_m(15,:) = [233.00 249.00 1.00];
x_m(16,:) = [245.00 237.00 1.00];
x_m(17,:) = [219.00 227.00 1.00];
x_m(18,:) = [271.00 314.00 1.00];
x_m(19,:) = [427.00 347.00 1.00];
x_m(20,:) = [612.00 116.00 1.00];
x_m(21,:) = [675.00 570.00 1.00];
x_m(22,:) = [425.00 508.00 1.00];
x_m(24,:) = [210.00 608.00 1.00];
x_m(23,:) = [451.00 699.00 1.00];
x_m(25,:) = [507.00 205.00 1.00];
x_m(26,:) = [653.00 222.00 1.00];
indices_all = [1:13 18:26];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










