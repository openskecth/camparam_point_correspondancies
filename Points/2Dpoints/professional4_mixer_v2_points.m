figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
object_id = 'mixer';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );
x_m(1,:) = [549.00 681.50 1.00];
x_m(2,:) = [1047.50 631.50 1.00];
x_m(3,:) = [945.50 637.50 1.00];
x_m(4,:) = [661.50 637.50 1.00];
x_m(5,:) = [763.50 1079.50 1.00];
x_m(6,:) = [833.50 1097.50 1.00];
x_m(7,:) = [969.50 1079.50 1.00];
x_m(8,:) = [633.50 1073.50 1.00];
x_m(9,:) = [597.50 1085.50 1.00];
x_m(10,:) = [573.50 1125.50 1.00];
x_m(11,:) = [1372.00 1111.00 1.00];
x_m(12,:) = [1268.00 368.00 1.00];
x_m(13,:) = [789.50 359.50 1.00];
x_m(14,:) = [615.50 417.50 1.00];
x_m(15,:) = [603.50 505.50 1.00];
x_m(16,:) = [627.50 469.50 1.00];
x_m(17,:) = [581.50 463.50 1.00];
x_m(18,:) = [677.50 593.50 1.00];
x_m(19,:) = [839.50 601.50 1.00];
x_m(20,:) = [1391.50 457.50 1.00];
x_m(21,:) = [1221.50 1155.50 1.00];
x_m(22,:) = [983.50 1061.50 1.00];
x_m(24,:) = [753.50 1059.50 1.00];
x_m(23,:) = [909.50 1165.50 1.00];

indices_all = [1:2 7:17 20:24];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x_m(ing_p, 1),x_m(ing_p,2), 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










