designer_type = 'professionals';
designer_id ='Professional5';
object_id = 'house';
view = 'view2';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(length(x_m));

x_m(1,:) = [545.00 190.00 1.00];
x_m(2,:) = [328.00 190.00 1.00];
x_m(3,:) = [241.00 313.00 1.00];
x_m(4,:) = [242.00 402.00 1.00];
x_m(5,:) = [277.00 438.00 1.00];
x_m(6,:) = [238.00 455.00 1.00];
x_m(7,:) = [246.00 621.00 1.00];
x_m(8,:) = [279.00 664.00 1.00];
x_m(9,:) = [375.00 725.00 1.00];
x_m(10,:) = [436.00 731.00 1.00];
x_m(11,:) = [437.00 522.00 1.00];
x_m(12,:) = [375.00 467.00 1.00];
x_m(13,:) = [434.00 456.00 1.00];
x_m(14,:) = [435.00 346.00 1.00];
x_m(15,:) = [641.00 316.00 1.00];
x_m(16,:) = [644.00 407.00 1.00];
x_m(17,:) = [589.00 419.00 1.00];
x_m(18,:) = [639.00 455.00 1.00];
x_m(19,:) = [639.00 631.00 1.00];
x_m(20,:) = [590.00 632.00 1.00];
x_m(21,:) = [486.00 591.00 1.00];
x_m(22,:) = [453.00 556.00 1.00];
x_m(23,:) = [451.00 419.00 1.00];
x_m(24,:) = [487.00 400.00 1.00];
x_m(25,:) = [450.00 371.00 1.00];
x_m(26,:) = [443.00 293.00 1.00];
x_m(27,:) = [248.00 193.00 1.00];
x_m(28,:) = [243.00 643.00 1.00];
x_m(29,:) = [433.00 762.00 1.00];
x_m(30,:) = [639.00 655.00 1.00];
x_m(31,:) = [647.00 170.00 1.00];
x_m(32,:) = [435.00 164.00 1.00];
x_m(33,:) = [435.00 164.00 1.00];
x_m(34,:) = [452.00 575.00 1.00];

indices_all = [1:20 26 28:30 34];
indices_opt = indices_all;
% 1 2 15 28:30 34 --- 0.057 0.356


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);