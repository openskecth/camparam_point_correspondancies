mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
object_id = 'mouse';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [253.00 564.00 1.00];
x_m(2,:) = [349.00 611.00 1.00];
x_m(3,:) = [813.00 491.00 1.00];
x_m(4,:) = [423.00 388.00 1.00];
x_m(5,:) = [479.00 317.00 1.00];
x_m(6,:) = [390.00 424.00 1.00];
x_m(7,:) = [495.00 381.00 1.00];
x_m(8,:) = [396.00 351.00 1.00];
x_m(9,:) = [305.00 583.00 1.00];
x_m(10,:) = [588.00 431.00 1.00];
x_m(11,:) = [625.00 309.00 1.00];
x_m(12,:) = [438.00 280.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










