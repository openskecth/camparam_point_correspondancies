x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view2';
global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);

imshow(img)
hold on;

% [x,y] = ginput(20);

x_m(1,:) = [367.50 743.50 1.00];
x_m(2,:) = [367.50 771.50 1.00];
x_m(3,:) = [675.50 819.50 1.00];
x_m(4,:) = [675.50 799.50 1.00];
x_m(5,:) = [1083.50 795.50 1.00];
x_m(6,:) = [1077.50 811.50 1.00];
x_m(7,:) = [1287.50 761.50 1.00];
x_m(8,:) = [1287.50 725.50 1.00];
x_m(9,:) = [851.50 805.50 1.00];
x_m(10,:) = [819.50 1111.50 1.00];
x_m(11,:) = [809.50 1175.50 1.00];
x_m(12,:) = [163.50 985.50 1.00];
x_m(13,:) = [169.50 953.50 1.00];
x_m(14,:) = [253.50 857.50 1.00];
x_m(15,:) = [648.00 970.00 1.00];
x_m(16,:) = [648.00 950.00 1.00];
x_m(17,:) = [743.00 858.00 1.00];
x_m(18,:) = [1395.50 841.50 1.00];
x_m(19,:) = [1519.50 1021.50 1.00];
x_m(20,:) = [1519.50 1051.50 1.00];




x = x_m(:,1);
y = x_m(:,2);



indices_all = [1:2 7:20];
indices_opt = indices_all;

a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '*')
