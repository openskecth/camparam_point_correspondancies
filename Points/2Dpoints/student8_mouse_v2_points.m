mouse_3D;

figure(3);
designer_type = 'students';
designer_id ='student8';
object_id = 'mouse';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [722.00 535.00 1.00];
x_m(2,:) = [603.00 498.00 1.00];
x_m(3,:) = [160.00 644.00 1.00];
x_m(4,:) = [518.00 789.00 1.00];
x_m(5,:) = [492.00 406.00 1.00];
x_m(6,:) = [569.00 443.00 1.00];
x_m(7,:) = [511.00 416.00 1.00];
x_m(8,:) = [575.00 435.00 1.00];
x_m(9,:) = [653.00 516.00 1.00];
x_m(10,:) = [340.00 718.00 1.00];
x_m(11,:) = [334.00 378.00 1.00];
x_m(12,:) = [539.00 433.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










