figure(3);

designer_type = 'students';
designer_id ='student9';
object_id = 'house';
view = 'view1';


global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);

x_m(1,:) = [374 95  1];
x_m(2,:) = [532 167 1];
x_m(3,:) = [374 349  1];
x_m(4,:) = [370 474  1];
x_m(5,:) = [431 499  1];
x_m(6,:) = [370 563  1];
x_m(7,:) = [372 838  1];
x_m(8,:) = [426 864  1];
x_m(9,:) = [617 782  1];
x_m(10,:) = [651 729  1];
x_m(11,:) = [652 504  1];
x_m(12,:) = [601 458  1];
x_m(13,:) = [nan nan  1];
x_m(14,:) = [656.7 307.2  1];
x_m(15,:) = [508 217  1];
x_m(16,:) = [nan nan  1];
x_m(17,:) = [463 369  1];
x_m(18,:) = [nan nan  1];
x_m(19,:) = [nan nan  1];
x_m(20,:) = [470 638  1];
x_m(21,:) = [296 692  1];
x_m(22,:) = [244 662  1];
x_m(23,:) = [243 455  1];
x_m(24,:) = [313 399  1];
x_m(25,:) = [nan nan  1];
x_m(26,:) = [246 249  1];

% indices_all  = [1:3 5:10 12 14 15 17 19:24 26];
% indices = [1:3 14 15 26];




x_m(27,:) = [370 190  1];
x_m(28,:) = [371 888  1];
x_m(29,:) = [652 765  1];
x_m(30,:) = [493 630  1];
x_m(31,:) = [508 89  1];
x_m(32,:) = [657 150  1];
x_m(33,:) = [240 115  1];
x_m(34,:) = [244 709  1];

indices_all = [1:12 14:15 17 20:24 26 27:34];

% indices_opt = [27:34];
indices_opt = indices_all;
x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')