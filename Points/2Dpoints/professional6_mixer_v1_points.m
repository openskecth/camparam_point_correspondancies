figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
object_id = 'mixer';
view = 'view1';
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [240.00 556.00 1.00];
x_m(2,:) = [392.00 403.00 1.00];
x_m(3,:) = [502.00 519.00 1.00];
x_m(4,:) = [149.00 440.00 1.00];
x_m(5,:) = [217.00 797.00 1.00];
x_m(6,:) = [419.00 839.00 1.00];
x_m(7,:) = [341.00 762.00 1.00];
x_m(8,:) = [274.00 854.00 1.00];
x_m(9,:) = [261.00 864.00 1.00];
x_m(10,:) = [250.00 894.00 1.00];
x_m(11,:) = [578.00 459.00 1.00];
x_m(12,:) = [515.00 19.00 1.00];
x_m(13,:) = [330.00 163.00 1.00];
x_m(14,:) = [220.00 325.00 1.00];
x_m(15,:) = [225.00 374.00 1.00];
x_m(16,:) = [247.00 352.00 1.00];
x_m(17,:) = [204.00 341.00 1.00];
x_m(18,:) = [221.00 364.00 1.00];
x_m(19,:) = [413.00 396.00 1.00];
x_m(20,:) = [565.00  56.00 1.00];
x_m(21,:) = [638.00  634.00 1.00];
x_m(22,:) = [355.00  554.00 1.00];
x_m(24,:) = [175.00  766.00 1.00];
x_m(23,:) = [476.00  867.00 1.00];

indices_all = [1:24];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = 1:20;
plot(x(ing_p),y(ing_p), 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 16);










