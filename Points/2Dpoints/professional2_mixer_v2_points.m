figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
object_id = 'mixer';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [621.00 293.00 1.00];
x_m(2,:) = [305.00 340.00 1.00];
x_m(3,:) = [635.00 335.00 1.00];
x_m(4,:) = [356.00 304.00 1.00];
x_m(5,:) = [467.00 618.00 1.00];
x_m(6,:) = [539.00 614.00 1.00];
x_m(7,:) = [454.00 621.00 1.00];
x_m(8,:) = [577.00 615.00 1.00];
x_m(9,:) = [663.00 629.00 1.00];
x_m(10,:) = [708.00 663.00 1.00];
x_m(11,:) = [191.00 670.00 1.00];
x_m(12,:) = [186.00 203.00 1.00];
x_m(13,:) = [472.00 121.00 1.00];
x_m(14,:) = [607.00 130.00 1.00];
x_m(15,:) = [615.00 187.00 1.00];
x_m(16,:) = [623.00 168.00 1.00];
x_m(17,:) = [599.00 152.00 1.00];
x_m(18,:) = [418.00 265.00 1.00];
x_m(19,:) = [564.00 281.00 1.00];
x_m(20,:) = [146.00 296.00 1.00];
x_m(21,:) = [377.40 662.00 1.00];
x_m(22,:) = [158.00 666.00 1.00];
x_m(24,:) = [390.00 663.00 1.00];
x_m(23,:) = [624.30 661.00 1.00];
indices_all = [1:19 21:24];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










