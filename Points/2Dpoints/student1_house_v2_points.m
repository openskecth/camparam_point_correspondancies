
x_m = ones(size(H,1), 3);

designer_type = 'students';
designer_id ='student1';
object_id = 'house';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);



% x_m(:,1:2) = ginput(length(x_m));

x_m(1,:) = [494.94 109.52 1.00];
x_m(2,:) = [312.07 157.34 1.00];
x_m(3,:) = [191.10 279.72 1.00];
x_m(4,:) = [191.10 414.76 1.00];
x_m(5,:) = [251.59 473.84 1.00];
x_m(6,:) = [192.51 486.50 1.00];
x_m(7,:) = [189.70 729.85 1.00];
x_m(8,:) = [251.59 795.96 1.00];
x_m(9,:) = [343.02 843.79 1.00];
x_m(10,:) = [423.20 835.35 1.00];
x_m(11,:) = [424.61 582.15 1.00];
x_m(12,:) = [341.61 503.38 1.00];
x_m(13,:) = [426.01 482.28 1.00];
x_m(14,:) = [427.42 328.95 1.00];
x_m(15,:) = [629.98 245.96 1.00];
x_m(16,:) = [631.39 364.12 1.00];
x_m(17,:) = [548.39 390.85 1.00];
x_m(18,:) = [632.79 434.45 1.00];
x_m(19,:) = [635.61 666.55 1.00];
x_m(20,:) = [539.95 679.21 1.00];
x_m(21,:) = [428.83 645.45 1.00];
x_m(22,:) = [376.78 589.19 1.00];
x_m(23,:) = [418.98 411.95 1.00];
x_m(24,:) = [459.77 385.22 1.00];
x_m(25,:) = [395.07 361.31 1.00];
x_m(26,:) = [393.66 209.39 1.00];
x_m(27,:) = [192.51 140.46 1.00];
x_m(28,:) = [192.51 797.37 1.00];
x_m(29,:) = [421.79 912.72 1.00];
x_m(30,:) = [635.61 718.60 1.00];
x_m(31,:) = [634.20 133.43 1.00];
x_m(32,:) = [424.61 186.88 1.00];
x_m(33,:) = [395.07 92.64 1.00];
x_m(34,:) = [386.63 680.62 1.00];



indices_all = [1:19 26];
indices_opt = indices_all;










% C_est_ = [-6.065 2.589 -5.662];
%  theta_x_ = 2.75;
%  theta_y_ = 0.74; 
%  theta_z_ = -0.28;
 
 
% theta_x_0 = -3.04; 
% theta_y_0 = -0.65; 
% theta_z_0 = -0.02;