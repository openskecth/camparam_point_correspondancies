figure(3);
designer_type = 'students';
designer_id ='student3';
object_id = 'mixer';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [895.00 269.00 1.00];
x_m(2,:) = [504.00 375.00 1.00];
x_m(3,:) = [824.00 399.00 1.00];
x_m(4,:) = [547.00 250.00 1.00];
x_m(5,:) = [609.00 659.00 1.00];
x_m(6,:) = [804.00 653.00 1.00];
x_m(8,:) = [795.00 654.00 1.00];
x_m(9,:) = [908.00 648.00 1.00];
x_m(7,:) = [610.00 660.00 1.00];
x_m(10,:) = [925.00 701.00 1.00];
x_m(11,:) = [232.00 712.00 1.00];
x_m(12,:) = [273.00 255.00 1.00];
x_m(13,:) = [690.00 51.00 1.00];
x_m(14,:) = [786.00 81.00 1.00];
x_m(15,:) = [786.00 124.00 1.00];
x_m(16,:) = [801.00 115.00 1.00];
x_m(17,:) = [770.00 86.00 1.00];
x_m(18,:) = [598.00 249.00 1.00];
x_m(19,:) = [786.00 336.00 1.00];
x_m(20,:) = [207.00 329.00 1.00];
x_m(21,:) = [426.00 707.00 1.00];
x_m(22,:) = [145.00 711.00 1.00];
x_m(23,:) = [513.00 705.00 1.00];
x_m(24,:) = [817.00 699.00 1.00];
x_m(25,:) = [168.00 386.00 1.00];
x_m(26,:) = [289.00 426.00 1.00];
indices_all = [1:4 9:10 12:21 22 ];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










