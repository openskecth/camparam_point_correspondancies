designer_type = 'professionals';
designer_id ='Professional6';
object_id = 'house';
view = 'view1';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(length(x_m));

x_m(1,:) = [313 71  1];
x_m(2,:) = [521 187 1];
x_m(3,:) = [346 404 1];
x_m(4,:) = [349 525  1];
x_m(5,:) = [434 549  1];
x_m(6,:) = [346 622  1];
x_m(7,:) = [345 881  1];
x_m(8,:) = [436 891  1];
x_m(9,:) = [593 827  1];
x_m(10,:) = [668 758  1];
x_m(11,:) = [672 519  1];
x_m(12,:) = [594 503  1];
x_m(13,:) = [672 430  1];
x_m(14,:) = [675 306  1];
x_m(15,:) = [440 208  1];
x_m(16,:) = [434 309  1];
x_m(17,:) = [370 379  1];
x_m(18,:) = [433 396  1];
x_m(19,:) = [432 610  1];
x_m(20,:) = [373 669  1];
x_m(21,:) = [224 724  1];
x_m(22,:) = [155 705  1];
x_m(23,:) = [155 485  1];
x_m(24,:) = [237 415  1];
x_m(25,:) = [152 390  1];
x_m(26,:) = [154 277  1];


x_m(27,:) = [344 232  1];
x_m(28,:) = [343 922  1];
x_m(29,:) = [665 795  1];
x_m(30,:) = [431 647  1];
x_m(31,:) = [438  55  1];
x_m(32,:) = [676 147  1];
x_m(33,:) = [153 101  1];
x_m(34,:) = [151 745  1];

indices_all = [1:34];
% indices_opt = [27:34];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);