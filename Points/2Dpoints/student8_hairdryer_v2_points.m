x_m = zeros(size(H,1), 3);

% figure(3);
designer_type = 'students';
designer_id ='student8';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');

%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


% imshow(img)
% hold on;

num_points = length(H);

x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(num_points );

x_m(1,:) = [390.16 307.93 1.00];
x_m(2,:) = [392.61 332.48 1.00];
x_m(3,:) = [193.80 382.80 1.00];
x_m(4,:) = [193.80 361.93 1.00];
x_m(5,:) = [433.11 269.89 1.00];
x_m(6,:) = [419.61 498.16 1.00];
x_m(7,:) = [703.11 574.25 1.00];
x_m(8,:) = [716.61 397.52 1.00];
x_m(9,:) = [788.48 466.20 1.00];
x_m(10,:) = [630.70 479.75 1.00];
x_m(11,:) = [692.07 434.34 1.00];
x_m(12,:) = [649.11 418.39 1.00];
x_m(13,:) = [516.57 456.43 1.00];
x_m(14,:) = [487.11 449.07 1.00];
x_m(15,:) = [607.39 835.66 1.00];
x_m(16,:) = [574.25 845.48 1.00];
x_m(17,:) = [506.75 835.66 1.00];
x_m(18,:) = [532.52 828.30 1.00];


indices_all = [1:18];
indices_opt = indices_all;


% x =  x_m(indices_all,1);
% y =  x_m(indices_all,2);
% plot(x,y, 'o-');
% a = [indices_all]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);
% 








