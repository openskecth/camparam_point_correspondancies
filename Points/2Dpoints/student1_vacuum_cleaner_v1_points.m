
figure(3);
designer_type = 'students';
designer_id ='student1';
view = 'view1';

x_m = ones(size(H,1), 3);

num_points = length(H);
x_m = ...
    [211   650     1
   214   530     1
   234   493     1
   304   429     1
   385   374     1
   186   528     1
   445   745     1
   881   302     1
   648   210     1
   610   392     1
   387   271     1
   604   243     1
   605   443     1
   866   370     1
   871   592     1
   653   374     1
   935   540     1
   364   466     1
   627   667     1];


indices_all = [1:19];
indices_opt = indices_all;



x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);






