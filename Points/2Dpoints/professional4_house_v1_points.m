
designer_type = 'professionals';
designer_id ='Professional4';
object_id = 'house';
view = 'view1';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;



x_m = ones(size(H,1), 3);

x_m(1,:) = [609 385  1];
x_m(2,:) = [327 442 1];
x_m(3,:) = [225 471  1];
x_m(4,:) = [223 540  1];
x_m(5,:) = [261 606  1];
x_m(6,:) = [222 604  1];
x_m(7,:) = [229 771  1];
x_m(8,:) = [265 871  1];
x_m(9,:) = [368 1033  1];
x_m(10,:) = [436 1083  1];
x_m(11,:) = [430 796  1];
x_m(12,:) = [361 699  1];
x_m(13,:) = [432 705  1];
x_m(14,:) = [433 622  1];
x_m(15,:) = [739 528  1];
x_m(16,:) = [739 585  1];
x_m(17,:) = [nan nan  1];
x_m(18,:) = [741 651  1];
x_m(19,:) = [744 892  1];
x_m(20,:) = [nan nan  1];
x_m(21,:) = [nan nan  1];
x_m(22,:) = [nan nan  1];
x_m(23,:) = [nan nan  1];
x_m(24,:) = [nan nan  1];
x_m(25,:) = [nan nan  1];
x_m(26,:) = [nan nan  1];
indices_all = [1:16 18:19 27 28 29 31 32 33];



x_m(27,:) = [223 373  1];
x_m(28,:) = [228 807  1];
x_m(29,:) = [438 1131  1];
x_m(30,:) = [741 927  1];
x_m(31,:) = [741 424  1];
x_m(32,:) = [429 496  1];
x_m(33,:) = [477 348  1];
% x_m(34,:) = [244 709  1];

% indices_opt = [27:33];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

