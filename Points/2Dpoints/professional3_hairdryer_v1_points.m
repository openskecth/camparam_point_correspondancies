x_m = ones(size(H,1), 3);

% figure(3);s
designer_type = 'professionals';
designer_id ='Professional3';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');

%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));

height  = size(img,2);
width   = size(img,1);


% imshow(img)
% hold on;

num_points = length(H);

% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m = ...
    [367.0000  504.0000    1.0000
  368.0000  535.0000    1.0000
  188.0000  424.0000    1.0000
  189.0000  401.0000    1.0000
  363.0000  314.0000    1.0000
  365.0000  541.0000    1.0000
  643.0000  386.0000    1.0000
  642.0000  192.0000    1.0000
  737.0000  321.0000    1.0000
  561.0000  246.0000    1.0000
  644.0000  368.0000    1.0000
  612.0000  389.0000    1.0000
  454.0000  295.0000    1.0000
  418.0000  312.0000    1.0000
  547.0000  771.0000    1.0000
  520.0000  787.0000    1.0000
  479.0000  768.0000    1.0000
  503.0000  752.0000    1.0000
  ];

indices_all = [1:18];
indices_opt = indices_all;

% x =  x_m(indices_all,1);
% y =  x_m(indices_all,2);
% plot(x,y, 'o-');
% a = [indices_all]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);











