hairdryer_3D;
object_id= 'hairdryer';

figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
view = 'view2';
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [614.91 219.97 1.00];
x_m(2,:) = [613.88 248.62 1.00];
x_m(3,:) = [714.15 343.77 1.00];
x_m(4,:) = [713.13 313.07 1.00];
x_m(5,:) = [537.15 201.55 1.00];
x_m(6,:) = [529.98 416.41 1.00];
x_m(7,:) = [134.02 513.61 1.00];
x_m(8,:) = [124.81 291.59 1.00];
x_m(9,:) = [87.98 354.00 1.00];
x_m(10,:) = [171.88 454.27 1.00];
x_m(11,:) = [250.66 317.17 1.00];
x_m(12,:) = [291.59 305.91 1.00];
x_m(13,:) = [361.00 409.00 1.00];
x_m(14,:) = [405.16 396.97 1.00];
x_m(15,:) = [301.82 786.80 1.00];
x_m(16,:) = [326.38 776.57 1.00];
x_m(17,:) = [365.26 817.49 1.00];
x_m(18,:) = [330.47 826.70 1.00];

indices_all = [1:18];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);










