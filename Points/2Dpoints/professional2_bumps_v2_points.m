x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
view = 'view2';

global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));

[img, ~, t] =  imread(fullfile(folder, [view '_concept.png']));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
img = img(:,width:-1:1,:); 
t = t(:, width:-1:1);
imwrite(img, fullfile(folder, 'view2_concept_mirrored.png'), 'Alpha', t);

[img, ~, ~] =  imread(fullfile(folder, [view '_concept_opaque.png']));
img = img(:,width:-1:1,:); 

imshow(img)
hold on;

%   [x,y] = ginput(20);

x_m(1,:) = [220.00 440.00 1.00];
x_m(2,:) = [220.00 475.00 1.00];
x_m(3,:) = [117.00 294.00 1.00];
x_m(4,:) = [118.00 277.00 1.00];
x_m(5,:) = [411.00 248.00 1.00];
x_m(6,:) = [412.00 268.00 1.00];
x_m(7,:) = [673.00 374.00 1.00];
x_m(8,:) = [672.00 346.00 1.00];
x_m(9,:) = [491.00 480.00 1.00];
x_m(10,:) = [514.00 685.00 1.00];
x_m(11,:) = [524.00 705.00 1.00];
x_m(12,:) = [63.00 580.00 1.00];
x_m(13,:) = [58.00 558.00 1.00];
x_m(14,:) = [112.00 399.00 1.00];
x_m(15,:) = [262.00 378.00 1.00];
x_m(16,:) = [262.00 365.00 1.00];
x_m(17,:) = [272.00 264.00 1.00];
x_m(18,:) = [570.00 302.00 1.00];
x_m(19,:) = [621.00 406.00 1.00];
x_m(20,:) = [621.00 431.00 1.00];

x = x_m(:,1);
y = x_m(:,2);

indices_all = [1:20];
indices_opt = indices_all;


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '*')



