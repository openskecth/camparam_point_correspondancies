designer_type = 'professionals';
designer_id ='Professional5';
object_id = 'house';
view = 'view1';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);

x_m(1,:) = [473 243  1];
x_m(2,:) = [315 265 1];
x_m(3,:) = [204 373 1];
x_m(4,:) = [207 458  1];
x_m(5,:) = [253 494  1];
x_m(6,:) = [207 510  1];
x_m(7,:) = [207 684  1];
x_m(8,:) = [252 724  1];
x_m(9,:) = [368 764  1];
x_m(10,:) = [430 750  1];
x_m(11,:) = [432 555  1];
x_m(12,:) = [368 516  1];
x_m(13,:) = [434 496  1];
x_m(14,:) = [435 396  1];
x_m(15,:) = [581 349  1];
x_m(16,:) = [580 434  1];
x_m(17,:) = [524 456  1];
x_m(18,:) = [579 485  1];
x_m(19,:) = [576 649  1];
x_m(20,:) = [515 667  1];
x_m(21,:) = [nan nan  1];
x_m(22,:) = [nan nan  1];
x_m(23,:) = [nan nan  1];
x_m(24,:) = [nan nan  1];
x_m(25,:) = [nan nan  1];
x_m(26,:) = [365 341  1];


x_m(27,:) = [nan nan  1];
x_m(28,:) = [207 709  1];
x_m(29,:) = [431 783  1];
x_m(30,:) = [574 677  1];
x_m(31,:) = [nan nan  1];
x_m(32,:) = [nan nan  1];
x_m(33,:) = [nan nan  1];
x_m(34,:) = [360 633  1];

indices_all = [1:20 26 28:30 34];
indices_opt = [1 2 3 28 8:9 29 30 34];
indices_opt = indices_all;
% 1 2 15 28:30 34 --- 0.057 0.356


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);