mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
object_id = 'mouse';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [574.00 635.00 1.00];
x_m(2,:) = [644.00 572.00 1.00];
x_m(3,:) = [495.00 360.00 1.00];
x_m(4,:) = [221.00 541.00 1.00];
x_m(5,:) = [433.00 341.00 1.00];
x_m(6,:) = [509.00 434.00 1.00];
x_m(7,:) = [503.00 354.00 1.00];
x_m(8,:) = [429.00 406.00 1.00];
x_m(9,:) = [614.00 602.00 1.00];
x_m(10,:) = [363.00 446.00 1.00];
x_m(11,:) = [473.06 256.08 1.00];
x_m(12,:) = [306.90 364.57 1.00]
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










