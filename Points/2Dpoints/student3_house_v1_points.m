x_m = zeros(size(H,1), 3);
x_m(1,:) = [429 62  1];
x_m(2,:) = [625 118 1];
x_m(3,:) = [460 290 1];
x_m(4,:) = [461 396  1];
x_m(5,:) = [552 426  1];
x_m(6,:) = [465 522  1];
x_m(7,:) = [465 724  1];
x_m(8,:) = [545 749  1];
x_m(9,:) = [683 664  1];
x_m(10,:) = [746 570  1];
x_m(11,:) = [745 414  1];
x_m(12,:) = [687 379  1];
x_m(13,:) = [748 313  1];
x_m(14,:) = [754 219  1];
x_m(15,:) = [561 160  1];
x_m(16,:) = [nan nan  1];
x_m(17,:) = [nan nan  1];
x_m(18,:) = [nan nan  1];
x_m(19,:) = [nan nan  1];
x_m(20,:) = [nan nan  1];
x_m(21,:) = [356 567  1];
x_m(22,:) = [275 538  1];
x_m(23,:) = [274 371  1];
x_m(24,:) = [364 302  1];
x_m(25,:) = [272 260  1];
x_m(26,:) = [274 193  1];

x_m(27,:) = [463 147  1];
x_m(28,:) = [457 798  1];
x_m(29,:) = [745 629  1];
x_m(30,:) = [545 495  1];
x_m(31,:) = [564 50  1];
x_m(32,:) = [760 94  1];
x_m(33,:) = [270 80  1];
x_m(34,:) = [271 598  1];

indices_all = [1:15 21:26];
% indices_opt = [1:2 7 14 27 30:34];

% indices_opt =[1:14 21:26];

indices_opt = [1:2 7 8 9 10 27 30 21 29 31:34];
indices_opt  = indices_all;



designer_type = 'students';
designer_id ='student3';
object_id = 'house';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);