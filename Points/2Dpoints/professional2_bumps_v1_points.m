designer_type = 'professionals';
designer_id ='Professional2';
object_id = 'bumps';
view = 'view1';

figure(3);
global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);

folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

%   [x,y] = ginput(20);

x = [ 660.0000  659.0000  185.0000  184.0000  239.0000  239.0000  610.0000  610.0000  686.0000  741.0000  743.0000  403.0000  402.0000  413.0000   81.0000   83.0000  161.0000  432.0000  428.0000  425.0000];
y = [ 499.0000  525.0000  517.0000  490.0000  361.0000  389.0000  388.0000  370.0000  482.0000  567.0000  588.0000  729.0000 703.0000  572.0000  576.0000  555.0000  464.0000  414.0000  463.0000  475.0000];

a = [1:20]'; b = num2str(a); c = cellstr(b);
t = text(x, y, c);

num_points = length(x);

x_m(1:num_points, 1) = x;
x_m(1:num_points, 2) = y;
x_m(1:num_points, 3) = 1.0;

indices_all = [1:2 3:4 5 7:8 9:10 12:14 16 17 18 19];
indices_opt = indices_all;





x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);
