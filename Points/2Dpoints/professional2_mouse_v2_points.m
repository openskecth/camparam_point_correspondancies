mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
object_id = 'mouse';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [560.00 352.00 1.00];
x_m(2,:) = [464.00 329.00 1.00];
x_m(3,:) = [84.00 397.00 1.00];
x_m(4,:) = [476.00 558.00 1.00];
x_m(5,:) = [384.00 230.00 1.00];
x_m(6,:) = [412.00 242.00 1.00];
x_m(7,:) = [380.00 231.00 1.00];
x_m(8,:) = [423.00 239.00 1.00];
x_m(9,:) = [513.00 339.00 1.00];
x_m(10,:) = [262.00 467.00 1.00];
x_m(11,:) = [257.57 211.13 1.00];
x_m(12,:) = [433.57 251.06 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










