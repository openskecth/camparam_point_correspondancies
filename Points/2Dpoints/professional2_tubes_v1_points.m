figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
view = 'view1';


num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m = ...
    [553.5246  636.0485    1.0000
  558.1093  434.3234    1.0000
  622.2945  504.2395    1.0000
  493.9240  583.3249    1.0000
  146.6360  414.8386    1.0000
  278.4450  351.7996    1.0000
  213.1136  471.0007    1.0000
  209.6751  286.4682    1.0000
  457.2467  278.4450    1.0000
  569.5709  226.8676    1.0000
  513.4088  342.6302    1.0000
  511.1165  156.9515    1.0000
  350.6534  272.7142    1.0000
  218.8444   77.8661    1.0000
  347.2149  339.1918    1.0000
  239.4754  299.0760    1.0000];





indices_all = [1:15];
indices_opt = indices_all;


x =  x_m(:,1);
y =  x_m(:,2);
plot(x(indices_all),y(indices_all), 'o-');
a = indices_all'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c);






