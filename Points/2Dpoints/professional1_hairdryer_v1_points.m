x_m = zeros(size(H,1), 3);

% figure(3);
designer_type = 'professionals';
designer_id ='Professional1';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');

%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);

% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m = ...
    [ 218.3925  332.1373    1.0000
      219.8143  356.3081    1.0000
      101  300    1.0000
      100.3822  282.3740    1.0000
      253.2269  213.4162    1.0000
      248.9614  363.4172    1.0000
      516.9727  284.5067    1.0000
      523.3709  141.6147    1.0000
      572.4234  231.8997    1.0000
      460.1003  203.4635    1.0000
      486.4038  265.3122    1.0000
      450.8585  275.9758    1.0000
      344.9336  225.5015    1.0000
      306.5448  236.1651    1.0000
      430.9532  527.6363    1.0000
      399.6734  551.0962    1.0000
      359.1517  531.1908    1.0000
      391.8534  509.1528    1.0000];

indices_all = [1:18];
indices_opt = indices_all;


% x =  x_m(indices_all,1);
% y =  x_m(indices_all,2);
% plot(x,y, 'o');
% a = [indices_all]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c);











