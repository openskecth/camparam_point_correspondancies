mouse_3D;

figure(3);
designer_type = 'students';
designer_id ='student6';
object_id = 'mouse';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [171.00 487.00 1.00];
x_m(2,:) = [278.00 458.00 1.00];
x_m(3,:) = [652.00 516.00 1.00];
x_m(4,:) = [320.00 581.00 1.00];
x_m(5,:) = [360.00 316.00 1.00];
x_m(6,:) = [293.00 363.00 1.00];
x_m(7,:) = [373.00 324.00 1.00];
x_m(8,:) = [277.00 359.00 1.00];
x_m(9,:) = [227.00 472.00 1.00];
x_m(10,:) = [457.00 558.00 1.00];
x_m(11,:) = [489.00 287.00 1.00];
x_m(12,:) = [282.00 358.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










