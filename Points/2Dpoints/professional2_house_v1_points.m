
designer_type = 'professionals';
designer_id ='Professional2';
object_id = 'house';
view = 'view1';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;



x_m = ones(size(H,1), 3);

x_m(1,:) = [218 174  1];
x_m(2,:) = [381 285 1];
x_m(3,:) = [271 449  1];

x_m(4,:) = [275 502  1];
x_m(5,:) = [309 511  1];
x_m(6,:) = [279 562  1];
x_m(7,:) = [nan nan  1];
x_m(8,:) = [nan nan  1];
x_m(9,:) = [nan nan  1];
x_m(10,:) = [nan nan  1];
x_m(11,:) = [442 435  1];
x_m(12,:) = [426 420  1];
x_m(13,:) = [451 386  1];
x_m(14,:) = [460 335  1];
x_m(15,:) = [314 226  1];
x_m(16,:) = [nan nan  1];
x_m(17,:) = [nan nan  1];
x_m(18,:) = [nan nan  1];
x_m(19,:) = [nan nan  1];
x_m(20,:) = [nan nan  1];
x_m(21,:) = [nan nan  1];
x_m(22,:) = [nan nan  1];
x_m(23,:) = [150 427  1];
x_m(24,:) = [nan nan  1];
x_m(25,:) = [133 357  1];
x_m(26,:) = [120 302  1];





x_m(27,:) = [270 355  1];
x_m(28,:) = [282 658  1];
x_m(29,:) = [425 545  1];
x_m(30,:) = [nan nan  1];
x_m(31,:) = [316 132  1];
x_m(32,:) = [480 220  1];
x_m(33,:) = [101 223  1];
x_m(34,:) = [176 520  1];

% indices_opt = [1:2 27:29 31:34];
indices_all = [1:6 11:15 23 25:26 27:29 31:34];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

