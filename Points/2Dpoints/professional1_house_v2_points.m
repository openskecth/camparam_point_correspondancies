designer_type = 'professionals';
designer_id ='Professional1';
object_id = 'house';
view = 'view2';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);



x_m(1,:) = [243.00 172.00 1.00];
x_m(2,:) = [437.00 114.00 1.00];
x_m(3,:) = [365.00 206.00 1.00];
x_m(4,:) = [363.00 304.00 1.00];
x_m(5,:) = [398.00 330.00 1.00];
x_m(6,:) = [364.00 347.00 1.00];
x_m(7,:) = [365.00 538.00 1.00];
x_m(8,:) = [406.00 548.00 1.00];
x_m(9,:) = [472.00 537.00 1.00];
x_m(10,:) = [499.00 516.00 1.00];
x_m(11,:) = [504.00 360.00 1.00];
x_m(12,:) = [471.00 334.00 1.00];
x_m(13,:) = [501.00 314.00 1.00];
x_m(14,:) = [503.00 232.00 1.00];
x_m(15,:) = [611.00 122.00 1.00];
x_m(16,:) = [611.00 122.00 1.00];
x_m(17,:) = [611.00 122.00 1.00];
x_m(18,:) = [611.00 122.00 1.00];
x_m(19,:) = [611.00 122.00 1.00];
x_m(20,:) = [611.00 122.00 1.00];
x_m(21,:) = [611.00 122.00 1.00];
x_m(22,:) = [194.00 509.00 1.00];
x_m(23,:) = [186.00 362.00 1.00];
x_m(24,:) = [221.00 347.00 1.00];
x_m(25,:) = [189.00 322.00 1.00];
x_m(26,:) = [185.00 245.00 1.00];
x_m(27,:) = [364.00 108.00 1.00];
x_m(28,:) = [364.00 557.00 1.00];
x_m(29,:) = [498.00 532.00 1.00];
x_m(30,:) = [635.00 486.00 1.00];
x_m(31,:) = [635.00 486.00 1.00];
x_m(32,:) = [501.00 230.00 1.00];
x_m(33,:) = [654.00 300.00 1.00];
x_m(34,:) = [194.00 531.00 1.00];

% x_m(34,:) = [444 589  1];

indices_all = [1:6 11:14 23:26 28 29 34];
% indices_opt = [1 2 3 10 11 14 15 22 26];
indices_opt  = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

