designer_type = 'students';
designer_id ='student2';
object_id = 'house';
view = 'view1';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);
x_m(1,:) = [595 257  1];
x_m(2,:) = [414 324 1];
x_m(3,:) = [320 412  1];
x_m(4,:) = [319 499  1];
x_m(5,:) = [362 535  1];
x_m(6,:) = [320 540  1];
x_m(7,:) = [326 730  1];
x_m(8,:) = [358 767  1];
x_m(9,:) = [475 828  1];
x_m(10,:) = [nan nan  1];
x_m(11,:) = [518 610  1];
x_m(12,:) = [470 572  1];
x_m(13,:) = [516 566  1];
x_m(14,:) = [514 470  1];
x_m(15,:) = [704 375  1];
x_m(16,:) = [702 471  1];
x_m(17,:) = [658 484  1];
x_m(18,:) = [700 509  1];
x_m(19,:) = [699 688  1];
x_m(20,:) = [657 701  1];
x_m(21,:) = [nan nan  1];
x_m(22,:) = [nan nan  1];
x_m(23,:) = [nan nan  1];
x_m(24,:) = [nan nan  1];
x_m(25,:) = [nan nan  1];
x_m(26,:) = [nan nan  1];

x_m(27,:) = [321 300  1];
x_m(28,:) = [323 749  1];
x_m(29,:) = [516 845  1];
x_m(30,:) = [695 712  1];
x_m(31,:) = [709 273  1];
x_m(32,:) = [519 349  1];
x_m(33,:) = [499 241  1];

% x_m(34,:) = [444 589  1];

indices_all = [1:9 11:20 27:33];
% indices_opt = [1:2 27:33];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);



