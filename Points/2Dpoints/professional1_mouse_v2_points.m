mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional1';
object_id = 'mouse';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [232.00 319.00 1.00];
x_m(2,:) = [152.00 340.00 1.00];
x_m(3,:) = [226.00 601.00 1.00];
x_m(4,:) = [644.00 427.00 1.00];
x_m(5,:) = [355.00 239.00 1.00];
x_m(6,:) = [283.00 253.00 1.00];
x_m(7,:) = [278.00 253.00 1.00];
x_m(8,:) = [383.00 227.00 1.00];
x_m(9,:) = [191.00 329.00 1.00];
x_m(10,:) = [465.00 500.00 1.00];
x_m(11,:) = [205.97 296.71 1.00];
x_m(12,:) = [547.11 207.71 1.00];
indices_all = [1:5 9:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










