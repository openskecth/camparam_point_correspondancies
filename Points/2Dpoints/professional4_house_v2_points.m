
designer_type = 'professionals';
designer_id ='Professional4';
object_id = 'house';
view = 'view2';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;



x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(length(x_m));

x_m(1,:) = [775.25 242.75 1.00];
x_m(2,:) = [440.75 223.25 1.00];
x_m(3,:) = [304.25 329.75 1.00];
x_m(4,:) = [304.25 406.25 1.00];
x_m(5,:) = [347.75 443.75 1.00];
x_m(6,:) = [302.75 469.25 1.00];
x_m(7,:) = [311.75 650.75 1.00];
x_m(8,:) = [352.25 692.75 1.00];
x_m(9,:) = [512.75 761.75 1.00];
x_m(10,:) = [574.25 742.25 1.00];
x_m(11,:) = [572.75 523.25 1.00];
x_m(12,:) = [514.25 460.25 1.00];
x_m(13,:) = [572.75 433.25 1.00];
x_m(14,:) = [572.75 317.75 1.00];
x_m(15,:) = [877.25 313.25 1.00];
x_m(16,:) = [874.25 415.25 1.00];
x_m(17,:) = [826.25 449.75 1.00];
x_m(18,:) = [871.25 484.25 1.00];
x_m(19,:) = [869.75 659.75 1.00];
x_m(20,:) = [812.75 680.75 1.00];
x_m(21,:) = [622.25 631.25 1.00];
x_m(22,:) = [599.75 596.75 1.00];
x_m(23,:) = [595.25 476.75 1.00];
x_m(24,:) = [625.25 451.25 1.00];
x_m(25,:) = [610.25 416.75 1.00];
x_m(26,:) = [616.25 334.25 1.00];
x_m(27,:) = [302.75 248.75 1.00];
x_m(28,:) = [311.75 677.75 1.00];
x_m(29,:) = [574.25 787.25 1.00];
x_m(30,:) = [866.75 692.75 1.00];
x_m(31,:) = [874.25 217.25 1.00];
x_m(32,:) = [574.25 199.25 1.00];
x_m(33,:) = [617.75 244.25 1.00];
x_m(34,:) = [598.25 647.75 1.00];


indices_all = [1:20 27:32];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

