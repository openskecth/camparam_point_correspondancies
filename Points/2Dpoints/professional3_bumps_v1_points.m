x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
view = 'view1';

global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

%    [x,y] = ginput(20);

x = [ 606.1591  606.1591  219.5682  219.5682  288.2955  288.2955  661.3864  665.0682  719.0682  828.2955  822.1591  393.8409  390.1591  403.6591   98.0682   98.0682  193.7955  461.3409  463.7955  465.0227];
y = [ 522.7045  548.4773  512.8864  479.7500  328.7955  348.4318  391.3864  359.4773  553.3864  677.3409  700.6591  847.9318  822.1591  622.1136  570.5682  539.8864  453.9773  414.7045  461.3409  472.3864];

indices_all = [1:20];
indices_opt = indices_all;

a = [indices_all ]'; b = num2str(a); c = cellstr(b);
t = text(x, y, c);

x_m(indices_all ,1) = x;
x_m(indices_all ,2) = y;
x_m(indices_all ,3) = 1.0;

% indices_all = [1:4 5:6 8 9:10 12:14 16 17 18 19];







