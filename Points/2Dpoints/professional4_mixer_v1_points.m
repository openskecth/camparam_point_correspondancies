figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
object_id = 'mixer';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );
x_m(1,:) = [605.19 875.30 1.00];
x_m(2,:) = [987.85 715.69 1.00];
x_m(3,:) = [891.67 895.76 1.00];
x_m(4,:) = [633.83 693.18 1.00];
x_m(5,:) = [717.73 1039.01 1.00];
x_m(6,:) = [813.91 1124.95 1.00];
x_m(7,:) = [850.74 1043.10 1.00];
x_m(8,:) = [687.04 1114.72 1.00];
x_m(9,:) = [603.14 1137.23 1.00];
x_m(10,:) = [580.63 1174.06 1.00];
x_m(11,:) = [1155.00 817.0 1.00];
x_m(12,:) = [1129.04 410.79 1.00];
x_m(13,:) = [736.15 519.24 1.00];
x_m(14,:) = [611.33 619.51 1.00];
x_m(15,:) = [607.23 697.27 1.00];
x_m(16,:) = [639.97 680.90 1.00];
x_m(17,:) = [586.77 635.88 1.00];
x_m(18,:) = [713.64 707.50 1.00];
x_m(19,:) = [820.05 770.94 1.00];
x_m(20,:) = [1235.45 457.85 1.00];
x_m(21,:) = [1157.69 1012.40 1.00];
x_m(22,:) = [946.92 832.33 1.00];

x_m(24,:) = [719.00 969.00 1.00];
x_m(23,:) = [927.00 1187.00 1.00];

indices_all = [1:4 9:17 19:24];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x_m(ing_p, 1),x_m(ing_p,2), 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










