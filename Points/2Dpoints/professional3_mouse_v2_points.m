mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
object_id = 'mouse';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [312.00 550.00 1.00];
x_m(2,:) = [518.00 603.00 1.00];
x_m(3,:) = [860.00 450.00 1.00];
x_m(4,:) = [351.00 357.00 1.00];
x_m(5,:) = [495.00 352.00 1.00];
x_m(6,:) = [466.00 404.00 1.00];
x_m(7,:) = [548.00 391.00 1.00];
x_m(8,:) = [422.00 369.00 1.00];
x_m(9,:) = [422.00 576.00 1.00];
x_m(10,:) = [590.00 398.00 1.00];
x_m(11,:) = [687.00 347.00 1.00];
x_m(12,:) = [383.00 290.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










