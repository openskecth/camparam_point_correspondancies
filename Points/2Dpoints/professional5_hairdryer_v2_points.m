figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
view = 'view2';
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

num_points = length(H);

% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);

x_m(1,:) = [367.20 584.90 1.00];
x_m(2,:) = [366.10 614.59 1.00];
x_m(3,:) = [172.58 556.32 1.00];
x_m(4,:) = [171.48 529.93 1.00];
x_m(5,:) = [365.00 410.08 1.00];
x_m(6,:) = [366.10 593.70 1.00];
x_m(7,:) = [538.72 449.66 1.00];
x_m(8,:) = [540.92 305.62 1.00];
x_m(9,:) = [613.49 397.98 1.00];
x_m(10,:) = [482.65 368.30 1.00];
x_m(11,:) = [566.21 433.17 1.00];
x_m(12,:) = [539.82 451.86 1.00];
x_m(13,:) = [419.97 403.48 1.00];
x_m(14,:) = [393.58 425.47 1.00];
x_m(15,:) = [506.84 728.94 1.00];
x_m(16,:) = [478.25 754.23 1.00];
x_m(17,:) = [427.67 734.44 1.00];
x_m(18,:) = [449.66 712.45 1.00];

indices_all = [1:18];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);









