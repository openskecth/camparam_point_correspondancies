mouse_3D;

figure(3);
designer_type = 'students';
designer_id ='student6';
object_id = 'mouse';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [121.00 442.00 1.00];
x_m(2,:) = [187.00 522.00 1.00];
x_m(3,:) = [605.00 497.00 1.00];
x_m(4,:) = [370.00 313.00 1.00];
x_m(5,:) = [326.00 255.00 1.00];
x_m(6,:) = [258.00 321.00 1.00];
x_m(7,:) = [327.00 320.00 1.00];
x_m(8,:) = [274.00 269.00 1.00];
x_m(9,:) = [149.00 481.00 1.00];
x_m(10,:) = [460.00 393.00 1.00];
x_m(11,:) = [423.00 285.00 1.00];
x_m(12,:) = [317.00 221.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










