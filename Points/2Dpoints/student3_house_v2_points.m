designer_type = 'students';
designer_id ='student3';
object_id = 'house';
view = 'view2';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
% [img, ~, t] = imread(fullfile(folder, 'view2_concept.png'));
%[img, ~, t] =  imread(fullfile(folder, 'view2_concept_opaque.png'));
[img1, ~, t1] = imread(fullfile(folder, 'view2_concept.png'));
[img2, ~, t2] = imread(fullfile(folder, 'view2_presentation.png'));

img = 255 - (t1+t2)/2;

height  = size(img,2);
width   = size(img,1);
img = repmat(img, 1,1,3);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(34);

x_m(1,:) = [432.00 199.00 1.00];
x_m(2,:) = [647.00 202.00 1.00];
x_m(3,:) = [480.00 339.00 1.00];
x_m(4,:) = [482.00 429.00 1.00];
x_m(5,:) = [559.00 469.00 1.00];
x_m(6,:) = [475.00 538.00 1.00];
x_m(7,:) = [484.00 759.00 1.00];
x_m(8,:) = [572.00 787.00 1.00];
x_m(9,:) = [686.00 742.00 1.00];
x_m(10,:) = [746.00 669.00 1.00];
x_m(11,:) = [745.00 497.00 1.00];
x_m(12,:) = [686.00 460.00 1.00];
x_m(13,:) = [757.00 411.00 1.00];
x_m(14,:) = [760.00 326.00 1.00];
x_m(15,:) = [532.00 299.00 1.00];
x_m(16,:) = [530.00 361.00 1.00];
x_m(17,:) = [467.00 395.00 1.00];
x_m(18,:) = [532.00 422.00 1.00];
x_m(19,:) = [530.00 581.00 1.00];
x_m(20,:) = [462.00 645.00 1.00];
x_m(21,:) = [359.00 667.00 1.00];
x_m(22,:) = [281.00 626.00 1.00];
x_m(23,:) = [281.00 458.00 1.00];
x_m(24,:) = [358.00 401.00 1.00];
x_m(25,:) = [280.00 355.00 1.00];
x_m(26,:) = [278.00 294.00 1.00];
x_m(27,:) = [472.00 190.00 1.00];
x_m(28,:) = [472.00 833.00 1.00];
x_m(29,:) = [744.00 718.00 1.00];
x_m(30,:) = [759.00 212.00 1.00];
x_m(31,:) = [528.00 184.00 1.00];
x_m(32,:) = [530.00 633.00 1.00];
x_m(33,:) = [278.00 198.00 1.00];
x_m(34,:) = [273.00 687.00 1.00];

indices_all = [1:26];
% indices_all = [1:10];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);





