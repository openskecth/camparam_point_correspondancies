x_m = zeros(size(H,1), 3);
% x_m(5,:) = [699 434  1];
% x_m(6,:) = [703 459  1];
% x_m(8,:) = [283 436  1];
% x_m(7,:) = [281 463  1];
% 
% x_m(9,:) = [267 450  1];
% x_m(10,:) = [149 552  1];
% x_m(11,:) = [144 576  1];
% 
% x_m(17,:) = [731 434  1];
% x_m(16,:) = [814 537  1];
% x_m(15,:) = [814 561  1];
% 
% x_m(18,:) = [488 525  1];
% x_m(19,:) = [486 700  1];
% x_m(20,:) = [486 727  1];
% 
% x_m(14,:) = [485 390  1];
% x_m(13,:) = [485 454  1];


indices_all = [1:4 9:19];
indices_opt = indices_all;


figure(3);
designer_type = 'students';
designer_id ='student9';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, 'students_clean\');
folder = fullfile(folder_students, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view2_concept_opaque.png'));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

% [x,y] = ginput(20);

x_m(1,:) = [397.00 593.00 1.00];
x_m(2,:) = [396.00 627.00 1.00];
x_m(3,:) = [318.00 467.00 1.00];
x_m(4,:) = [316.00 439.00 1.00];
x_m(5,:) = [616.00 374.00 1.00];
x_m(6,:) = [619.00 391.00 1.00];
x_m(7,:) = [759.00 515.00 1.00];
x_m(8,:) = [757.00 491.00 1.00];
x_m(9,:) = [612.00 659.00 1.00];
x_m(10,:) = [647.00 832.00 1.00];
x_m(11,:) = [650.00 845.00 1.00];
x_m(12,:) = [203.00 739.00 1.00];
x_m(13,:) = [202.00 727.00 1.00];
x_m(14,:) = [305.00 597.00 1.00];
x_m(15,:) = [426.00 505.00 1.00];
x_m(16,:) = [426.00 496.00 1.00];
x_m(17,:) = [461.00 452.00 1.00];
x_m(18,:) = [745.00 481.00 1.00];
x_m(19,:) = [819.00 534.00 1.00];
x_m(20,:) = [819.00 552.00 1.00];

% x_m(:,1) = x;
% x_m(:,2) = y;
% x_m(:,3) = 1.0;

indices_all = [1:20];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y,'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);







