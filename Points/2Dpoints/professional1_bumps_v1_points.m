designer_type = 'professionals';
designer_id ='Professional1';
object_id = 'bumps';
view = 'view1';

figure(3);
global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, [object_id '\drawings_' object_id '\']);
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

%  [x,y] = ginput(20);

x = [ 211.9943  211.9943  215.5489  214.8380  441.6168  442.3277  460.8112  458.7  349.1991  349.9100  349.1991  127.3966 127.3966  187.8236  332.1373  326.4501  324.3174  496.3565  550.3853  550.3853];
y = [ 320.7629  333.5592  253.9378  243.2742  243.2742  254.6487  331.4264  317.2  383.3225  478.5838  493.5129  397.5406 384.0334  317.2083  317.2083  310.0993  264.6013  314.3647  376.9244  389.7207];

a = [1:20]'; b = num2str(a); c = cellstr(b);
t = text(x, y, c);

num_points = length(x);

x_m(1:num_points, 1) = x;
x_m(1:num_points, 2) = y;
x_m(1:num_points, 3) = 1.0;

indices_all = [1:2 4 5 7:8 9:11 13 14 16 17 18 19];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);





