mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
object_id = 'mouse';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [393.50 961.50 1.00];
x_m(2,:) = [481.50 1109.50 1.00];
x_m(3,:) = [1211.50 961.50 1.00];
x_m(4,:) = [763.50 655.50 1.00];
x_m(5,:) = [729.50 655.50 1.00];
x_m(6,:) = [593.50 785.50 1.00];
x_m(7,:) = [709.50 765.50 1.00];
x_m(8,:) = [599.50 677.50 1.00];
x_m(9,:) = [441.50 1033.50 1.00];
x_m(10,:) = [933.50 769.50 1.00];
x_m(11,:) = [919.50 689.50 1.00];
x_m(12,:) = [711.50 555.50 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










