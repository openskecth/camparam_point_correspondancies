figure(3);

designer_type = 'students';
designer_id ='student8';
object_id = 'house';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);

x_m(1,:) = [461 166  1];
x_m(2,:) = [628 233 1];
x_m(3,:) = [517 360 1];
x_m(4,:) = [516 444  1];
x_m(5,:) = [575 452  1];
x_m(6,:) = [516 506  1];
x_m(7,:) = [521 663  1];
x_m(8,:) = [574 666  1];
x_m(9,:) = [675 611  1];
x_m(10,:) = [710 569  1];
x_m(11,:) = [715 415  1];
x_m(12,:) = [667 411  1];
x_m(13,:) = [717 359  1];
x_m(14,:) = [716 289  1];
x_m(15,:) = [nan nan 1];
x_m(16,:) = [nan nan  1];
x_m(17,:) = [nan nan  1];
x_m(18,:) = [nan nan  1];
x_m(19,:) = [nan nan  1];
x_m(20,:) = [525 530  1];
x_m(21,:) = [426 566  1];
x_m(22,:) = [373 557  1];
x_m(23,:) = [366 415  1];
x_m(24,:) = [420 363  1];
x_m(25,:) = [367 356  1];
x_m(26,:) = [366 282  1];
% Scaffold:
x_m(27,:) = [514 271  1];
x_m(28,:) = [517 698  1];
x_m(29,:) = [710 591  1];
x_m(30,:) = [549 520  1];
x_m(31,:) = [544 145  1];
x_m(32,:) = [717 203  1];
x_m(33,:) = [363 189  1];
x_m(34,:) = [376 586  1];

indices_all = [1:14 20:26 27:34];

% indices_opt = [27:28 30:34];
indices_opt = indices_all;
x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')
