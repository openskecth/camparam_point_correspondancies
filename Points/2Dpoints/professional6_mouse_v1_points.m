mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
object_id = 'mouse';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [168.81 550.45 1.00];
x_m(2,:) = [272.15 641.51 1.00];
x_m(3,:) = [828.75 526.91 1.00];
x_m(4,:) = [439.95 277.26 1.00];
x_m(5,:) = [465.53 205.64 1.00];
x_m(6,:) = [363.21 315.12 1.00];
x_m(7,:) = [468.59 295.68 1.00];
x_m(8,:) = [358.09 228.15 1.00];
x_m(9,:) = [210.76 596.49 1.00];
x_m(10,:) = [610.81 382.65 1.00];
x_m(11,:) = [675.00 268.00 1.00];
x_m(12,:) = [396.00 99.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










