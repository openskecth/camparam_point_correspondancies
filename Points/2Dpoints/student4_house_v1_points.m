figure(3);

designer_type = 'students';
designer_id ='student4';
object_id = 'house';
view = 'view1';


global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);

imshow(img);
hold on;

x_m = ones(size(H,1), 3);
x_m(1,:) = [517 134  1];
x_m(2,:) = [304 186 1];
x_m(3,:) = [205 304  1];

x_m(4,:) = [216 449  1];
x_m(5,:) = [260 489  1];
x_m(6,:) = [219 496  1];
x_m(7,:) = [235 673  1];
x_m(8,:) = [285 733  1];
x_m(9,:) = [382 817  1];
x_m(10,:) = [441 831  1];
x_m(11,:) = [435 630  1];
x_m(12,:) = [369 559  1];
x_m(13,:) = [431 561  1];
x_m(14,:) = [434 411  1];

x_m(15,:) = [646 323  1];
x_m(16,:) = [638 443  1];

x_m(18,:) = [638 494  1];
x_m(19,:) = [626 681  1];






x_m(27,:) = [196 154  1];
x_m(28,:) = [237 688  1];
x_m(29,:) = [446 871  1];
x_m(30,:) = [626 699  1];
x_m(31,:) = [654 165  1];
x_m(32,:) = [430 225  1];
% x_m(33,:) = [240 115  1];
% x_m(34,:) = [244 709  1];

indices_all = [1:16 18:19];
indices_opt = indices_all;


x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')
