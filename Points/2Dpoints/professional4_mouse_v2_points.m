mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
object_id = 'mouse';
view = 'view2';
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [483.50 1261.50 1.00];
x_m(2,:) = [593.50 1291.50 1.00];
x_m(3,:) = [1683.50 1259.50 1.00];
x_m(4,:) = [1061.50 1207.50 1.00];
x_m(5,:) = [1039.50 931.50 1.00];
x_m(6,:) = [857.50 1023.50 1.00];
x_m(7,:) = [1017.50 993.50 1.00];
x_m(8,:) = [887.50 977.50 1.00];
x_m(9,:) = [529.50 1267.50 1.00];
x_m(10,:) = [1381.50 1235.50 1.00];
x_m(11,:) = [1378.69 905.99 1.00];
x_m(12,:) = [1094.26 910.09 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










