mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
object_id = 'mouse';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [518.00 565.00 1.00];
x_m(2,:) = [651.00 487.00 1.00];
x_m(3,:) = [483.00 351.00 1.00];
x_m(4,:) = [165.00 514.00 1.00];
x_m(5,:) = [465.00 330.00 1.00];
x_m(6,:) = [503.00 365.00 1.00];
x_m(7,:) = [523.00 328.00 1.00];
x_m(8,:) = [445.00 369.00 1.00];
x_m(9,:) = [595.00 524.00 1.00];
x_m(10,:) = [349.00 422.00 1.00];
x_m(11,:) = [484.00 266.00 1.00];
x_m(12,:) = [295.00 341.00 1.00];

indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










