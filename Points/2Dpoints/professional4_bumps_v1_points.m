figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view1';

global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');

%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

%    [x,y] = ginput(20);

x = 1.0e+03 *[ 0.9955    0.9955    0.9355    0.9355    0.4335    0.4395    0.5555    0.5575    0.7515    0.7395    0.7415    1.1035 1.1015    1.0255    0.6395    0.6395    0.6375    0.4315    0.3795    0.3835];
y = 1.0e+03 *[ 0.5535    0.5955    0.9035    0.8715    0.8055    0.8395    0.5415    0.5155    0.5715    0.6135    0.6275    0.9215 0.8835    0.7435    1.2635    1.2375    0.9795    0.7095    0.7715    0.7955];

indices_all = [1:20];
indices_opt = indices_all;

a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x, y, c);

x_m(indices_all,1) = x;
x_m(indices_all,2) = y;
x_m(indices_all,3) = 1.0;


% bounding_box = read_sketch_pixel_coordinates(fullfile(folder, [view '_concept.json']));
% hold on;
% plot([bounding_box(1) bounding_box(2) bounding_box(2) bounding_box(1) bounding_box(1)],...
% [bounding_box(3) bounding_box(3) bounding_box(4) bounding_box(4) bounding_box(3)]);









