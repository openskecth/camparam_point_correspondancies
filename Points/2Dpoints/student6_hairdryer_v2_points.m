hairdryer_3D;
% figure(3);
designer_type = 'students';
designer_id ='student6';
view = 'view2';
object_id= 'hairdryer';

global folder_sketches;
folder_students = fullfile(folder_sketches, 'students_clean\');
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');

[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);

% 
% imshow(img)
% hold on;

num_points = length(H);

x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [194.33 294.37 1.00];
x_m(2,:) = [238.52 303.54 1.00];
x_m(3,:) = [183.49 589.50 1.00];
x_m(4,:) = [143.48 568.66 1.00];
x_m(5,:) = [166.82 391.08 1.00];
x_m(6,:) = [366.07 466.95 1.00];
x_m(7,:) = [530.31 430.26 1.00];
x_m(8,:) = [352.00 384.00 1.00];
x_m(9,:) = [425.26 299.38 1.00];
x_m(10,:) = [417.76 515.30 1.00];
x_m(11,:) = [416.93 289.37 1.00];
x_m(12,:) = [363.57 286.04 1.00];
x_m(13,:) = [371.91 522.80 1.00];
x_m(14,:) = [327.00 532.00 1.00];
x_m(15,:) = [757.07 464.45 1.00];
x_m(16,:) = [732.06 471.11 1.00];
x_m(17,:) = [721.22 552.82 1.00];
x_m(18,:) = [742.90 544.48 1.00];




indices_all = [1:18];
indices_opt = indices_all;


% x =  x_m(indices_all,1);
% y =  x_m(indices_all,2);
% plot(x,y, 'o-');
% a = [indices_all]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);






