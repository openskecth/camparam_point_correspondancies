mouse_3D;

figure(3);
designer_type = 'students';
designer_id ='student8';
object_id = 'mouse';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [328.00 589.00 1.00];
x_m(2,:) = [435.00 653.00 1.00];
x_m(3,:) = [813.00 543.00 1.00];
x_m(4,:) = [504.00 404.00 1.00];
x_m(5,:) = [501.00 319.00 1.00];
x_m(6,:) = [444.00 430.00 1.00];
x_m(7,:) = [519.00 398.00 1.00];
x_m(8,:) = [428.00 356.00 1.00];
x_m(9,:) = [378.00 620.00 1.00];
x_m(10,:) = [639.00 464.00 1.00];
x_m(11,:) = [739.00 318.00 1.00];
x_m(12,:) = [456.00 218.00 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










