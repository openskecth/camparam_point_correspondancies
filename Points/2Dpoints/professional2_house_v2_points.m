
designer_type = 'professionals';
designer_id ='Professional2';
object_id = 'house';
view = 'view2';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;



x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(length(x_m));

x_m(1,:) = [453.00 214.00 1.00];
x_m(2,:) = [255.00 166.00 1.00];
x_m(3,:) = [194.00 293.00 1.00];
x_m(4,:) = [195.00 412.00 1.00];
x_m(5,:) = [229.00 426.00 1.00];
x_m(6,:) = [197.00 456.00 1.00];
x_m(7,:) = [206.00 635.00 1.00];
x_m(8,:) = [232.00 669.00 1.00];
x_m(9,:) = [297.00 698.00 1.00];
x_m(10,:) = [328.00 687.00 1.00];
x_m(11,:) = [334.00 450.00 1.00];
x_m(12,:) = [308.00 417.00 1.00];
x_m(13,:) = [326.00 383.00 1.00];
x_m(14,:) = [331.00 243.00 1.00];
x_m(15,:) = [550.00 295.00 1.00];
x_m(16,:) = [551.00 397.00 1.00];
x_m(17,:) = [515.00 422.00 1.00];
x_m(18,:) = [548.00 454.00 1.00];
x_m(19,:) = [545.00 634.00 1.00];
x_m(20,:) = [495.00 657.00 1.00];
x_m(21,:) = [690.00 80.00 1.00];
x_m(22,:) = [690.00 80.00 1.00];
x_m(23,:) = [690.00 80.00 1.00];
x_m(24,:) = [690.00 80.00 1.00];
x_m(25,:) = [690.00 80.00 1.00];
x_m(26,:) = [690.00 80.00 1.00];
x_m(27,:) = [690.00 80.00 1.00];
x_m(28,:) = [204.00 657.00 1.00];
x_m(29,:) = [325.00 715.00 1.00];
x_m(30,:) = [544.00 662.00 1.00];
x_m(31,:) = [555.00 244.00 1.00];
x_m(32,:) = [332.00 117.00 1.00];
x_m(33,:) = [719.00 82.00 1.00];
x_m(34,:) = [691.00 514.00 1.00];

% indices_opt = [1:2 27:29 31:34];
indices_all = [1:19];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

