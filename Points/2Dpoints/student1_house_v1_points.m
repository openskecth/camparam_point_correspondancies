x_m = zeros(size(H,1), 3);

x_m(1,:) = [306 146  1];
x_m(2,:) = [533 177 1];
x_m(3,:) = [375 345 1];
x_m(4,:) = [376 476  1];
x_m(5,:) = [457 494  1];
x_m(6,:) = [373 564  1];
x_m(7,:) = [375 881  1];
x_m(8,:) = [467 901  1];
x_m(9,:) = [602 831  1];
x_m(10,:) = [661 763  1];
x_m(11,:) = [656 480  1];
x_m(12,:) = [593 462  1];
x_m(13,:) = [661 416  1];
x_m(14,:) = [663 298  1];
x_m(15,:) = [nan nan  1];
x_m(16,:) = [nan nan  1];
x_m(17,:) = [nan nan  1];
x_m(18,:) = [nan nan  1];
x_m(19,:) = [nan nan  1];
x_m(20,:) = [nan nan  1];
x_m(21,:) = [nan nan  1];
x_m(22,:) = [131 760  1];
x_m(23,:) = [134 492  1];
x_m(24,:) = [nan nan  1];
x_m(25,:) = [141 421  1];
x_m(26,:) = [139 294  1];
%Scaffolds
x_m(27,:) = [382 198  1];
x_m(28,:) = [373 945  1];
x_m(29,:) = [659 802  1];
x_m(30,:) = [nan nan  1];
x_m(31,:) = [nan nan  1];
x_m(32,:) = [666 165  1];
x_m(33,:) = [139 163  1];
x_m(34,:) = [132 798  1];

indices_all = [1:14 22:23 25:26 27:29 32:34];
% indices_opt = [1:2 3 27 28 8:9 29 32 26 34];
indices_opt = indices_all;









designer_type = 'students';
designer_id ='student1';
object_id = 'house';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'house\drawings_house\');
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));
height  = size(img,2);
width   = size(img,1);

% C_est_ = [-6.065 2.589 -5.662];
%  theta_x_ = 2.75;
%  theta_y_ = 0.74; 
%  theta_z_ = -0.28;
 
 
% theta_x_0 = -3.04; 
% theta_y_0 = -0.65; 
% theta_z_0 = -0.02;