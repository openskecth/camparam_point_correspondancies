hairdryer_3D;

figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
view = 'view1';
object_id= 'hairdryer';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

num_points = length(H);

% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m = ...   
[ 319.9155  538.7234    1.0000
  319.9155  565.1123    1.0000
  182.4734  467.2535    1.0000
  182.4734  446.3623    1.0000
  371.5937  375.9919    1.0000
  369.3947  528.8275    1.0000
  677.2650  392.4850    1.0000
  686.0613  246.2465    1.0000
  735.5405  356.2002    1.0000
  615.6910  282.5313    1.0000
  642.0799  402.3808    1.0000
  591.5012  425.4711    1.0000
  502.4387  322.1146    1.0000
  454.0590  339.7072    1.0000
  568.0000  688.0000    1.0000
  527.0000  711.0000    1.0000
  479.3484  677.2650    1.0000
  522.2303  657.4734    1.0000
];

indices_all = [1:18];
indices_opt = indices_all;
x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);









