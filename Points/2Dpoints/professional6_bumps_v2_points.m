x_m = zeros(size(H,1), 3);

figure(3);
designer_type = 'professionals';
designer_id ='Professional6';

view = 'view2';

global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

% [x,y] = ginput(20);

x_m(1,:) = [40.91 508.50 1.00];
x_m(2,:) = [42.96 537.15 1.00];
x_m(3,:) = [288.52 195.41 1.00];
x_m(4,:) = [287.50 169.83 1.00];
x_m(5,:) = [704.94 172.90 1.00];
x_m(6,:) = [706.99 198.48 1.00];
x_m(7,:) = [917.76 525.89 1.00];
x_m(8,:) = [919.81 494.17 1.00];
x_m(9,:) = [469.62 634.35 1.00];
x_m(10,:) = [467.57 813.40 1.00];
x_m(11,:) = [469.62 841.02 1.00];
x_m(12,:) = [61.38 434.83 1.00];
x_m(13,:) = [59.33 411.30 1.00];
x_m(14,:) = [109.47 331.49 1.00];
x_m(15,:) = [491.10 267.03 1.00];
x_m(16,:) = [490.08 251.69 1.00];
x_m(17,:) = [490.08 152.44 1.00];
x_m(18,:) = [874.79 385.72 1.00];
x_m(19,:) = [950.00 510.00 1.00];
x_m(20,:) = [949.00 535.00 1.00];
x_m(21,:) = [287.00 272.00 1.00];
x_m(22,:) = [602.00 152.00 1.00];

x = x_m(:,1);
y = x_m(:,2);



indices_all = [1:22];
indices_opt = indices_all;

a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')

indices_opt = indices_all;






