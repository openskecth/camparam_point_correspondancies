figure(3);
designer_type = 'professionals';
designer_id ='Professional1';
view = 'view1';

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m = ...
    [202.4779  474.6900    1.0000
  200.7330  321.1345    1.0000
  281.8731  420.5966    1.0000
  130.0625  386.5701    1.0000
  376.9729  317.6446    1.0000
  494.7569  333.3491    1.0000
  424.0865  393.5499    1.0000
  426.0461  264.0000    1.0000
  196.3706  228.6521    1.0000
  293.2153  232.1420    1.0000
  237.3769  283.6181    1.0000
  239.1218  171.0688    1.0000
  351.6711  251.3365    1.0000
  408.3819  136.1698    1.0000
  349.0537  281.0006    1.0000
  413.6168  270.5309    1.0000];





indices_all = [1:16];
indices_opt = indices_all;


x =  x_m(:,1);
y =  x_m(:,2);
plot(x(indices_all),y(indices_all), 'o-');
a = indices_all'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c);






