figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view1';


num_points = length(H);
x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m = ...
    [  341.0000  934.0000    1.0000
  342.0000  757.0000    1.0000
  415.0000  937.0000    1.0000
  276.0000  780.0000    1.0000
  689.0000  644.0000    1.0000
  816.0000  733.0000    1.0000
  750.0000  753.0000    1.0000
  754.0000  629.0000    1.0000
  402.0000  593.0000    1.0000
  515.0000  662.0000    1.0000
  455.0000  691.0000    1.0000
  455.0000  560.0000    1.0000
  619.0000  642.0000    1.0000
  729.0000  504.0000    1.0000
  606.0000  678.0000    1.0000
  725.0000  645.0000    1.0000];





indices_all = [1:4 6 8:12 14:15];
indices_opt = indices_all;


x =  x_m(:,1);
y =  x_m(:,2);
plot(x(indices_all),y(indices_all), 'o-');
a = indices_all'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c);






