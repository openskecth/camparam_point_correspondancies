x_m = ones(size(H,1), 3);

figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view1';
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);

% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m = ...   
1.0e+03 *[0.4785    0.6012    0.0010
    0.4785    0.6212    0.0010
    0.3532    0.5332    0.0010
    0.3545    0.5198    0.0010
    0.5745    0.4318    0.0010
    0.5678    0.6065    0.0010
    0.9572    0.4625    0.0010
    0.9545    0.3358    0.0010
    1.0345    0.4078    0.0010
    0.8972    0.3852    0.0010
    0.9212    0.4478    0.0010
    0.8705    0.4638    0.0010
    0.7958    0.4065    0.0010
    0.7265    0.4225    0.0010
    0.8652    0.6598    0.0010
    0.7980    0.6990    0.0010
    0.7492    0.6545    0.0010
    0.8038    0.6172    0.0010
];

indices_all = [1:9 11:12 16 18];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);












