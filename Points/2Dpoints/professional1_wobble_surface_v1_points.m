designer_type = 'professionals';
designer_id ='Professional1';
object_id = 'wobble_surface';
view = 'view1';



x_m = ones(size(H,1), 3);

x_m(1,:) = [72 420  1];
x_m(2,:) = [452 256  1];
x_m(3,:) = [582 269 1];
x_m(4,:) = [382 481 1];
x_m(5,:) = [331 469  1];
x_m(6,:) = [201 400  1];
x_m(7,:) = [136 434  1];
x_m(8,:) = [371 351  1];
x_m(9,:) = [419 320  1];
x_m(10,:) = [476 288  1];
x_m(11,:) = [410.34 253.94 1.00];
x_m(12,:) = [604.41 272.42 1.00];
x_m(13,:) = [450.86 260.34 1.00];
x_m(14,:) = [582.38 275.98 1.00];
x_m(15,:) = [379.77 489.25 1.00];
x_m(16,:) = [73.37 428.11 1.00];
x_m(17,:) = [202.04 408.20 1.00];

% indices_all = [1:10];
indices_all = [1:10 13:17];
indices_opt = indices_all;
