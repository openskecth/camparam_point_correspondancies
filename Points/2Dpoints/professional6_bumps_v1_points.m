x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
view = 'view1';
global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');

%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

%    [x,y] = ginput(20);

% x_m(:,1) = x;
% x_m(:,2) = y;
% x_m(:,3) = 1.0;

x_m = ...
  [519.7526  509.5211    1.0000;
  520.7758  532.0305    1.0000;
  170.8558  418.4600    1.0000;
  170.8558  391.8579    1.0000;
  302.8432  198.4811    1.0000;
  302.8432  218.9442    1.0000;
  590.3505  314.0979    1.0000;
  590.3505  288.5189    1.0000;
  630.2537  432.7842    1.0000;
  687.5505  518.7295    1.0000;
  688.5737  545.3316    1.0000;
  307.9589  660.9484    1.0000;
  307.9589  638.4389    1.0000;
  331.4916  509.5211    1.0000;
   93.0958  358.0937    1.0000;
  104.3505  329.4453    1.0000;
  166.7632  284.4263    1.0000;
  464.5021  219.9674    1.0000;
  440.9695  267.0326    1.0000;
  437.9000  276.2411    1.0000];



indices_all = [1:14 16:19];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);



