

figure(3);

designer_type = 'students';
designer_id ='student4';
object_id = 'house';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);
% x_m(:,1:2) = ginput(length(x_m));


x_m(1,:) = [561.00 265.00 1.00];
x_m(2,:) = [336.00 149.00 1.00];
x_m(3,:) = [228.00 373.00 1.00];
x_m(4,:) = [226.00 497.00 1.00];
x_m(5,:) = [268.00 510.00 1.00];
x_m(6,:) = [230.00 521.00 1.00];
x_m(7,:) = [253.00 695.00 1.00];
x_m(8,:) = [299.00 755.00 1.00];
x_m(9,:) = [370.00 809.00 1.00];
x_m(10,:) = [445.00 822.00 1.00];
x_m(11,:) = [453.00 547.00 1.00];
x_m(12,:) = [356.00 510.00 1.00];
x_m(13,:) = [450.00 472.00 1.00];
x_m(14,:) = [454.00 292.00 1.00];
x_m(15,:) = [679.00 379.00 1.00];
x_m(16,:) = [686.00 491.00 1.00];
x_m(17,:) = [624.00 513.00 1.00];
x_m(18,:) = [689.00 541.00 1.00];
x_m(19,:) = [668.00 702.00 1.00];
x_m(20,:) = [623.00 728.00 1.00];
x_m(21,:) = [492.00 674.00 1.00];
x_m(22,:) = [471.00 644.00 1.00];
x_m(23,:) = [484.00 529.00 1.00];
x_m(24,:) = [517.00 511.00 1.00];
x_m(25,:) = [488.00 496.00 1.00];
x_m(26,:) = [490.00 363.00 1.00];
x_m(27,:) = [239.00 255.00 1.00];
x_m(28,:) = [256.00 732.00 1.00];
x_m(29,:) = [450.00 874.00 1.00];
x_m(30,:) = [671.00 740.00 1.00];
x_m(31,:) = [668.00 304.00 1.00];
x_m(32,:) = [442.00 158.00 1.00];
x_m(33,:) = [456.00 280.00 1.00];
x_m(34,:) = [461.00 711.00 1.00];

indices_all = [1:16 18:19];
indices_opt = indices_all;


x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')
