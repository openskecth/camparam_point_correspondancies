
designer_type = 'professionals';
designer_id ='Professional6';
object_id = 'house';
view = 'view2';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(length(x_m));
x_m(1,:) = [332.00 305.00 1.00];
x_m(2,:) = [569.00 130.00 1.00];
x_m(3,:) = [389.00 277.00 1.00];
x_m(4,:) = [394.00 412.00 1.00];
x_m(5,:) = [503.00 497.00 1.00];
x_m(6,:) = [394.00 532.00 1.00];
x_m(7,:) = [396.00 861.00 1.00];
x_m(8,:) = [514.00 918.00 1.00];
x_m(9,:) = [651.00 921.00 1.00];
x_m(10,:) = [756.00 870.00 1.00];
x_m(11,:) = [736.00 599.00 1.00];
x_m(12,:) = [643.00 527.00 1.00];
x_m(13,:) = [732.00 494.00 1.00];
x_m(14,:) = [731.00 378.00 1.00];
x_m(15,:) = [467.00 493.00 1.00];
x_m(16,:) = [469.00 608.00 1.00];
x_m(17,:) = [382.00 640.00 1.00];
x_m(18,:) = [469.00 695.00 1.00];
x_m(19,:) = [473.00 863.00 1.00];
x_m(20,:) = [400.00 913.00 1.00];
x_m(21,:) = [268.00 913.00 1.00];
x_m(22,:) = [167.00 871.00 1.00];
x_m(23,:) = [173.00 647.00 1.00];
x_m(24,:) = [264.00 615.00 1.00];
x_m(25,:) = [181.00 549.00 1.00];
x_m(26,:) = [187.00 425.00 1.00];
x_m(27,:) = [390.00 68.00 1.00];
x_m(28,:) = [395.00 913.00 1.00];
x_m(29,:) = [759.00 919.00 1.00];
x_m(30,:) = [475.00 918.00 1.00];
x_m(31,:) = [460.00 355.00 1.00];
x_m(32,:) = [722.00 192.00 1.00];
x_m(33,:) = [195.00 254.00 1.00];
x_m(34,:) = [169.00 916.00 1.00];


indices_all = [1:34];
% indices_opt = [27:34];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);