figure(3);

designer_type = 'students';
designer_id ='student5';
object_id = 'house';
view = 'view2';


global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);

x_m(1,:) = [517 195  1];
x_m(2,:) = [300 162 1];
x_m(3,:) = [202 265  1];
x_m(4,:) = [206 337  1];
x_m(5,:) = [261 361  1];
x_m(6,:) = [205 394  1];
x_m(7,:) = [204 608  1];
x_m(8,:) = [265 654  1];
x_m(9,:) = [376 676  1];
x_m(10,:) = [437 643  1];

x_m(11,:) = [438 393  1];
x_m(12,:) = [369 363  1];
x_m(13,:) = [437 337  1];
x_m(14,:) = [437 244  1];
x_m(15,:) = [639 263  1];

x_m(16,:) = [640 343  1];
x_m(17,:) = [580 364  1];
x_m(18,:) = [642 380  1];
x_m(19,:) = [639 596  1];
x_m(20,:) = [584 607  1];
% x_m(21,:) = [297 691  1];
% x_m(22,:) = [242 667  1];

% x_m(23,:) = [245 446  1];
% x_m(25,:) = [331 308  1];
% x_m(26,:) = [335 231  1];


x_m(27,:) = [202 174  1];
x_m(28,:) = [205 642  1];
x_m(29,:) = [436 690  1];
x_m(29,:) = [436 690  1];
x_m(30,:) = [639 615  1];
x_m(31,:) = [638 180  1];
x_m(32,:) = [433 143  1];

x_m(34,:) = [444 589  1];

indices = [1:4 6:7 10 11 13:16 18 19];

indices_all = [1:20];
indices_opt = indices_all;

x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x_m(indices_all), y(indices_all), c, 'Color', 'r');
plot(x_m(indices_all), y(indices_all), '-*')