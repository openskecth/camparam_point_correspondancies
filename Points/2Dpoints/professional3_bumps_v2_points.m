x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
view = 'view2';
global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

%    [x,y] = ginput(20);

x_m(1,:) = [685.00 439.00 1.00];
x_m(2,:) = [685.00 460.00 1.00];
x_m(3,:) = [274.00 539.00 1.00];
x_m(4,:) = [272.00 511.00 1.00];
x_m(5,:) = [274.00 364.00 1.00];
x_m(6,:) = [274.00 384.00 1.00];
x_m(7,:) = [631.00 295.00 1.00];
x_m(8,:) = [631.00 277.00 1.00];
x_m(9,:) = [694.00 408.00 1.00];
x_m(10,:) = [792.00 514.00 1.00];
x_m(11,:) = [793.00 541.00 1.00];
x_m(12,:) = [508.00 833.00 1.00];
x_m(13,:) = [505.00 813.00 1.00];
x_m(14,:) = [499.00 594.00 1.00];
x_m(15,:) = [96.00 632.00 1.00];
x_m(16,:) = [96.00 613.00 1.00];
x_m(17,:) = [220.00 495.00 1.00];
x_m(18,:) = [464.00 358.00 1.00];
x_m(19,:) = [424.00 417.00 1.00];
x_m(20,:) = [423.00 432.00 1.00];

x = x_m(:,1);
y = x_m(:,2);



indices_all = [1:20];
indices_opt = indices_all;

a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '*')






