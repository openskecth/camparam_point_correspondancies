figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
view='view2';

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m(1,:) = [245.00 641.00 1.00];
x_m(2,:) = [243.00 367.00 1.00];
x_m(3,:) = [308.00 522.00 1.00];
x_m(4,:) = [180.00 468.00 1.00];
x_m(5,:) = [614.00 402.00 1.00];
x_m(6,:) = [752.00 444.00 1.00];
x_m(7,:) = [694.00 545.00 1.00];
x_m(8,:) = [680.00 312.00 1.00];
x_m(9,:) = [290.00 156.00 1.00];
x_m(10,:) = [387.00 191.00 1.00];
x_m(11,:) = [341.00 260.00 1.00];
x_m(12,:) = [339.00 97.00 1.00];
x_m(13,:) = [508.00 222.00 1.00];
x_m(14,:) = [625.00 93.00 1.00];
x_m(15,:) = [492.00 339.00 1.00];
x_m(16,:) = [626.00 323.00 1.00];




indices_all = [1:16];
indices_opt = indices_all;


x =  x_m(:,1);
y =  x_m(:,2);
plot(x(indices_all),y(indices_all), 'o-');
a = indices_all'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c);






