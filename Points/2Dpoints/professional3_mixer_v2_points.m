figure(3);
designer_type = 'professionals';
designer_id ='Professional3';
object_id = 'mixer';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [155.00 433.00 1.00];
x_m(2,:) = [455.00 397.00 1.00];
x_m(3,:) = [351.00 453.00 1.00];
x_m(4,:) = [224.00 378.00 1.00];
x_m(5,:) = [274.00 637.00 1.00];
x_m(6,:) = [322.00 673.00 1.00];
x_m(7,:) = [385.00 648.00 1.00];
x_m(8,:) = [267.00 672.00 1.00];
x_m(9,:) = [144.00 695.00 1.00];
x_m(10,:) = [138.00 710.00 1.00];
x_m(11,:) = [707.00 594.00 1.00];
x_m(12,:) = [639.00 154.00 1.00];
x_m(13,:) = [291.00 157.00 1.00];
x_m(14,:) = [162.00 253.00 1.00];
x_m(15,:) = [164.00 297.00 1.00];
x_m(16,:) = [175.00 286.00 1.00];
x_m(17,:) = [153.00 272.00 1.00];
x_m(18,:) = [244.00 370.00 1.00];
x_m(19,:) = [335.00 417.00 1.00];
x_m(20,:) = [716.00 247.00 1.00];
x_m(21,:) = [721.00 692.00 1.00];
x_m(22,:) = [494.00 580.00 1.00];
x_m(24,:) = [245.00 622.00 1.00];
x_m(23,:) = [420.00 765.00 1.00];

x_m(25,:) = [578.00 335.00 1.00];
x_m(26,:) = [696.00 359.00 1.00];
indices_all = [1:26];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










