figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view2';


num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);

x_m(1,:) = [977.75 1013.75 1.00];
x_m(2,:) = [965.75 815.75 1.00];
x_m(3,:) = [1076.75 866.75 1.00];
x_m(4,:) = [872.75 956.75 1.00];
x_m(5,:) = [488.75 853.25 1.00];
x_m(6,:) = [656.75 757.25 1.00];
x_m(7,:) = [557.75 901.25 1.00];
x_m(8,:) = [556.25 731.75 1.00];
x_m(9,:) = [791.75 691.25 1.00];
x_m(10,:) = [923.75 632.75 1.00];
x_m(11,:) = [862.25 743.75 1.00];
x_m(12,:) = [856.25 584.75 1.00];
x_m(13,:) = [719.75 709.25 1.00];
x_m(14,:) = [604.25 526.25 1.00];
x_m(15,:) = [731.75 758.75 1.00];
x_m(16,:) = [598.25 728.75 1.00];


indices_all = [1:5 7:12 14:16];
indices_opt = indices_all;


x =  x_m(:,1);
y =  x_m(:,2);
plot(x(indices_all),y(indices_all), 'o-');
a = indices_all'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c);






