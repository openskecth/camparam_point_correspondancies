figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
object_id = 'mixer';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [209.00 439.00 1.00];
x_m(2,:) = [500.00 358.00 1.00];
x_m(3,:) = [494.00 450.00 1.00];
x_m(4,:) = [246.00 355.00 1.00];
x_m(5,:) = [313.00 602.00 1.00];
x_m(6,:) = [418.00 651.00 1.00];
x_m(7,:) = [414.00 597.00 1.00];
x_m(8,:) = [313.00 638.00 1.00];
x_m(9,:) = [206.00 679.00 1.00];
x_m(10,:) = [191.00 716.00 1.00];
x_m(11,:) = [686.00 521.00 1.00];
x_m(12,:) = [609.00 59.00 1.00];
x_m(13,:) = [369.00 82.00 1.00];
x_m(14,:) = [257.00 174.00 1.00];
x_m(15,:) = [258.00 240.00 1.00];
x_m(16,:) = [278.00 209.00 1.00];
x_m(17,:) = [243.00 199.00 1.00];
x_m(18,:) = [326.00 303.00 1.00];
x_m(19,:) = [398.00 328.00 1.00];
x_m(20,:) = [683.00 134.00 1.00];
x_m(21,:) = [694.00 657.00 1.00];
x_m(22,:) = [392.00 514.00 1.00];

x_m(24,:) = [232.62 565.91 1.00];
x_m(23,:) = [553.20 723.26 1.00];


indices_all = [1:24];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = 1:22;
plot(x(ing_p),y(ing_p), 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 16);










