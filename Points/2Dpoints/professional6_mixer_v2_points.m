figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
object_id = 'mixer';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);

imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [876.00 494.00 1.00];
x_m(2,:) = [425.00 491.00 1.00];
x_m(3,:) = [648.00 367.00 1.00];
x_m(4,:) = [663.00 599.00 1.00];
x_m(5,:) = [648.00 819.00 1.00];
x_m(6,:) = [626.00 716.00 1.00];
x_m(7,:) = [528.00 772.00 1.00];
x_m(8,:) = [767.00 762.00 1.00];
x_m(9,:) = [794.00 755.00 1.00];
x_m(10,:) = [838.00 764.00 1.00];
x_m(11,:) = [120.00 763.00 1.00];
x_m(12,:) = [300.00 203.00 1.00];
x_m(13,:) = [726.00 208.00 1.00];
x_m(14,:) = [853.00 290.00 1.00];
x_m(15,:) = [848.00 368.00 1.00];
x_m(16,:) = [841.00 321.00 1.00];
x_m(17,:) = [859.00 337.00 1.00];
x_m(18,:) = [674.00 491.00 1.00];
x_m(19,:) = [632.00 372.00 1.00];
x_m(20,:) = [167.00 334.00 1.00];
x_m(21,:) = [315.00 658.00 1.00];
x_m(22,:) = [257.00 891.00 1.00];
x_m(24,:) = [668.00 888.00 1.00];
x_m(23,:) = [607.00 653.00 1.00];

indices_all = [1:24];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = 1:20;
plot(x(ing_p),y(ing_p), 'o-', 'color', 'b');

a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');
