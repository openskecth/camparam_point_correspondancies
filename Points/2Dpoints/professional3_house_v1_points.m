
designer_type = 'professionals';
designer_id ='Professional3';
object_id = 'house';
view = 'view1';

figure(3);
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

x_m = ones(size(H,1), 3);

x_m(1,:) = [531 237  1];
x_m(2,:) = [314 307 1];
x_m(3,:) = [230 380  1];
x_m(4,:) = [229 447  1];
x_m(5,:) = [274 529  1];
x_m(6,:) = [235 525  1];
x_m(7,:) = [240 670  1];
x_m(8,:) = [271 740  1];
x_m(9,:) = [348 809  1];
x_m(10,:) = [nan nan  1];
x_m(11,:) = [408 662  1];
x_m(12,:) = [342 582  1];
x_m(13,:) = [404 575  1];
x_m(14,:) = [405 496  1];
x_m(15,:) = [621 403  1];
x_m(16,:) = [621 471  1];
x_m(17,:) = [567 485  1];
x_m(18,:) = [625 557  1];
x_m(19,:) = [622 688  1];
x_m(20,:) = [569 685  1];
x_m(21,:) = [479 633  1];
x_m(22,:) = [437 575  1];
x_m(23,:) = [436 443  1];
x_m(24,:) = [476 431  1];
x_m(25,:) = [431 363  1];
x_m(26,:) = [428 309  1];

x_m(27,:) = [228 250  1];
x_m(28,:) = [240 712  1];
x_m(29,:) = [403 850  1];
x_m(30,:) = [622 716  1];
x_m(31,:) = [625 273  1];
x_m(32,:) = [407 362  1];
x_m(33,:) = [430 201  1];

% x_m(34,:) = [444 589  1];

indices_all = [1:9 11:26 ];
% indices_opt = [1:2 27:33];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);