tx_m = ones(size(H,1), 3);

figure(3);
designer_type = 'students';
designer_id ='student2';
object_id = 'house';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

% x_m(:,1:2) = ginput(length(x_m));

x_m(1,:) = [349.00 426.00 1.00];
x_m(2,:) = [483.00 367.00 1.00];
x_m(3,:) = [410.00 408.00 1.00];
x_m(4,:) = [407.00 473.00 1.00];
x_m(5,:) = [445.00 497.00 1.00];
x_m(6,:) = [408.00 505.00 1.00];
x_m(7,:) = [409.00 686.00 1.00];
x_m(8,:) = [461.00 712.00 1.00];
x_m(9,:) = [572.00 708.00 1.00];
x_m(10,:) = [606.00 694.00 1.00];
x_m(11,:) = [576.00 534.00 1.00];
x_m(12,:) = [539.00 514.00 1.00];
x_m(13,:) = [572.00 510.00 1.00];
x_m(14,:) = [562.00 460.00 1.00];
x_m(15,:) = [447.00 501.00 1.00];
x_m(16,:) = [447.00 526.00 1.00];
x_m(17,:) = [451.00 546.00 1.00];
x_m(18,:) = [448.00 572.00 1.00];
x_m(19,:) = [449.00 676.00 1.00];
x_m(20,:) = [443.00 710.00 1.00];
x_m(21,:) = [308.00 715.00 1.00];
x_m(22,:) = [255.00 699.00 1.00];
x_m(23,:) = [270.00 550.00 1.00];
x_m(24,:) = [314.00 530.00 1.00];
x_m(25,:) = [272.00 522.00 1.00];
x_m(26,:) = [276.00 466.00 1.00];
x_m(27,:) = [405.00 336.00 1.00];
x_m(28,:) = [410.00 711.00 1.00];
x_m(29,:) = [610.00 709.00 1.00];
x_m(30,:) = [455.00 713.00 1.00];
x_m(31,:) = [407.00 448.00 1.00];
x_m(32,:) = [553.00 390.00 1.00];
x_m(33,:) = [279.00 409.00 1.00];
x_m(34,:) = [254.00 716.00 1.00];
x = x_m(:,1);
y = x_m(:,2);

indices_all = [1:14 21:26];
indices_opt = indices_all;

a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')

indices_opt = indices_all;








% C_est_ = [-6.065 2.589 -5.662];
%  theta_x_ = 2.75;
%  theta_y_ = 0.74; 
%  theta_z_ = -0.28;
 
 
% theta_x_0 = -3.04; 
% theta_y_0 = -0.65; 
% theta_z_0 = -0.02;