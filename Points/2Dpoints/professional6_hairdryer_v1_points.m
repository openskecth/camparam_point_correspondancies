hairdryer_3D;
object_id= 'hairdryer';


figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
view = 'view1';
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m = ...   
[ 320.2368  556.5863    1.0000
  322.2832  594.4432    1.0000
   78.7716  487.0116    1.0000
   80.8179  452.2242    1.0000
  310.0053  292.6116    1.0000
  321.2600  576.0263    1.0000
  687.5505  281.3568    1.0000
  684.4811   35.7989    1.0000
  804.1905  215.8747    1.0000
  573.9800  122.7674    1.0000
  652.7632  312.0516    1.0000
  604.6747  356.0474    1.0000
  395.9505  234.2916    1.0000
  349.9084  261.9168    1.0000
  564.7716  889.1126    1.0000
  521.7989  934.1316    1.0000
  458.3632  895.2516    1.0000
  501.3358  849.2095    1.0000
];



indices_all = [1:18];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);










