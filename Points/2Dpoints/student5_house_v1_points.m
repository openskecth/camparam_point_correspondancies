figure(3);

designer_type = 'students';
designer_id ='student5';
object_id = 'house';
view = 'view1';


global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);

x_m(1,:) = [446 150  1];
x_m(2,:) = [613 205 1];
x_m(3,:) = [490 350 1];
x_m(4,:) = [492 405  1];
x_m(5,:) = [570 430  1];
x_m(6,:) = [495 485  1];
x_m(7,:) = [499 696  1];
x_m(8,:) = [571 699  1];
x_m(9,:) = [672 650  1];
x_m(10,:) = [722 598  1];
x_m(11,:) = [726 421  1];

x_m(12,:) = [669 396  1];
x_m(13,:) = [728 345  1];
x_m(14,:) = [730 267  1];
x_m(15,:) = [544 206  1];

% x_m(16,:) = [640 343  1];

% x_m(18,:) = [642 380  1];
% x_m(19,:) = [639 596  1];
x_m(21,:) = [404 580  1];
x_m(22,:) = [333 574  1];
x_m(23,:) = [324 385  1];
x_m(24,:) = [395 341  1];
x_m(25,:) = [321 315  1];
x_m(26,:) = [318 250  1];


x_m(27,:) = [492 238  1];
x_m(28,:) = [500 731  1];
x_m(29,:) = [723 625  1];
x_m(30,:) = [544 532  1];
x_m(31,:) = [543 133  1];
x_m(32,:) = [433 143  1];
x_m(32,:) = [730 172  1];
x_m(33,:) = [323 171  1];
x_m(34,:) = [335 604  1];

indices_all = [1:15 21:26];
% indices_opt = [27:34];
indices_opt = indices_all;



x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')