x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional1';
view = 'view2';
global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

% [x,y] = ginput(20);

x_m(1,:) = [148.00 241.00 1.00];
x_m(2,:) = [148.00 256.00 1.00];
x_m(3,:) = [405.00 271.00 1.00];
x_m(4,:) = [404.00 256.00 1.00];
x_m(5,:) = [573.00 230.00 1.00];
x_m(6,:) = [576.00 251.00 1.00];
x_m(7,:) = [303.00 222.00 1.00];
x_m(8,:) = [299.00 200.00 1.00];
x_m(9,:) = [148.00 316.00 1.00];
x_m(10,:) = [86.00 420.00 1.00];
x_m(11,:) = [86.00 436.00 1.00];

x_m(20,:) = [553.00 449.00 1.00];
x_m(19,:) = [549.00 432.00 1.00];
x_m(18,:) = [496.00 308.00 1.00];

x_m(15,:) = [630.00 418.00 1.00];
x_m(16,:) = [625.00 401.00 1.00];
x_m(17,:) = [561.00 313.00 1.00];

x_m(12,:) = [359.00 320.00 1.00];
x_m(13,:) = [359.00 393.00 1.00];
x_m(14,:) = [358.00 407.00 1.00];

x = x_m(:,1);
y = x_m(:,2);



% indices_all = [1:2 5:17];
indices_all = [1:2 5:11 15:20];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);






