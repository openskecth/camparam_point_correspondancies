figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
view = 'view2';


num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m(1,:) = [323.25 701.58 1.00];
x_m(2,:) = [292.22 521.46 1.00];
x_m(3,:) = [430.11 698.13 1.00];
x_m(4,:) = [200.01 540.42 1.00];
x_m(5,:) = [445.62 224.14 1.00];
x_m(6,:) = [631.77 285.33 1.00];
x_m(7,:) = [547.32 349.10 1.00];
x_m(8,:) = [521.46 177.60 1.00];
x_m(9,:) = [268.95 274.12 1.00];
x_m(10,:) = [413.74 341.34 1.00];
x_m(11,:) = [352.55 375.82 1.00];
x_m(12,:) = [323.25 246.55 1.00];
x_m(13,:) = [431.00 262.00 1.00];
x_m(14,:) = [500.78 22.48 1.00];
x_m(15,:) = [435.28 308.60 1.00];
x_m(16,:) = [524.91 193.98 1.00];





indices_all = [1:16];
indices_opt = indices_all;



x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);





