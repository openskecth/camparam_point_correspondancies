figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
view = 'view1';


num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);


x_m = ...
    [ 616.5637  649.8025    1.0000
  621.1483  546.6476    1.0000
  616.5637  492.7779    1.0000
  599.3712  471.0007    1.0000
  537.4783  414.8386    1.0000
  427.4465  717.4262    1.0000
  648.6563  524.8705    1.0000
  305.9530  228.0137    1.0000
   92.7663  292.1990    1.0000
  511.1165  276.1527    1.0000
  303.6606  383.8922    1.0000
  112.2511  349.5072    1.0000
  119.1281  528.3090    1.0000
  368.9920  242.9139    1.0000
  364.4074  399.9385    1.0000
   66.4045  491.6317    1.0000
  310.5376  374.7229    1.0000
  287.6143  636.0485    1.0000
  515.7012  481.3162    1.0000];


% x =  x_m(:,1);
% y =  x_m(:,2);
% plot(x,y, 'o-');
% a = [1:num_points]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c);


indices_all = [1:19];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);





