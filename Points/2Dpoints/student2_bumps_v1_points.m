

figure(3);
designer_type = 'students';
designer_id ='student2';
view = 'view1';
global folder_sketches;
folder_students = fullfile(folder_sketches, 'students_clean\');
folder = fullfile(folder_students, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
img = img(:,width:-1:1,:); 
imshow(img)
hold on;

% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end

x_m(1,:) = [691 436  1];
x_m(2,:) = [688 466  1];

x_m(3,:) = [271 456  1];
x_m(4,:) = [271 432  1];

x_m(9,:) = [706 450  1];
x_m(10,:) = [825 551  1];
x_m(11,:) = [825 576  1];

x_m(14,:) = [481 527  1];
x_m(13,:) = [479 701  1];
x_m(12,:) = [479 727  1];

x_m(17,:) = [240 432  1];
x_m(16,:) = [160 536  1];
x_m(15,:) = [156 557  1];


x_m(18,:) = [501 389  1];
x_m(19,:) = [502 453  1];


indices_all = [1:4 9:19];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);

% bounding_box = read_sketch_pixel_coordinates(fullfile(folder, [view '_concept.json']));
% hold on;
% plot([bounding_box(1) bounding_box(2) bounding_box(2) bounding_box(1) bounding_box(1)],...
% [bounding_box(3) bounding_box(3) bounding_box(4) bounding_box(4) bounding_box(3)]);


