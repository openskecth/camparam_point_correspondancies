figure(3);
designer_type = 'students';
designer_id ='student3';
object_id = 'mixer';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [143.28 136.24 1.00];
x_m(2,:) = [337.39 295.19 1.00];
x_m(3,:) = [42.00 250.18 1.00];
x_m(4,:) = [420.39 179.85 1.00];
x_m(5,:) = [322.00 452.00 1.00];
x_m(6,:) = [178.00 496.00 1.00];
x_m(7,:) = [307.04 518.07 1.00];
x_m(8,:) = [223.00 441.11 1.00];
x_m(9,:) = [201.00 420.00 1.00];
x_m(10,:) = [202.00 427.00 1.00];
x_m(11,:) = [624.35 818.47 1.00];
x_m(12,:) = [537.14 137.65 1.00];
x_m(13,:) = [247.00 29.00 1.00];
x_m(14,:) = [167.19 47.62 1.00];
x_m(15,:) = [167.19 96.86 1.00];
x_m(16,:) = [147.50 79.98 1.00];
x_m(17,:) = [181.00 69.00 1.00];
x_m(18,:) = [316.00 190.00 1.00];
x_m(19,:) = [163.00 219.00 1.00];
x_m(20,:) = [593.41 272.69 1.00];
x_m(21,:) = [285.60 774.90 1.00];
x_m(22,:) = [593.30 601.50 1.00];
x_m(23,:) = [109.00 563.00 1.00];
x_m(24,:) = [410.00 452.00 1.00];
x_m(25,:) = [609.00 322.00 1.00];
x_m(26,:) = [414.00 381.00 1.00];
indices_all = [1:15 18:26];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










