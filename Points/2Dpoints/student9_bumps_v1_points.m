x_m = zeros(size(H,1), 3);
% x_m(5,:) = [699 434  1];
% x_m(6,:) = [703 459  1];
% x_m(8,:) = [283 436  1];
% x_m(7,:) = [281 463  1];
% 
% x_m(9,:) = [267 450  1];
% x_m(10,:) = [149 552  1];
% x_m(11,:) = [144 576  1];
% 
% x_m(17,:) = [731 434  1];
% x_m(16,:) = [814 537  1];
% x_m(15,:) = [814 561  1];
% 
% x_m(18,:) = [488 525  1];
% x_m(19,:) = [486 700  1];
% x_m(20,:) = [486 727  1];
% 
% x_m(14,:) = [485 390  1];
% x_m(13,:) = [485 454  1];


indices_all = [1:4 9:19];
indices_opt = indices_all;


figure(3);
designer_type = 'students';
designer_id ='student9';
view = 'view1';
global folder_sketches;
folder_students = fullfile(folder_sketches, 'students_clean\');
folder = fullfile(folder_students, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

% [x,y] = ginput(20);

x_m(1,:) = [700.00 582.00 1.00];
x_m(2,:) = [701.00 619.00 1.00];
x_m(3,:) = [272.00 564.00 1.00];
x_m(4,:) = [272.00 538.00 1.00];
x_m(5,:) = [373.00 403.00 1.00];
x_m(6,:) = [372.00 428.00 1.00];
x_m(7,:) = [698.00 447.00 1.00];
x_m(8,:) = [696.00 427.00 1.00];
x_m(9,:) = [745.00 587.00 1.00];
x_m(10,:) = [837.00 673.00 1.00];
x_m(11,:) = [837.00 692.00 1.00];
x_m(12,:) = [432.00 872.00 1.00];
x_m(13,:) = [433.00 848.00 1.00];
x_m(14,:) = [465.00 686.00 1.00];
x_m(15,:) = [182.00 630.00 1.00];
x_m(16,:) = [185.00 607.00 1.00];
x_m(17,:) = [284.00 544.00 1.00];
x_m(18,:) = [542.00 472.00 1.00];
x_m(19,:) = [538.00 501.00 1.00];
x_m(20,:) = [539.00 517.00 1.00];


indices_all = [1:2 4 5 8 9 10 12 13 14 16 17 18];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y,'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);








