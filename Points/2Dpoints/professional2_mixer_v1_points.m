mixer_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional2';
object_id = 'mixer';
view = 'view1';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [651.94 326.02 1.00];
x_m(2,:) = [353.72 280.39 1.00];
x_m(3,:) = [605.00 264.00 1.00];
x_m(4,:) = [402.00 339.00 1.00];
x_m(5,:) = [464.54 624.24 1.00];
x_m(6,:) = [560.69 563.94 1.00];
x_m(7,:) = [426.24 567.20 1.00];
x_m(8,:) = [545.20 609.57 1.00];
x_m(9,:) = [591.65 656.02 1.00];
x_m(10,:) = [612.83 690.24 1.00];
x_m(11,:) = [201.35 486.54 1.00];
x_m(12,:) = [309.72 30.24 1.00];
x_m(13,:) = [506.09 65.28 1.00];
x_m(14,:) = [613.65 163.06 1.00];
x_m(15,:) = [614.46 210.31 1.00];
x_m(16,:) = [626.69 183.43 1.00];
x_m(17,:) = [599.80 189.94 1.00];
x_m(18,:) = [449.87 298.31 1.00];
x_m(19,:) = [570.46 256.76 1.00];
x_m(20,:) = [206.00 108.00 1.00];
x_m(21,:) = [405.00 483.00 1.00];
x_m(22,:) = [167.94 594.09 1.00];
x_m(24,:) = [357.80 706.54 1.00];
x_m(23,:) = [623.43 558.24 1.00];
x_m(25,:) = [228.24 202.98 1.00];
x_m(26,:) = [370.02 170.39 1.00];
indices_all = [1:26];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










