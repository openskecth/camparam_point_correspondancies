x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
view = 'view2';

global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

% [x,y] = ginput(20);

x_m(1,:) = [530.72 516.06 1.00];
x_m(2,:) = [531.70 545.38 1.00];
x_m(3,:) = [217.96 493.58 1.00];
x_m(4,:) = [218.94 467.19 1.00];
x_m(5,:) = [319.61 359.68 1.00];
x_m(6,:) = [319.61 377.27 1.00];
x_m(7,:) = [532.68 396.82 1.00];
x_m(8,:) = [532.68 380.21 1.00];
x_m(9,:) = [597.18 502.38 1.00];
x_m(10,:) = [650.94 595.23 1.00];
x_m(11,:) = [652.89 614.77 1.00];
x_m(12,:) = [351.86 744.76 1.00];
x_m(13,:) = [355.77 722.28 1.00];
x_m(14,:) = [367.50 590.34 1.00];
x_m(15,:) = [173.01 514.11 1.00];
x_m(16,:) = [171.05 491.63 1.00];
x_m(17,:) = [230.67 430.05 1.00];
x_m(18,:) = [432.98 391.94 1.00];
x_m(19,:) = [430.05 431.03 1.00];
x_m(20,:) = [427.12 442.76 1.00];

x = x_m(:,1);
y = x_m(:,2);



indices_all = [1:20];
indices_opt = indices_all;

a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')






