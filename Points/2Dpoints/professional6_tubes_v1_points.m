x_m = ones(size(H,1), 3);

figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
view = 'view1';

num_points = length(H);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m(1,:) = [198.48 856.37 1.00];
x_m(2,:) = [198.48 568.86 1.00];
x_m(3,:) = [306.94 767.36 1.00];
x_m(4,:) = [85.93 666.06 1.00];
x_m(5,:) = [505.00 486.00 1.00];
x_m(6,:) = [700.85 556.59 1.00];
x_m(7,:) = [599.56 639.46 1.00];
x_m(8,:) = [611.84 405.16 1.00];
x_m(9,:) = [220.99 299.77 1.00];
x_m(10,:) = [362.19 370.37 1.00];
x_m(11,:) = [293.63 428.69 1.00];
x_m(12,:) = [296.70 235.31 1.00];
x_m(13,:) = [451.20 363.21 1.00];
x_m(14,:) = [575.00 140.16 1.00];
x_m(15,:) = [441.99 476.78 1.00];
x_m(16,:) = [588.00 417.00 1.00];





indices_all = [1:16];
indices_opt = indices_all;


x =  x_m(:,1);
y =  x_m(:,2);
plot(x(indices_all),y(indices_all), 'o-');
a = indices_all'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c);






