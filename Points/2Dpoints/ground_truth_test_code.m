
(focal_mm / sensor_width_mm) * image_width_in_pixels

K = [5/22*500  0           120 0; 
     0          7/22*500   200 0;
     0          0           1 0];

theta_z =  180/180*pi;
R_z = [cos(theta_z) -sin(theta_z) 0;
       sin(theta_z)  cos(theta_z) 0;
        0 0 1];

    
theta_y =  0/180*pi;  
R_y = [cos(theta_y)   0 sin(theta_y);
       0            1 0;
       -sin(theta_y)  0 cos(theta_y)];
   
theta_x =  0/180*pi;      
R_x = [ 1 0 0; 
        0 cos(theta_x) -sin(theta_x);
        0 sin(theta_x)  cos(theta_x)];
    


R =  R_z*R_y*R_x;

C = [4; -2; 2];


T = [R -R*C; zeros(1,3) 1];

x = zeros(size(H,1), 3);


for i = 1:size(H,1)
   x(i, :)  = (K*T*H(i, :)')'; %change the dimensionality
   x(i, :) = x(i, :)/x(i, 3);    
end



indices = 1:26;