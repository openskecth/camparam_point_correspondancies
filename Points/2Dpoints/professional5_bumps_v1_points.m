x_m = zeros(size(H,1), 3);


figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
view = 'view1';

global folder_sketches;
folder_designers = fullfile(folder_sketches,[designer_type '_clean\']);
folder = fullfile(folder_designers, designer_id, 'bumps\drawings_bumps\');
%  [img, ~, t] = imread(fullfile(folder, 'view1_concept.png'));
[img, ~, t] =  imread(fullfile(folder, 'view1_concept_opaque.png'));
% imshow(255 - img)
height  = size(img,2);
width   = size(img,1);
% img = img(:,width:-1:1,:); 
% t = t(:, width:-1:1);
% imwrite(img, fullfile(folder, 'view1_concept_mirrored.png'), 'Alpha', t);
% for i = 1:length(x_m)
%     x_m(i, 1) = width - x_m(i, 1);
% end


imshow(img)
hold on;

%    [x,y] = ginput(20);

x = [645.0730  644.0957  270.7418  268.7870  309.8364  307.8817  598.1595  598.1595  683.1903  755.5154  753.5607  439.8261 438.8488  451.5545  153.4578  153.4578  235.5566  451.5545  452.5319  452.5319];
y = [507.2644  542.4496  540.4949  512.1512  421.2562  439.8261  442.7582  422.2335  514.1060  607.9331  628.4578  809.2706 785.8138  608.9105  620.6389  599.1368  503.3549  454.4866  493.5813  508.2418];

indices_all = [1:20];
indices_opt = indices_all;

a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x, y, c);

x_m(indices_all,1) = x;
x_m(indices_all,2) = y;
x_m(indices_all,3) = 1.0;








