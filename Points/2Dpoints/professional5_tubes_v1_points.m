figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
view='view1';


num_points = length(H);
x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m = ...
    [  559.0000  703.0000    1.0000
  561.0000  512.0000    1.0000
  645.0000  570.0000    1.0000
  477.0000  653.0000    1.0000
  203.0000  485.0000    1.0000
  362.0000  448.0000    1.0000
  283.0000  558.0000    1.0000
  280.0000  380.0000    1.0000
  449.0000  383.0000    1.0000
  568.0000  343.0000    1.0000
  504.0000  443.0000    1.0000
  507.0000  288.0000    1.0000
  392.0000  390.0000    1.0000
  297.0000  216.0000    1.0000
  394.0000  434.0000    1.0000
  294.0000  391.0000    1.0000];





indices_all = [1:16];
indices_opt = indices_all;


x =  x_m(:,1);
y =  x_m(:,2);
plot(x(indices_all),y(indices_all), 'o-');
a = indices_all'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(indices_all), y(indices_all), c);






