figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view2';


num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m(1,:) = [308.75 740.75 1.00];
x_m(2,:) = [302.75 670.25 1.00];
x_m(3,:) = [316.25 632.75 1.00];
x_m(4,:) = [374.75 595.25 1.00];
x_m(5,:) = [454.25 517.25 1.00];
x_m(6,:) = [316.00 691.00 1.00];
x_m(7,:) = [565.25 799.25 1.00];
x_m(8,:) = [934.25 446.75 1.00];
x_m(9,:) = [664.25 419.75 1.00];
x_m(10,:) = [748.25 490.25 1.00];
x_m(11,:) = [461.75 437.75 1.00];
x_m(12,:) = [599.75 437.75 1.00];
x_m(13,:) = [602.75 614.75 1.00];
x_m(14,:) = [944.75 499.25 1.00];
x_m(15,:) = [947.75 721.25 1.00];
x_m(16,:) = [662.75 578.75 1.00];
x_m(17,:) = [998.75 652.25 1.00];
x_m(18,:) = [466.00 631.00 1.00];
x_m(19,:) = [790.25 724.25 1.00];


indices_all = [1:19];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);





