figure(3);

designer_type = 'students';
designer_id ='student9';
object_id = 'house';
view = 'view2';


global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img);
hold on;

x_m = ones(size(H,1), 3);
% x_m(:,1:2) = ginput(length(x_m));

x_m(1,:) = [560.00 265.00 1.00];
x_m(2,:) = [394.00 175.00 1.00];
x_m(3,:) = [310.00 351.00 1.00];
x_m(4,:) = [308.00 448.00 1.00];
x_m(5,:) = [350.00 478.00 1.00];
x_m(6,:) = [307.00 515.00 1.00];
x_m(7,:) = [306.00 711.00 1.00];
x_m(8,:) = [365.00 754.00 1.00];
x_m(9,:) = [445.00 768.00 1.00];
x_m(10,:) = [499.00 744.00 1.00];
x_m(11,:) = [501.00 500.00 1.00];
x_m(12,:) = [450.00 467.00 1.00];
x_m(13,:) = [502.00 434.00 1.00];
x_m(14,:) = [500.00 301.00 1.00];
x_m(15,:) = [655.00 365.00 1.00];
x_m(16,:) = [652.00 467.00 1.00];
x_m(17,:) = [595.00 490.00 1.00];
x_m(18,:) = [649.00 514.00 1.00];
x_m(19,:) = [649.00 716.00 1.00];
x_m(20,:) = [611.00 735.00 1.00];
x_m(21,:) = [504.00 722.00 1.00];
x_m(22,:) = [480.00 695.00 1.00];
x_m(23,:) = [486.00 523.00 1.00];
x_m(24,:) = [515.00 491.00 1.00];
x_m(25,:) = [489.00 464.00 1.00];
x_m(26,:) = [490.00 390.00 1.00];
x_m(27,:) = [310.00 209.00 1.00];
x_m(28,:) = [306.00 739.00 1.00];
x_m(29,:) = [497.00 782.00 1.00];
x_m(30,:) = [649.00 741.00 1.00];
x_m(31,:) = [661.00 234.00 1.00];
x_m(32,:) = [510.00 128.00 1.00];
x_m(33,:) = [489.00 283.00 1.00];
x_m(34,:) = [480.00 720.00 1.00];



indices_all = [1:26];

% indices_opt = [27:34];
indices_opt = indices_all;
x = x_m(:,1);
y = x_m(:,2);


a = [indices_all]'; b = num2str(a); c = cellstr(b);
t = text(x(indices_all), y(indices_all), c, 'Color', 'r');
plot(x(indices_all), y(indices_all), '-*')