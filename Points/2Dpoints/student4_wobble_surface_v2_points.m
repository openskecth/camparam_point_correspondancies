designer_type = 'students';
designer_id ='student4';
object_id = 'wobble_surface';
view = 'view2';



x_m = ones(size(H,1), 3);
% [x_m(:,1), x_m(:,2)] = ginput(17);

x_m(1,:) = [452.00 704.00 1.00];
x_m(2,:) = [132.00 571.00 1.00];
x_m(3,:) = [301.00 549.00 1.00];
x_m(4,:) = [819.00 616.00 1.00];
x_m(5,:) = [793.00 626.00 1.00];
x_m(6,:) = [685.00 605.00 1.00];
x_m(7,:) = [552.00 693.00 1.00];
x_m(8,:) = [425.00 610.00 1.00];
x_m(9,:) = [321.00 587.00 1.00];
x_m(10,:) = [250.00 569.00 1.00];
x_m(11,:) = [83.00 588.00 1.00];
x_m(12,:) = [357.00 540.00 1.00];
x_m(13,:) = [131.00 574.00 1.00];
x_m(14,:) = [300.00 553.00 1.00];
x_m(15,:) = [816.00 619.00 1.00];
x_m(16,:) = [455.00 710.00 1.00];
x_m(17,:) = [682.00 611.00 1.00];

indices_all = [1:10 13:17];
% indices_all = [1:10];
indices_opt = indices_all;

