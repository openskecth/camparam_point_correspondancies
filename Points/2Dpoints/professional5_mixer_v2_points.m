figure(3);
designer_type = 'professionals';
designer_id ='Professional5';
object_id = 'mixer';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [748.00 504.00 1.00];
x_m(2,:) = [467.00 471.00 1.00];
x_m(3,:) = [640.00 443.00 1.00];
x_m(4,:) = [558.00 538.00 1.00];
x_m(5,:) = [581.00 731.00 1.00];
x_m(6,:) = [623.00 689.00 1.00];
x_m(7,:) = [563.00 699.00 1.00];
x_m(8,:) = [657.00 717.00 1.00];
x_m(9,:) = [711.00 721.00 1.00];
x_m(10,:) = [734.00 744.00 1.00];
x_m(11,:) = [296.00 661.00 1.00];
x_m(12,:) = [384.00 262.00 1.00];
x_m(13,:) = [602.00 275.00 1.00];
x_m(14,:) = [684.00 332.00 1.00];
x_m(15,:) = [682.00 367.00 1.00];
x_m(16,:) = [691.00 340.00 1.00];
x_m(17,:) = [678.00 358.00 1.00];
x_m(18,:) = [578.00 465.00 1.00];
x_m(19,:) = [623.00 420.00 1.00];
x_m(20,:) = [316.00 321.00 1.00];
x_m(21,:) = [463.00 607.00 1.00];
x_m(22,:) = [338.00 770.00 1.00];
indices_all = [1:22];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = 1:22;
plot(x(ing_p),y(ing_p), 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










