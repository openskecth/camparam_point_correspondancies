figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view1';


num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points);
x_m = 1.0e+03 *...
    [  0.4138    0.7077    0.0010
    0.4153    0.6282    0.0010
    0.4407    0.5952    0.0010
    0.4512    0.5772    0.0010
    0.5248    0.4992    0.0010
    0.4090    0.5750    0.0010
    0.6490    0.7930    0.0010
    0.9957    0.3507    0.0010
    0.8232    0.3072    0.0010
    0.7408    0.4767    0.0010
    0.5218    0.3912    0.0010
    0.7768    0.3252    0.0010
    0.7798    0.5067    0.0010
    0.9928    0.4167    0.0010
    0.9912    0.6102    0.0010
    0.8290    0.3970    0.0010
    1.0153    0.5397    0.0010
    0.6837    0.5247    0.0010
    0.7632    0.7122    0.0010];


indices_all = [1:5 7:11  14 17 19];
indices_opt = indices_all;

x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c);





