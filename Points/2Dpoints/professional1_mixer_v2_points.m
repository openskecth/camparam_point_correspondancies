figure(3);
designer_type = 'professionals';
designer_id ='Professional1';
object_id = 'mixer';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [116.06 307.00 1.00];
x_m(2,:) = [345.00 315.00 1.00];
x_m(3,:) = [344.00 303.00 1.00];
x_m(4,:) = [116.00 319.00 1.00];
x_m(5,:) = [218.00 523.00 1.00];
x_m(6,:) = [268.00 528.04 1.00];
x_m(7,:) = [290.60 522.68 1.00];
x_m(8,:) = [189.00 528.00 1.00];
x_m(9,:) = [153.62 529.66 1.00];
x_m(10,:) = [143.15 551.47 1.00];
x_m(11,:) = [536.64 519.19 1.00];
x_m(12,:) = [479.05 122.21 1.00];
x_m(13,:) = [199.86 91.67 1.00];
x_m(14,:) = [133.55 129.19 1.00];
x_m(15,:) = [135.30 174.56 1.00];
x_m(16,:) = [156.24 144.02 1.00];
x_m(17,:) = [115.23 153.62 1.00];
x_m(18,:) = [170.00 264.00 1.00];
x_m(19,:) = [278.00 257.00 1.00];
x_m(20,:) = [545.36 194.63 1.00];
x_m(21,:) = [524.42 532.27 1.00];
x_m(22,:) = [399.66 521.80 1.00];
x_m(24,:) = [162.34 527.04 1.00];
x_m(23,:) = [325.50 559.32 1.00];
indices_all = [1:24];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










