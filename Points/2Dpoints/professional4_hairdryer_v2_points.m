figure(3);
designer_type = 'professionals';
designer_id ='Professional4';
view = 'view2';
global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, 'hairdryer\drawings_hairdryer\');
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));
height  = size(img,2);
width   = size(img,1);
imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [454.50 635.83 1.00];
x_m(2,:) = [454.50 655.83 1.00];
x_m(3,:) = [319.83 649.17 1.00];
x_m(4,:) = [321.17 627.83 1.00];
x_m(5,:) = [537.17 555.83 1.00];
x_m(6,:) = [534.50 703.83 1.00];
x_m(7,:) = [914.50 649.17 1.00];
x_m(8,:) = [909.17 546.50 1.00];
x_m(9,:) = [954.50 598.50 1.00];
x_m(10,:) = [866.50 594.50 1.00];
x_m(11,:) = [830.50 642.50 1.00];
x_m(12,:) = [773.17 649.17 1.00];
x_m(13,:) = [643.83 625.17 1.00];
x_m(14,:) = [589.17 631.83 1.00];
x_m(15,:) = [765.17 873.17 1.00];
x_m(16,:) = [687.83 901.17 1.00];
x_m(17,:) = [614.50 859.83 1.00];
x_m(18,:) = [681.17 837.17 1.00];

indices_all = [1:18];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
plot(x,y, 'o-');
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r', 'FontSize', 12);










