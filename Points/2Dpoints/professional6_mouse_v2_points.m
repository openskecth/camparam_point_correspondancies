mouse_3D;
figure(3);
designer_type = 'professionals';
designer_id ='Professional6';
object_id = 'mouse';
view = 'view2';

global folder_sketches;
folder_students = fullfile(folder_sketches, [designer_type '_clean\']);
folder = fullfile(folder_students, designer_id, [object_id '\drawings_' object_id '\']);
[img, ~, t] =  imread(fullfile(folder, [view '_concept_opaque.png']));

height  = size(img,2);
width   = size(img,1);


imshow(img)
hold on;

num_points = length(H);
% x_m = ones(size(H,1), 3);
% [x_m(:,1),x_m(:,2)] = ginput(num_points );

x_m(1,:) = [103.89 459.77 1.00];
x_m(2,:) = [106.70 659.52 1.00];
x_m(3,:) = [596.22 627.17 1.00];
x_m(4,:) = [597.63 77.16 1.00];
x_m(5,:) = [590.59 576.53 1.00];
x_m(6,:) = [445.71 579.34 1.00];
x_m(7,:) = [534.33 653.89 1.00];
x_m(8,:) = [525.89 490.72 1.00];
x_m(9,:) = [102.48 562.46 1.00];
x_m(10,:) = [594.81 345.83 1.00];
x_m(11,:) = [690.47 784.71 1.00];
x_m(12,:) = [694.69 302.23 1.00];
indices_all = [1:12];
indices_opt = indices_all;


x =  x_m(indices_all,1);
y =  x_m(indices_all,2);
ing_p = indices_all;
plot(x,y, 'o-', 'color', 'b');


a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x, y, c, 'color', 'r');










