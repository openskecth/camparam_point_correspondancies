addpath('./Points');
addpath('./Points/3DPoints/');
addpath('./Points/2DPoints/');

object_id = 'mouse';

[H, folder_data] = mouse_3D();
% H = scale_center_mesh(H, folder_data, object_id);
filepath_mat = fullfile(folder_data, [object_id '.mat']);
centroid = [0.0 0.0 0.0 0.0];
scale = 1.0;
save(filepath_mat, 'scale', 'centroid');

scripts_launch = [
    {'student6_mouse_v1_points'}, ... 
    {'student6_mouse_v2_points'}, ...
    {'student8_mouse_v1_points'}, ... 
    {'student8_mouse_v2_points'}, ...
    {'professional1_mouse_v1_points'}, ... 
    {'professional1_mouse_v2_points'}, ...    
    {'professional2_mouse_v1_points'}, ... 
    {'professional2_mouse_v2_points'}, ...    
    {'professional3_mouse_v1_points'}, ... 
    {'professional3_mouse_v2_points'}, ...
    {'professional4_mouse_v1_points'}, ... 
    {'professional4_mouse_v2_points'}, ...
    {'professional5_mouse_v1_points'}, ... 
    {'professional5_mouse_v2_points'}, ...
    {'professional6_mouse_v1_points'}, ... 
    {'professional6_mouse_v2_points'}, ...
    ];

for i = 1:2:length(scripts_launch)
    close all;
    clearvars -except scripts_launch i object_id H folder_centered_sketches
    eval(scripts_launch{i});
    
   figure(1);
    imshow(img)
    hold on;
    x =  x_m(indices_opt,1);
    y =  x_m(indices_opt,2);
    plot(x,y, 'o-');
    a = [indices_opt]'; b = num2str(a); c = cellstr(b);
    clear text; t = text(x, y, c);

    [img, scale, bottom, left, dx, dy] = scale_center_sketch(folder, view, img);
    imwrite(img, fullfile(folder_centered_sketches, [designer_id '_' object_id '_' view '.png']));
    x_m(indices_opt,1) = (x_m(indices_opt,1) - double(left))*scale + double(dx);
    x_m(indices_opt,2) = (x_m(indices_opt,2) - double(bottom))*scale + double(dy);

    figure(2);
    imshow(img)
    hold on;
    x =  x_m(indices_opt,1);
    y =  x_m(indices_opt,2);
    plot(x,y, 'o-');
    a = [indices_opt]'; b = num2str(a); c = cellstr(b);
    clear text; t = text(x, y, c);
    
    estimate_camera_parameters_from_3D_to_2D_correspondancies;
end




