clear all
folder_file = fileparts(which(mfilename)); 
folderOld = cd(folder_file);

addpath('./3DPoints/');
addpath('./2DPoints/');

folder_3D_points = './3DPoints';
folder_2D_points = './2DPoints';
folder_save = './correspondanciesJSON';
if ~exist(folder_save,'dir')
   mkdir(folder_save);
end

objects = objectsData();
global folder_sketches;
folder_sketches = 'D:\Projects\SketchItProject\CollectedSketches\SKETCHES';

for i = 1:length(objects)
    [points_3D, ~] = eval([objects(i).name '_3D']);
    H = points_3D;
    
    files2D = dir(fullfile(folder_2D_points, ['*' objects(i).name '*']));
    files2D = {files2D.name};
    
    for j = 1:length(files2D)
        file_points_2D = strrep(files2D{j}, '.m', '');
        eval(file_points_2D);
        
        correspondencies.points_2D_sketch = x_m(indices_opt, 1:2);
        correspondencies.points_3D_object = points_3D(indices_opt, 1:3);
        
        text_json = jsonencode(correspondencies);%table(points_2D_sketch,points_3D_object));
        filepath = fullfile(folder_save, strrep(files2D{j}, '.m', '.json'));
        fid = fopen(filepath,'w');
        fwrite(fid,text_json);
        fclose(fid);
    end
    

end


cd(folderOld);