% X - 3D points
% x - 2D image points n x 3
% P - camera matrix
% A - 2n x 12 matrix
% n - point correspondancies

function [P, error] = dlt(X, x)

    n = size(x,1); % number of point correspondancies
    
    %Normilize 2d points:
    [x, T] = normalise2dpts(x');
    x = x';
    
    %todo: normilize 3d points:
       
    A = zeros(2*n, 12);
   
    for i = 1:n
        j = 2*(i-1)+1;
        
        A(j,5:8)    =  -x(i,3)*X(i,:);
        A(j,9:12)   =   x(i,2)*X(i,:);
        
        A(j+1,1:4)  =   x(i,3)*X(i,:);
        A(j+1,9:12) =  -x(i,1)*X(i,:);
    end
    
    [U,S,V] = svd(A);
    
    
 
    p = V(:,end);
    P = reshape(V(:,end),4,3)';
    P = inv(T)*P;
%     P = P / norm(P);% * sign(P(1,1));
    
    e = zeros(2*n, 1);
    for i = 1:n
        j = 2*(i-1)+1;
        e(j:j+1, :) = A(j:j+1, :)*p;
    end
    error = norm(e);

end

