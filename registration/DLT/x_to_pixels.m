function x = x_to_pixels(x, width)
    x(:,2) = x(:,2)*-1;
    x(:,1:2) = (x(:,1:2)+1.0)/2.0*width;
end