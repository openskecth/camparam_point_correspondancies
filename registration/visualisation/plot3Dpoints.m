%%Plot 3d points
function plot3Dpoints(H, camera_parameters, indices_all)
figure(12);

plot3(H(indices_all,1), H(indices_all,2), H(indices_all,3));
camup([0 1 0])
xlabel('x')
ylabel('y')
zlabel('z')
hold on;
axis equal
grid on


% T = [R -R*C; zeros(1,3) 1];

% Plot camera position:
C = camera_parameters.C;

E = camera_parameters.focal_point;
plot3([C(1) E(1)], [C(2) E(2)], [C(3) E(3)], 'x-');

U = camera_parameters.up;
U = U + C;
plot3([C(1) U(1)], [C(2) U(2)], [C(3) U(3)], 'o-');

end
% plot3(E(1), E(2), E(3), 'o');

% plot3([0 R(1,1)]+C(1), [0 R(2,1)]+C(2), [0 R(3,1)]+C(3), 'r');
% plot3([0 R(1,2)]+C(1), [0 R(2,2)]+C(2), [0 R(3,2)]+C(3), 'g');
% plot3([0 R(1,3)]+C(1), [0 R(2,3)]+C(2), [0 R(3,3)]+C(3), 'b');
% 
% 
% 
% plot3(C_init(1), C_init(2), C_init(3), 'o');
% 
% plot3([0 R_init(1,1)]+C_init(1), [0 R_init(2,1)]+C_init(2), [0 R_init(3,1)]+C_init(3), 'r:');
% plot3([0 R_init(1,2)]+C_init(1), [0 R_init(2,2)]+C_init(2), [0 R_init(3,2)]+C_init(3), 'g:');
% plot3([0 R_init(1,3)]+C_init(1), [0 R_init(2,3)]+C_init(2), [0 R_init(3,3)]+C_init(3), 'b:');
% 
% hold off;