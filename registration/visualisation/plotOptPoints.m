function plotOptPoints(x, X, T_x, ...
    t_general, ...
    t_init, t_opt,...
    width, ...
    designer_id, ...
    folder_save, view)

h = figure(3);
hold on;

%Set parameters:
line_width = 2;

%% Plot the general solution:
[x_general, error_geometric_general_median, ...
    error_geometric_general_mean] = computeProjectionGeneral(t_general, x, X);

% Convert to pixel coordiantes:
x_general =  T_x\x_general';
x_general = x_general';
x_general = x_to_pixels(x_general, width);

plot(x_general(:,1), x_general(:,2), 'o', 'LineWidth', line_width);
l_general = sprintf('general, dist.median = %.3f, dist.mean = %.3f', ...
    error_geometric_general_median, error_geometric_general_mean);

%% Plot the initial approxiamtion for restricted case:
[x_restricted_init, error_geometric_restricted_init_median, ...
    error_geometric_restricted_init_mean] = computeProjectionRestricted(t_init, x, X);


x_restricted_init =  T_x\x_restricted_init';
x_restricted_init = x_restricted_init';
x_restricted_init = x_to_pixels(x_restricted_init, width);


plot(x_restricted_init(:,1), x_restricted_init(:,2), '*', 'LineWidth', line_width);
l_restricted_init = sprintf('restricted init, dist.median = %.3f, dist.mean = %.3f', ...
    error_geometric_restricted_init_median, error_geometric_restricted_init_mean);



%% Plot the optimal for restricted case:
[x_restricted_opt, error_geometric_restricted_opt_median, error_geometric_restricted_opt_mean] = ...
    computeProjectionRestricted(t_opt, x, X);

x_restricted_opt =  T_x\x_restricted_opt';
x_restricted_opt = x_restricted_opt';

x_restricted_opt = x_to_pixels(x_restricted_opt, width);

plot(x_restricted_opt(:,1), x_restricted_opt(:,2), 'd', 'LineWidth', line_width);
l_restricted_opt = sprintf('restricted opt, dist.median = %.3f, dist.mean = %.3f', ...
    error_geometric_restricted_opt_median, error_geometric_restricted_opt_mean);


%% Plot the marked points:

x = T_x\x';
x = x';
x = x_to_pixels(x, width);

l_marked = sprintf('slected inliers');
plot(x(:,1), x(:,2), '*:', 'LineWidth', line_width);


%% Add legend
set(h, 'Name', 'The inliers')
title([designer_id ' ' view]);

legend('all points', l_general, l_restricted_init, l_restricted_opt, l_marked);
% legend(l_marked, l_dlt, l_restricted_opt);
% saveas(h, fullfile(folder_save, 'selected_inliers.png'));
hold off;




end