function plotInliersVsAll(img, x_inliers, x_all, ...
    T_x,...
    error_mean_general_inliers, ...
    error_mean_general_all, width, designer_id, view)

h = figure(10);
imshow(img);
hold on;

x_inliers = T_x\x_inliers';
x_inliers = x_inliers';
x_inliers = x_to_pixels(x_inliers, width);

l_inliers = sprintf('inliers: %.3f (%d)', error_mean_general_inliers, length(x_inliers));
plot(x_inliers(:,1), x_inliers(:,2), '*');


x_all = T_x\x_all';
x_all = x_all';
x_all = x_to_pixels(x_all, width);

l_all = sprintf('all points marked: %.3f(%d)', error_mean_general_all, length(x_all));
plot(x_all(:,1), x_all(:,2), 'o');

%Set parameters:



%% Add legend
set(h, 'Name', 'InliersVsAll')
title([designer_id ' ' view]);

legend(l_inliers, l_all);

end