function error_geometric_general_mean_scaled = plotAllPoints(img, indices_all, x, X, T_x, t_general, t_restricted, width, designer_id, folder_save, view)
%Set parameters:
line_width = 2;

%% Plot the marked points:

x = T_x\x';
x = x';
x = x_to_pixels(x, width);

h = figure(4);
imshow(img)
set(h, 'Name', 'All points: dlt')
hold on;
h = plot(x(:,1), x(:,2), '*-', 'LineWidth', line_width);
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(:,1)+5.5, x(:,2)-5.5, c, 'Color', h.Color);
title([designer_id ' ' view]);

h = figure(5);
imshow(img)
set(h, 'Name', 'All points: constrained')
hold on;
title([designer_id ' ' view]);
h = plot(x(:,1), x(:,2), '*-', 'LineWidth', line_width);
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(:,1)+5.5, x(:,2)-5.5, c, 'Color', h.Color);


h = figure(6);
imshow(img);
set(h, 'Name', 'All points: golden general')
hold on;
title([designer_id ' ' view]);
h = plot(x(:,1), x(:,2), '*-', 'LineWidth', line_width);
a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(x(:,1)+5.5, x(:,2)-5.5, c, 'Color', h.Color);

x(:,1:2) = x(:,1:2)/width;
x(:,1:2) = x(:,1:2)*2 - 1; 
x(:,2) = x(:,2)*-1;
x = T_x*x';
x = x';

% h = figure(7);
% imshow(img)
% set(h, 'Name', 'All points: differently estiamted dlt projection')
% hold on;
% title([designer_id ' ' view]);
% h = plot(x(:,1), x(:,2), '*-', 'LineWidth', line_width);
% a = [indices_all]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x(:,1)+5.5, x(:,2)-5.5, c, 'Color', h.Color);




l_marked = sprintf('marked');

% %% Plot the DLT estimates points:
% 
% x = T_x*x';
% x = x';
% 
% x_ = zeros(size(X,1), 3);
% e = zeros(size(x,1), 1);
% for i = 1:size(X,1)
%    x_(i, :)  = (P*X(i, :)')'; 
%    x_(i, :) = x_(i, :)/ x_(i, 3);
%    e(i) = norm(x(i, 1:2) - x_(i, 1:2));
% end
% error_geometric_dlt_median = median(e); %sum(e.^2)/size(X,1);
% error_geometric_dlt_mean = mean(e);
% 
% x_ =  T_x\x_';
% x_ = x_';
% 
% x_ = x_to_pixels(x_, width);
% 
% h = figure(4);
% hp = plot(x_(:,1), x_(:,2), 's--', 'LineWidth', line_width);
% a = [indices_all]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x_(:,1)+5.5, x_(:,2)-5.5, c, 'Color', hp.Color);
% 
% l_dlt = sprintf('dlt, dist.median = %.3f, dist.mean = %.3f', ...
%     error_geometric_dlt_median, error_geometric_dlt_mean);
% 
% legend(l_marked, l_dlt);
% saveas(h, fullfile(folder_save, 'all_points_dlt.png'));
%% Plot the initial approxiamtion for constrained case:


% [x_constrained_init, error_geometric_constrained_init] = computeProjectionRestricted(t, x, X);
% 
% x_constrained_init =  T_x\x_constrained_init';
% x_constrained_init = x_constrained_init';
% 
% plot(x_constrained_init(:,1), x_constrained_init(:,2), 'o:', 'LineWidth', line_width);
% l_constrained_init = sprintf('constrained init, er = %.3f', error_geometric_constrained_init);
%% Plot the optimal for constrained case:
[x_constrained_opt, error_geometric_constrained_opt_median, ...
    error_geometric_constrained_opt_mean] = computeProjectionRestricted(t_restricted, x, X);

x_constrained_opt =  T_x\x_constrained_opt';
x_constrained_opt = x_constrained_opt';
% x_constrained_opt(:,1:2) = (x_constrained_opt(:,1:2)+1.0)/2.0*width;


x_constrained_opt = x_to_pixels(x_constrained_opt, width);

h = figure(5);
plot(x_constrained_opt(:,1), x_constrained_opt(:,2), 'gd-.', 'LineWidth', line_width);
l_constrained_opt = sprintf('constrained opt, dist.median = %.3f, dist.mean = %.3f',...
    error_geometric_constrained_opt_median/2.0*double(width), error_geometric_constrained_opt_mean/2.0*double(width));

% legend(l_marked, l_dlt, l_constrained_init, l_constrained_opt);
clear text; t = text(x_constrained_opt(:,1)+5.5, x_constrained_opt(:,2)-5.5, c, 'Color', h.Color);
legend(l_marked, l_constrained_opt);
% saveas(h, fullfile(folder_save, 'all_points_constrained.png'));

hold off;






%% Plot the golden for constrained case:
h = figure(6);
[x_general, error_geometric_general_median, ...
    error_geometric_general_mean] = computeProjectionGeneral(t_general, x, X);

x_general =  T_x\x_general';
x_general = x_general';


x_general = x_to_pixels(x_general, width);


plot(x_general(:,1), x_general(:,2), 'gd-.', 'LineWidth', line_width);
l_general = sprintf('general, dist.median = %.3f, dist.mean = %.3f',...
    error_geometric_general_median/2.0*double(width), error_geometric_general_mean/2.0*double(width));
clear text; t = text(x_general(:,1)+5.5, x_general(:,2)-5.5, c, 'Color', 'r');
% legend(l_marked, l_dlt, l_constrained_init, l_general);
legend(l_marked, l_general);
% saveas(h, fullfile(folder_save, 'all_points_golden.png'));

hold off;


error_geometric_general_mean_scaled = error_geometric_general_mean/2.0*double(width);

% 
% %% Plot the golden for constrained case:
% h = figure(7);
% [x_general, error_geometric_general_median, ...
%     error_geometric_general_mean] = computeProjectionGeneral(t0, x, X);
% 
% x_general =  T_x\x_general';
% x_general = x_general';
% 
% 
% x_general = x_to_pixels(x_general, width);
% 
% 
% plot(x_general(:,1), x_general(:,2), 'gd-.', 'LineWidth', line_width);
% l_general = sprintf('dlt (reconstructed), dist.median = %.3f, dist.mean = %.3f',...
%     error_geometric_general_median, error_geometric_general_mean);
% 
% % legend(l_marked, l_dlt, l_constrained_init, l_general);
% legend(l_marked, l_general);
% hold off;


end

