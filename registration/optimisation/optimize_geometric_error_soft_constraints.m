function [ tOpt, resnorm, exitflag,output] = ...
    optimize_geometric_error_soft_constraints( t0, X, x, thrOptimalityTolerance)

    if (~exist('thrOptimalityTolerance', 'var'))
        thrOptimalityTolerance = 1e-1;
    end
        
    options = optimoptions('lsqnonlin','Display', 'off',...
        'OptimalityTolerance', thrOptimalityTolerance, 'MaxIterations', 1000, ...
        'MaxFunctionEvaluations', 1000, 'Algorithm', 'levenberg-marquardt');%, 'PlotFcn', @optimplotx);%,...
    
%     options = optimoptions('lsqnonlin','Display', 'iter',...
%         'OptimalityTolerance', thrOptimalityTolerance, 'MaxIterations', 1000, ...
%         'MaxFunctionEvaluations', 1000);%,...
% 'PlotFcn', @optimplotfirstorderopt);%, 'Jacobian', 'off');% 'MaxFunctionEvaluations', '3000');
%     options = optimoptions('lsqnonlin','Display', 'iter', 'OptimalityTolerance', 1e-1, 'MaxIterations', 1000, 'MaxFunctionEvaluations', 10000);%, 'Jacobian', 'off');% 'MaxFunctionEvaluations', '3000');
        
%     if (length(x) > 9)
%         
%         lb =[ 0 -Inf -Inf -Inf -Inf -Inf -Inf -Inf -Inf];
%         ub =[ Inf Inf Inf Inf*ones(1,6)];
%     else
        lb = [];
        ub = [];
%     end
    
    w = 10;

    [tOpt, resnorm, residual, exitflag, output] = lsqnonlin(@projection_matrix, t0(1:end), lb(1:end), ub(1:end), options);
    resnorm = output.firstorderopt;
   
  
   
   function F = projection_matrix(t)                
        f_x = t(1); 
        f_y = t(2); 
        s= t(3);
        u = t(4);
        v = t(5); 
        theta_x  = t(6);
        theta_y = t(7);
        theta_z = t(8);
        c_x = t(9);
        c_y = t(10); 
        c_z = t(11);
        
        
        K = [f_x s u 0; 
             0 f_y v 0;
             0 0 1.0 0];
         

       R_z = [cos(theta_z) -sin(theta_z) 0;
              sin(theta_z)  cos(theta_z) 0;
              0 0 1];

        R_y = [cos(theta_y)   0 sin(theta_y);
                    0            1 0;
               -sin(theta_y)  0 cos(theta_y)];
   

        R_x = [ 1 0 0; 
                0 cos(theta_x) -sin(theta_x);
                0 sin(theta_x)  cos(theta_x)];

    
        R =  R_z*R_y*R_x;      
        
        C = [c_x; c_y; c_z];
        
        T = [R -R*C; zeros(1,3) 1];

        x_ = zeros(size(X,1), 3);
        F = zeros(size(x,1)+2, 1);
        for i = 1:size(X,1)
           x_(i, :)  = (K*T*X(i, :)')'; %change the dimensionality
           x_(i, :) = x_(i, :)/ x_(i, 3);
           F(i) = norm(x(i, 1:2) - x_(i, 1:2));
        end
        i = size(X,1) +1;
        
        
        F(i) = sqrt(w)*s;
        F(i+1) = sqrt(w)*(f_x-f_y);
        
%         w = w*1.1;     
        
    end

end

