function [ tOpt, resnorm, exitflag,output] = ...
    optimize_geometric_error_restricted( f_0, u_0, v_0, ...
        theta_x_0, theta_y_0, theta_z_0, ...
        c_x_0, c_y_0, c_z_0, X, x, thrOptimalityTolerance)

    if (~exist('thrOptimalityTolerance', 'var'))
        thrOptimalityTolerance = 1e-1;
    end
    
%    t0 = [f_0, u_0, v_0, theta_x_0, theta_y_0, theta_z_0, c_x_0, c_y_0, c_z_0]; 
%    options = optimset('Display', 'off', 'Jacobian', 'off');
%    lb =[ 0 -Inf -Inf -Inf -Inf -Inf -Inf -Inf -Inf];
%    ub =[ Inf Inf Inf Inf*ones(1,6)];
%    
% % lb = [];
% % ub = [];
%    [tOpt, resnorm] = lsqnonlin(@projection_matrix, t0(1:end), lb(1:end), ub(1:end), options);
% %     tOpt =[tOpt(1) 0 0 tOpt(2:end)];

    t0 = [f_0, u_0, v_0, theta_x_0, theta_y_0, theta_z_0, c_x_0, c_y_0, c_z_0]; 
    
    options = optimoptions('lsqnonlin','Display', 'off',...
        'OptimalityTolerance', thrOptimalityTolerance, 'MaxIterations', 1000, ...
        'MaxFunctionEvaluations', 1000);%,...
    
%     options = optimoptions('lsqnonlin','Display', 'iter',...
%         'OptimalityTolerance', thrOptimalityTolerance, 'MaxIterations', 1000, ...
%         'MaxFunctionEvaluations', 1000);%,...
% 'PlotFcn', @optimplotfirstorderopt);%, 'Jacobian', 'off');% 'MaxFunctionEvaluations', '3000');
%     options = optimoptions('lsqnonlin','Display', 'iter', 'OptimalityTolerance', 1e-1, 'MaxIterations', 1000, 'MaxFunctionEvaluations', 10000);%, 'Jacobian', 'off');% 'MaxFunctionEvaluations', '3000');
        
    if (length(x) > 9)
        
        lb =[ 0 -Inf -Inf -Inf -Inf -Inf -Inf -Inf -Inf];
        ub =[ Inf Inf Inf Inf*ones(1,6)];
    else
        lb = [];
        ub = [];
    end

    [tOpt, resnorm, residual, exitflag,output] = lsqnonlin(@projection_matrix, t0(1:end), lb(1:end), ub(1:end), options);
    resnorm = output.firstorderopt;
   function F = projection_matrix(t)        
%         f = t(1); 
%         u = t(2);
%         v = t(3); 
%         theta_x  = t(4);
%         theta_y = t(5);
%         theta_z = t(6);
%         c_x = t(7);
%         c_y = t(8); 
%         c_z = t(9);
            
%         f = t(1); 
%         u = 0;
%         v = 0; 
%         theta_x  = t(2);
%         theta_y = t(3);
%         theta_z = t(4);
%         c_x = t(5);
%         c_y = t(6); 
%         c_z = t(7);
        
        f = t(1); 
        u = t(2);
        v = t(3); 
        theta_x  = t(4);
        theta_y = t(5);
        theta_z = t(6);
        c_x = t(7);
        c_y = t(8); 
        c_z = t(9);
        
        
        K = [f 0 u 0; 
             0 f v 0;
             0 0 1.0 0];
         

       R_z = [cos(theta_z) -sin(theta_z) 0;
              sin(theta_z)  cos(theta_z) 0;
              0 0 1];

    

        R_y = [cos(theta_y)   0 sin(theta_y);
                    0            1 0;
               -sin(theta_y)  0 cos(theta_y)];
   

        R_x = [ 1 0 0; 
                0 cos(theta_x) -sin(theta_x);
                0 sin(theta_x)  cos(theta_x)];

    
        R =  R_z*R_y*R_x;      
        
        C = [c_x; c_y; c_z];
        
        T = [R -R*C; zeros(1,3) 1];

        x_ = zeros(size(X,1), 3);
        e = zeros(size(x,1), 1);
        for i = 1:size(X,1)
           x_(i, :)  = (K*T*X(i, :)')'; %change the dimensionality
           x_(i, :) = x_(i, :)/ x_(i, 3);
           e(i) = norm(x(i, 1:2) - x_(i, 1:2));
        end
        
%         e(ind_outliers) = min(1e-1, e(ind_outliers));

% error_geometric = sum(e.^2);


%         F = error_geometric;
        F = e;
    end

end

