% RANSAC implementation based on ransac algorithm from section 4.7.1 Multi
% View Geometry in Computer Vision.
% 
%  Outputs:
%  P_opt --- estimated projection matrix
%  inlier_idx_opt --- the number of inliers

function [P_opt, inlier_idx_opt, idx_opt, error_algebraic_opt, distances_opt] = ransac(pts2D, pts3D, T_x)

% Initialize outputs:
P_opt = zeros(3,4);
inlier_idx_opt = [];
idx_opt = [];
error_algebraic_opt = realmax;

% Initialize algorithm parameters:
num_matches = length(pts2D);

max_num_inliers = 0;
min_dist = realmax;

% The number of iteration for random slection of number of inliers:
min_num_matches = 6; %the minimum number of points need to 
sampleProb = 0.99;
inlPcent = min_num_matches / num_matches;
nb_sets = 5*lqs_numtries(min_num_matches, inlPcent, sampleProb); %min(10*lqs_numtries(min_num_matches, inlPcent, sampleProb), 20000);


thr_inliers = min_num_matches; %max(min_num_matches, num_matches/2);

sufficcient_num_inliers = ceil(prctile(1:num_matches, 95));


threshDistTight = 0.4;%0.2;
threshDist = 10;

thrFxFy = 100;
nbytes = 0;
thrYUp = 0.1;

inlierNum = 0;

i = 1;
while (i < nb_sets && min_dist > 0.02) % && inlierNum/num_matches < 0.95)
   i = i+1;
   idx = randperm(num_matches, min_num_matches);
   sample_pts2D = pts2D(idx, :); 
   sample_pts3D = pts3D(idx, :);
   
   %Estimate camera matrix:
   [P, error_algebraic] = dlt(sample_pts3D, sample_pts2D);

%    [K_est,R_est,C_est] = DecomposeCameraMatrix(T_x\P);
    [K_est,R_est,C_est] = DecomposeCameraMatrix(P);
   P = P./abs(K_est(3,3));
   K_est = K_est./abs(K_est(3,3));
      
   if isPlausibleSolution(K_est, R_est, C_est, thrYUp, thrFxFy)
%        fprintf("feasible solution, K = (%.3f, %.3f, %.3f\n %.3f, %.3f, %.3f\n %.3f, %.3f, %.3f)", K_est);
       
       %% Compute the distances between all 2d points and estimated projections:
%        distances = geometric_errors(pts2D, pts3D, P);
%        distance = mean(distances);
%        distance = norm(distances);
       
       %% Compute the inliers with distances smaller than the threshold
       [inlierIdx, inlierNum, distances, distance] = computeThresholdedDistances();
       
     
       
%        e2 = geometric_errors(sample_pts2D, sample_pts3D, P)
       
        
        %% If the number of inliers is larger -> compute camera matrix on all the inliers:
%         if (inlierNum >= max_num_inliers &&  distance < min_dist) % f_sq < f_sq_opt)

        update_optimal_solution = (inlierNum > max_num_inliers) || (inlierNum == max_num_inliers &&  distance < min_dist);


        if (update_optimal_solution)  
%         if (inlierNum >= thr_inliers  && inlierNum >= max_num_inliers &&  distance < min_dist) % f_sq < f_sq_opt)
            % Update optimal solution:
            
            max_num_inliers = inlierNum;
            P_opt = P;
            inlier_idx_opt = inlierIdx;
            min_dist = distance;
            distances_opt = distances;
            error_algebraic_opt = error_algebraic;
            
            if max_num_inliers >= sufficcient_num_inliers
                break; 
            end
%             
            %% Reestimate camera matrix on all inliers:
%             sample_pts2D = pts2D(inlierIdx, :); 
%             sample_pts3D = pts3D(inlierIdx, :);
%             
%             u_0 = K_est(1,3);
%             v_0 = K_est(2,3);
%             [theta_x_0, theta_y_0, theta_z_0, ~, ~, ~, ~ ] = rotm2eulr( R_est );
%             t0 = [K_est(1,1), K_est(2,2), K_est(1,2), u_0, v_0, theta_x_0, theta_y_0, theta_z_0, C_est(1), C_est(2), C_est(3)];
%             
%            [ t_golden, ~, ~, ~] = optimize_geometric_error_general( t0, sample_pts3D, sample_pts2D);
%             P = constructGeneralProjectionMatrix(t_golden);
%             
% %             [P, error_algebraic] = dlt(sample_pts3D, sample_pts2D);
% 
% 
% %             [K_est,R_est,C_est] = DecomposeCameraMatrix(T_x\P);
%             [K_est,R_est,C_est] = DecomposeCameraMatrix(P);
%             P = P./abs(K_est(3,3)); 
%             K_est = K_est./abs(K_est(3,3));
% 
%             if ~isPlausibleSolution(K_est, R_est, C_est, thrYUp, thrFxFy)
%                 continue;
%             end
%             
%             %Check that the number of inliers did not decreased after recomputation:
%             [inlierIdx, inlierNum_, distances, distance] = computeThresholdedDistances();
%             if (inlierNum_ < inlierNum)
%                 disp('Number of inliers decreased');
%                 continue;
%             else
%                 inlierNum = inlierNum_;
%             end
%             
%             update_optimal_solution = (distance < min_dist); %((inlierNum == max_num_inliers)&& (distance < min_dist)) || (inlierNum > max_num_inliers);
%             
%             if (update_optimal_solution)
%                 max_num_inliers = inlierNum;
%                 P_opt = P;
%                 inlier_idx_opt = inlierIdx;
%                 min_dist = distance;
%                 distances_opt = distances;
%                 error_algebraic_opt = error_algebraic;
%                 
%                 if max_num_inliers >=sufficcient_num_inliers
%                    break; 
%                 end
% %                 inlPcent = max_num_inliers/num_matches;
% %                 nb_sets = min(10*lqs_numtries(min_num_matches, inlPcent, sampleProb), nb_sets);
%             end
            
            
%             [P_opt, inlier_idx_opt, min_dist, distances_opt, error_algebraic_opt, max_num_inliers, nb_sets]...
%                 = reestimate_the_model_on_inliers(thrYUp, T_x, inlierNum, inlierIdx, max_num_inliers, distance, distances, min_dist, pts2D, pts3D, nb_sets, P, error_algebraic, thrFxFy);
        end
    
    
   end
   
%    [K_est,R_est,C_est] = DecomposeCameraMatrix(P);
   
   
   
   fprintf(repmat('\b', 1, nbytes));
    nbytes = fprintf("%d/%d, min_dist = %.3f, inl_per = %.2f\n", i, nb_sets, min_dist, max_num_inliers/num_matches);
   
end


   
%     Outputs:
%       distances --- the ditances for the points where the distance is less
%       than 95 percentile of all comuted distances.
%       distance --- the mean of diastances.
% 
    function [inlierIdx, inlierNum, distances, mean_distance] = computeThresholdedDistances()         
        distances = geometric_errors(pts2D, pts3D, P);
        
        inlierIdx = find(distances <= threshDistTight);
        inlierNum = length(inlierIdx);
        
        
%         threshDist = min(max(prctile(distances, 95), threshDistTight), threshDist);
%         thr = threshDistTight;
%         disp(prctile(distances, 85));
%         inlierIdx = find((distances)<=threshDistTight);

%         threshAvg = prctile(distances, 95);
        threshAvg = prctile(distances, 95);
        inliersAvg = (distances) <= threshAvg;

%         thr = prctile(distances, pThr);
%         ind = union( find(distances <= thr), idx);
%        ind = union(inlierIdx , idx);
        
        mean_distance = mean(distances(inliersAvg));%norm(distances);
        
%         if (inlierNum == length(pts2D))
%            disp(mean_distance);
%         end
    end

    


end


function [is_plausible] = isPlausibleSolution(K_est, R_est, C_est, thrYUp, thrFxFy)
     T = [R_est -R_est*C_est; zeros(1,3) 1];
     up = -T(2,2);
     view_dir = T(3, 1:3);
     
     vD = -view_dir;
     vO = -C_est;
     vO = vO/norm(vO);
     
     
     
     
%                if (up < thrYUp || view_dir(1) < 0  || view_dir(2) < 0 || view_dir(3) < 0 )
%                     continue; 
%                 end  
%        focal_point = C_est' - view_dir;

   focal_plane_in_front_of_the_object = true; 
   
   focal_plane_in_front_of_the_object = acos(dot(vO, vD))*180/pi < 40; %cahnge 90 for some meaningful angle
   
%    is_plausible = (up > thrYUp) && (focal_plane_in_front_of_the_object);%view_dir(1) < 0  && view_dir(2) < 0 && view_dir(3) < 0;%(max(abs(C_est') < abs(focal_point)));
   is_plausible = (focal_plane_in_front_of_the_object);%view_dir(1) < 0  && view_dir(2) < 0 && view_dir(3) < 0;%(max(abs(C_est') < abs(focal_point)));    
%     Constrains on the filed of view:
    f_sq = K_est(1,1)*K_est(2,2);
%     is_plausible = is_plausible && (f_sq > 1e-2 && f_sq < thrFxFy);
%     is_plausible = is_plausible &&  K_est(1,1) > 1e-2 && K_est(2,2) > 1e-2 && f_sq < thrFxFy;
    is_plausible = is_plausible &&  K_est(1,1) > 1e-2 && K_est(2,2) > 1e-2;
end

function [P_opt, inlier_idx_opt, min_dist, distances_opt, error_algebraic_opt, max_num_inliers, nb_sets]...
    = reestimate_the_model_on_inliers(thrYUp, T_x, inlierNum, inlierIdx, max_num_inliers, distance, distances, min_dist, pts2D, pts3D, nb_sets, P, error_algebraic,thrFxFy)
%             disp(max(distances(idx)));
%             disp(distances);
%             disp(distances(idx));
%             disp(f_sq);
            if (inlierNum == max_num_inliers)
                if (distance < min_dist)
                    P_opt = P;
                    inlier_idx_opt = inlierIdx;
%                     idx_opt = idx;
                    min_dist = distance;
                    distances_opt = distances;
                    error_algebraic_opt = error_algebraic;
%                     f_sq_opt = f_sq;
                end
            else
                max_num_inliers = inlierNum;
%                 inlPcent = max_num_inliers/num_matches;
%                 nb_sets = lqs_numtries(min_num_matches, inlPcent, sampleProb);
                P_opt = P;
                inlier_idx_opt = inlierIdx;
%                 idx_opt = idx;
                error_algebraic_opt = error_algebraic;
                min_dist = distance;
                distances_opt = distances;
                
               sample_pts2D = pts2D(inlierIdx, :); 
               sample_pts3D = pts3D(inlierIdx, :);

               %Reestimate camera matrix on all inliers:
               [P, error_algebraic] = dlt(sample_pts3D, sample_pts2D);
                

               [K_est,R_est,C_est] = DecomposeCameraMatrix(T_x\P);
               P = P./abs(K_est(3,3)); 
               K_est = K_est./abs(K_est(3,3));
                 

                if ~isPlausibleSolution(K_est, R_est, C_est, thrYUp,  thrFxFy)
                    return;
                end
   
                f_sq = K_est(1,1)*K_est(2,2);
%                 feasible = K_est(1,1)>0 & K_est(2,2)>0;
                if (f_sq > 1e-2 && f_sq < thrFxFy)
                  [distances, distance] = computeThresholdedDistances();

%                    if (distance <= min_dist)
                      P_opt = P;
                      error_algebraic_opt = error_algebraic;
                      min_dist = distance;
                      distances_opt = distances;
                      max_num_inliers = inlierNum;
                        inlPcent = max_num_inliers/num_matches;
                        nb_sets = min(10*lqs_numtries(min_num_matches, inlPcent, sampleProb), nb_sets);
                
                     inlier_idx_opt = inlierIdx;
%                      idx_opt = idx;
                
                
                
%                       f_sq_opt = f_sq;
%                    end
               end 
            end

end







function e = geometric_errors(pts2D, pts3D, P)
    e = zeros(size(pts2D,1), 1);
    for i = 1:size(pts2D,1)
        x_  = (P*pts3D(i, :)');
        x_ = x_/x_(3);
        e(i) = norm(pts2D(i, 1:2) - x_(1:2)');
    end
%     error_geometric = sum(e.^2)/size(pts2D,1);
end

%   Eq. 4.18: Multi View Geometry in CV book:
% 
%   Inputs:
%   s       size of a subsample 
%   omega   expected fraction of inliers (probability that any selected data point is an inlier)
%   p       probability to have a good subsample
% 
function nb_sample = lqs_numtries(s,omega,p)
    if(omega<0.0 || omega>1.0 || s<=0 || p>=1.0 || p<=0.0)
      fprintf (stderr, "Error: invalid arguments to `lqs_numtries'!\n");
      exit(1);
    end
    
    E = omega^s;
    
    if(E>=.9999999999) 
        nb_sample = 0; % E close to 1.0, avoid division by -infty
    elseif(E<=1E-08) 
        nb_sample = intmax; % E close to 0.0, avoid division by zero
    else
        nb_sample = ceil(log(1.0 - p)/log(1.0 - E));
    end
    
end




