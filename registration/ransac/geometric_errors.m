function e = geometric_errors(pts2D, pts3D, P)
    e = zeros(size(pts2D,1), 1);
    for i = 1:size(pts2D,1)
        x_  = (P*pts3D(i, :)');
        x_ = x_/x_(3);
        e(i) = norm(pts2D(i, 1:2) - x_(1:2)');
    end
%     error_geometric = sum(e.^2)/size(pts2D,1);
end