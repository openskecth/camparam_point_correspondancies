function R = rotationMatrix( theta_x, theta_y, theta_z )

R_z =    [cos(theta_z) -sin(theta_z) 0;
               sin(theta_z)  cos(theta_z) 0;
                0 0 1];

    

        R_y = [cos(theta_y)   0 sin(theta_y);
                    0            1 0;
               -sin(theta_y)  0 cos(theta_y)];
   

        R_x = [ 1 0 0; 
                0 cos(theta_x) -sin(theta_x);
                0 sin(theta_x)  cos(theta_x)];

R =  R_z*R_y*R_x;      

end

