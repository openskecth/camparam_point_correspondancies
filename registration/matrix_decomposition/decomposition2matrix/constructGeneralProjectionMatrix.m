function P = constructGeneralProjectionMatrix(t)
    f_x = t(1); 
    f_y = t(2); 
    s= t(3);
    u = t(4);
    v = t(5); 
    theta_x  = t(6);
    theta_y = t(7);
    theta_z = t(8);
    c_x = t(9);
    c_y = t(10); 
    c_z = t(11);
        
        
    K = [f_x s u 0; 
         0 f_y v 0;
         0 0 1.0 0];
    
    R_z = [cos(theta_z) -sin(theta_z) 0;
           sin(theta_z)  cos(theta_z) 0;
            0 0 1];

    R_y = [cos(theta_y)   0 sin(theta_y);
                0            1 0;
           -sin(theta_y)  0 cos(theta_y)];


    R_x = [ 1 0 0; 
            0 cos(theta_x) -sin(theta_x);
            0 sin(theta_x)  cos(theta_x)];


    R =  R_z*R_y*R_x;      

    C = [c_x; c_y; c_z];

    T = [R -R*C; zeros(1,3) 1];
    P = K*T;
end