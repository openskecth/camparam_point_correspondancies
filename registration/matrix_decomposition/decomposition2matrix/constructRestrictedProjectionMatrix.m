function P = constructRestrictedProjectionMatrix(t)
    f = t(1); 
    u = t(2);
    v = t(3); 
    theta_x  = t(4);
    theta_y = t(5);
    theta_z = t(6);
    c_x = t(7);
    c_y = t(8); 
    c_z = t(9);

    K = [f 0 u 0; 
         0 f v 0;
         0 0 1 0];
    
     R_z =    [cos(theta_z) -sin(theta_z) 0;
           sin(theta_z)  cos(theta_z) 0;
            0 0 1];

    R_y = [cos(theta_y)   0 sin(theta_y);
                0            1 0;
           -sin(theta_y)  0 cos(theta_y)];


    R_x = [ 1 0 0; 
            0 cos(theta_x) -sin(theta_x);
            0 sin(theta_x)  cos(theta_x)];


    R =  R_z*R_y*R_x;      

    C = [c_x; c_y; c_z];

    T = [R -R*C; zeros(1,3) 1];
    P = K*T;
end