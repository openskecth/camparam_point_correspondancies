
function  [x_, error_geometric_median, error_geometric_mean, error_geometric_std] = computeProjectionGeneral(t, x, X)
    
    P = constructGeneralProjectionMatrix(t);
    x_ = zeros(size(X,1), 3);

    for i = 1:size(X,1)
       x_(i, :)  = (P*X(i, :)')'; %change the dimensionality
       x_(i, :) = x_(i, :)/ x_(i, 3);
    end
    
    [error_geometric_median, error_geometric_mean, error_geometric_std] = compute_errors(x, x_);
end

