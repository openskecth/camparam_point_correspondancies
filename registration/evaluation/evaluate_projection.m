% function evaluate_projection(t_general, t_restricted, x_inliers, X_inliers, x_all, X_all)
% 
% t_general:        vector of 11 parameters for camera model
% t_restricted:     vector of 9 parameters for camera model, skew = 0 and
%                   f_x = f_y
% 


function [error_median_general, error_mean_general, error_std_general, ...
          error_median_restricted, error_mean_restricted, error_std_restricted] = ...
          evaluate_projection(t_general, t_restricted, x, X)

[~, error_median_general, error_mean_general, error_std_general] = ...
    computeProjectionGeneral(t_general, x, X);
[~, error_median_restricted, error_mean_restricted, error_std_restricted] = ...
    computeProjectionRestricted(t_restricted, x, X);
end