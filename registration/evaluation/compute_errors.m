% function compute_rms(x, x_)
% 
% Input:
% x --- the measured points in hte image
% x_ --- the points obtained through projection

function  [error_geometric_median, error_geometric_mean, error_geometric_std] = compute_errors(x, x_)
    
    e = zeros(size(x,1), 1);
    for i = 1:size(x,1)
        p1 = x(i, 1:2);
        p2 = x_(i, 1:2);
        e(i) = sum((p1-p2).^2);
    end
    
    error_geometric_median = median(e);%sum(e.^2)/size(x,1);
    error_geometric_mean = mean(e); 
    error_geometric_std = std(e); 
end