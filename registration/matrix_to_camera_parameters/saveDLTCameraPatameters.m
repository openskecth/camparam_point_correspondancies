function dlt_estimated = saveDLTCameraPatameters(T_x, P)
    P = T_x\P;
    
    [K_est,R_est,C_est] = DecomposeCameraMatrix(P);
    P = P./abs(K_est(3,3));

    K_est = K_est./abs(K_est(3,3));
    [theta_x, theta_y, theta_z, theta_x2, theta_y2, theta_z2, R_ ] = rotm2eulr( R_est );
    K_est(:,4) = 0;

    dlt_estimated.fx = K_est(1,1);
    dlt_estimated.fy = K_est(2,2);
    dlt_estimated.skew = K_est(1,2);
    dlt_estimated.u = K_est(1,3);
    dlt_estimated.v = K_est(2,3);
    dlt_estimated.theta_x = theta_x;
    dlt_estimated.theta_y = theta_y;
    dlt_estimated.theta_z = theta_z;
    dlt_estimated.C = C_est';
    T = [R_est -R_est*C_est; zeros(1,3) 1];
    view_dir = T(3, 1:3);
    T(1,:) = -T(1,:);
    T(2,:) = -T(2,:);
    dlt_estimated.mvMatrix = T';
    dlt_estimated.focal_point = C_est' - view_dir;
    dlt_estimated.up = T(2,1:3);
end