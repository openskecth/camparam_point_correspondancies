function camera_model = denormaliseProjectionGeneral(t, T_x)


P = constructProjectionMatrix(t);
P = T_x\P;
[K,R,C] = DecomposeCameraMatrix(P);

T = [R -R*C; zeros(1,3) 1];
view_dir = T(3, 1:3);
T(1,:) = -T(1,:);
T(2,:) = -T(2,:);
focal_point = C' - view_dir;
up = T(2, 1:3);
camera_model.C = C';
camera_model.focal_point = focal_point;
camera_model.up = up;
camera_model.fx = K(1,1);
camera_model.fy = K(2,2);
camera_model.skew = K(1,2);
camera_model.u =  K(1,3);
camera_model.v =  K(2,3);
camera_model.mvMatrix = T';

% R_ = rotationMatrixFromView(C', focal_point, up)
 
 
end

function P = constructProjectionMatrix(t)
    f_x = t(1); 
        f_y = t(2); 
        s= t(3);
        u = t(4);
        v = t(5); 
        theta_x  = t(6);
        theta_y = t(7);
        theta_z = t(8);
        c_x = t(9);
        c_y = t(10); 
        c_z = t(11);
        
        
        K = [f_x s u 0; 
             0 f_y v 0;
             0 0 1.0 0];
    
     R_z =    [cos(theta_z) -sin(theta_z) 0;
           sin(theta_z)  cos(theta_z) 0;
            0 0 1];

    R_y = [cos(theta_y)   0 sin(theta_y);
                0            1 0;
           -sin(theta_y)  0 cos(theta_y)];


    R_x = [ 1 0 0; 
            0 cos(theta_x) -sin(theta_x);
            0 sin(theta_x)  cos(theta_x)];


    R =  R_z*R_y*R_x;      

    C = [c_x; c_y; c_z];

    T = [R -R*C; zeros(1,3) 1];
    P = K*T;
end