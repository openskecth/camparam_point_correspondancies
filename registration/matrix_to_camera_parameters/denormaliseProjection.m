function camera_model = denormaliseProjection(t, T_x)
    P = constructRestrictedProjectionMatrix(t);
    P = T_x\P;
    [K,R,C] = DecomposeCameraMatrix(P);

    T = [R -R*C; zeros(1,3) 1];
    view_dir = T(3, 1:3);
    focal_point = C' - view_dir;
    T(1,:) = -T(1,:);
    T(2,:) = -T(2,:);
    up = T(2, 1:3);
    
    
  
    
    
    camera_model.C = C';
    camera_model.focal_point = focal_point;
    camera_model.up = up;
    camera_model.f = K(1,1);
    camera_model.skew = 0.0;
    camera_model.u =  K(1,3);
    camera_model.v =  K(2,3);

    camera_model.mvMatrix = T';
end

