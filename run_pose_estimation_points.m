function run_pose_estimation_points()
    folder = fileparts(which('run_v2.m')); 
    addpath(genpath(folder));
    global folder_sketches;
    folder_sketches = 'D:\Projects\SketchItProject\CollectedSketches\SKETCHES';
    global obj_files_folder;
    obj_files_folder = 'D:\Projects\SketchItProject\SketchItPaper\supplemental\objects_obj_files'; 

    global folder_centered_sketches;
    folder_centered_sketches = 'D:\Projects\SketchItProject\CollectedSketches\SKETCHES_TRAIN\centered_sketches';

    global folder_camera_parameters;
    folder_camera_parameters = 'D:\Projects\SketchItProject\CollectedSketches\camera_parameters\camera_parameters_v4';
    
    if ~exist(folder_camera_parameters ,'dir')
        mkdir(folder_camera_parameters);
    end
    
    
    global designer_type;
    global designer_id;
    global object_id;
    global view;

    [~, designers(1, 1:9), ~]   = studentsData();
    [~, designers(1, 10:15), ~] = professionalsData();
    objects = objectsData();
    
    i = 0;
    for di = 1:length(designers)
       
        designer_type   = designers(di).designer_type;
        designer_id     = designers(di).id;

        view = 'view1';
        designer_objects = designers(di).objects;
        for oi = 1:length(designer_objects)
            close all;
            object_id = objects(designer_objects(oi)).name;
            i = i+1;
            [f(i),  f_centeres_pp(i), f_original(i), f_artificial(i),...
             azimuth(i,:), elevation(i,:), radiuses(i,:),...
             alpha_h(i,:), alpha_v(i,:)] = estimateCamParam(designer_id, objects(designer_objects(oi)).name);
        end

%         view = 'view2';
%         designer_objects = designers(di).objects_v2;
%         for oi = 1:length(designer_objects)
%             object_id = designer_objects(oi);           
%             estimateCamParam(designer_id, object_id);
%         end

    % bumps_data;
    % flange_data;
    % hairdryer_data;
    % house_data;
    % mixer_data;
    % mouse_data;
    % waffle_data;
    % wobble_surface_data;
    % potato_chip_data;
    % shampoo_bottle_data;
    % tubes_data;
    % vacuum_cleaner_data;
    end
    
    figure;
    subplot(4,1,1);
    histogram(f,i);
    axis([0 8 0 30]);
    title('Centered, non-fixed principal point')
    
    subplot(4,1,2);
    histogram(f_centeres_pp,i);
    axis([0 8 0 30]);
    title('Extended, fixed principal point')
    
    subplot(4,1,3);
    histogram(f_original,i);
    title('Original, non-fixed principal point')
    axis([0 8 0 30]);
    
    subplot(4,1,4);
    histogram(f_artificial,i);
    title('Artificial, centered sketch, fixed principal point')
    axis([0 8 0 30]);
    
    fprintf('mean f    = %.3f, std f    = %.3f\n', mean(f), std(f));
    fprintf('mean f pp = %.3f, std f pp = %.3f\n', mean(f_centeres_pp), std(f_centeres_pp));
    f_original_ = f_original(f_original < prctile(f_original, 95));
    fprintf('mean f or = %.3f, std f or = %.3f\n', mean(f_original_),    std(f_original_));
    f_artificial_ = f_artificial(f_artificial < prctile(f_artificial, 95));
     fprintf('mean f art = %.3f, std f art = %.3f\n', mean(f_artificial_),    std(f_artificial_));
     
    figure;
%     subplot(3,1,1);
%     fov = 2.0 * atan(1.0./f) * 180.0 / pi;
%     histogram(f,i);
%     axis([0 120 0 10]);
%     title('Centered, non-fixed principal point')
%     subplot(3,1,2);
    fov_centered_pp = 2.0 * atan(1.0./f_centeres_pp) * 180.0 / pi;
    histogram(fov_centered_pp,i);
    axis([0 120 0 10]);
    title('FOV, Extended sketch, fixed principal point')
    
    
  
figure; hold on;
title('Azimuth');  
for i = 1:4 
plot(1:size(azimuth,1), azimuth(:,i)/pi*180, '*-');
end
legend('Centered, non-fixed principal point', ...
       'Extended, fixed principal point',...
       'Original, non-fixed principal point',...
       'Artificial, centered sketch, fixed principal point');


figure; hold on;
title('elevation');    
for i = 1:4 
plot(1:size(elevation,1), elevation(:,i)/pi*180, '*-');
end
legend('Centered, non-fixed principal point', ...
       'Extended, fixed principal point',...
       'Original, non-fixed principal point',...
       'Artificial, centered sketch, fixed principal point');

figure; hold on;
title('radiuses');    
for i = 1:4 
plot(1:size(radiuses,1), radiuses(:,i), '*-');
end
legend('Centered, non-fixed principal point', ...
       'Extended, fixed principal point',...
       'Original, non-fixed principal point',...
       'Artificial, centered sketch, fixed principal point');

focal_lengths = zeros(107,4);
focal_lengths(:,1) = f;
focal_lengths(:,2) = f_centeres_pp;
focal_lengths(:,3) = f_original;
focal_lengths(:,4) = f_artificial;

figure; hold on;
title('focal_lengths');    
for i = 1:4 
plot(1:size(focal_lengths,1), focal_lengths(:,i), '*-');
end
legend('Centered, non-fixed principal point', ...
       'Extended, fixed principal point',...
       'Original, non-fixed principal point',...
       'Artificial, centered sketch, fixed principal point');

figure; hold on;
title('obj direction elev diff');    
for i = 1:4 
plot(1:size(alpha_h,1), alpha_h(:,i), '*-');
end
legend('Centered, non-fixed principal point', ...
       'Extended, fixed principal point',...
       'Original, non-fixed principal point',...
       'Artificial, centered sketch, fixed principal point');

   
figure; hold on;
title('obj azimuth elev diff');    
for i = 1:4 
plot(1:size(alpha_v,1), alpha_v(:,i), '*-');
end
legend('Centered, non-fixed principal point', ...
       'Extended, fixed principal point',...
       'Original, non-fixed principal point',...
       'Artificial, centered sketch, fixed principal point');
   
%     subplot(3,1,3);
%     fov_centered_pp = 2.0 * atan(1.0./f_centeres_pp) * 180.0 / pi;
%     histogram(f_original,i);
%     title('Original, non-fixed principal point')
%     axis([0 120 0 10]);
   
end

function [f,  f_centeres_pp, f_original, f_artificial, azimuth, elevation, radiuses,...
        alpha_h, alpha_v] = ...
        estimateCamParam(designer_id, object_id)
    global view;
    global folder_centered_sketches;
    global folder_data;
    global indices_all;
    global folder_sketches;
    global designer_type;


    
    if strcmp(view, 'view1')
        view_str = 'v1';
    else
        view_str = 'v2';
    end

    [H, folder_data] = eval([object_id '_3D']);

    H = scale_center_mesh(H, folder_data, object_id);
    
    eval([lower(designer_id) '_' object_id '_' view_str '_points']);
    folder_prof = fullfile(folder_sketches, [designer_type '_clean\']);
    folder = fullfile(folder_prof, designer_id, [object_id '\drawings_' object_id '\']);
    img =  imread(fullfile(folder, [view '_concept_opaque.png']));
    
    x_m = x_m(indices_opt, :);
    H = H(indices_opt, :);
    %% Original sketch
    
    global folder_camera_parameters;
    
    folder_save = fullfile([folder_camera_parameters '_original'], designer_type, designer_id, object_id, view);
    if (~exist(folder_save, 'dir'))
       mkdir(folder_save); 
    end
    
 
    path_save = fullfile(folder_save, 'camera_parameters.json');
    save_mat_file = fullfile(folder_save, 'errors.mat');
    
    if ~exist(path_save, 'file')
        for k = 1:3
            [camera_parameters_original(k), error_geometric_general_mean_scaled(k), mat_data(k)] = estimate_camparam_correspondancies(x_m, size(img,1), H, img);            
        end
        [~, ind] = min(k);
        camera_parameters_original = camera_parameters_original(ind);
        text_data = jsonencode(camera_parameters_original);
        fileID = fopen(path_save,'w');
        fprintf(fileID,text_data);
        fclose(fileID);
        mat_data = mat_data(ind);
        save(save_mat_file, 'mat_data');
    else
       
        fileID = fopen(path_save, 'r');
        text_data = fscanf(fileID, '%s');
        fclose(fileID);
        camera_parameters_original =...
                jsondecode(text_data);
    end
    
    %% Center sketch:
    [img, scale, bottom, left, dx, dy] = scale_center_sketch(folder, view, img);
    imwrite(img, fullfile(folder_centered_sketches, [designer_id '_' object_id '_' view '.png']));
    

    x_m(:,1) = (x_m(:,1) - double(left))*scale + double(dx);
    x_m(:,2) = (x_m(:,2) - double(bottom))*scale + double(dy);
     
    %% Check if the parameters are already computed:

    folder_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, view);
    if (~exist(folder_save, 'dir'))
       mkdir(folder_save); 
    end
    
    
    folder_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, view);
    path_save = fullfile(folder_save, 'camera_parameters.json');
    save_mat_file = fullfile(folder_save, 'errors.mat');
    
    if ~exist(path_save, 'file')
        for k = 1:3
            [camera_parameters(k), error_geometric_general_mean_scaled(k), mat_data(k)] = estimate_camparam_correspondancies(x_m, size(img,1), H, img);            
        end
        [~, ind] = min(k);
        camera_parameters = camera_parameters(ind);
        text_data = jsonencode(camera_parameters);
        fileID = fopen(path_save,'w');
        fprintf(fileID,text_data);
        fclose(fileID);
        mat_data = mat_data(ind);
        save(save_mat_file, 'mat_data');
    else
       
        fileID = fopen(path_save, 'r');
        text_data = fscanf(fileID, '%s');
        fclose(fileID);
        camera_parameters =...
                jsondecode(text_data);
    end
    
    %% Exp 3: Computed fields of view centered sketches fixed view point
    folder_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, view);
    path_save = fullfile(folder_save, 'camera_parameters_centered_sketch_centered_pp.json');
    save_mat_file = fullfile(folder_save, 'errors_centered_sketch_centered_pp.mat');
    
    if ~exist(path_save, 'file')
        for k = 1:3
            [camera_parameters_centr_sketch_centr_pp(k), error_geometric_general_mean_scaled(k), mat_data(k)] = estimate_camparam_correspondancies_centerd_pp(x_m, size(img,1), H, img);            
        end
        [~, ind] = min(k);
        camera_parameters_centr_sketch_centr_pp = camera_parameters_centr_sketch_centr_pp(ind);    
        text_data = jsonencode(camera_parameters_centr_sketch_centr_pp);
        fileID = fopen(path_save,'w');
        fprintf(fileID,text_data);
        fclose(fileID);
        mat_data = mat_data(ind);
        save(save_mat_file, 'mat_data');
        
    else    
        fileID = fopen(path_save, 'r');
        text_data = fscanf(fileID, '%s');
        fclose(fileID);
        camera_parameters_centr_sketch_centr_pp =...
                jsondecode(text_data);
    end
    
    %% Move sketch points so that the estimated principal point is at the center of the image plane:
    [img, x_m, scale] = ...
        transalteSketchAndImage(img, x_m, camera_parameters);
    
%     h = figure;
%    
%     imshow(img)
%     hold on;
%     x =  x_m(:,1);
%     y =  x_m(:,2);
%     plot(x,y, 'o-');
%     a = [1:size(x_m,1)]'; b = num2str(a); c = cellstr(b);
%     text(x, y, c);
% 
%     close(h); 
    folder_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, view);
    path_save = fullfile(folder_save, 'camera_parameters_centered_pp.json');
    save_mat_file = fullfile(folder_save, 'errors_centered_pp.mat');
    
    if ~exist(path_save, 'file')
        for k = 1:3
            [camera_parameters_centered_pp(k), error_geometric_general_mean_scaled(k), mat_data(k)] = estimate_camparam_correspondancies_centerd_pp(x_m, size(img,1), H, img);            
        end
        [~, ind] = min(k);
        camera_parameters_centered_pp = camera_parameters_centered_pp(ind);    
        text_data = jsonencode(camera_parameters_centered_pp);
        fileID = fopen(path_save,'w');
        fprintf(fileID,text_data);
        fclose(fileID);
        mat_data = mat_data(ind);
        save(save_mat_file, 'mat_data');
        
    else    
        fileID = fopen(path_save, 'r');
        text_data = fscanf(fileID, '%s');
        fclose(fileID);
        camera_parameters_centered_pp =...
                jsondecode(text_data);
    end
    
    disp('--------');
    fprintf('Original:\n f=%.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
            camera_parameters_original.restricted.f, camera_parameters_original.restricted.u, camera_parameters_original.restricted.v,...
            camera_parameters_original.restricted.C, camera_parameters_original.restricted.up, camera_parameters_original.restricted.focal_point);
        
    fprintf('First:\n f=%.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
            camera_parameters.restricted.f, camera_parameters.restricted.u, camera_parameters.restricted.v,...
            camera_parameters.restricted.C, camera_parameters.restricted.up, camera_parameters.restricted.focal_point);

    fprintf('Centered_pp:\n f=%.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
            camera_parameters_centered_pp.restricted.f, camera_parameters_centered_pp.restricted.u, camera_parameters_centered_pp.restricted.v,...
            camera_parameters_centered_pp.restricted.C, camera_parameters_centered_pp.restricted.up, camera_parameters_centered_pp.restricted.focal_point);
        
   fprintf('im scale = %.3f, focal scale = %.3f\n', scale, camera_parameters.restricted.f/camera_parameters_centered_pp.restricted.f); 
   
   f = camera_parameters.restricted.f;
   f_centeres_pp = camera_parameters_centered_pp.restricted.f;
   f_original = camera_parameters_original.restricted.f;
   f_artificial = camera_parameters_centr_sketch_centr_pp.restricted.f;
   
   %% Convert to camera azimuth and elevation angles:
   C = camera_parameters.restricted.C;
   focal_point  = camera_parameters.restricted.focal_point;
   [azimuth(1,1),...
    elevation(1,1),...
    radiuses(1,1)] = cart2sph(C(1),-C(3),C(2));
   
    [alpha_h(1,1), alpha_v(1,1)] = ...
        findDevitionCamDirObjectCenter(C, focal_point);
   
    C = camera_parameters_centered_pp.restricted.C;
    focal_point  = camera_parameters_centered_pp.restricted.focal_point;
   [azimuth(1,2),...
    elevation(1,2),...
    radiuses(1,2)] = cart2sph(C(1),-C(3),C(2));
    [alpha_h(1,2), alpha_v(1,2)] = ...
        findDevitionCamDirObjectCenter(C, focal_point);

    C = camera_parameters_original.restricted.C;
    focal_point  = camera_parameters_original.restricted.focal_point;
   [azimuth(1,3),...
    elevation(1,3),...
    radiuses(1,3)] = cart2sph(C(1),-C(3),C(2));
    [alpha_h(1,3), alpha_v(1,3)] = ...
        findDevitionCamDirObjectCenter(C, focal_point);
    
    C = camera_parameters_centr_sketch_centr_pp.restricted.C;
    focal_point = camera_parameters_centr_sketch_centr_pp.restricted.focal_point;
   [azimuth(1,4),...
    elevation(1,4),...
    radiuses(1,4)] = cart2sph(C(1),-C(3),C(2));

     [alpha_h(1,4), alpha_v(1,4)] = ...
        findDevitionCamDirObjectCenter(C, focal_point);
end


function [alpha_h, alpha_v] = findDevitionCamDirObjectCenter(C, focal_point)

  view_dir = (C - focal_point)';
  dir_object = C';
          
  %% XZ plane
  proj_xz = view_dir - dot(view_dir, [0.0 1.0 0.0])*[0.0 1.0 0.0];
  proj_xz = proj_xz/norm(proj_xz);

  proj_xz_dir_object = dir_object - dot(dir_object, [0.0 1.0 0.0])*[0.0 1.0 0.0];
  proj_xz_dir_object = proj_xz_dir_object/norm(proj_xz_dir_object);

  alpha_h = acos(dot(proj_xz_dir_object, proj_xz))*180.0/pi;
  alpha_h(alpha_h > 90)  = alpha_h(alpha_h > 90) - 90;
  
  %% YX plane
  proj_yx = view_dir - dot(view_dir, [0.0 0.0 1.0])*[0.0 0.0 1.0];
  proj_yx = proj_yx/norm(proj_yx);

  proj_yx_dir_object = dir_object - dot(dir_object, [0.0 0.0 1.0])*[0.0 0.0 1.0];
  proj_yx_dir_object = proj_yx_dir_object/norm(proj_yx_dir_object);

  alpha_v = acos(dot(proj_yx_dir_object, proj_yx))*180.0/pi;
  alpha_v(alpha_v > 90)  = alpha_v(alpha_v > 90) - 90;      
end