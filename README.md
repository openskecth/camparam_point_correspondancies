This is a MatLab code to find camera parameters from a sparse set of point correspondacies between 3D models and points in a sketch.

The code is not cleaned.

Start working with the code from run.m.

The most importatn functionality is in estimate_camera_parameters_from_3D_to_2D_correspondancies.m.

## This code is a part of OpenSketch publication
Project page:
https://ns.inria.fr/d3/OpenSketch/

Additional data:
https://repo-sam.inria.fr/d3/OpenSketch/


## Contact
If you have any questions please contact Yulia Gryaditskaya yulia.gryaditskaya@gmail.com
If something is missing please contact yulia.gryaditskaya@gmail.com.