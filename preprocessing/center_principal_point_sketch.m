function center_principal_point_sketch(view)

% Read principal point (camera parameters):
[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

designers = readCameraParametersForEachDesigner(designers, view);
objects = objectsData();

folder_centered_sketches = 'D:\Projects\SketchItProject\CollectedSketches\SKETCHES_TRAIN\centered_sketches';
folder_centered_pp_sketches = 'D:\Projects\SketchItProject\CollectedSketches\SKETCHES_TRAIN\centered_pp_sketches';

if ~exist(folder_centered_pp_sketches, 'dir')
    mkdir(folder_centered_pp_sketches);
end


for di = 1:length(designers)
%     designer_type   = designers(di).designer_type;
    designer_id     = designers(di).id;
    
    if (strcmp(view, 'view1'))
        designer_objects = designers(di).objects;
    else
        designer_objects = designers(di).objects_v2;
    end
    
    
    for oj = 1:length(designers(di).objects)
        cam_params = designers(di).camera_parameters_v1(oj);
        obj_num = designer_objects(oj);        
        object_id    = objects(obj_num).name;
        
        file_img_centerd = fullfile(folder_centered_sketches, [designer_id '_' object_id '_' view '.png']); 
        img = imread(file_img_centerd);
        
        width = size(img, 1);
        pp(1) = cam_params.restricted.u;
        pp(2) = cam_params.restricted.v;
        pp_pixels = x_to_pixels(pp, width);
        
        
            
        new_width = double(int32(width*max(abs(pp(1)) + 1.0, abs(pp(2)) + 1.0)));
        
        img_new = 255*ones(new_width, new_width, 3, 'uint8');
        
        
        dx = int32(new_width/2.0 - pp_pixels(1)); 
        dy = int32(new_width/2.0 - pp_pixels(2)); 
        
%         figure(1);
%         hold off
%         plot(pp_pixels(1), pp_pixels(2), '*r');
%         hold on;
%         imshow(img);
%         hold on;
%         plot(pp_pixels(1), pp_pixels(2), '*r');
        
        
        
        img_new((1+dy):(width+dy),...
                (1+dx):(width+dx),...
                 :) = img;
        
%         figure(2);
%         hold off
%         imshow(img_new);
%         hold on;
%         plot(new_width/2.0, new_width/2.0, '*r');
        
        file_img_centerd_pp = fullfile(folder_centered_pp_sketches, [designer_id '_' object_id '_' view '.png']); 
        imwrite(img_new, file_img_centerd_pp);
    end
    
end
    
end


function x = x_to_pixels(x, width)
    x(:,2) = x(:,2)*-1;
    x(:,1:2) = (x(:,1:2)+1.0)/2.0*width;
end
