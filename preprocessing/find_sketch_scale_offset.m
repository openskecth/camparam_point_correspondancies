function [scale, dx, dy, sketch_dim_new] = find_sketch_scale_offset(sketch, sketch_dim_new)
% Find the bounding box   
sketch_dim_cur = sketch.canvas.width;
[minx, maxx, miny, maxy] = find_bounding_box_sketch(sketch, sketch_dim_cur);

  minx/sketch_dim_cur*2 - 1
  maxx/sketch_dim_cur*2 - 1
  -(miny/sketch_dim_cur*2 - 1)
  -(maxy/sketch_dim_cur*2 - 1)
  
 % Remap sketch to the area 
 w = maxx - minx;
 h = maxy - miny;

%  sketch_dim_new = 256;
 img_area = 0.85*sketch_dim_new;
 padding  = 0.05*sketch_dim_new;
 
 if w > h
     scale = img_area/w;
     dx = padding - minx*scale;
     dy = (sketch_dim_new - h*scale)/2 - miny*scale;
 else
     scale = img_area/h;
     dy = padding - miny*scale;
     dx = (sketch_dim_new - w*scale)/2 - minx*scale;
 end

end