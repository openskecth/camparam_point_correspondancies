function [H] = scale_center_mesh(H, folder_data, object_id)

global obj_files_folder;
file_object = fullfile(obj_files_folder, [object_id '.obj']);

%% Find parameters:
filepath_mat = fullfile(folder_data, [object_id '.mat']);

if ~exist(filepath_mat, 'file')
    obj = read_obj(file_object);
    vertices = obj.v;
    min_corner = min(vertices, [], 1);
    max_corner = max(vertices, [], 1);
    centroid = [0.5*(min_corner + max_corner) 0.0];
    scale = 2.0 / max(abs((max_corner-min_corner)));
    save(filepath_mat, 'scale', 'centroid');
else
    load(filepath_mat, 'scale', 'centroid');
end

% Center and scale:
H = H - repmat(centroid, size(H,1), 1);
H(:,1:3) = H(:,1:3)*scale;
end

