function [img_scaled_centered, scale, bottom, left, dx, dy] = scale_center_sketch(folder_sketch, view, img)
%SCALE_CENTER_SKETCH Summary of this function goes here
%   Detailed explanation goes here


if exist(fullfile(folder_sketch, [view '_presentation.json']), 'file')
    presentation_sketch_polylines = readSketchJson(fullfile(folder_sketch, [view '_presentation.json']));
    if ~isfield(presentation_sketch_polylines,'strokes')
        presentation_sketch_polylines = readSketchJson(fullfile(folder_sketch, [view '_concept.json']));
    end
    presentation_sketch_polylines = keepOnlyNonRemovedStrokes(presentation_sketch_polylines); 
else
     presentation_sketch_polylines = readSketchJson(fullfile(folder_sketch, [view '_concept.json']));
     presentation_sketch_polylines = keepOnlyNonRemovedStrokes(presentation_sketch_polylines); 
end

% sketch_dim_new = 500;
sketch_dim_cur = presentation_sketch_polylines.canvas.width;
sketch_dim_new = sketch_dim_cur;

[minx, maxx, miny, maxy] = find_bounding_box_sketch(presentation_sketch_polylines, sketch_dim_cur);

window_size = 20; %set window search size

pad_left   = min(minx, window_size);
pad_right  = min(maxx, window_size);
pad_top    = min(maxy, window_size);
pad_bottom = min(miny, window_size); 


left   = max(minx-window_size,1);
right  = min(maxx+window_size, sketch_dim_cur);
bottom = max(miny-window_size,1);
top    = min(maxy+window_size, sketch_dim_cur);

% Remap sketch to the area 
w = maxx - minx;
h = maxy - miny;

%  sketch_dim_new = 256;
area_fraction = 0.74;
img_area = area_fraction*sketch_dim_new;
padding  = 0.5*(1.0-area_fraction)*sketch_dim_new;

 if w > h
     scale = img_area/w;
  
%      dx = padding - minx*scale;
%      dy = (new_width - h*scale)/2 - miny*scale;
     
     dx = int32(padding-pad_left*scale);
     dy = int32((sketch_dim_new - h*scale)/2-pad_bottom*scale);   
 else
     scale = img_area/h;
  
 
%      dy = padding - miny*scale;
%      dx = (new_width - w*scale)/2 - minx*scale;
     dy = int32(padding-pad_bottom*scale);
     dx = int32((sketch_dim_new - w*scale)/2-pad_left*scale);  
 end


 %% Crop
%  dcx = dx;
%  dcy = dy;
drawing_area = img(bottom:top, left:right, :);
% 
% x_m(:,1) = (x_m(:,1) - double(left));
% x_m(:,2) = (x_m(:,2) - double(bottom));
% 
% figure;
% imshow(drawing_area)
% hold on;
% x =  x_m(:,1);
% y =  x_m(:,2);
% plot(x,y, 'o-');
% a = [1:length(x_m)]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c);
    
drawing_area = imresize(drawing_area, scale);
% x_m(:,1) = x_m(:,1)*scale;
% x_m(:,2) = x_m(:,2)*scale;
% figure;
% imshow(drawing_area)
% hold on;
% x =  x_m(:,1);
% y =  x_m(:,2);
% plot(x,y, 'o-');
% a = [1:length(x_m)]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c);
%     

img_scaled_centered = 255*ones(sketch_dim_new, sketch_dim_new, 3, 'uint8');


% drawing_area = imresize(drawing_area, scale);

if dx < 0
    dnminx = -dx;
    dx = 1;
elseif dx ==0
    dnminx = 1;
    dx = 1;
else
    dnminx = 1;    
end
dnmaxx = min(sketch_dim_new-dx+1, size(drawing_area,2)-dnminx+1);

if dy < 0
    dnminy = -dy;
    dy = 1;
elseif dy ==0
    dnminy = 1;
    dy = 1;
else
    dnminy = 1;
end
dnmaxy = min(sketch_dim_new-dy+1, size(drawing_area,1)-dnminy+1);

img_scaled_centered(dy:(dy+dnmaxy-dnminy), dx:(dx+dnmaxx-dnminx),  :) = ...
    drawing_area(dnminy:dnmaxy, dnminx:dnmaxx,  :);

% figure; imshow(img_scaled_centered);
% 
% x_m(:,1) = x_m(:,1) + double(dx);
% x_m(:,2) = x_m(:,2) + double(dy);
% figure;
% imshow(img_scaled_centered)
% hold on;
% x =  x_m(:,1);
% y =  x_m(:,2);
% plot(x,y, 'o-');
% a = [1:length(x_m)]'; b = num2str(a); c = cellstr(b);
% clear text; t = text(x, y, c);
    
end

