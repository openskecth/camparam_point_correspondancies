function  [minx, maxx, miny, maxy] = find_bounding_box_sketch(sketch, sketch_dim)
minx = Inf;
miny = Inf;
maxx = -Inf;
maxy = -Inf;

for i = 1:length(sketch.strokes)
    minx = min(minx, min(cat(1, sketch.strokes(i).points(:).x)));
    maxx = max(maxx, max(cat(1, sketch.strokes(i).points(:).x)));
    miny = min(miny, min(cat(1, sketch.strokes(i).points(:).y)));
    maxy = max(maxy, max(cat(1, sketch.strokes(i).points(:).y)));
end


minx = (max(floor(minx-1), 1.0));
maxx = (min(ceil(maxx+1), sketch_dim));
miny = (max(floor(miny-1), 1.0));
maxy = (min(ceil(maxy+1), sketch_dim));

  
end