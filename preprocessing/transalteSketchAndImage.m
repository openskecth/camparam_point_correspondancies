function [img_tr, marked_2d_points_tr, scale] = ...
    transalteSketchAndImage(img, marked_2d_points, cam_params)
    
    width = size(img, 1);
    pp(1) = cam_params.restricted.u;
    pp(2) = cam_params.restricted.v;
    pp_pixels = x_to_pixels(pp, width);



    new_width = double(int32(width*max(abs(pp(1)) + 1.0, abs(pp(2)) + 1.0)));
    scale = new_width/width;
    
   
    img_tr = 255*ones(new_width, new_width, 3, 'uint8');

    dx = int32(new_width/2.0 - pp_pixels(1)); 
    dy = int32(new_width/2.0 - pp_pixels(2)); 
        
%         figure(1);
%         hold off
%         plot(pp_pixels(1), pp_pixels(2), '*r');
%         hold on;
%         imshow(img);
%         hold on;
%         plot(pp_pixels(1), pp_pixels(2), '*r');
        
    img_tr((1+dy):(width+dy),...
                (1+dx):(width+dx),...
                 :) = img;
    
%     scale = 1.0;
%     sketch_tr = rescale_and_center_sketches(sketch, scale, dx, dy, new_width);

       marked_2d_points_tr(:,1) = marked_2d_points(:,1) + double(new_width/2.0 - pp_pixels(1));
       marked_2d_points_tr(:,2) = marked_2d_points(:,2) + double(new_width/2.0 - pp_pixels(2));
       marked_2d_points_tr(:,3) = marked_2d_points(:,3);
%         figure(2);
%         hold off
%         imshow(img_new);
%         hold on;
%         plot(new_width/2.0, new_width/2.0, '*r');
        
%         file_img_centerd_pp = fullfile(folder_centered_pp_sketches, [designer_id '_' object_id '_' view '.png']); 
%         imwrite(img_tr, file_img_centerd_pp);
end


function x = x_to_pixels(x, width)
    x(:,2) = x(:,2)*-1;
    x(:,1:2) = (x(:,1:2)+1.0)/2.0*width;
end