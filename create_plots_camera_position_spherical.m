function create_plots_camera_position_spherical()
    [~, designers(1, 1:9), ~] = studentsData();
    [~, designers(1, 10:15), ~] = professionalsData();
    
    objects = objectsData();
    
    [azimuths_all_v1,elevations_all_v1,radiuses_all_v1,fovs_all_v1,strs_v1] = ...
                getSphericalCameraParameters(designers, objects, 'view1');  
            
    [azimuths_all_v2,elevations_all_v2,radiuses_all_v2,fovs_all_v2,strs_v2] = ...
                getSphericalCameraParameters(designers, objects, 'view2');  
            
     azimuths_all_v1 = 180/pi*azimuths_all_v1;
     elevations_all_v1 = 180/pi*elevations_all_v1;
     azimuths_all_v1 = sort(azimuths_all_v1);
     elevations_all_v1  = sort(elevations_all_v1);
     radiuses_all_v1  = sort(radiuses_all_v1);
     
     azimuths_all_v2 = 180/pi*azimuths_all_v2;
     elevations_all_v2 = 180/pi*elevations_all_v2;
     [azimuths_all_v2, ind_sort_v2] = sort(azimuths_all_v2);
     elevations_all_v2  = sort(elevations_all_v2);
     radiuses_all_v2  = sort(radiuses_all_v2);
     
     close all;
     folder = 'D:\users\ygryadit\Data\SketchItProject\SketchItPaper\paper_new\images_drafts';
     
     azimuth_pmax = findPmax(azimuths_all_v1,azimuths_all_v2, [-180:3:180]);
     elevations_pmax = findPmax(elevations_all_v1,elevations_all_v2, [-90:1.5:90]);
     fov_pmax = findPmax(fovs_all_v1,fovs_all_v2, [1:1.5:180]);
     
     plotCamParams(azimuths_all_v1,elevations_all_v1,radiuses_all_v1, 'view1', azimuth_pmax, elevations_pmax);
     plotFOV(sort(fovs_all_v1), 'Filed of view in degrees', 'Percentage of sketches', 'view1', fov_pmax);
     print('-bestfit', fullfile(folder, ['fov_' 'view1' '.pdf']), '-dpdf');
     
     
     plotCamParams(azimuths_all_v2,elevations_all_v2,radiuses_all_v2, 'view2', azimuth_pmax, elevations_pmax);     
     plotFOV(sort(fovs_all_v2), 'Filed of view in degrees', 'Percentage of sketches', 'view2', fov_pmax);
     print('-bestfit', fullfile(folder, ['fov_' 'view2' '.pdf']), '-dpdf');
end

function pmax = findPmax(values_v1,values_v2, centers)
    counts = hist(values_v1, centers);
    counts_v1 = counts./sum(counts);
    
    counts = hist(values_v2, centers);
    counts_v2 = counts./sum(counts);
    
    pmax = max(max(counts_v1), max(counts_v2));
end

function plotCamParams(azimuths_all, elevations_all, radiuses_all, view, azimuth_pmax, elevations_pmax)
    folder = 'D:\users\ygryadit\Data\SketchItProject\SketchItPaper\paper_new\images_drafts';
    
    plotParameter(azimuths_all, 'Azimuth degrees', 'Percentage of sketches', view, -180, 180, 8, [-180:3:180], azimuth_pmax);    
    print('-bestfit', fullfile(folder, ['camera_azimuth_' view '.pdf']), '-dpdf');
    
    plotParameter(elevations_all, 'Elevation degrees', 'Percentage of sketches', view, -90, 90, 6, [-90:1.5:90], elevations_pmax);
    print('-bestfit', fullfile(folder, ['camera_elevation_' view '.pdf']), '-dpdf');
    
%     plotParameter(radiuses_all, 'Distancies', 'Percentage of sketches', view);
%     print('-bestfit', fullfile(folder, ['camera_distancies_' view '.pdf']), '-dpdf');
end

function plotParameter(vals, xlabel_str, ylabels_str, title_str, x_min, x_max, paper_sz_w, centers, pmax)
    h = figure;
%     paper_sz_w = 7;
    paper_sz_h = 3;
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
     
%     centers = 100;
    [counts,centers] = hist(vals, centers);
    counts = counts./sum(counts);
   
    
    bar(centers, counts, 'EdgeColor', 'none');
    axis([x_min, x_max 0 pmax]);
    xlabel(xlabel_str);
    ylabel(ylabels_str);
    title(title_str);
    set(h, 'Name', [title_str ' ' xlabel_str ' ' ylabels_str ' ' ]);
    set(gca,'FontSize', 6);
end

function plotFOV(vals, xlabel_str, ylabels_str, title_str, pmax)
    h = figure;
    paper_sz_w = 6;
    paper_sz_h = 3;
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
     
    centers = [1:1.5:180];
    [counts,centers] = hist(vals, centers);
    counts = counts./sum(counts);
    bar(centers, counts, 'EdgeColor', 'none');
    axis([0 150 0 pmax]);
    xlabel(xlabel_str);
    ylabel(ylabels_str);
    title(title_str);
    set(h, 'Name', [title_str ' ' xlabel_str ' ' ylabels_str ' ' ]);
    set(gca,'FontSize', 6);
end

function [azimuths_all,elevations_all,radiuses_all,fovs_all,strs] = ...
                getSphericalCameraParameters(designers, objects, view)
            
    filepath_pose = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters';
    
    azimuths_all = [];
    elevations_all = [];
    radiuses_all = [];
    fovs_all = [];
    strs = {};
    
    num_designers = length(designers);
    num_sketches_all = 0;
     
    for obj_num = 1:length(objects)

        azimuths = zeros(num_designers,1);
        elevations = zeros(num_designers,1);
        radiuses = zeros(num_designers,1);
        num_sketches = 0;
        
        for designer_num = 1:num_designers
           designer = designers(designer_num);
           designer_objects = getObjectsListDesigner(designer, view);
           
           if ~ismember(obj_num, designer_objects)
                continue;
           end
           
           num_sketches = num_sketches +1;
           num_sketches_all = num_sketches_all +1;
           strs{num_sketches_all} = [objects(obj_num).name ' ' designer.id];
           filepath = fullfile( filepath_pose,...
                                     designer.designer_type,...
                                     designer.id,...
                                     objects(obj_num).name,...
                                     view);
           

                                 
           cam_params = loadCamParams(filepath);
           C = cam_params.restricted.C;  
           f = cam_params.restricted.f
           %f-assumes that image is nrmilise to [-1 1]
           width = 2.0;
           fov = 2*atand(2.0/(2.0*f));
           fovs_all(num_sketches_all) = fov;
           
           [azimuths(num_sketches),...
            elevations(num_sketches),...
            radiuses(num_sketches)] = cart2sph(C(1),-C(3),C(2));
           
        end
        azimuths    = azimuths(1:num_sketches);
        elevations  = elevations(1:num_sketches);
        radiuses    = radiuses(1:num_sketches);
        
        azimuths_all = [azimuths_all; azimuths];
        elevations_all = [elevations_all; elevations];
        radiuses_all = [radiuses_all; radiuses];
    end

end

function designer_objects = getObjectsListDesigner(designer, view)
    if strcmp(view, 'view1')
       designer_objects = designer.objects;
    else
       designer_objects = designer.objects_v2;
    end
end

function cam_params = loadCamParams(filepath)
    fileID = fopen(fullfile(filepath, 'camera_parameters.json'), 'r');
    text = fscanf(fileID, '%s');    
    fclose(fileID);
    cam_params = jsondecode(text);
end

