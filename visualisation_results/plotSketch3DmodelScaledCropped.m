function plotSketch3DmodelScaledCropped(designer_num, object_num, view)
close all
folder_save_figures = 'D:\users\ygryadit\Data\SketchItProject\Presentations\October2018\Frames3DModelSketchCroppedScaled';

if ~exist(folder_save_figures, 'dir')
   mkdir(folder_save_figures); 
end

[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

objects = objectsData();

designer = designers(designer_num);
clear 'desginers';

object = objects(object_num);
clear 'objects';

dir_path = fullfile(designer.folder_home, designer.id, object.name, ...
                    ['drawings_' object.name]);
object_id = object.name;

%Read 3D model
filename_dlt = fullfile(dir_path, [view '_dlt.png']);  
filename_constrained = fullfile(dir_path, [view '_constrained.png']);  

if (~exist(filename_dlt, 'file'))
    return;
end

[img_dlt, ~, t_dlt] = imread(filename_dlt);
[img_constrained, ~, t_constrained] = imread(filename_constrained);
t_dlt = repmat(t_dlt, [1 1 3])./255.0;
img_dlt = double(img_dlt.*t_dlt + 255.*(1.0 - t_dlt))/255.0;
t_constrained = repmat(t_constrained, [1 1 3])./255.0;
img_constrained = double(img_constrained.*t_constrained + 255.*(1.0 - t_constrained))/255.0;

%Read transparent sketches
filename_sketch = fullfile(dir_path, [view '_' 'concept' '.png']);  
[~, ~, t_sketch] = imread(filename_sketch);
img_sketch = double(256.0 - repmat(3.0*t_sketch, [1 1 3]))/256.0;

filename_sketch = fullfile(dir_path, [view '_' 'presentation' '.png']);  
if exist(filename_sketch)
    [~, ~, t_present] = imread(filename_sketch);
    img_present = double(256.0 - repmat(3.0*t_present, [1 1 3]))/256.0;
else
    t_present = zeros(size(t_sketch));
    img_present = double(256.0 - repmat(3.0*t_present, [1 1 3]))/256.0;
end



%% Load 3D and 2D poitns:
eval([object.name '_3D']);

if (strcmp(view, 'view1'))
    short_view = 'v1';
else
    short_view = 'v2';
end

eval([lower(designer.id) '_' object.name '_' short_view '_points']);

%% Scale and crop images:
[x_min, x_max, y_min, y_max, scale] = findScaleAndCrop(x_m, indices_opt, width);



%% Combine the three 
img_dlt = (0.5*img_dlt+0.25*img_sketch+0.25*img_present);
img_constrained = (0.5*img_constrained+0.25*img_sketch+0.25*img_present);

% figure(1); imshow(img_constrained); hold on;
% plot([x_min x_max x_max x_min], [y_min y_min y_max y_max], '-*');
img_dlt = scaleAndCrop(img_dlt, x_min, x_max, y_min, y_max, scale);
img_constrained = scaleAndCrop(img_constrained, x_min, x_max, y_min, y_max, scale);


% figure(1); imshow(img_dlt);
% figure(2); imshow(img_constrained);


imwrite(img_constrained, fullfile(folder_save_figures, [designer.id '_' object.name '_' view '_' 'constraines' '.png']));
imwrite(img_dlt, fullfile(folder_save_figures, [designer.id '_' object.name '_' view '_' 'general' '.png']));
end

function [x_min, x_max, y_min, y_max, scale] = findScaleAndCrop(x, indices_opt, width)
    x_min = 1;
    x_max = 0;
    y_min = 1;
    y_max = 0;
    
%     x_m(:,1:2) = x_m(:,1:2)/width;
%     x_m(:,1:2) = x_m(:,1:2)*2 - 1; 
%     x_m(:,2) = x_m(:,2)*-1; %cordinated in webgl -1 (bottom left) matlab 1 bottom left
% 
%     x = x_m(indices_opt, :);    
%     [x, T_x] = normalise2dpts(x');
%     x = x';
% 
    x_min = ceil(min(x(:,1)));
    x_max = floor(max(x(:,1)));
    y_min = ceil(min(x(:,2)));
    y_max = floor(max(x(:,2)));


    area = (x_max - x_min)*...
        (y_max-y_min);
    scale = sqrt(400.0*400.0/area);
end

function img_final = scaleAndCrop(img, x_min, x_max, y_min, y_max, scale)
    padding = int32(50*scale);
    border_x_min = min(padding,x_min);
    border_y_min = min(padding,y_min);
    border_x_max = min(padding, size(img,1)-x_max);
    border_y_max = min(padding,size(img,1)-y_max);
   
   
    img = img((y_min-border_y_min*scale):(y_max+border_y_max*scale),(x_min-border_x_min*scale):(x_max+border_x_max*scale),:);
    
    img_f = ones(y_max-y_min+padding*2*scale, x_max-x_min+padding*2*scale, 3)*256;
    img_f((padding-border_y_min+1):(padding-border_y_min + size(img,1)), ...
          (padding-border_x_min+1):(padding-border_x_min + size(img,2)),:) = img;
    img = imresize(img_f, scale);
    
    img_final = ones(800, 800, 3)*256;
    offset_x = (800  - size(img,1))/2 + 1;
    offset_y = (800  - size(img,2))/2 + 1;
    img_final(offset_x:(offset_x+size(img,1)-1), offset_y:(offset_y+size(img,2)-1), :) = img;
end

function  [ points_2D, points_2D_all] = getProjectionGeneral(t, x, points_3D, H, T_x, width)
    [points_2D, ~, ...
        ~] = computeProjectionGeneral(t, x, points_3D);
    points_2D =  T_x\points_2D';
    points_2D = points_2D';
    points_2D = x_to_pixels(points_2D, width);

    [points_2D_all, ~, ...
        ~] = computeProjectionGeneral(t, x,H);
    points_2D_all = T_x\points_2D_all';
    points_2D_all = points_2D_all';
    points_2D_all = x_to_pixels(points_2D_all, width);
end


function  [ points_2D, points_2D_all] = getProjectionConstrained(t, x, points_3D, H, T_x, width)
    [points_2D, ~, ...
        ~] = computeProjectionRestricted(t, x, points_3D);
    points_2D =  T_x\points_2D';
    points_2D = points_2D';
    points_2D = x_to_pixels(points_2D, width);

    [points_2D_all, ~, ...
        ~] = computeProjectionRestricted(t, x,H);
    points_2D_all = T_x\points_2D_all';
    points_2D_all = points_2D_all';
    points_2D_all = x_to_pixels(points_2D_all, width);
end