function plotCorrespondanciesForPaperFigure(designer_num, object_num, view, font_size)
close all
folder_save_figures = 'D:\users\ygryadit\Data\SketchItProject\SketchItPaper\paper_new\images_drafts\registration_visualisation';
if ~exist(folder_save_figures, 'dir')
   mkdir(folder_save_figures); 
end

[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

objects = objectsData();

designer = designers(designer_num);
clear 'desginers';

object = objects(object_num);
clear 'objects';

dir_path = fullfile(designer.folder_home, designer.id, object.name, ...
                    ['drawings_' object.name]);
object_id = object.name;
%Read 3D model
filename_dlt = fullfile(dir_path, [view '_dlt.png']);  
filename_constrained = fullfile(dir_path, [view '_constrained.png']);  
[img_dlt, ~, t_dlt] = imread(filename_dlt);
[img_constrained, ~, t_constrained] = imread(filename_constrained);
t_dlt = repmat(t_dlt, [1 1 3])./255.0;
img_dlt = double(img_dlt.*t_dlt + 255.*(1.0 - t_dlt))/255.0;
t_constrained = repmat(t_constrained, [1 1 3])./255.0;
img_constrained = double(img_constrained.*t_constrained + 255.*(1.0 - t_constrained))/255.0;

%Read transparent sketches
filename_sketch = fullfile(dir_path, [view '_' 'concept' '.png']);  
[~, ~, t_sketch] = imread(filename_sketch);
img_sketch = double(255.0 - repmat(3.0*t_sketch, [1 1 3]))/255.0;

filename_sketch = fullfile(dir_path, [view '_' 'presentation' '.png']);  
if exist(filename_sketch)

    [~, ~, t_present] = imread(filename_sketch);
    img_present = double(255.0 - repmat(3.0*t_present, [1 1 3]))/255.0;
else
    t_present = zeros(size(t_sketch));
    img_present = double(255.0 - repmat(3.0*t_present, [1 1 3]))/255.0;
end

%Combine the three 
img_dlt = (0.5*img_dlt+0.25*img_sketch+0.25*img_present);
img_constrained = (0.5*img_constrained+0.25*img_sketch+0.25*img_present);

figure(1); imshow(img_dlt);
figure(2); imshow(img_constrained);

%Load 3D and 2D poitns:
eval([object.name '_3D']);

if (strcmp(view, 'view1'))
    short_view = 'v1';
else
    short_view = 'v2';
end

eval([lower(designer.id) '_' object.name '_' short_view '_points']);

%Load camera parameters
folder_cam_param = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters';
folder_save = fullfile(folder_cam_param, designer_type, designer_id, object.name, view);
mat_file = fullfile(folder_save, 'errors.mat');
load(mat_file, 't_general', 't_restricted');


%Plot point correspondancies
%indices_all, H
points_2D = x_m(indices_opt, :);
points_3D = H(indices_opt, :);
% points_3D_rem = setxor(points_3D, H);


x_m(:,1:2) = x_m(:,1:2)/width;
x_m(:,1:2) = x_m(:,1:2)*2 - 1; 
x_m(:,2) = x_m(:,2)*-1;
x = x_m(indices_opt, :);
[x, T_x] = normalise2dpts(x');
x = x';

width = size(img_dlt,1);

[ points_2D_general, points_2D_general_all] = getProjectionGeneral(t_general, x, points_3D, H, T_x, width);

close(figure(3));
h_dlt = figure(1);
hold on;
for i = 1:length(points_2D)
    plot([points_2D(i,1) points_2D_general(i,1)], ...
         [points_2D(i,2) points_2D_general(i,2)], 'm-', 'LineWidth', 2);

   p2 = plot(points_2D(i,1), ...
         points_2D(i,2), 'm*'); 
     
    p3 = plot(points_2D_general(i,1), ...
         points_2D_general(i,2), 'k*'); 
end

a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(points_2D(:,1), points_2D(:,2), c, 'Color', 'm', ...
    'FontSize', font_size);

a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(points_2D_general(:,1), points_2D_general(:,2), c, 'Color', 'k', ...
    'FontSize', font_size);
legend([p2 p3], {'Marked', 'Projected'}, 'FontSize',14);
filename = fullfile(dir_path, [designer.id '_' object.name '_' view '_general.png'])
print(filename, '-dpng');

% for i = 1:length(points_2D_general_all)   
%     plot(points_2D_general_all(i,1), ...
%          points_2D_general_all(i,2), 'm*'); 
% end




[ points_2D_constrained, points_2D_constrained_all] = getProjectionConstrained(t_restricted, x, points_3D, H, T_x, width);

h_restr = figure(2);
hold on;
for i = 1:length(points_2D)
    plot([points_2D(i,1) points_2D_constrained(i,1)], ...
         [points_2D(i,2) points_2D_constrained(i,2)], 'm-', 'LineWidth', 2);

    p2 = plot(points_2D(i,1), ...
         points_2D(i,2), 'm*'); 
     
    p3 = plot(points_2D_constrained(i,1), ...
         points_2D_constrained(i,2), 'k*'); 
end

a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(points_2D(:,1), points_2D(:,2), c, 'Color', 'm', ...
    'FontSize', font_size);

a = [indices_all]'; b = num2str(a); c = cellstr(b);
clear text; t = text(points_2D_constrained(:,1), points_2D_constrained(:,2), c, 'Color', 'k', ...
    'FontSize', font_size);

% for i = 1:length(points_2D_constrained_all)   
%     plot(points_2D_constrained_all(i,1), ...
%          points_2D_constrained_all(i,2), 'k*'); 
% end

legend([p2 p3], {'Marked', 'Projected'}, 'FontSize',14);
filename = fullfile(dir_path, [designer.id '_' object.name '_' view '_restricted.png']);
print(filename, '-dpng');


end


function  [ points_2D, points_2D_all] = getProjectionGeneral(t, x, points_3D, H, T_x, width)
    [points_2D, ~, ...
        ~] = computeProjectionGeneral(t, x, points_3D);
    points_2D =  T_x\points_2D';
    points_2D = points_2D';
    points_2D = x_to_pixels(points_2D, width);

    [points_2D_all, ~, ...
        ~] = computeProjectionGeneral(t, x,H);
    points_2D_all = T_x\points_2D_all';
    points_2D_all = points_2D_all';
    points_2D_all = x_to_pixels(points_2D_all, width);
end


function  [ points_2D, points_2D_all] = getProjectionConstrained(t, x, points_3D, H, T_x, width)
    [points_2D, ~, ...
        ~] = computeProjectionRestricted(t, x, points_3D);
    points_2D =  T_x\points_2D';
    points_2D = points_2D';
    points_2D = x_to_pixels(points_2D, width);

    [points_2D_all, ~, ...
        ~] = computeProjectionRestricted(t, x,H);
    points_2D_all = T_x\points_2D_all';
    points_2D_all = points_2D_all';
    points_2D_all = x_to_pixels(points_2D_all, width);
end