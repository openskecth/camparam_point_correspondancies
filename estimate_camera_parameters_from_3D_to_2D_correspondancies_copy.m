 clear;
 close all;

house_data;

% wobble_surface_data;


x_m(:,1:2) = x_m(:,1:2)/width;
x_m(:,1:2) = x_m(:,1:2)*2 - 1; 
x_m(:,2) = x_m(:,2)*-1; %cordinated in webgl -1 (bottom left) matlab 1 bottom left

x = x_m(indices_opt, :);
X = H(indices_opt, :);


%% Find initial projection matrix:

[x, T_x] = normalise2dpts(x');

x = x';

[P, inlierIdx, idx, error_algebraic] = ransac( x, X);
indices_opt = indices_opt(inlierIdx);
% [P, error_algebraic] = dlt(X, x);
x = x(inlierIdx, :);
X = X(inlierIdx, :);


% Remove effect of normalizaion:
% x = T_x\x';
% x = x';
% P = T_x\P;
% T_x = eye(3);

[K_est,R_est,C_est] = DecomposeCameraMatrix(P);
P = P./abs(K_est(3,3));

K_est = K_est./abs(K_est(3,3));
% TT = K_est*R_est;
% 
% 
% 
% K_est(3,3) = -1;
% R_est = TT\K_est;
[theta_x, theta_y, theta_z, theta_x2, theta_y2, theta_z2, R_ ] = rotm2eulr( R_est );

disp('K_est'); disp(K_est);
disp('R_est'); disp(R_est);
disp('C_est'); disp(C_est');



disp('P'); disp(P);
disp('P_decomposed'); K_est(:,4) = 0; disp(K_est*[R_est -R_est*C_est; zeros(1,3) 1]);



e = zeros(size(x,1), 1);
for i = 1:size(x,1)
    x_  = (P*X(i, :)');
    x_ = x_/x_(3);
    e(i) = norm(x(i, 1:2) - x_(1:2)');
end
error_geometric_dlt_median = median(e); %sum(e.^2)/size(x,1);
error_geometric_dlt_mean = mean(e);


dlt_estimated = saveDLTCameraPatameters(T_x, P);


%% Minimize geometeric errror
if ((K_est(1,1) > 0) && (K_est(2,2) >0))
    f_0 = (K_est(1,1) + K_est(2,2))/2.0;
else
    f_0 = K_est(1,1);
end
% f_0 = max(K_est(1,1), K_est(2,2))

u_0= K_est(1,3);
v_0= K_est(2,3);
theta_x_0 = theta_x;
theta_y_0 = theta_y;
theta_z_0 = theta_z;
% fprintf('Intial:\n f=%.2f, u = %.2f, v = %.2f, theta_x = %.2f, theta_y = %.2f, theta_z = %.2f, C_x = %.2f, C_y = %.2f, C_z = %.2f\n', ...
%     f_0, u_0, v_0, theta_x_0, theta_y_0, theta_z_0, C_est(1), C_est(2), C_est(3));





% f_0 =6.67;
% u_0 =1.07; 
% v_0 = -2.25;
% theta_x_0 = -3.04; 
% theta_y_0 = -0.65; 
% theta_z_0 = -0.02;
% C_est(1) = -2.41;
% C_est(2) = 2.20; 
% C_est(3) = 4.54;

% if (use_given_1)
%     theta_x_0 = 0.2755;
%     theta_y_0 = 2.3967;
%     theta_z_0 = -3.0171;
% end
% 
% if (use_given)
%     theta_x_0 = theta_x_; 
%     theta_y_0 = theta_y_; 
%     theta_z_0 = theta_z_;  
%     C_est = C_est_;
%     f_0 = f_0_;
% end
% 
% if (use_given_3) %bottom-right
% theta_x_0 = -1.56; 
% theta_y_0 = -0.01; 
% theta_z_0 = -0.83;
% % C_est = [4 -1.2 1.2];
% end
% 
% if (use_given_4) %bottom-right
%     theta_x_0 = -1.57; 
%     theta_y_0 = -0.01; 
%     theta_z_0 = -2.1835;
% %     C_est = [1.0 1.0 2.02];
% end
% 
% if use_given_5
% %     f_0 =0.01;
% %     u_0 = -0.35;
% %     v_0 = 0.35;
%     theta_x_0 = 0.2755;
%     theta_y_0 = 2.3967;
%     theta_z_0 = -3.0171;
%     C_est = [2 2 1.19];
% end
%% Plot the intial parameters:
t = [f_0, u_0, v_0, theta_x_0, theta_y_0, theta_z_0, C_est(1), C_est(2), C_est(3)];


[x_constrained_init, error_geometric_constrained_init_median, ...
    error_geometric_constrained_init_mean, R_init, C_init] = computeProjection(t, x, X);


[ tOpt, resnorm_1] = optimize_geometric_error( f_0, u_0, v_0, theta_x_0, theta_y_0, theta_z_0, C_est(1), C_est(2), C_est(3), X, x);
% 
% fprintf('tOpt1:\n f=%.2f, u = %.2f, v = %.2f, theta_x = %.2f, theta_y = %.2f, theta_z = %.2f, C_x = %.2f, C_y = %.2f, C_z = %.2f\n', ...
%     tOpt1);

% theta_x_0 = theta_x2;
% theta_y_0 = theta_y2;
% theta_z_0 = theta_z2;
% [ tOpt2, resnorm_2] = optimize_geometric_error( f_0, u_0, v_0, theta_x_0, theta_y_0, theta_z_0, C_est(1), C_est(2), C_est(3), X, x);
% 
% fprintf('tOpt2:\n f=%.2f, u = %.2f, v = %.2f, theta_x = %.2f, theta_y = %.2f, theta_z = %.2f, C_x = %.2f, C_y = %.2f, C_z = %.2f\n', ...
%     tOpt2);

% if (resnorm_1 < resnorm_2)
%     tOpt = tOpt1;
%     
%     theta_x_0 = theta_x;
%     theta_y_0 = theta_y;
%     theta_z_0 = theta_z;
% else
%     tOpt = tOpt2;
%     
%     theta_x_0 = theta_x2;
%     theta_y_0 = theta_y2;
%     theta_z_0 = theta_z2;
% end


camera_parameters = denormaliseProjection(t, T_x);



%% Plot the optimised parameters:
[x_constrained_opt, error_geometric_constrained_opt_median, error_geometric_constrained_opt_mean, R, C] = ...
    computeProjection(tOpt, x, X);

path_save = fullfile('D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters', designer_type, desginer_id, object_id, 'camera_parameters.json');
folder_save = fullfile('D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters', designer_type, desginer_id, object_id);
if (~exist(folder_save, 'dir'))
   mkdir(folder_save); 
end

camera_parameters.width = width;
camera_parameters.dlt_estimated = dlt_estimated;
text = jsonencode(camera_parameters);
fileID = fopen(path_save,'w');
fprintf(fileID,text);
fclose(fileID);
%% Plot 3D figure 1
% plot3Dpoints;
fprintf('tDLT:\n fx=%.2f, fy=%.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
            camera_parameters.dlt_estimated.fx, camera_parameters.dlt_estimated.fy, ...
            camera_parameters.dlt_estimated.u, camera_parameters.dlt_estimated.v,...
            camera_parameters.dlt_estimated.C, camera_parameters.dlt_estimated.up, camera_parameters.dlt_estimated.focal_point);

fprintf('tOpt:\n f=%.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
            camera_parameters.constrained.f, camera_parameters.constrained.u, camera_parameters.constrained.v,...
            camera_parameters.constrained.C, camera_parameters.constrained.up, camera_parameters.constrained.focal_point);

plotOptPoints;

% h = figure(3); 
folder = fullfile(folder, 'pose_estimation');
if (~exist(folder, 'dir'))
    mkdir(folder);
end

save_png_name = fullfile(folder, 'v1_00_cam_param_optimised_points.png');
save_mat_name = fullfile(folder, 'v1_00_cam_param_optimised_points.mat');

save_png_all_name = fullfile(folder, 'v1_00_cam_param_all_points.png');
save_mat_all_name = fullfile(folder, 'v1_00_cam_param_all_points.mat');



save(save_mat_name, 'height', 'width', ...
                    'error_algebraic', 'P', 'K_est','R_est','C_est', ...
                    'theta_x', 'theta_y', 'theta_z', ...
                     'error_geometric_dlt_median', 'error_geometric_constrained_opt_mean', ...
                        'error_geometric_constrained_opt_median', 'error_geometric_constrained_opt_mean');
                    
plotAllPoints;
