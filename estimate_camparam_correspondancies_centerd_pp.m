function [camera_parameters, error_geometric_general_mean_scaled, mat_data] = estimate_camparam_correspondancies_centerd_pp(x_m, width, H, img)
%% Normilize

x_m(:,1:2) = x_m(:,1:2)/width;
x_m(:,1:2) = x_m(:,1:2)*2 - 1; 
x_m(:,2) = x_m(:,2)*-1; %cordinated in webgl -1 (bottom left) matlab 1 bottom left

% x = x_m(indices_opt, :);
% X = H(indices_opt, :);

x = x_m;
X = H;

[x, T_x] = normalise2dpts(x');
x = x';

x_all = x;
X_all = X;

exitflag = 0;

max_num_iter = 1;
iter = 0;

while ((exitflag~=1) && iter < max_num_iter)
    fprintf("iteration: %d of %d\n", iter, max_num_iter);
    iter = iter + 1;
    %% Find initial projection matrix:
    [P, inlierIdx, ~, error_algebraic, distances] = ransac( x_all, X_all, T_x);
      
%     [P, error_algebraic] = dlt(X_all, x_all);
%     inlierIdx = indices_all;
%     distances = geometric_errors(x_all, X_all, P);
    
    fprintf("percentage of inliers: %.2f", length(inlierIdx)/length(x_all));

    [K_est,R_est,C_est] = DecomposeCameraMatrix(P);
    P = P./abs(K_est(3,3));
    K_est = K_est./abs(K_est(3,3));
    [theta_x_0, theta_y_0, theta_z_0, ~, ~, ~, ~ ] = rotm2eulr( R_est );
   
    ransac_initial = saveDLTCameraPatameters(T_x, P);
    thrOptimalityTolerance  = 1e-20; %min(mean(distances(inlierIdx))*(max(K_est(1,1), K_est(2,2))/min(K_est(1,1), K_est(2,2)))^(1/2), 0.2);%(4/5);
     
     
%         thr = min(max(prctile(distances, 90), 1e-2), 1e-2);% max(1e-1, prctile(distances, 70));
%         thr = 2*1e-1;
    thr = prctile(distances, 90);
    ind_optimize = union(find(distances <= thr), inlierIdx);
%         ind_optimize  = sort(ind_optimize);
%         ind_optimize = find(distances <= thr);        
%         ind_optimize = inlierIdx;   
    
    x_inliers = x_all(inlierIdx, :);
    X_inliers = X_all(inlierIdx, :);
    x_opt = x_all(ind_optimize, :);
    X_opt = X_all(ind_optimize, :);
      
    % disp('K_est'); disp(K_est);
    % disp('R_est'); disp(R_est);
    % disp('C_est'); disp(C_est');
    % disp('P'); disp(P);
    % disp('P_decomposed'); K_est(:,4) = 0; disp(K_est*[R_est -R_est*C_est; zeros(1,3) 1]);


     
    %% Update P with general matrix optimisation (11 parameters):
    t0 = [K_est(1,1), K_est(2,2), K_est(1,2), K_est(1,3), K_est(2,3), theta_x_0, theta_y_0, theta_z_0, C_est(1), C_est(2), C_est(3)];
    [ t_general, ~, ~, ~] = optimize_geometric_error_general( t0, X_opt, x_opt, thrOptimalityTolerance);
    P = constructGeneralProjectionMatrix(t_general);
    [K_est,R_est,C_est] = DecomposeCameraMatrix(P);
    P = P./abs(K_est(3,3));
    K_est = K_est./abs(K_est(3,3));
    
    dlt_estimated = saveDLTCameraPatameters(T_x, P);
    
    %% Use gental optimisation with soft constraints to draw the skew to zero and equalize the aspect ratio:
    [tOpt_soft, resnorm_soft, exitflag_soft, output_soft] = ...
        optimize_geometric_error_soft_constraints_center_pp(t_general, X_opt, x_opt, thrOptimalityTolerance);
%   [tOpt_soft, resnorm_soft, exitflag_soft, output_soft] = ...
%         optimize_geometric_error_soft_constraints(t_general, X_opt, x_opt, 1e-6);
    

        
   
    %% Set intial solution for the optimisation:
   
%     view_dir_centered = C_est'/norm(C_est);
%     R_est(3, 1:3) = view_dir_centered;
%     R_est = recenterProjectionMatrix(R_est, C_est);

    
%     u_0 = K_est(1,3);
%     v_0 = K_est(2,3);
%     
% %     u_0 = 0;
% %     v_0 = 0;
%     
%     f_(1) = (K_est(1,1) + K_est(2,2))/2.0;
%     f_(2) = max(K_est(1,1), K_est(2,2));
%     f_(3) = min(K_est(1,1), K_est(2,2));
%     
%     %% Plot the intial parameters:
%     resnorm = zeros(3,1);
%     tOpt_ = zeros(3,9);
%     exitflag = zeros(3,1);
%     output_ = cell(3,1);
%    
%     
%     
%   
% %     camera_parameters_init = denormaliseProjection(t_init, T_x);
% 
%     
%     
%     for i = 1:3
%     [ tOpt_(i,:), resnorm(i), exitflag(i), output_{i}] = ...
%         optimize_geometric_error( f_(i), u_0, v_0, ...
%         theta_x_0, theta_y_0, theta_z_0, ...
%         C_est(1), C_est(2), C_est(3), ...
%         X_opt, x_opt, thrOptimalityTolerance);
%     end
%     
%       
%     % [~, ~, resnorm_all(3), ~, ~] = computeProjection(t_restricted(3,:), x_all, X_all);
%     
%     [~,ind] = min(resnorm);
%     f_0 = f_(ind);
%     t_restricted = tOpt_(ind, :);
%     exitflag = exitflag(ind);
%     output = output_{ind};
    
    
    %% Resticted optimisation (9 parameters):
    
    u_0 = tOpt_soft(4);
    v_0 = tOpt_soft(5);
    f_0 = (tOpt_soft(1) + tOpt_soft(2))/2.0;
    theta_x  = tOpt_soft(6);
    theta_y = tOpt_soft(7);
    theta_z =tOpt_soft(8);
    c_x = tOpt_soft(9);
    c_y = tOpt_soft(10); 
    c_z = tOpt_soft(11);
    
%     fprintf('Opt soft:\n fx=%.2f,fy=%.2f,  rot = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
%            f_0, f_0, theta_x, theta_y, theta_z, c_x, c_y, c_z);
       
    [ t_restricted, resnorm, exitflag, output] = ...
        optimize_geometric_error_restricted_7param( f_0,...
            theta_x, theta_y, theta_z, ...
            c_x, c_y, c_z, ...
            X_opt, x_opt, thrOptimalityTolerance);
%       [ t_restricted, resnorm, exitflag, output] =  optimize_geometric_error_restricted( f_0, u_0, v_0, ...
%             theta_x, theta_y, theta_z, ...
%             c_x, c_y, c_z, ...
%             X_opt, x_opt, thrOptimalityTolerance);
%      
%     fprintf('Opt\n fx=%.2f,fy=%.2f, rot = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
%            t_restricted(1), t_restricted(1), t_restricted(4), t_restricted(5), t_restricted(6), t_restricted(7), t_restricted(8), t_restricted(9));
       
       
    t_init = [f_0, u_0, v_0, theta_x, theta_y, theta_z, c_x, c_y, c_z];
   
end

%% Save to mat files:
global folder_camera_parameters;
global designer_type;
global designer_id;
global object_id;
global view;

% path_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, view, 'camera_parameters_centered_pp.json');
% folder_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, view);
% if (~exist(folder_save, 'dir'))
%    mkdir(folder_save); 
% end


[error_median_general_all, error_mean_general_all, error_std_general_all,...
          error_median_restricted_all, error_mean_restricted_all, error_std_restricted_all] = ...
          evaluate_projection(t_general, t_restricted, x_all, X_all);

[error_median_general_inliers, error_mean_general_inliers, error_std_general_inliers,...
          error_median_restricted_inliers, error_mean_restricted_inliers, error_std_restricted_inliers] = ...
          evaluate_projection(t_general, t_restricted, x_opt, X_opt);
          
% save_mat_file = fullfile(folder_save, 'errors_centered_pp.mat');
      

mat_data.object_id = object_id;
mat_data.designer_id = designer_id;
mat_data.view = view;
mat_data.t_general = t_general;
mat_data.t_restricted = t_restricted;
mat_data.error_median_general_all = error_median_general_all;
mat_data.error_mean_general_all = error_mean_general_all;
mat_data.error_std_general_all = error_std_general_all;
mat_data.error_median_restricted_all = error_median_restricted_all;
mat_data.error_mean_restricted_all = error_mean_restricted_all;
mat_data.error_std_restricted_all = error_std_restricted_all;
mat_data.error_median_general_inliers = error_median_general_inliers;
mat_data.error_mean_general_inliers = error_mean_general_inliers;
mat_data.error_std_general_inliers = error_std_general_inliers;
mat_data.error_median_restricted_inliers = error_median_restricted_inliers;
mat_data.error_mean_restricted_inliers = error_mean_restricted_inliers;
mat_data.error_std_restricted_inliers = error_std_restricted_inliers;
mat_data.width = width;



% save(save_mat_file, ...
%     'object_id', 'designer_id', 'view',... 
%     't_general', 't_restricted', ...
%     'error_median_general_all', 'error_mean_general_all', 'error_std_general_all',...
%     'error_median_restricted_all', 'error_mean_restricted_all', 'error_std_restricted_all',...
%     'error_median_general_inliers', 'error_mean_general_inliers', 'error_std_general_inliers',...
%     'error_median_restricted_inliers', 'error_mean_restricted_inliers', 'error_std_restricted_inliers');      
      
      


%% Save estimated camera parameters to a json file:
camera_parameters.restricted = denormaliseProjection(t_restricted, T_x);
camera_parameters.general = denormaliseProjectionGeneral(t_general, T_x);
camera_parameters.width = width;

global folder_data;
filepath_mat_obj = fullfile(folder_data, [object_id '.mat']);
load(filepath_mat_obj, 'scale', 'centroid');
camera_parameters.object_scale = scale;
camera_parameters.object_centroid = centroid;

% text = jsonencode(camera_parameters);
% fileID = fopen(path_save,'w');
% fprintf(fileID,text);
% fclose(fileID);

%% Display information: 
% fprintf('DLT:\n fx=%.2f, fy=%.2f, skew = %.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
%             ransac_initial.fx, ransac_initial.fy, ...
%             ransac_initial.skew, ...th
%             ransac_initial.u, ransac_initial.v,...
%             ransac_initial.C, -ransac_initial.up, ransac_initial.focal_point);

fprintf('General:\n fx=%.2f, fy=%.2f, skew = %.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
            camera_parameters.general.fx, camera_parameters.general.fy, ...
            camera_parameters.general.skew,...
            camera_parameters.general.u, camera_parameters.general.v,...
            camera_parameters.general.C, camera_parameters.general.up, camera_parameters.general.focal_point);        
        
fprintf('Opt:\n f=%.2f, u = %.2f, v = %.2f, eye = (%.2f, %.2f, %.2f), up = (%.2f, %.2f, %.2f), center = (%.2f, %.2f, %.2f)\n', ...
            camera_parameters.restricted.f, camera_parameters.restricted.u, camera_parameters.restricted.v,...
            camera_parameters.restricted.C, camera_parameters.restricted.up, camera_parameters.restricted.focal_point);
        
%% Plot the optimised parameters:

%% Plot 3D figure 1
% plot3Dpoints;


plotInliersVsAll(img,x_inliers,  x_all,  T_x, error_mean_general_inliers, ...
    error_mean_general_all,width, designer_id, view);

plotOptPoints(x_inliers, X_inliers, T_x, t_general, t_init, t_restricted, width, designer_id, [], view);

% h = figure(3); 
% save_png_name = fullfile(folder, 'v1_00_cam_param_optimised_points.png');
% save_mat_name = fullfile(folder, 'v1_00_cam_param_optimised_points.mat');
% 
% save_png_all_name = fullfile(folder, 'v1_00_cam_param_all_points.png');
% save_mat_all_name = fullfile(folder, 'v1_00_cam_param_all_points.mat');


% save(save_mat_name, 'height', 'width', 'inlierIdx',...
%                     'error_algebraic', 'P', 'T_x', 't_init', 't_restricted', 'exitflag', 'output', ...
%                      'error_geometric_dlt_median', 'error_geometric_dlt_mean', ...
%                      'error_geometric_constrained_opt_median', 'error_geometric_constrained_opt_mean');

global indices_all;              
error_geometric_general_mean_scaled = plotAllPoints(img, indices_all, x_all, X_all, T_x, t_general, t_restricted, width, designer_id, [], view);
disp('done');
end
% save(save_mat_all_name, 'height', 'width', ...
%                     'error_geometric_dlt_median', 'error_geometric_dlt_mean', ...
%                     'error_geometric_constrained_opt_median', 'error_geometric_constrained_opt_mean');

    
%     plot3Dpoints(H, dlt_estimated, indices_all)
%     plot3Dpoints(H, camera_parameters_init.constrained, indices_all)
    
%     close all;