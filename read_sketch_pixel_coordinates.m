function bounding_box = read_sketch_pixel_coordinates(filename)

fileID = fopen(filename, 'r');
text = fscanf(fileID, '%s');
fclose(fileID);
sketch_data = jsondecode(text);

x_min = Inf;
x_max = -Inf;
y_min = Inf;
y_max = -Inf;

it = 0;
for stroke_num = 1:length(sketch_data.strokes)
   stroke = sketch_data.strokes(stroke_num);
   for point_num = 1:length(stroke.points) 
        it = it + 1;
        x(it) =  stroke.points(point_num).x;
        y(it) =  stroke.points(point_num).y;
        
        x_min= min(x_min, x(it));
        x_max= max(x_max, x(it));
        
        y_min= min(y_min, y(it));
        y_max= max(y_max, y(it));
   end
end

x = sort(x);
y = sort(y);



bounding_box = [x_min, x_max, y_min, y_max]

x_min = prctile(x, 0.2);
x_max = prctile(x, 99.8);
y_min = prctile(y, 0.2);
y_max = prctile(y, 99.8);

bounding_box = [x_min, x_max, y_min, y_max]

end