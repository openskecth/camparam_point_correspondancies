function [  ranking, ind_designers, ...
            perspective_ranking,...
            proportions_ranking,...
            line_execution_ranking] = MJ_wobble_surface_ranking()


ind_designers = [1:9 11:15];
% Professional 6
ranking(15).perspective      = 6;
ranking(15).proportions      = 12;
ranking(15).line_execution   = 7;

% Professional 5
ranking(14).perspective      = 3;
ranking(14).proportions      = 6;
ranking(14).line_execution   = 1;

% Professional 4
ranking(13).perspective      = 15;
ranking(13).proportions      = 11;
ranking(13).line_execution   = 12;

% Professional 3
ranking(12).perspective      = 12;
ranking(12).proportions      = 9;
ranking(12).line_execution   = 9;

% Professional 2
ranking(11).perspective      = 11;
ranking(11).proportions      = 10;
ranking(11).line_execution   = 15;

% Professional 1 (was not evaluated)
ranking(10).perspective      = NaN;
ranking(10).proportions      = NaN;
ranking(10).line_execution   = NaN;

% Stundent 9
ranking(9).perspective      = 4;
ranking(9).proportions      = 5;
ranking(9).line_execution   = 4;

% Stundent 8
ranking(8).perspective      = 7;
ranking(8).proportions      = 3;
ranking(8).line_execution   = 6;

% Student 7
ranking(7).perspective      = 2;
ranking(7).proportions      = 2;
ranking(7).line_execution   = 5;

%Stundent 6
ranking(6).perspective      =  9;
ranking(6).proportions      = 14;
ranking(6).line_execution   = 14;

% Student 5
ranking(5).perspective      = 5;
ranking(5).proportions      = 4;
ranking(5).line_execution   = 8;

% Stundent 4
ranking(4).perspective      = 13;
ranking(4).proportions      = 8;
ranking(4).line_execution   = 10;

% Student 3
ranking(3).perspective      = 10;
ranking(3).proportions      = 13;
ranking(3).line_execution   = 11;

% Student 2
ranking(2).perspective      =  8;
ranking(2).proportions      =  7;
ranking(2).line_execution   =  2;

% Student 1
ranking(1).perspective      = 14;
ranking(1).proportions      = 15;
ranking(1).line_execution   = 13;


perspective_ranking = zeros(1, 15);
proportions_ranking = zeros(1, 15);
line_execution_ranking = zeros(1, 15);
for i = 1:15
    perspective_ranking(i)      = ranking(i).perspective;
    proportions_ranking(i)      = ranking(i).proportions;
    line_execution_ranking(i)   = ranking(i).line_execution;
end
perspective_ranking = perspective_ranking(ind_designers);
proportions_ranking = proportions_ranking(ind_designers);
line_execution_ranking = line_execution_ranking(ind_designers);








end