% Evaluation of professional desginers of sketches of the first view of the
% house object:

% for perspective, consistency was regarded (do parallel lines go to the 
% same vanishing points etc.), as well as construction methods (how was the
% middle found, what �scaffolding lines� were used? Etc.) 

% for line execution, three aspects were considered:
% control � is a line that�s supposed to be straight actually straight?)
% weight difference and variation � for clarity�s sake, was line weight used properly? Were construction lines less pronounced than eventual communicative lines?
% quantitative efficiency � how much lines were put down (for both construction and eventual pronunciation) 
% for line execution, three aspects were considered:
% control � is a line that�s supposed to be straight actually straight?)
% weight difference and variation � for clarity�s sake, was line weight used properly? Were construction lines less pronounced than eventual communicative lines?
% quantitative efficiency � how much lines were put down (for both construction and eventual pronunciation) 
%   
% 
% If the ranking position numbers are then seen as scores, and the scores 
% for the three criteria are added, we get a �total score�, which is also 
% marked (in black). The sketches were subsequently ranked from low to high
% score (low score is hence actually a good thing).
% It should be noted that it was overall pretty clear-cut which one was the
% best/or worst for a criterion, but it was very ambiguous to rank the ones
% in between those extremes. This was by no means an exact approach.

function computeCorrealtionWithDesignersEvaluation(errors_mean_general_v1,...
    errors_mean_restricted_v1)

if (~exist('errors_mean_general_v1', 'var') || ...
    ~exist('errors_mean_general_v2', 'var'))
    
    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_mat_files\';
    filename = [folder 'view1_reprojection_errors.mat'];
    load(filename, ...
            'errors_mean_general_v1',...
            'errors_mean_restricted_v1');
end


[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();
object_id = objectsData();

hairdryer_id = 3;
house_id = 4;
potato_id = 7;
wobble_id = 12;


errors_mean_general_rated_objects = [];
errors_mean_restricted_rated_objects = [];

%designer 7 did not draw the house

obj_id = potato_id;

[ranking, ind_designers, ...
          perspective_ranking,...
          proportions_ranking,...
          line_execution_ranking] = MJ_potato_ranking();%MJ_hairdryer_ranking();%MJ_wobble_surface_ranking();%MJ_house_ranking();
disp('potato');
fprintf('min = %d, max = %d\n',  min(perspective_ranking), max(perspective_ranking));
fprintf('min = %d, max = %d\n',  min(proportions_ranking), max(proportions_ranking));
errors_mean_general_rated_objects = [errors_mean_general_rated_objects  errors_mean_general_v1(obj_id, ind_designers)];
errors_mean_restricted_rated_objects = [errors_mean_restricted_rated_objects errors_mean_restricted_v1(obj_id, ind_designers)];  


obj_id = hairdryer_id;

[ranking, ind_designers, ...
          perspective_ranking_,...
          proportions_ranking_,...
          line_execution_ranking_] = MJ_hairdryer_ranking();%MJ_wobble_surface_ranking();%MJ_house_ranking();
perspective_ranking = [perspective_ranking perspective_ranking_];   
proportions_ranking = [proportions_ranking proportions_ranking_];
disp('hairdryer');
fprintf('min = %d, max = %d\n',  min(perspective_ranking_), max(perspective_ranking_));
fprintf('min = %d, max = %d\n',  min(proportions_ranking_), max(proportions_ranking_));
errors_mean_general_rated_objects = [errors_mean_general_rated_objects  errors_mean_general_v1(obj_id, ind_designers)];
errors_mean_restricted_rated_objects = [errors_mean_restricted_rated_objects errors_mean_restricted_v1(obj_id, ind_designers)];  


obj_id = wobble_id;

[ranking, ind_designers, ...
          perspective_ranking_,...
          proportions_ranking_,...
          line_execution_ranking] = MJ_wobble_surface_ranking();%MJ_house_ranking();
perspective_ranking = [perspective_ranking perspective_ranking_];   
proportions_ranking = [proportions_ranking proportions_ranking_];   
disp('wobble');
fprintf('min = %d, max = %d\n',  min(perspective_ranking_), max(perspective_ranking_));
fprintf('min = %d, max = %d\n',  min(proportions_ranking_), max(proportions_ranking_));

errors_mean_general_rated_objects = [errors_mean_general_rated_objects  errors_mean_general_v1(obj_id, ind_designers)];
errors_mean_restricted_rated_objects = [errors_mean_restricted_rated_objects errors_mean_restricted_v1(obj_id, ind_designers)];  


obj_id = house_id;

[ranking, ind_designers, ...
          perspective_ranking_,...
          proportions_ranking_,...
          line_execution_ranking] = MJ_house_ranking();
perspective_ranking = [perspective_ranking perspective_ranking_];   
proportions_ranking = [proportions_ranking proportions_ranking_];  

disp('hosue');
fprintf('min = %d, max = %d\n',  min(perspective_ranking_), max(perspective_ranking_));
fprintf('min = %d, max = %d\n',  min(proportions_ranking_), max(proportions_ranking_));

errors_mean_general_rated_objects = [errors_mean_general_rated_objects  errors_mean_general_v1(obj_id, ind_designers)];
errors_mean_restricted_rated_objects = [errors_mean_restricted_rated_objects errors_mean_restricted_v1(obj_id, ind_designers)];  


        
% total_ranking = (perspective_ranking + proportions_ranking + line_execution_ranking)/3.0;
proportions_perspective_ranking = (perspective_ranking + proportions_ranking )/2.0;

[~, ind_sorted] = sort(errors_mean_general_rated_objects);
table = cell(4,9);
table{2,1} = 'perspective';
table{3,1} = 'proportions';
table{4,1} = 'both';

table{1,2} = 'general, r';
table{1,3} = 'general, p';

table{1,4} = 'general, r_s';
table{1,5} = 'general, p_s';


table{1,6} = 'restricted, r';
table{1,7} = 'restricted, p';

table{1,8} = 'restricted, r_s';
table{1,9} = 'restricted, p_s';

table = computeCorrealtionValues('Pearson', table);
table = computeCorrealtionValues('Spearman',table);



    function table = computeCorrealtionValues(correlation_type_str, table)
       [corr_perspetive_general, p_corr_perspetive_general] = ...
            corr(perspective_ranking', errors_mean_general_rated_objects', 'type', correlation_type_str)
        [corr_proportions_general, p_corr_proportions_general] = ...
            corr(proportions_ranking', errors_mean_general_rated_objects', 'type', correlation_type_str)
%         [corr_line_execution_general, p_corr_line_execution_general] = ...
%             corr(line_execution_ranking', errors_mean_general_rated_objects')
%         [corr_total_general, p_corr_total_general] = ...
%             corr(total_ranking', errors_mean_general_rated_objects')
        [corr_proportions_perspective_general, p_corr_proportions_perspective_general ] = ...
            corr(proportions_perspective_ranking', errors_mean_general_rated_objects', 'type', correlation_type_str)


        if strcmp(correlation_type_str, 'Pearson')
            table{2,2} = corr_perspetive_general;
            table{2,3} = p_corr_perspetive_general;

            table{3,2} = corr_proportions_general;
            table{3,3} = p_corr_proportions_general;

            table{4,2} = corr_proportions_perspective_general;
            table{4,3} = p_corr_proportions_perspective_general;
        else
            table{2,4} = corr_perspetive_general;
            table{2,5} = p_corr_perspetive_general;

            table{3,4} = corr_proportions_general;
            table{3,5} = p_corr_proportions_general;

            table{4,4} = corr_proportions_perspective_general;
            table{4,5} = p_corr_proportions_perspective_general;
        end


        [corr_perspetive_restricted, p_perspetive_restricted]           =...
            corr(perspective_ranking', errors_mean_restricted_rated_objects', 'type', correlation_type_str);
        [corr_proportions_restricted, p_proportions_restricted]         = ...
            corr(proportions_ranking', errors_mean_restricted_rated_objects', 'type', correlation_type_str);
    %     [corr_line_execution_restricted, p_line_execution_restricted]   = ...
    %         corr(line_execution_ranking', errors_mean_restricted_rated_objects')
    %     [corr_total_restricted, p_total_restricted]                     = ...
    %         corr(total_ranking', errors_mean_restricted_rated_objects')
        [corr_proportions_perspective_restricted, p_proportions_perspective_restricted] = ...
            corr(proportions_perspective_ranking', errors_mean_restricted_rated_objects', 'type', correlation_type_str);

        if strcmp(correlation_type_str, 'Pearson')
            table{2,6} = corr_perspetive_restricted;
            table{2,7} = p_perspetive_restricted;

            table{3,6} = corr_proportions_restricted;
            table{3,7} = p_proportions_restricted;

            table{4,6} = corr_proportions_perspective_restricted;
            table{4,7} = p_proportions_perspective_restricted;
        else
            table{2,8} = corr_perspetive_restricted;
            table{2,9} = p_perspetive_restricted;

            table{3,8} = corr_proportions_restricted;
            table{3,9} = p_proportions_restricted;

            table{4,8} = corr_proportions_perspective_restricted;
            table{4,9} = p_proportions_perspective_restricted;
        end
    end













end