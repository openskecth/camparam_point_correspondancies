function c = computeRankingCorrelation(ranking1, ranking2)
    % Find designers that draw both objects:
    ind = (ranking1 > 0) & (ranking2 > 0);
    ranking1 = ranking1(ind);
    ranking2 = ranking2(ind);
    
    c = corr2(ranking1, ranking2);
end

