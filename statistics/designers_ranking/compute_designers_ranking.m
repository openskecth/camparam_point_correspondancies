
function correlation_matrix = ...
            compute_designers_ranking(num_designers, errors_mean_general_inliers,...
                std_general_inliers, ...
                errors_mean_restricted_inliers,...
                std_restricted_inliers,...
                designers_label,...
                objects_id, ...
                view)

    designers_ranking = zeros(num_designers, 12);

    for ind_obj  = 1:12
    object_data_str = objects_id(ind_obj).title;
    [~, ~, ~, ~, ~, ~, ...
    designers_ranking(:,ind_obj)] = plot_average_mean_and_std_per_professional(...
        errors_mean_general_inliers(ind_obj, :), std_general_inliers(ind_obj, :),...
        errors_mean_restricted_inliers(ind_obj, :), std_restricted_inliers(ind_obj, :),...
        designers_label, view, object_data_str);
    end


    num_ranks = sum(designers_ranking > 0, 2);
    total_rank = sum(designers_ranking,2)./num_ranks;
    [total_rank_sorted, ind_accuracy_ranking] = sort(total_rank);

    designers_accuracy_ranking = designers_label(ind_accuracy_ranking,1);

    mean_general_error_per_designer = sum(errors_mean_general_inliers,1)./sum(errors_mean_general_inliers>0,1);
    [sorted_mean_geometric_errors, ind_average_accuracy] = sort(mean_general_error_per_designer );
    designers_accuracy_ranking_naive = designers_label(ind_average_accuracy,1);

    comment = 'designers_accuracy_ranking_naive -- Ranking of designers_label based on the average accuracy. designers_accuracy_ranking --- based on ranking within each category of objects.';

    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
    save([folder 'designers_ranking_based_on_' view '.mat'], ...
        'designers_accuracy_ranking', 'ind_accuracy_ranking', 'total_rank_sorted', ...
        'designers_accuracy_ranking_naive', 'ind_average_accuracy', 'sorted_mean_geometric_errors', 'comment');


    correlation_matrix = zeros(12,12);

    for i = 1:12
        for j = 1:12
            correlation_matrix(i, j) = ...
                computeRankingCorrelation(errors_mean_restricted_inliers(i, :),...
                                          errors_mean_restricted_inliers(j, :));
            correlation_matrix(j, i) = correlation_matrix(i, j); 
        end
    end

end


