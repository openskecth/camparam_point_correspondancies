function [  ranking, ind_designers, ...
            perspective_ranking,...
            proportions_ranking,...
            line_execution_ranking] = MJ_potato_ranking()


ind_designers = [4 5 10:15];
% Professional 6
ranking(15).perspective      = 3;
ranking(15).proportions      = 3;
ranking(15).line_execution   = 2;

% Professional 5
ranking(14).perspective      = 1;
ranking(14).proportions      = 2;
ranking(14).line_execution   = 1;

% Professional 4
ranking(13).perspective      = 8;
ranking(13).proportions      = 4;
ranking(13).line_execution   = 6;

% Professional 3
ranking(12).perspective      = 4;
ranking(12).proportions      = 7;
ranking(12).line_execution   = 3;

% Professional 2
ranking(11).perspective      = 6;
ranking(11).proportions      = 5;
ranking(11).line_execution   = 7;

% Professional 1 (was not evaluated)
ranking(10).perspective      = 2;
ranking(10).proportions      = 1;
ranking(10).line_execution   = 8;

% Stundent 9
ranking(9).perspective      = NaN;
ranking(9).proportions      = NaN;
ranking(9).line_execution   = NaN;

% Stundent 8
ranking(8).perspective      = NaN;
ranking(8).proportions      = NaN;
ranking(8).line_execution   = NaN;

% Student 7
ranking(7).perspective      = NaN;
ranking(7).proportions      = NaN;
ranking(7).line_execution   = NaN;

%Stundent 6
ranking(6).perspective      = NaN;
ranking(6).proportions      = NaN;
ranking(6).line_execution   = NaN;

% Student 5
ranking(5).perspective      = 7;
ranking(5).proportions      = 8;
ranking(5).line_execution   = 4;

% Stundent 4
ranking(4).perspective      = 5;
ranking(4).proportions      = 6;
ranking(4).line_execution   = 5;

% Student 3
ranking(3).perspective      = NaN;
ranking(3).proportions      = NaN;
ranking(3).line_execution   = NaN;

% Student 2
ranking(2).perspective      =  NaN;
ranking(2).proportions      =  NaN;
ranking(2).line_execution   =  NaN;

% Student 1
ranking(1).perspective      = NaN;
ranking(1).proportions      = NaN;
ranking(1).line_execution   = NaN;


perspective_ranking = zeros(1, 15);
proportions_ranking = zeros(1, 15);
line_execution_ranking = zeros(1, 15);
for i = 1:15
    perspective_ranking(i)      = ranking(i).perspective;
    proportions_ranking(i)      = ranking(i).proportions;
    line_execution_ranking(i)   = ranking(i).line_execution;
end
perspective_ranking = perspective_ranking(ind_designers);
proportions_ranking = proportions_ranking(ind_designers);
line_execution_ranking = line_execution_ranking(ind_designers);








end