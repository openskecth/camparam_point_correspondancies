% Evaluation of professional desginers of sketches of the first view of the
% house object:

% for perspective, consistency was regarded (do parallel lines go to the 
% same vanishing points etc.), as well as construction methods (how was the
% middle found, what �scaffolding lines� were used? Etc.) 

% for line execution, three aspects were considered:
% control � is a line that�s supposed to be straight actually straight?)
% weight difference and variation � for clarity�s sake, was line weight used properly? Were construction lines less pronounced than eventual communicative lines?
% quantitative efficiency � how much lines were put down (for both construction and eventual pronunciation) 
% for line execution, three aspects were considered:
% control � is a line that�s supposed to be straight actually straight?)
% weight difference and variation � for clarity�s sake, was line weight used properly? Were construction lines less pronounced than eventual communicative lines?
% quantitative efficiency � how much lines were put down (for both construction and eventual pronunciation) 
%   
% 
% If the ranking position numbers are then seen as scores, and the scores 
% for the three criteria are added, we get a �total score�, which is also 
% marked (in black). The sketches were subsequently ranked from low to high
% score (low score is hence actually a good thing).
% It should be noted that it was overall pretty clear-cut which one was the
% best/or worst for a criterion, but it was very ambiguous to rank the ones
% in between those extremes. This was by no means an exact approach.

function computeCorrealtionWithDesignersEvaluationOfHouse(errors_mean_general_v1,...
    errors_mean_restricted_v1)

if (~exist('errors_mean_general_v1', 'var') || ...
    ~exist('errors_mean_general_v2', 'var'))
    
    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_mat_files\';
    filename = [folder 'view1_reprojection_errors.mat'];
    load(filename, ...
            'errors_mean_general_v1',...
            'errors_mean_restricted_v1');
end


[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();
object_id = objectsData();
house_id = 4;

num_designers = length(designers);

%designer 7 did nto draw the house
ind_designers = [1:6 8:15];
errors_mean_general_v1 = errors_mean_general_v1(house_id, ind_designers);
errors_mean_restricted_v1 = errors_mean_restricted_v1(house_id, ind_designers);

[  ranking, ind_designers, ...
            perspective_ranking,...
            proportions_ranking,...
            line_execution_ranking] = MJ_house_ranking();


total_ranking = (perspective_ranking + proportions_ranking + line_execution_ranking)/3.0;
proportions_perspective_ranking = (perspective_ranking + proportions_ranking )/2.0;

[~, ind_sorted] = sort(errors_mean_general_v1);
table = cell(4,9);
table{2,1} = 'perspective';
table{3,1} = 'proportions';
table{4,1} = 'both';

table{1,2} = 'general, r';
table{1,3} = 'general, p';

table{1,4} = 'general, r_s';
table{1,5} = 'general, p_s';


table{1,6} = 'restricted, r';
table{1,7} = 'restricted, p';

table{1,8} = 'restricted, r_s';
table{1,9} = 'restricted, p_s';

table = computeCorrealtionValues('Pearson', table);
table = computeCorrealtionValues('Spearman',table);

sprintf('%.3f/%.3f & %.3f/%.3f\n', corr_perspetive_general,corr_perspetive_restricted,...
    p_corr_perspetive_general, p_perspetive_restricted)

sprintf('%.3f/%.3f & %.3f/%.3f\n', corr_proportions_general,corr_proportions_restricted,...
    p_corr_proportions_general, p_proportions_restricted)

sprintf('%.3f/%.3f & %.3f/%.3f\n', corr_line_execution_general,corr_line_execution_restricted,...
    p_corr_line_execution_general, p_line_execution_restricted)



corr_proportions_errors_ratios = corr2(proportions_ranking, 1 - errors_mean_general_v1./errors_mean_restricted_v1)

designers_id = cell(length(ind_sorted), 1);
for i = 1:length(ind_designers)
    designers_id{i} = designers(ind_designers(i)).id;
end


ind =  errors_mean_general_v1./errors_mean_restricted_v1 > 1;

close(figure(1));
h = figure(1);
h.Units = 'centimeters';
paper_sz_w = 16;
paper_sz_h = 10;
h.PaperSize  = [paper_sz_w paper_sz_h];
h.Position(3)  = paper_sz_w;
h.Position(4)  = paper_sz_h;
hold off;
hold on;

v = [perspective_ranking proportions_ranking line_execution_ranking];
axis_y_min = min(v);         
axis_y_max = 1.5*max(v);






plot(errors_mean_general_v1(ind_sorted), perspective_ranking(ind_sorted), ':');
plot(errors_mean_general_v1(ind_sorted), proportions_ranking(ind_sorted), '-.');
plot(errors_mean_general_v1(ind_sorted), line_execution_ranking(ind_sorted), '--');
plot(errors_mean_general_v1(ind_sorted), total_ranking(ind_sorted));

ylabel('Sketch ranking by proffesionals');
xlabel('Estimated accuracy with general projection matrix');

legend( 'perspective', ...
        'proportions',...
        'line execution', ...
        'total');
% set(gca, 'xtick', errors_mean_general_v1(ind_sorted), 'xticklabel', designers_id(ind_sorted), 'FontSize', 8);
set(gca, 'FontSize', 8);
xtickangle(90);
axis([-inf inf axis_y_min axis_y_max]);
% set(gca,'xtick',errors_mean_general_v1,'xticklabel',designers(:,1), 'FontSize', 8);
paper_sz_h = 7;
close(figure(2));
h = figure(2);
h.Units = 'centimeters';
h.PaperSize  = [paper_sz_w paper_sz_h];
h.Position(3)  = paper_sz_w;
h.Position(4)  = paper_sz_h;
hold off;
hold on;


[~, ind_sorted] = sort(errors_mean_restricted_v1);

p_perspective = polyfit(errors_mean_restricted_v1(ind_sorted),proportions_perspective_ranking(ind_sorted),1);
y_p_perspective = polyval(p_perspective,errors_mean_restricted_v1(ind_sorted));
plot(errors_mean_restricted_v1(ind_sorted), proportions_perspective_ranking(ind_sorted), 'b');
plot(errors_mean_restricted_v1(ind_sorted), y_p_perspective, 'b:');

% Find the designers with the largest deviations from the line:
diff = abs(proportions_perspective_ranking(ind_sorted) - y_p_perspective);
[sorted_deviations, ind_sorted_line_deviations] = sort(diff);

designers_id(ind_sorted(ind_sorted_line_deviations))
p75 = prctile(sorted_deviations, 75);
ind_large_deviations = diff > p75;

plot(errors_mean_restricted_v1(ind_sorted(ind_large_deviations)),...
         proportions_perspective_ranking(ind_sorted(ind_large_deviations)), 'b*');

% plot(errors_mean_restricted_v1(ind_sorted), perspective_ranking(ind_sorted), 'k:');
% 
% 
% plot(errors_mean_restricted_v1(ind_sorted), proportions_ranking(ind_sorted), 'r-.');
% plot(errors_mean_restricted_v1(ind_sorted), line_execution_ranking(ind_sorted), 'y--');
% plot(errors_mean_restricted_v1(ind_sorted), total_ranking(ind_sorted), 'c' );

ylabel('Sketch ranking by proffesionals');
xlabel('Estimated accuracy with restricted projection matrix');

% legend( 'perspective', ...
%         'proportions',...
%         'line execution', ...
%         'total');

legend( 'perspective + proportions', ...
        'fitted line');

% set(gca,'xtick',errors_mean_restricted_v1(ind_sorted),'xticklabel',designers_id(ind_sorted), 'FontSize', 8);
set(gca, 'FontSize', 8);
xtickangle(90);
axis([-inf inf axis_y_min axis_y_max]);






%% 
close(figure(2));
h = figure(2);
h.Units = 'centimeters';
h.PaperSize  = [paper_sz_w paper_sz_h];
h.Position(3)  = paper_sz_w;
h.Position(4)  = paper_sz_h;
hold off;
hold on;


[~, ind_sorted] = sort(errors_mean_restricted_v1);

p_perspective = polyfit(errors_mean_restricted_v1(ind_sorted),proportions_ranking(ind_sorted),1);
y_p_perspective = polyval(p_perspective,errors_mean_restricted_v1(ind_sorted));
plot(errors_mean_restricted_v1(ind_sorted), proportions_ranking(ind_sorted), 'b');
plot(errors_mean_restricted_v1(ind_sorted), y_p_perspective, 'b:');

% Find the designers with the largest deviations from the line:
diff = abs(proportions_ranking(ind_sorted) - y_p_perspective);
[sorted_deviations, ind_sorted_line_deviations] = sort(diff);

designers_id(ind_sorted(ind_sorted_line_deviations))
p75 = prctile(sorted_deviations, 75);
ind_large_deviations = diff > p75;

plot(errors_mean_restricted_v1(ind_sorted(ind_large_deviations)),...
         proportions_ranking(ind_sorted(ind_large_deviations)), 'b*');

% plot(errors_mean_restricted_v1(ind_sorted), perspective_ranking(ind_sorted), 'k:');
% 
% 
% plot(errors_mean_restricted_v1(ind_sorted), proportions_ranking(ind_sorted), 'r-.');
% plot(errors_mean_restricted_v1(ind_sorted), line_execution_ranking(ind_sorted), 'y--');
% plot(errors_mean_restricted_v1(ind_sorted), total_ranking(ind_sorted), 'c' );

ylabel('Sketch ranking by proffesionals');
xlabel('Estimated accuracy with restricted projection matrix');

% legend( 'perspective', ...
%         'proportions',...
%         'line execution', ...
%         'total');

legend( 'proportions', ...
        'fitted line');

% set(gca,'xtick',errors_mean_restricted_v1(ind_sorted),'xticklabel',designers_id(ind_sorted), 'FontSize', 8);
set(gca, 'FontSize', 8);
xtickangle(90);
axis([-inf inf axis_y_min axis_y_max]);




    function table = computeCorrealtionValues(correlation_type_str, table)
       [corr_perspetive_general, p_corr_perspetive_general] = ...
            corr(perspective_ranking', errors_mean_general_v1', 'type', correlation_type_str)
        [corr_proportions_general, p_corr_proportions_general] = ...
            corr(proportions_ranking', errors_mean_general_v1', 'type', correlation_type_str)
%         [corr_line_execution_general, p_corr_line_execution_general] = ...
%             corr(line_execution_ranking', errors_mean_general_v1')
%         [corr_total_general, p_corr_total_general] = ...
%             corr(total_ranking', errors_mean_general_v1')
        [corr_proportions_perspective_general, p_corr_proportions_perspective_general ] = ...
            corr(proportions_perspective_ranking', errors_mean_general_v1', 'type', correlation_type_str)


        if strcmp(correlation_type_str, 'Pearson')
            table{2,2} = corr_perspetive_general;
            table{2,3} = p_corr_perspetive_general;

            table{3,2} = corr_proportions_general;
            table{3,3} = p_corr_proportions_general;

            table{4,2} = corr_proportions_perspective_general;
            table{4,3} = p_corr_proportions_perspective_general;
        else
            table{2,4} = corr_perspetive_general;
            table{2,5} = p_corr_perspetive_general;

            table{3,4} = corr_proportions_general;
            table{3,5} = p_corr_proportions_general;

            table{4,4} = corr_proportions_perspective_general;
            table{4,5} = p_corr_proportions_perspective_general;
        end


        [corr_perspetive_restricted, p_perspetive_restricted]           =...
            corr(perspective_ranking', errors_mean_restricted_v1', 'type', correlation_type_str)
        [corr_proportions_restricted, p_proportions_restricted]         = ...
            corr(proportions_ranking', errors_mean_restricted_v1', 'type', correlation_type_str)
    %     [corr_line_execution_restricted, p_line_execution_restricted]   = ...
    %         corr(line_execution_ranking', errors_mean_restricted_v1')
    %     [corr_total_restricted, p_total_restricted]                     = ...
    %         corr(total_ranking', errors_mean_restricted_v1')
        [corr_proportions_perspective_restricted, p_proportions_perspective_restricted] = ...
            corr(proportions_perspective_ranking', errors_mean_restricted_v1', 'type', correlation_type_str) 

        if strcmp(correlation_type_str, 'Pearson')
            table{2,6} = corr_perspetive_restricted;
            table{2,7} = p_perspetive_restricted;

            table{3,6} = corr_proportions_restricted;
            table{3,7} = p_proportions_restricted;

            table{4,6} = corr_proportions_perspective_restricted;
            table{4,7} = p_proportions_perspective_restricted;
        else
            table{2,8} = corr_perspetive_restricted;
            table{2,9} = p_perspetive_restricted;

            table{3,8} = corr_proportions_restricted;
            table{3,9} = p_proportions_restricted;

            table{4,8} = corr_proportions_perspective_restricted;
            table{4,9} = p_proportions_perspective_restricted;
        end
    end













end