% professionals = 
% 
%   1�9 struct array with fields:
% 
%     path          --- student identifier
%     experience    --- years of post studies experience
%     object        --- 
%                       .strokesType
%                       .line_present   --- 1 x (number of lines) mask if the
%                                           type of line is present
%                       .line_type_count --- number of strokes with this
%                       label
%                       .statistics     --- 
function [folder_home, professionals, num_professionals] = professionalsData()
    folder_home = 'D:\Projects\SketchItProject\CollectedSketches\SKETCHES\professionals_clean';
    folder_home_rel = '.\professionals_clean';
     for i = 1:6
        professionals(i).folder_home = folder_home;
        professionals(i).folder_home_rel = folder_home_rel;
     end
    
    professionals(1).name = 'arjati'; %'Professional3'
    professionals(2).name = 'dstle'; %'Professional1'
    professionals(3).name = 'Markerstift'; %'Professional2'
    professionals(4).name = 'seavibe'; %'Professional4'
    professionals(5).name = 'Sketchables85949'; %'Professional6'
    professionals(6).name = 'SketchR'; %'Professional5'

    num_professionals = length(professionals);
        
    professionals(1).experience = 6; %'>5';
    professionals(2).experience = 5; %'2-5';
    professionals(3).experience = 5; %2-5';
    professionals(4).experience = 6; %'>5';
    professionals(5).experience = 7; %'>10';
    professionals(6).experience = 6; %'>5';
   
    professionals(1).designer_type = 'professionals'; 
    professionals(2).designer_type = 'professionals'; 
    professionals(3).designer_type = 'professionals'; 
    professionals(4).designer_type = 'professionals'; 
    professionals(5).designer_type = 'professionals';
    professionals(6).designer_type = 'professionals';
    
    professionals(2).id = 'Professional1'; %'2-5';
    professionals(3).id = 'Professional2'; %2-5';
    professionals(1).id = 'Professional3'; %'>5';    
    professionals(4).id = 'Professional4'; %'>5';
    professionals(6).id = 'Professional5'; %'>5';
    professionals(5).id = 'Professional6'; %'>10';
    
    professionals(1).objects_complexity         = [1 4 4 4 4 4 4 4 2 4 4 4];%'arjati';
    professionals(1).objects_animation_useful   = [5 5 5 5 5 5 5 5 5 5 5 5];%'arjati';
    professionals(1).drawing_interface_easy = 3;
    professionals(1).similar_task = "Once per week";
    professionals(1).approximates_from_imagination = 2;
    professionals(1).tablet_experience = ">5";
    professionals(1).drawign_from_observation = 4;
    professionals(1).drawign_from_imagination = 4;
    professionals(1).tools = "Digital tools";
    
    professionals(2).objects_complexity         = [1 2 4 4 1 3 3 4 3 4 4 3];%'arjati';
    professionals(2).objects_animation_useful   = [5 4 2 5 5 4 4 3 3 4 4 5];%'arjati';
    professionals(2).drawing_interface_easy = 2;
    professionals(2).similar_task = "Every day";
    professionals(2).approximates_from_imagination = 3;
    professionals(2).tablet_experience = ">5";
    professionals(2).drawign_from_observation = 4;
    professionals(2).drawign_from_imagination = 5;
    professionals(2).tools = "Pen";
    professionals(2).comment = "The objects were a good range of complexity and forms, I think my biggest point for improvement would be the interface. I felt very restricted by having an interface with no redo, canvas rotate, zoom and eraser tools, and felt that the drawings suffered as a result. Being able to zoom/pan/rotate to get a line or ellipse spot on is really important in digital sketching as a fixed orientation makes certain lines very uncomfortable to manage. ";
    
    professionals(3).objects_complexity         = [2 3 3 5 2 3 3 4 4 3 4 2];
    professionals(3).objects_animation_useful   = [5 2 2 2 4 5 4 2 4 3 3 4];
    professionals(3).drawing_interface_easy = 2 ;
    professionals(3).similar_task = "Once per month";
    professionals(3).approximates_from_imagination = 2;
    professionals(3).tablet_experience = "2-5";
    professionals(3).drawign_from_observation = 3;
    professionals(3).drawign_from_imagination = 4;
    professionals(3).tools = "Digital tools";
    professionals(3).comment = "because the flange is made up out of many round shapes an elipses, the exercise focussed more on motor skills and coordination and was harder to perform with this drawing interface";
    
    professionals(4).objects_complexity         = [3 2 3 5 3 3 3 4 3 4 5 3];
    professionals(4).objects_animation_useful   = [5 3 4 3 5 5 5 4 3 5 5 5];
    professionals(4).drawing_interface_easy = 2 ;
    professionals(4).similar_task = "Once per month";
    professionals(4).approximates_from_imagination = 3;
    professionals(4).tablet_experience = "2-5";
    professionals(4).drawign_from_observation = 3;
    professionals(4).drawign_from_imagination = 5;
    professionals(4).tools = "Pencils";
    professionals(4).comment = "";
    
    
    professionals(5).objects_complexity       = [1 2 4 2 1 4 1 4 1 3 3 2];
    professionals(5).objects_animation_useful = [4 3 4 4 3 4 5 4 4 5 4 5];
    professionals(5).drawing_interface_easy = 3;
    professionals(5).similar_task = "Once per week";
    professionals(5).approximates_from_imagination = 4;
    professionals(5).tablet_experience = ">5";
    professionals(5).drawign_from_observation = 5;
    professionals(5).drawign_from_imagination = 5;
    professionals(5).tools = "Pencils";
    professionals(5).comment = "";
    
    
    professionals(6).objects_complexity       = [3 2 4 5 2 4 4 4 4 3 4 4];
    professionals(6).objects_animation_useful = [4 4 2 5 5 2 5 3 3 3 3 4];
    % Mixer: Most difficult&lifelike one. Very different curves and shapes from the other objects. Nice test.
   %      Hatching for shadows is not optimal it is not responsive enough. 
%     Not always happy with the shading, but drawings still convey shape info okay
    professionals(6).drawing_interface_easy = 2;
    professionals(6).similar_task = "Once per week";
    professionals(6).approximates_from_imagination = 3;
    professionals(6).tablet_experience = ">5";
    professionals(6).drawign_from_observation = 3;
    professionals(6).drawign_from_imagination = 4;
    professionals(6).tools = "Digital tools";
    professionals(6).comment = "Have done drawing mostly for product design, also mostly using analog tools: fineliners, pencils, copic markers.\r\n\r\nCurrently (past 5yrs) I use drawings professionally outside product design, but more as a facilitation/communication/activation tool: Visual Thinking at my current job (www.flatland.agency)\r\nBoth with analog & digital tools.";
   
    for i = 1:6
        professionals(i).objects = 1:12;
    end
    
    for i = 1:6
        professionals(i).objects_v2 = 1:12;
    end
    
    ind = [2 3 1 4 6 5];
    professionals(1, :) = professionals(ind);
end