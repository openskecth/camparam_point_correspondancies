function [mean_general_inliers_designers_all,...
            mean_restricted_inliers_designers_all] = ...
        average_errors_on_all_sketches_per_designer(errors_mean_general_inliers_v1, ...
                    errors_mean_general_inliers_v2,...
                    errors_mean_restricted_inliers_v1,...
                    errors_mean_restricted_inliers_v2)
                
num_designers = 15;
mean_general_inliers_designers_all = zeros(1,num_designers);  
mean_general_inliers_v1= zeros(1,num_designers);  
mean_general_inliers_v2= zeros(1,num_designers);  

for designer_num = 1:num_designers
    
    e1 = errors_mean_general_inliers_v1(:, designer_num);
    e2 = errors_mean_general_inliers_v2(:, designer_num);
    
    for object_num = 1:12
        
        mean_general_inliers_designers_all(:, designer_num) = ...
            (sum(e1) +sum(e2))/ (sum(e1>0) + sum(e2>0)); 
        mean_general_inliers_v1(:, designer_num) = ...
            (sum(e1))/(sum(e1>0)); 
    end
end

mean_restricted_inliers_designers_all = zeros(1,num_designers);                
for designer_num = 1:num_designers
    
    e1 = errors_mean_restricted_inliers_v1(:, designer_num);
    e2 = errors_mean_restricted_inliers_v2(:, designer_num);
    
    for object_num = 1:12
        
        mean_restricted_inliers_designers_all(:, designer_num) = ...
            (sum(e1) +sum(e2))/ (sum(e1>0) + sum(e2>0)); 
        
    end
end


end