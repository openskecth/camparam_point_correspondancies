function plot_object_designer_accuracy_track(inaccuracy)
    close(figure(7));
    figure(7);
    hold on;

    % Load students information:
    [~, designers(1, 1:9), ~] = studentsData();
    [~, designers(1, 10:15), ~] = professionalsData();

    
    for i = 1:length(designers)
        ind_objects = sort(designers(i).objects);
        plot(ind_objects, inaccuracy(ind_objects, i), '*-');
    end
    hold off
end