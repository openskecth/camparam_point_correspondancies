%  plot_errorbars(designers, val1, val2, title_str, ylabel_str,...
%     legend_str_val1, legend_str_val2)
% 
% 

function plot_errorbars(designers, val1, val2, std_val1, std_val2, title_str, ylabel_str,...
    legend_str_val1, legend_str_val2, axis_y_min, axis_y_max)

if (~exist('axis_y_min', 'var'))
      [axis_y_min, axis_y_max] = compute_axis_limits([val1, val2], ...
                                 [std_val1, std_val2]);
end


num_designers = length(designers);
errorbar(1:num_designers, val1, std_val1, 'b');
hold on;
errorbar(1:num_designers, val2, std_val2, 'r');

mean_values1 = mean(val1);
mean_values2 = mean(val2);

plot([1:num_designers], ones(1,num_designers)*mean_values1, 'b:');
plot([1:num_designers], ones(1,num_designers)*mean_values2, 'r:');

title(title_str);
ylabel(ylabel_str);

l = legend(legend_str_val1, legend_str_val2,...
 ['mean ' legend_str_val1 sprintf(' = %.3f', mean_values1)], ...
 ['mean ' legend_str_val2 sprintf(' = %.3f', mean_values2)]);
l.FontSize = 7;
set(gca,'xtick',1:num_designers,'xticklabel',designers(:,1), 'FontSize', 8);
xtickangle(90);
axis([-inf inf axis_y_min axis_y_max]);

end