% function plot_average_mean_and_std_per_object(errors_mean_general_inliers, std_general_inliers,...
%     errors_mean_restricted_inliers, std_restricted_inliers,...
%     objects_id, view)

function [mean_general_error_per_object,    mean_restricted_error_per_object, ...
          mean_std_general_per_object,      mean_std_restricted_per_object] = ...
                plot_average_mean_and_std_per_object(errors_mean_general_inliers, std_general_inliers,...
                    errors_mean_restricted_inliers, std_restricted_inliers,...
                    objects_id, view)

    %Creare cell array o dobjects names to use in the figure labels:
    objects = cell(length(objects_id), 1);
    for i = 1:length(objects_id)
        objects{i} = objects_id(i).title;
    end
    
    num_objects= length(objects_id);
    dim = 2;
    
    % Compute average among non-zero elements:
    mean_general_error_per_object     = sum(errors_mean_general_inliers,dim)./...
                                          sum(errors_mean_general_inliers>0,dim);
    mean_std_general_per_object       = sum(std_general_inliers,dim)./...
                                          sum(std_general_inliers>0,dim);
    mean_restricted_error_per_object  = sum(errors_mean_restricted_inliers,dim)./...
                                          sum(errors_mean_restricted_inliers>0,dim);
    mean_std_restricted_per_object    = sum(std_restricted_inliers,dim)./...
                                          sum(std_restricted_inliers>0,dim);

    % Transpose:
    mean_general_error_per_object       = mean_general_error_per_object';
    mean_std_general_per_object         = mean_std_general_per_object';
    mean_restricted_error_per_object    = mean_restricted_error_per_object';
    mean_std_restricted_per_object      = mean_std_restricted_per_object';
    
                                      
    stds_of_means_general       = zeros(1, num_objects);
    stds_of_means_restricted    = zeros(1, num_objects);
    for i = 1:num_objects
        objects_indices                = find(errors_mean_restricted_inliers(:,i)>0);
        stds_of_means_general(i)        = std(errors_mean_general_inliers(objects_indices,i));
        stds_of_means_restricted(i)     = std(errors_mean_restricted_inliers(objects_indices,i));
    end
    
    %% Plot Average over all designers who sketched the object -- Mean errors and mean standard deviations
    close(figure(1));
    h = figure(1);
    h.Units = 'centimeters';
    paper_sz_w = 16;
    paper_sz_h = 8;
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
    title_str = ['Average over all designers who sketched the object' ' -- inliers -- ' view];
    ylabel_str = 'Mean errors and mean standard deviations';
    legend_str_val1 = 'general';
    legend_str_val2 = 'restricted';
    
    [axis_y_min, axis_y_max] = ...
            compute_axis_limits([mean_general_error_per_object, mean_restricted_error_per_object], ...
                                [mean_std_general_per_object, mean_std_restricted_per_object]);
                             
    plot_errorbars(objects, ...
        mean_general_error_per_object, mean_restricted_error_per_object,...
        mean_std_general_per_object, mean_std_restricted_per_object,...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\errors_average_over_all_designers_for_each_object';
    filename_str = 'Mean_errors_and_mean_standard_deviations';
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');
   
%     hold off;
%     errorbar([1:num_objects], mean_general_error_per_object,  mean_std_general_per_object, 'b');
%     hold on;
%     errorbar([1:num_objects], mean_restricted_error_per_object, mean_std_restricted_per_object, 'r:');
%     
%     title('Mean of geometric errors: inliers');
%     ylabel('Mean errors');
%     legend('general', 'restricted');
%     set(gca,'xtick',[1:num_objects],'xticklabel',objects_id)
    
    
%     [axis_y_min, axis_y_max] = compute_axis_limits([mean_general_error_per_object mean_restricted_error_per_object], ...
%         [mean_std_general_per_object mean_std_restricted_per_object]);
%     
%     
%     axis([-inf inf axis_y_min axis_y_max])
%     xtickangle(90);
%     hold off;
    
   %% Plot Average over all designers who sketched the object -- Mean errors and standard deviations of mean errors
    close(figure(1));
    h = figure(1);
    h.Units = 'centimeters';
    paper_sz_w = 16;
    paper_sz_h = 8;
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
    title_str = ['Average over all designers who sketched the object' ' -- inliers -- ' view];
    ylabel_str = 'Mean errors and standard deviations of mean errors';
    legend_str_val1 = 'general';
    legend_str_val2 = 'restricted';
    
    [axis_y_min, axis_y_max] = ...
            compute_axis_limits([mean_general_error_per_object, mean_restricted_error_per_object], ...
                                [stds_of_means_general, stds_of_means_restricted]);
                             
    plot_errorbars(objects, ...
        mean_general_error_per_object, mean_restricted_error_per_object,...
        stds_of_means_general, stds_of_means_restricted,...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\errors_average_over_all_designers_for_each_object';
    filename_str = 'Mean_errors_and_standard_deviations_of_mean_errors';
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');
   %% Plot (Sorted by general) the mean errors in sketches per designer and standard deviations of mean errors in sketches:    

    close(figure(5));
    h = figure(5);
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
       
    ylabel_str = 'Mean errors and standard deviations of means';
    
    [~, ind] = sort(mean_general_error_per_object);
   
    plot_errorbars(objects(ind), ...
        mean_general_error_per_object(ind), mean_restricted_error_per_object(ind),...
        stds_of_means_general(ind), stds_of_means_restricted(ind),...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    filename_str = 'Mean_errors_and_standard_deviations_of_means_sorted_general';
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');

    
    %% Plot (Sorted by restricted) the mean errors in sketches per designer and standard deviations of mean errors in sketches:    

    close(figure(6));
    h = figure(6);
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
       
    ylabel_str = 'Mean errors and standard deviations of means';
    
    [~, ind] = sort(mean_restricted_error_per_object);
   
    plot_errorbars(objects(ind), ...
        mean_general_error_per_object(ind), mean_restricted_error_per_object(ind),...
        stds_of_means_general(ind), stds_of_means_restricted(ind),...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    filename_str = 'Mean_errors_and_standard_deviations_of_means_sorted_restricted';
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');
   
    %%
%     close(figure(2));
%     h = figure(2);
%     h.Units = 'centimeters';
%     paper_sz_w = 16;
%     paper_sz_h = 8;
%     h.PaperSize  = [paper_sz_w paper_sz_h];
%     h.Position(3)  = paper_sz_w;
%     h.Position(4)  = paper_sz_h;
%     
%     title_str = 'All designers average';
%     plot_three_figures( mean_general_error_per_object, mean_restricted_error_per_object, ...
%             mean_std_general_per_object, mean_std_restricted_per_object ,...
%             stds_of_means_general, stds_of_means_restricted, title_str, objects);
    
    %% Save
%     folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
   
    filename= [folder '_average_over_designers_per_object_' view  '.mat'];
    save(filename, ...
        'mean_general_error_per_object', 'mean_restricted_error_per_object',...
        'stds_of_means_general', 'stds_of_means_restricted',...
        'mean_std_general_per_object', 'mean_std_restricted_per_object')
    
    
    
    
%     %% Plot sorted values:
%     [mean_general_error_per_object, ind] = sort(mean_general_error_per_object);
%     
%     mean_restricted_error_per_object = mean_restricted_error_per_object(ind);
%     mean_std_general_per_object = mean_std_general_per_object(ind);
%     mean_std_restricted_per_object = mean_std_restricted_per_object(ind);
%     stds_of_means_general = stds_of_means_general(ind);
%     stds_of_means_restricted = stds_of_means_restricted(ind);
%     
%     close(figure(4));
%     h_sorted = figure(4);
%     plot_three_figures( mean_general_error_per_object, mean_restricted_error_per_object, ...
%             mean_std_general_per_object, mean_std_restricted_per_object ,...
%             stds_of_means_general, stds_of_means_restricted, 'All designers average sorted', objects_id(ind));    
%     
%      %% Save
%     saveas(h_sorted , [folder view '_average_over_designers_per_object_sorted.pdf']);
        
   
end




