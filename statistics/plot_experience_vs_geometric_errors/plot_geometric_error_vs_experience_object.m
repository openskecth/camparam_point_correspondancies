% function   [errors_mean_general_inliers, errors_mean_restricted_inliers, std_general_inliers, std_restricted_inliers] = ...
%     plot_geometric_error_vs_experience_object(folder_camera_parameters, object_id, designers, do_plot)

% Output:
%   errors_mean_general_inliers --- 1 x num_designers
%   errors_mean_restricted_inliers --- 1 x num_designers
%   std_general_inliers --- 1 x num_designers
%   std_restricted_inliers --- 1 x num_designers

% Input:
% 
%   
% 

% function   [errors_mean_general_inliers, errors_mean_restricted_inliers, std_general_inliers, std_restricted_inliers, bounding_box] = ...
function   [errors_mean_general, errors_mean_restricted,...
            std_general, std_restricted,...
            bounding_box] = ...
                plot_geometric_error_vs_experience_object(folder_camera_parameters, object_id, designers, view, points_set,  do_plot)

    % Bounding box for drawings
    x_min = 0;
    x_max = 0;
    y_min = 0;
    y_max = 0;
    
    
    if (~exist('do_plot', 'var'))
        do_plot = false;
    end
    
    if (~exist('points_set', 'var'))
        points_set = 'all';
    end
    
     
     
     
    close all;

    hNormilisedPoints = figure(2);
    axis equal 
    grid off
    hold on;
    
    hObjectSketch = figure(3);
    
    num_designers = length(designers);

    errors_mean_general     = zeros(1, num_designers );
    errors_mean_restricted  = zeros(1, num_designers );
    std_general             = zeros(1, num_designers );
    std_restricted          = zeros(1, num_designers );
    errors_median_restricted= zeros(1, num_designers );
    errors_median_general   = zeros(1, num_designers );
    
%     errors_mean_general_all         = zeros(1, num_designers );
%     errors_mean_restricted_all      = zeros(1, num_designers );
%     std_general_all                 = zeros(1, num_designers );
%     std_restricted_all              = zeros(1, num_designers );
%     errors_median_general_all       = zeros(1, num_designers );
%     errors_median_restricted_all    = zeros(1, num_designers );
%     
%     errors_mean_general_all         = zeros(1, num_designers );
%     errors_mean_restricted_all      = zeros(1, num_designers );
%     std_general_all                 = zeros(1, num_designers );
%     std_restricted_all              = zeros(1, num_designers );
%     errors_median_general_all       = zeros(1, num_designers );
%     errors_median_restricted_all    = zeros(1, num_designers );

    for i = 1:num_designers 
        designer_id = designers{i,1};
        designer_type = designers{i,2};
        
%         hObjectSketch = figure(3);
        eval([object_id '_3D']);
        if (strcmp(view, 'view1'))
            eval([designer_id '_' object_id '_v1_points']);
            disp([designer_id '_' object_id '_v1_points']);
            v = 'v1';
        else
            eval([designer_id '_' object_id '_v2_points']);
            disp([designer_id '_' object_id '_v2_points']);
            v = 'v2';
        end
        
%         close(hObjectSketch);
        
        x_m(:,1:2) = x_m(:,1:2)/width;
        x_m(:,1:2) = x_m(:,1:2)*2 - 1; 
        x_m(:,2) = x_m(:,2)*-1; %cordinated in webgl -1 (bottom left) matlab 1 bottom left

        x = x_m(indices_opt, :);
        X = H(indices_opt, :);
        [x, T_x] = normalise2dpts(x');
        x = x';
        
        if do_plot
            h = figure(hNormilisedPoints);
            h.Units = 'centimeters';
            paper_sz_w = 7;
            paper_sz_h = 16;
            h.PaperSize  = [paper_sz_w paper_sz_h];
            h.Position(3)  = paper_sz_w;
            h.Position(4)  = paper_sz_h;

            subplot(2,1,1);
            hold on;
            axis equal
            plot(x(:,1),x(:,2));
        end
        x_min = min(x_min, min(x(:,1)));
        x_max = max(x_max, max(x(:,1)));
        y_min = min(y_min, min(x(:,2)));
        y_max = max(y_max, max(x(:,2)));
    
        
        area = (x_max - x_min)*...
            (y_max-y_min);
        f = sqrt(400.0*400.0/area);
        
        if do_plot
            subplot(2,1,2);
            hold on;
            axis equal
            plot(x(:,1)*f,x(:,2)*f);
        end
        
        folder_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, view);
        save_mat_file = fullfile(folder_save, 'errors.mat');
        errors = load(save_mat_file);

        % Mean and median on the all points marked:
        if strcmp(points_set, 'all')
            errors_mean_general(i) = errors.error_mean_general_all*f;
            errors_mean_restricted(i) = errors.error_mean_restricted_all*f;
            errors_median_general(i) = errors.error_median_general_all*f;
            errors_median_restricted(i) = errors.error_median_restricted_all*f;
            std_general(i) = errors.error_std_general_all*f;
            std_restricted(i) = errors.error_std_restricted_all*f;
        end
        % Mean and median on the inliers:
        if strcmp(points_set, 'inliers')
            errors_mean_general(i) = errors.error_mean_general_inliers*f;
            errors_mean_restricted(i) = errors.error_mean_restricted_inliers*f;
            errors_median_general(i) = errors.error_median_general_inliers*f;
            errors_median_restricted(i) = errors.error_median_restricted_inliers*f;
            std_general(i) = errors.error_std_general_inliers*f;
            std_restricted(i) = errors.error_std_restricted_inliers*f;
        end
        
        % Mean and median on the inliers:
        if strcmp(points_set, 'common')
            errors_mean_general(i) = errors.error_mean_general_common*f;
            errors_mean_restricted(i) = errors.error_mean_restricted_common*f;
            errors_median_general(i) = errors.error_median_general_common*f;
            errors_median_restricted(i) = errors.error_median_restricted_common*f;
            std_general(i) = errors.error_std_general_common*f;
            std_restricted(i) = errors.error_std_restricted_common*f;
        end
    end
    if do_plot
        saveas(h, fullfile(folder_camera_parameters, [object_id '_' view '_normalisation.png']));
    end
    bounding_box = [x_min, x_max, y_min, y_max];
    object_name = strrep(object_id, '_', ' ');
    
   
    %% Plot all in one:
    if do_plot
        %   Select the unified coordinates for teh subplots:
            axis_y_min = min([errors_mean_general_all - std_general_all ...
                              errors_mean_restricted_all - std_restricted_all ...
                              errors_median_general_all - std_general_all ...
                              errors_median_restricted_all - std_restricted_all])
            axis_y_min = max(axis_y_min, -0.1);              
            axis_y_max = max([errors_mean_general_all + std_general_all ...
                              errors_mean_restricted_all + std_restricted_all ...
                              errors_median_general_all + std_general_all ...
                              errors_median_restricted_all + std_restricted_all])
            axis_y_max = 2*min(axis_y_max, 0.1);


            h = figure(1);
            h.Units = 'centimeters';
            h.PaperSize  = [16 18];
            h.Position(3)  = 16;
            h.Position(4)  = 18;

            d_x = 2; d_y = 2;

            subplot_errors([d_x, d_y, 1], num_designers, ...
                errors_mean_general_all, errors_mean_restricted_all, ...
                std_general_all, std_restricted_all, ...
                'Mean of geometric errors, all points', designers, object_name, ...
                axis_y_min, axis_y_max);

            subplot_errors([d_x, d_y, 2], num_designers, ...
                errors_mean_general_inliers, errors_mean_restricted_inliers, ...
                std_general_inliers, std_restricted_inliers,...
                'Mean of geometric errors, inliers', designers, object_name, ...
                axis_y_min, axis_y_max);

            subplot_errors([d_x, d_y, 3], num_designers, ...
                errors_median_general_all, errors_median_restricted_all, ...
                std_general_all, std_restricted_all,...
                'Median of geometric errors, all points', designers, object_name, ...
                axis_y_min, axis_y_max);

            subplot_errors([d_x, d_y, 4], num_designers, ...
                errors_median_general_inliers, errors_median_restricted_inliers, ...
                std_general_inliers, std_restricted_inliers,...
                'Median of geometric errors, inliers', designers, object_name, ...
                axis_y_min, axis_y_max);


            saveas(h, ['D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\errors_designer_object\errors_distribution_among_designers_for_' object_id  '_' v '.png']);
            saveas(h, ['D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\errors_designer_object\errors_distribution_among_designers_for_' object_id  '_' v '.pdf']);
    end
    %% Plot means and medians deviation separately
 if (do_plot)
    figure(3);
    d_x = 2; d_y = 2;
    subplot(d_x, d_y, 1);
    plot([1:num_designers], errors_mean_general_all, 'b');
    hold on;
    plot([1:num_designers], errors_mean_restricted_all, 'r:');
    title('Mean of geometric errors, all points');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);

    subplot(d_x, d_y, 2);
    plot([1:num_designers], errors_mean_general_inliers, 'b');
    hold on;
    plot([1:num_designers], errors_mean_restricted_inliers, 'r:');
    title('Mean of geometric errors, inliers');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);

    subplot(d_x, d_y, 3);
    plot([1:num_designers], errors_median_general_all, 'b');
    hold on;
    plot([1:num_designers], errors_median_restricted_all, 'r:');
    title('Median of geometric errors, all points');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers], 'xticklabel', designers(:,1))
    xtickangle(90);


    subplot(d_x, d_y, 4);
    plot([1:num_designers], errors_median_general_inliers, 'b');
    hold on;
    plot([1:num_designers], errors_median_restricted_inliers, 'r:');
    title('Median of geometric errors, inliers');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers], 'xticklabel', designers(:,1))
    xtickangle(90);
    
    
    %% Standard deviation:
    figure(4);
    d_x = 1; d_y =2;
    subplot(d_x, d_y, 1);
    plot([1:num_designers], std_general_all, 'b');
    hold on;
    plot([1:num_designers], std_restricted_all, 'r:');
    title('All points: standard deviations');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);
    
    subplot(d_x, d_y, 2);
    plot([1:num_designers], std_general_inliers, 'b');
    hold on;
    plot([1:num_designers], std_restricted_inliers, 'r:');
    title('Inliers: standard deviations');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);
    end
end


function supblot_size = subplot_errors(fig_num, num_designers, ...
        values_general, values_restricted, ...
        std_values_general, std_values_restricted,...
        title_str, designers, object_name,...
        axis_y_min, axis_y_max)
    d_x = fig_num(1);
    d_y = fig_num(2);
    n   = fig_num(3);
    
    hs = subplot(d_x, d_y, n);
    hold on;
    mean_values_general = mean(values_general);
    std_of_mean_of_values_general  = std(values_general);
    patch('vertices', [1, mean_values_general-std_of_mean_of_values_general; ...
                           1, mean_values_general+std_of_mean_of_values_general; ...
                           num_designers, mean_values_general+std_of_mean_of_values_general; ...
                           num_designers, mean_values_general-std_of_mean_of_values_general], ...
          'faces', [1, 2, 3, 4], ...
          'FaceColor', 'b', ...
          'FaceAlpha', 0.2, ...
           'LineStyle', 'none');
    
    mean_values_restricted = mean(values_restricted);
    std_of_mean_of_values_restricted  = std(values_restricted);
    
    
    patch('vertices', [1, mean_values_restricted-std_of_mean_of_values_restricted; ...
                           1, mean_values_restricted+std_of_mean_of_values_restricted; ...
                           num_designers, mean_values_restricted+std_of_mean_of_values_restricted; ...
                           num_designers, mean_values_restricted-std_of_mean_of_values_restricted], ...
          'faces', [1, 2, 3, 4], ...
          'FaceColor', 'r', ...
          'FaceAlpha', 0.2, ...
           'LineStyle', 'none');
    
    errorbar([1:num_designers], values_general, std_values_general, 'b');

    plot([1:num_designers], ones(1,num_designers)*mean_values_general, 'b:');
    
    
    errorbar([1:num_designers], values_restricted, std_values_restricted, 'r');
    plot([1:num_designers], ones(1,num_designers)*mean_values_restricted, 'r:');
    
    title(title_str);
    ylabel(['Error ' object_name]);
    l = legend('std general', 'std restricted', 'general', sprintf('general mean = %.3f',mean_values_general), ...
            'restricted', sprintf('restricted mean = %.3f',mean_values_restricted));
    l.FontSize = 7;
    set(gca,'xtick',[1:num_designers], 'xticklabel', designers(:,1))
    axis([-inf inf axis_y_min axis_y_max])
    xtickangle(90);
    
    
    set(gca, 'FontSize', 8);
    
%     hs.Units = 'centimeters';
%     x0 = hs.Position(1); 
%     y0 = hs.Position(2); 
%     width = 7; 
%     height = 7;
%     set(hs, 'position',[x0,y0,width,height])
 hs.Units = 'centimeters';
      supblot_size(1) = hs.Position(3);
      supblot_size(2) = hs.Position(4);
end