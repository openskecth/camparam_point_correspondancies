function [axis_y_min, axis_y_max] = compute_axis_limits(errors_mean, stds)

    axis_y_min = min([errors_mean - stds]);
    axis_y_min = max(axis_y_min, -0.1)   ;           
    axis_y_max = max([errors_mean + stds]);
    axis_y_max = 1.5*min(axis_y_max, 0.1);
    
end