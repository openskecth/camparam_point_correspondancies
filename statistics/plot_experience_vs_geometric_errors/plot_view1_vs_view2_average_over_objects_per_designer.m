function h = plot_view1_vs_view2_average_over_objects_per_designer(errors_mean_general_inliers, errors_mean_general_inliers_v2, ...
                    errors_mean_restricted_inliers,  errors_mean_restricted_inliers_v2, ...
                    std_general_inliers, std_general_inliers_v2,...
                    std_restricted_inliers, std_restricted_inliers_v2, designers)
    
    mean_general_error_per_designer_v1 = sum(errors_mean_general_inliers,1)./sum(errors_mean_general_inliers>0,1);
    mean_std_general_per_designer_v1 = sum(std_general_inliers,1)./sum(std_general_inliers>0,1);
    
    mean_restricted_error_per_designer_v1 = sum(errors_mean_restricted_inliers,1)./sum(errors_mean_restricted_inliers>0,1);
    mean_std_restricted_per_designer_v1 = sum(std_restricted_inliers,1)./sum(std_restricted_inliers>0,1);            
    
    
    mean_general_error_per_designer_v2 = sum(errors_mean_general_inliers_v2,1)./sum(errors_mean_general_inliers_v2>0,1);
    mean_std_general_per_designer_v2 = sum(std_general_inliers_v2,1)./sum(std_general_inliers_v2>0,1);
    
    mean_restricted_error_per_designer_v2 = sum(errors_mean_restricted_inliers_v2,1)./sum(errors_mean_restricted_inliers_v2>0,1);
    mean_std_restricted_per_designer_v2 = sum(std_restricted_inliers_v2,1)./sum(std_restricted_inliers_v2>0,1);            
    %%            
 
    close(figure(5));
    h = figure(5);
    paper_sz_w = 16;
    paper_sz_h = 8;
    h.Units         = 'centimeters';
    h.PaperSize     = [paper_sz_w paper_sz_h];
    h.Position(3)   = paper_sz_w;
    h.Position(4)   = paper_sz_h;
  
    subplot(1,2,1);
    title_str = 'All objects average';
    ylabel_str = 'Mean errors';
    
    legend_str_val1 = 'general, view 1';
    legend_str_val2 = 'restricted, view 1';
    legend_str_val3 = 'general, view 2';
    legend_str_val4 = 'restricted, view 2'; 
    
    num_designers = length(designers);
    hold on;
    plot(1:num_designers, mean_general_error_per_designer_v1, 'b');
    plot(1:num_designers, mean_restricted_error_per_designer_v1, 'r:');
    plot(1:num_designers, mean_general_error_per_designer_v2, 'c');
    plot(1:num_designers, mean_restricted_error_per_designer_v2, 'g:');
    title(title_str);
    ylabel(ylabel_str);

    legend(legend_str_val1, legend_str_val2, legend_str_val3, legend_str_val4);
    set(gca,'xtick',1:num_designers,'xticklabel',designers(:,1))
    set(gca,'FontSize', 8);
    set(gca,'FontName', 'Myriad Pro');
    xtickangle(90);
    
     %% Stds
     subplot(1,2,2);
   ylabel_str = 'Standard deviation of means of geom. errors';
    
   legend_str_val1 = 'general, view 1';
   legend_str_val2 = 'restricted, view 1';               
   legend_str_val3 = 'general, view 2';
   legend_str_val4 = 'restricted, view 2';
   
   num_designers = length(designers);
    hold on;
    plot(1:num_designers, mean_std_general_per_designer_v1, 'b');
    plot(1:num_designers, mean_std_restricted_per_designer_v1, 'r:');
    plot(1:num_designers, mean_std_general_per_designer_v2, 'c');
    plot(1:num_designers, mean_std_restricted_per_designer_v2, 'g:');
    title(title_str);
    ylabel(ylabel_str);

    legend(legend_str_val1, legend_str_val2, legend_str_val3, legend_str_val4);
    set(gca,'xtick',1:num_designers,'xticklabel',designers(:,1))
    set(gca,'FontSize', 8);
    set(gca,'FontName', 'Myriad Pro');
    xtickangle(90);

    
end