 function plot_three_figures( mean_general_error_per_designer, mean_restricted_error_per_designer, ...
            mean_std_general_per_designer, mean_std_restricted_per_designer ,...
            stds_of_means_general, stds_of_means_restricted, title_str, grouping)
    
        d_x = 1; d_y = 4;
    
        %% Average mean
        subplot(d_x, d_y, 1);

        ylabel_str = 'Mean errors';
        legend_str_val1 = 'general';
        legend_str_val2 = 'restricted';
        plot_format(grouping, ...
            mean_general_error_per_designer, mean_restricted_error_per_designer,...
            title_str, ylabel_str,...
            legend_str_val1, legend_str_val2);
        
        
        subplot(d_x, d_y, 2);

        ylabel_str = 'Mean errors';
        legend_str_val1 = 'general';
        legend_str_val2 = 'restricted';
  
        if (size(mean_general_error_per_designer, 1) == 1)
            plot_errorbars(grouping, mean_general_error_per_designer, mean_restricted_error_per_designer,...
                mean_std_general_per_designer, mean_std_restricted_per_designer,...
                title_str, ylabel_str,...
                legend_str_val1, legend_str_val2);
        else
            plot_errorbars(grouping, mean_general_error_per_designer, mean_restricted_error_per_designer,...
                stds_of_means_general, stds_of_means_restricted,...
                title_str, ylabel_str,...
                legend_str_val1, legend_str_val2);
        end
        %% Std of means

        ylabel_str = 'Standard deviation of means of geom. errors';
        legend_str_val1 = 'general';
        legend_str_val2 = 'restricted';
         subplot(d_x, d_y, 3);
        plot_format(grouping, ...
            stds_of_means_general, stds_of_means_restricted,...
            title_str, ylabel_str,...
            legend_str_val1, legend_str_val2);

        %% Average std
        subplot(d_x, d_y, 4);

        ylabel_str = 'Mean standard deviation of geom. errors';
        legend_str_val1 = 'general';
        legend_str_val2 = 'restricted';

        plot_format(grouping, ...
            mean_std_general_per_designer, mean_std_restricted_per_designer,...
            title_str, ylabel_str,...
            legend_str_val1, legend_str_val2);
    end