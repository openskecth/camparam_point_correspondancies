% function designers_ranking = plot_average_mean_and_std_per_professional(errors_mean_general_inliers, std_general_inliers,...
%     errors_mean_restricted_inliers, std_restricted_inliers,...
%     designers, view, object_data_str)

function [  mean_general_error_per_designer, mean_restricted_error_per_designer,...
            stds_of_means_general, stds_of_means_restricted,...
            mean_std_general_per_designer, mean_std_restricted_per_designer,...
            designers_ranking] = plot_average_mean_and_std_per_professional(errors_mean_general_inliers, std_general_inliers,...
    errors_mean_restricted_inliers, std_restricted_inliers,...
    designers, view, object_data_str)
    
    do_plot_additional = false;
    
    paper_sz_w = 16;
    paper_sz_h = 8;
    
    view_data = strrep(view, '_', ' ');
    designers_ranking = zeros(1, length(designers));

    close(figure(1));
    h = figure(1);
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    hold off;
    hold on;
    
    % Select indices of the designers that draw that object:
    designers_ind                   = find(sum(errors_mean_general_inliers,1)>0);
    designers = designers(designers_ind, :);
    num_designers = length(designers);
    % General errors and standard deviations:
    errors_mean_general_inliers     = errors_mean_general_inliers(:, designers_ind);
    std_general_inliers             = std_general_inliers(:, designers_ind);
    % Restricted errors and standard deviations:
    errors_mean_restricted_inliers  = errors_mean_restricted_inliers(:, designers_ind);
    std_restricted_inliers          = std_restricted_inliers(:, designers_ind);
    
    
    % Compute means per designer and means of standard deviations errors in sketches:
    % General:
    mean_general_error_per_designer = sum(errors_mean_general_inliers,1)./...
                                      sum(errors_mean_general_inliers>0,1);
    mean_std_general_per_designer   = sum(std_general_inliers,1)./...
                                      sum(std_general_inliers>0,1);
    % Restricted:
    mean_restricted_error_per_designer  = sum(errors_mean_restricted_inliers,1)./...
                                          sum(errors_mean_restricted_inliers>0, 1);
    mean_std_restricted_per_designer    = sum(std_restricted_inliers,1)./...
                                          sum(std_restricted_inliers>0, 1);
                                  
    % Find the reasonable axis limits to fit the legend:
    [axis_y_min, axis_y_max] = compute_axis_limits([mean_general_error_per_designer  mean_restricted_error_per_designer], ...
                                 [mean_std_general_per_designer  mean_std_restricted_per_designer]);
                                      
                                      
    % Plot the mean errors in sketches per designer and means of standard deviations errors in sketches:    
    
    title_str = [object_data_str ' -- inliers -- ' view_data];
    ylabel_str = 'Mean errors and mean standard deviations';
    legend_str_val1 = 'general';
    legend_str_val2 = 'restricted';
    
    plot_errorbars(designers, ...
        mean_general_error_per_designer, mean_restricted_error_per_designer,...
        mean_std_general_per_designer,   mean_std_restricted_per_designer,...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    object_filename_str = strrep(object_data_str, ' ', '_');
    
    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\errors_average_over_all_objects_the_designer_draw';
    filename_str = ['Mean_errors_and_mean_standard_deviations_' object_filename_str];
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');
    %% Compute std of means:
    stds_of_means_general = zeros(1, num_designers);
    stds_of_means_restricted = zeros(1, num_designers);
    for i = 1:num_designers
        objects_indices                = find(errors_mean_restricted_inliers(:,i)>0);
        stds_of_means_general(i)    = std(errors_mean_general_inliers(objects_indices,i));
        stds_of_means_restricted(i) = std(errors_mean_restricted_inliers(objects_indices,i));
    end
    
    
    % Plot the mean errors in sketches per designer and standard deviations of mean errors in sketches:    

    close(figure(2));
    h = figure(2);
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
       
    ylabel_str = 'Mean errors and standard deviations of means';
    
     % Find the reasonable axis limits to fit the legend:
    [axis_y_min, axis_y_max] = compute_axis_limits([mean_general_error_per_designer  mean_restricted_error_per_designer], ...
                                 [stds_of_means_general  stds_of_means_restricted]);
                             
    plot_errorbars(designers, ...
        mean_general_error_per_designer, mean_restricted_error_per_designer,...
        stds_of_means_general, stds_of_means_restricted,...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    filename_str = ['Mean_errors_and_standard_deviations_of_means_' object_filename_str];
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');
%     plot_format(designers, ...
%         mean_general_error_per_designer, mean_restricted_error_per_designer,...
%         title_str, ylabel_str,...
%         legend_str_val1, legend_str_val2);

    %% Plot (Sorted by general) the mean errors in sketches per designer and standard deviations of mean errors in sketches:    

    close(figure(5));
    h = figure(5);
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
       
    ylabel_str = 'Mean errors and standard deviations of means';
    
    [~, ind] = sort(mean_general_error_per_designer);
   
    plot_errorbars(designers(ind, 1), ...
        mean_general_error_per_designer(ind), mean_restricted_error_per_designer(ind),...
        stds_of_means_general(ind), stds_of_means_restricted(ind),...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    filename_str = ['Mean_errors_and_standard_deviations_of_means_sorted_general_' object_filename_str];
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');

    
    %% Plot (Sorted by restricted) the mean errors in sketches per designer and standard deviations of mean errors in sketches:    

    close(figure(6));
    h = figure(6);
    h.Units = 'centimeters';
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
       
    ylabel_str = 'Mean errors and standard deviations of means';
    
    [~, ind] = sort(mean_restricted_error_per_designer);
   
    plot_errorbars(designers(ind, 1), ...
        mean_general_error_per_designer(ind), mean_restricted_error_per_designer(ind),...
        stds_of_means_general(ind), stds_of_means_restricted(ind),...
        title_str, ylabel_str,...
        legend_str_val1, legend_str_val2,...
        axis_y_min, axis_y_max);
    
    filename_str = ['Mean_errors_and_standard_deviations_of_means_sorted_restricted_' object_filename_str];
    saveas(h, fullfile(folder, [filename_str '_inliers_' view '.png']));
    print('-bestfit', fullfile(folder, [filename_str '_inliers_' view '.pdf']), '-dpdf');

%     filename= [folder view '_average_over_' object_data_str '_objects_per_designer.mat'];
%     save(filename, ...
%             'mean_general_error_per_designer', 'mean_restricted_error_per_designer',...
%             'stds_of_means_general', 'stds_of_means_restricted',...
%             'mean_std_general_per_designer', 'mean_std_restricted_per_designer')
  
    %% Std of means
    if do_plot_additional
        close(figure(3));
        h = figure(3);
        h.Units = 'centimeters';
        h.PaperSize  = [16 9];
        h.Position(3)  = 16;
        h.Position(4)  = 11;
        d_x = 1; d_y = 2;

        title_str = [object_data_str ' ' view_data];
        ylabel_str = 'Standard deviation of means of geom. errors';

         subplot(d_x, d_y, 1);
        plot_format(designers, ...
            stds_of_means_general, stds_of_means_restricted,...
            title_str, ylabel_str,...
            legend_str_val1, legend_str_val2);

        %% Average std
        subplot(d_x, d_y, 2);
        title_str = [object_data_str ' ' view_data];
        ylabel_str = 'Mean standard deviation of geom. errors';
        legend_str_val1 = 'general';
        legend_str_val2 = 'restricted';

        plot_format(designers, ...
            mean_std_general_per_designer, mean_std_restricted_per_designer,...
            title_str, ylabel_str,...
            legend_str_val1, legend_str_val2);

        folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';

        saveas(h, [folder view '_average_over_' object_data_str '_objects_per_designer_sorted.pdf']);

       
    end
%      %% Plot sorted values:
%     [mean_general_error_per_designer, ind] = sort(mean_general_error_per_designer);
%     designers_ranking(designers_ind(ind)) = 1:length(designers_ind);
%     mean_restricted_error_per_designer = mean_restricted_error_per_designer(ind);
%     mean_std_general_per_designer = mean_std_general_per_designer(ind);
%     mean_std_restricted_per_designer = mean_std_restricted_per_designer(ind);
%     stds_of_means_general = stds_of_means_general(ind);
%     stds_of_means_restricted = stds_of_means_restricted(ind);
%     
%     if do_plot_additional
%         close(figure(4));
%         h_sorted = figure(4);
% 
%         plot_three_figures( mean_general_error_per_designer, mean_restricted_error_per_designer, ...
%                 mean_std_general_per_designer, mean_std_restricted_per_designer ,...
%                 stds_of_means_general, stds_of_means_restricted, [object_data_str ' ' view_data], designers(ind, 1));    
% 
%          %% Save
%         saveas(h_sorted , [folder view '_average_over_' object_data_str '_objects_per_designer_sorted.pdf']);
%     end
end
