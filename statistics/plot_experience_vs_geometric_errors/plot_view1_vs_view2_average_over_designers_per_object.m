function plot_view1_vs_view2_average_over_designers_per_object(errors_mean_general_inliers, errors_mean_general_inliers_v2, ...
                    errors_mean_restricted_inliers,  errors_mean_restricted_inliers_v2, ...
                    std_general_inliers, std_general_inliers_v2,...
                    std_restricted_inliers, std_restricted_inliers_v2, objects_id)
    
    objects = cell(length(objects_id), 1);
    for i = 1:length(objects_id)
        objects{i} = objects_id(i).title;
    end
    
    dim = 2;
    mean_general_error_per_designer_v1 = sum(errors_mean_general_inliers,dim)./sum(errors_mean_general_inliers>0,dim);
    mean_std_general_per_designer_v1 = sum(std_general_inliers,dim)./sum(std_general_inliers>0,dim);
    
    mean_restricted_error_per_designer_v1 = sum(errors_mean_restricted_inliers,dim)./sum(errors_mean_restricted_inliers>0,dim);
    mean_std_restricted_per_designer_v1 = sum(std_restricted_inliers,dim)./sum(std_restricted_inliers>0,dim);            
    
    
    mean_general_error_per_designer_v2 = sum(errors_mean_general_inliers_v2,dim)./sum(errors_mean_general_inliers_v2>0,dim);
    mean_std_general_per_designer_v2 = sum(std_general_inliers_v2,dim)./sum(std_general_inliers_v2>0,dim);
    
    mean_restricted_error_per_designer_v2 = sum(errors_mean_restricted_inliers_v2,dim)./sum(errors_mean_restricted_inliers_v2>0,dim);
    mean_std_restricted_per_designer_v2 = sum(std_restricted_inliers_v2,dim)./sum(std_restricted_inliers_v2>0,dim);            
    %%            
 
    close(figure(5));
    h = figure(5);
    d_x = 1; d_y = 2;
    subplot(1,2,1);
    title_str = 'All designers average';
    ylabel_str = 'Mean errors';
    
    legend_str_val1 = 'general, view 1';
    legend_str_val2 = 'restricted, view 1';
    legend_str_val3 = 'general, view 2';
    legend_str_val4 = 'restricted, view 2'; 
    
    num_objects = length(objects_id);
    hold on;
    plot(1:num_objects, mean_general_error_per_designer_v1, 'b');
    plot(1:num_objects, mean_restricted_error_per_designer_v1, 'r:');
    plot(1:num_objects, mean_general_error_per_designer_v2, 'c');
    plot(1:num_objects, mean_restricted_error_per_designer_v2, 'g:');
    title(title_str);
    ylabel(ylabel_str);

    legend(legend_str_val1, legend_str_val2, legend_str_val3, legend_str_val4);
    set(gca,'xtick',1:num_objects,'xticklabel',objects_id(:,1))
    set(gca,'FontSize', 6);
    set(gca,'FontName', 'Myriad Pro');
    xtickangle(90);
    
     %% Stds
     subplot(1,2,2);
   ylabel_str = 'Standard deviation of means of geom. errors';
    
   legend_str_val1 = 'general, view 1';
   legend_str_val2 = 'restricted, view 1';               
   legend_str_val3 = 'general, view 2';
   legend_str_val4 = 'restricted, view 2';
   
   num_objects = length(objects_id);
    hold on;
    plot(1:num_objects, mean_std_general_per_designer_v1, 'b');
    plot(1:num_objects, mean_std_restricted_per_designer_v1, 'r:');
    plot(1:num_objects, mean_std_general_per_designer_v2, 'c');
    plot(1:num_objects, mean_std_restricted_per_designer_v2, 'g:');
    title(title_str);
    ylabel(ylabel_str);

    legend(legend_str_val1, legend_str_val2, legend_str_val3, legend_str_val4);
    set(gca,'xtick',1:num_objects,'xticklabel',objects_id(:,1))
    set(gca,'FontSize', 6);
    set(gca,'FontName', 'Myriad Pro');
    xtickangle(90);
    
   folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
   saveas(h, [folder '_average_over_designer_per_object_v1_vs_v2.pdf']);
    
end