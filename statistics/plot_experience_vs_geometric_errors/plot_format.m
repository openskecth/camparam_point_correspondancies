%  plot_format(designers, val1, val2, title_str, ylabel_str,...
%     legend_str_val1, legend_str_val2)
% 
% 

function plot_format(designers, val1, val2, title_str, ylabel_str,...
    legend_str_val1, legend_str_val2)



num_designers = length(designers);
plot(1:num_designers, val1, 'b');
hold on;
plot(1:num_designers, val2, 'r:');
title(title_str);
ylabel(ylabel_str);

legend(legend_str_val1, legend_str_val2);
set(gca,'xtick',1:num_designers,'xticklabel',designers(:,1))
set(gca,'FontSize', 6);
set(gca,'FontName', 'Myriad Pro');
xtickangle(90);

end