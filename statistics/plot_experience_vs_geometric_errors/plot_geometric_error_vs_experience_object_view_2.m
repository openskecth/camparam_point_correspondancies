function   [errors_mean_general_inliers, errors_mean_restricted_inliers, std_general_inliers, std_restricted_inliers] = ...
    plot_geometric_error_vs_experience_object_view_2(folder_camera_parameters, object_id, designers, do_plot)

    if (~exist('do_plot', 'var'))
        do_plot= false;
    end

    close all;

    figure(2);
    axis equal 
    grid off
    hold on;
    
    num_designers = length(designers);

    errors_mean_general_all = zeros(1, num_designers );
    errors_mean_restricted_all = zeros(1, num_designers );
    std_general_all = zeros(1, num_designers );
    std_restricted_all = zeros(1, num_designers );
    errors_mean_general_inliers = zeros(1, num_designers );
    errors_mean_restricted_inliers = zeros(1, num_designers );
    std_general_inliers = zeros(1, num_designers );
    std_restricted_inliers = zeros(1, num_designers );
    errors_median_general_all = zeros(1, num_designers );
    errors_median_restricted_all = zeros(1, num_designers );
    errors_median_restricted_inliers = zeros(1, num_designers );
    errors_median_general_inliers = zeros(1, num_designers );

    for i = 1:num_designers 
        designer_id = designers{i,1};
        designer_type = designers{i,2};
        eval([object_id '_3D']);
        eval([designer_id '_' object_id '_v2_points']);

        x_m(:,1:2) = x_m(:,1:2)/width;
        x_m(:,1:2) = x_m(:,1:2)*2 - 1; 
        x_m(:,2) = x_m(:,2)*-1; %cordinated in webgl -1 (bottom left) matlab 1 bottom left

        x = x_m(indices_opt, :);
        X = H(indices_opt, :);
        [x, T_x] = normalise2dpts(x');
        x = x';
        figure(2);
        plot(x(:,1),x(:,2));


        folder_save = fullfile(folder_camera_parameters, designer_type, designer_id, object_id, 'view2');
        save_mat_file = fullfile(folder_save, 'errors.mat');
        errors = load(save_mat_file);

        % Mean and median on the all points marked:
        errors_mean_general_all(i) = errors.error_mean_general_all;
        errors_mean_restricted_all(i) = errors.error_mean_restricted_all;
        errors_median_general_all(i) = errors.error_median_general_all;
        errors_median_restricted_all(i) = errors.error_median_restricted_all;
        std_general_all(i) = errors.error_std_general_all;
        std_restricted_all(i) = errors.error_std_restricted_all;

        % Mean and median on the inliers:
        errors_mean_general_inliers(i) = errors.error_mean_general_inliers;
        errors_mean_restricted_inliers(i) = errors.error_mean_restricted_inliers;
        errors_median_general_inliers(i) = errors.error_median_general_inliers;
        errors_median_restricted_inliers(i) = errors.error_median_restricted_inliers;
        std_general_inliers(i) = errors.error_std_general_inliers;
        std_restricted_inliers(i) = errors.error_std_restricted_inliers;
    end


    object_name = strrep(object_id, '_', ' ');
    
    if (do_plot)
    %% Plot all in one:
    
    axis_y_min = min([errors_mean_general_all - std_general_all ...
                      errors_mean_restricted_all - std_restricted_all ...
                      errors_median_general_all - std_general_all ...
                      errors_median_restricted_all - std_restricted_all])
    axis_y_min = max(axis_y_min, -0.1)              
    axis_y_max = max([errors_mean_general_all + std_general_all ...
                      errors_mean_restricted_all + std_restricted_all ...
                      errors_median_general_all + std_general_all ...
                      errors_median_restricted_all + std_restricted_all])
    axis_y_max = min(axis_y_max, 0.1)
    
    
    figure(1);
    d_x = 2; d_y = 2;
    subplot(d_x, d_y, 1);
    errorbar([1:num_designers], errors_mean_general_all, std_general_all, 'b');
    hold on;
    errorbar([1:num_designers], errors_mean_restricted_all, std_restricted_all, 'r:');
    title('Mean of geometric errors, all points');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    axis([-inf inf axis_y_min axis_y_max])
    xtickangle(90);

    subplot(d_x, d_y, 2);
    errorbar([1:num_designers], errors_mean_general_inliers, std_general_inliers, 'b');
    hold on;
    errorbar([1:num_designers], errors_mean_restricted_inliers, std_restricted_inliers, 'r:');
    title('Mean of geometric errors, inliers');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    axis([-inf inf axis_y_min axis_y_max])
    xtickangle(90);

    subplot(d_x, d_y, 3);
    errorbar([1:num_designers], errors_median_general_all, std_general_all, 'b');
    hold on;
    errorbar([1:num_designers], errors_median_restricted_all, std_restricted_all, 'r:');
    title('Median of geometric errors, all points');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers], 'xticklabel', designers(:,1))
    axis([-inf inf axis_y_min axis_y_max])
    xtickangle(90);


    subplot(d_x, d_y, 4);
    errorbar([1:num_designers], errors_median_general_all, std_general_all, 'b');
    hold on;
    errorbar([1:num_designers], errors_median_restricted_all, std_restricted_all, 'r:');
    title('Median of geometric errors, all points');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers], 'xticklabel', designers(:,1))
    axis([-inf inf axis_y_min axis_y_max])
    xtickangle(90);

    %% Plot means and medians deviation separately

    figure(3);
    d_x = 2; d_y = 2;
    subplot(d_x, d_y, 1);
    plot([1:num_designers], errors_mean_general_all, 'b');
    hold on;
    plot([1:num_designers], errors_mean_restricted_all, 'r:');
    title('Mean of geometric errors, all points');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);

    subplot(d_x, d_y, 2);
    plot([1:num_designers], errors_mean_general_inliers, 'b');
    hold on;
    plot([1:num_designers], errors_mean_restricted_inliers, 'r:');
    title('Mean of geometric errors, inliers');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);

    subplot(d_x, d_y, 3);
    plot([1:num_designers], errors_median_general_all, 'b');
    hold on;
    plot([1:num_designers], errors_median_restricted_all, 'r:');
    title('Median of geometric errors, all points');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers], 'xticklabel', designers(:,1))
    xtickangle(90);


    subplot(d_x, d_y, 4);
    plot([1:num_designers], errors_median_general_inliers, 'b');
    hold on;
    plot([1:num_designers], errors_median_restricted_inliers, 'r:');
    title('Median of geometric errors, inliers');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers], 'xticklabel', designers(:,1))
    xtickangle(90);
    
    
    %% Standard deviation:
    figure(4);
    d_x = 1; d_y =2;
    subplot(d_x, d_y, 1);
    plot([1:num_designers], std_general_all, 'b');
    hold on;
    plot([1:num_designers], std_restricted_all, 'r:');
    title('All points: standard deviations');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);
    
    subplot(d_x, d_y, 2);
    plot([1:num_designers], std_general_inliers, 'b');
    hold on;
    plot([1:num_designers], std_restricted_inliers, 'r:');
    title('Inliers: standard deviations');
    ylabel(['Error ' object_name]);
    legend('general', 'restricted');
    set(gca,'xtick',[1:num_designers],'xticklabel',designers(:,1))
    xtickangle(90);
    end
end