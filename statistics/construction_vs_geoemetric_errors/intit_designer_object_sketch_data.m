function designers = intit_designer_object_sketch_data(designers, line_label)
    num_designers = length(designers);
    for designer_num = 1:num_designers
         for j = 1:length(designers(designer_num).objects)
%              object_num = designers(designer_num).objects(j);
             for label = 0:line_label.shadow_construction
                l = label+1;
                designers(designer_num).object(j).line_present(l) = -1;
                designers(designer_num).object(j).line_type_count(l) = -1;
                designers(designer_num).object(j).strokes_label = -1;
             end
         end
    end
end