function table = explore_construction_vs_accuracy(errors_mean_v1)

%% Load intial data about the designers

% Load line types information:
[line_label, line_types_names, line_types_colors, labels_order] = lineTypesData();

% Load objects information:
objects = objectsData();
for object_num = 1:12
    objects(object_num).num_students_sketched = 0.0;
    objects(object_num).num_professionals_sketched = 0.0;
    objects(object_num).num_designers_sketched = 0.0;
   
    
    for l = 1:(line_label.shadow_construction+1)
        objects(object_num).num_students_used_line(l) = 0.0;
        objects(object_num).num_professionals_used_line(l) = 0.0;        
        objects(object_num).num_designers_used_line(l) = 0.0;
        objects(object_num).line_frequency_per_designer(l) = 0.0;
    end
end

objects_id = cell(length(objects), 1);
for i = 1:length(objects)
    objects_id{i} = objects(i).title;
end
    
% Load students information:
[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();
num_designers = length(designers);

%% Read line types data for each designer
drawing_type = 'concept';%'presentation';%'concept';
designers = intit_designer_object_sketch_data(designers, line_label);


[designers(1, 1:9), objects] = ...
    loadLineTypeData(designers(1, 1:9), objects, line_label,  'student', drawing_type);
[designers(1, 10:15), objects] = ...
    loadLineTypeData(designers(1, 10:15), objects, line_label,  'professioanl', drawing_type);

%% Compute frequencies of lines usage per designer per object, average per designer, average per object
% and number of strokes in each sketch
num_line_types = line_label.shadow_construction+1;
[designers, objects] = compute_line_frequency_per_designer(designers, objects, num_line_types);

%% Load the accuracy of registration:

% folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
% folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_mat_files\';
% filename = [folder 'view1_reprojection_errors.mat'];
% load(filename);
% filename = [folder 'view2_reprojection_errors.mat'];
% load(filename);
% 



%% Inaccuracies vs binary lines presence:

%% Load:
ammount_of_different_lines_v1 = zeros(12, num_designers);

for num_designer = 1:num_designers   
     for j = 1:length(designers(num_designer).objects)
         obj_num = designers(num_designer).objects(j);
         
         ammount_of_different_lines_v1(obj_num, num_designer)...
                = sum(designers(num_designer).object(j).lines_frequencies > 0);
     end
end

% ammount_of_different_lines_v1 = ammount_of_different_lines_v1(ind_drawn);
%% Correlation compute:
cor_lines_presence_inaccuracy_v1 = zeros(12,1);
p_cor_lines_presence_inaccuracy_v1 = zeros(12,1);

for ind_obj = 1:12
    ind_drawn = errors_mean_v1(ind_obj, :) >0;
    vals1 = errors_mean_v1(ind_obj, ind_drawn);
    vals2 = ammount_of_different_lines_v1(ind_obj, ind_drawn);
    [cor_lines_presence_inaccuracy_v1(ind_obj),...
        p_cor_lines_presence_inaccuracy_v1(ind_obj)] = corr(vals1', vals2');
%     figure(1);
%     [vals1, ind] = sort(vals1);
%     vals2 = vals2(ind);
%     plot(vals1, vals2);
%     title([objects_id(ind_obj) ' correlation: ' sprintf('%.3f', cor_lines_presence_inaccuracy_v1(ind_obj)) ]);
end



% correlation_plot(inaccuracies_v1, ammount_of_different_lines_v1, [1 2 1],...
%     'If at least one stoke of a certain type', 'ammount of line types');

    cor_lines_presence_inaccuracy_v1_mean = mean(cor_lines_presence_inaccuracy_v1)
    p_cor_lines_presence_inaccuracy_v1_mean = mean(p_cor_lines_presence_inaccuracy_v1)
    
    

%% Inaccuracies vs lines presence if frequency is large than average per object:

% ammount_of_different_lines_v1 = zeros(12, num_designers);
% 
% for num_designer = 1:num_designers   
%      for j = 1:length(designers(num_designer).objects)
%             object_num = designers(num_designer).objects(j);
%             
%    
%             ind = (designers(num_designer).object(j).lines_frequencies>0) & ...
%                 (objects(object_num).lines_frequencies>0);
%             
%             ammount_of_different_lines_v1(object_num, num_designer)...
%                 = sum(designers(num_designer).object(j).lines_frequencies(ind) > ...
%                 1.0/3.0*objects(object_num).lines_frequencies(ind));
%      end
% end
% 
% cor_lines_presence_inaccuracy_v1 = zeros(12,1);
% for ind_obj = 1:12
%     ind_drawn = ammount_of_different_lines_v1(ind_obj, :) >0;
%     vals1 = errors_mean_v1(ind_obj, ind_drawn);
%     vals2 = ammount_of_different_lines_v1(ind_obj, ind_drawn);
%     cor_lines_presence_inaccuracy_v1(ind_obj) = corr2(vals1, vals2);
% %     figure(1);
% %     [vals1, ind] = sort(vals1);
% %     vals2 = vals2(ind);
% %     plot(vals1, vals2);
% %     title([objects_id(ind_obj) ' correlation: ' sprintf('%.3f', cor_lines_presence_inaccuracy_v1(ind_obj)) ]);
% end
% 
%     cor_lines_presence_inaccuracy_v1_mean = mean(cor_lines_presence_inaccuracy_v1)
%     
%     table2 = cell(12,2);
%     table2(:,1) = objects_id;
%     table2(:,2) = num2cell(cor_lines_presence_inaccuracy_v1);

%% Fractions:
% close(figure(2));
% h   = figure(2);
% hold on
% set(gca,'FontSize', 6);
% set(gca,'FontName', 'Myriad Pro');

%% Inaccuracies vs fraction of guide lines:
ind_guide_lines = [6:21]+1;


[fraction_of_guide_lines, cor_guide, p_guide, cor_guide_all, p_guide_all] = accuracy_vs_lines_fraction(num_designers, ...
    designers, ...
    errors_mean_v1,...
    ind_guide_lines)

    table = cell(14,2);
    table(1,1) = {'Objects'};
    table(2:end-1,1) = objects_id;
    table(1,2) = {'cor_guide'};
    table(2:end-1,2) = num2cell(round(cor_guide, 3));
    table(end,2) = num2cell(round(cor_guide_all, 3));
    table(1,3) = {'p_guide'};
    table(2:end-1,3) = num2cell(round(p_guide, 3));
    table(end,3) = num2cell(round(p_guide_all, 3));
    

% cor_guide_mean = mean(cor_guide)
% p_guide = mean(p_guide)

% accuracy_vs_lines_fraction(num_sketches, num_designers, designers, accuracy, ind_guide_lines, [2 3 1], ...
%     'Inaccuracies vs fraction of guide lines', 'fraction of guide lines');


% fraction_of_guide_lines = zeros(num_sketches, 1);
% sketch_num = 0;
% for num_designer = 1:num_designers   
%      for obj_num = 1:length(designers(num_designer).objects)
%             sketch_num = sketch_num + 1;  
%             fraction_of_guide_lines(sketch_num)...
%                 = sum(designers(num_designer).object(obj_num).lines_frequencies(ind_guide_lines));
%      end
% end
% 
% 
% correlation_plot(accuracy, fraction_of_guide_lines, [2 3 1], ...
%     'Inaccuracies vs fraction of guide lines', 'fraction of guide lines');

%% Inaccuracies vs fraction of context lines:
ind_context_lines = [6:10]+1;

[fraction_of_context_lines,...
    cor_context, p_context,...
    cor_context_all, p_context_all] = accuracy_vs_lines_fraction(num_designers, ...
    designers, ...
    errors_mean_v1,...
    ind_context_lines)

    table(1,4) = {'cor_context'};
    table(2:end-1,4) = num2cell(round(cor_context, 3));
    table(end,4) = num2cell(round(cor_context_all, 3));
    table(1,5) = {'p_context'};
    table(2:end-1,5) = num2cell(round(p_context, 3));
    table(end,5) = num2cell(round(p_context_all, 3));

% accuracy_vs_lines_fraction(num_sketches, num_designers, designers, accuracy, ind_context_lines, [2 3 2], ...
%     'Inaccuracies vs fraction of context lines', 'fraction of context lines');

%% Inaccuracies vs fraction of proportion lines:
ind_propotions_lines = [16:21]+1;
[fraction_of_propotions_lines,...
    cor_propotions, p_propotions,...
    cor_propotions_all, p_propotions_all] = accuracy_vs_lines_fraction(num_designers, ...
    designers, ...
    errors_mean_v1,...
    ind_propotions_lines)


    table(1,6) = {'cor_proportions'};
    table(2:end-1,6) = num2cell(round(cor_propotions, 3));
    table(end,6) = num2cell(round(cor_propotions_all, 3));
    table(1,7) = {'p_proportions'};
    table(2:end-1,7) = num2cell(round(p_propotions, 3));
    table(end,7) = num2cell(round(p_propotions_all, 3));
    
% accuracy_vs_lines_fraction(num_sketches, num_designers, designers, accuracy, ind_propotions_lines, [2 3 3], ...
%     'Inaccuracies vs fraction of proportion lines', 'fraction of proportion lines');


%% Inaccuracies vs fraction of surfaceing lines:
ind_surfacing_lines = [11:15]+1;


[fraction_of_surfacing_lines,...
    cor_surfacing, p_surfacing,...
    cor_surfacing_all, p_surfacing_all] = accuracy_vs_lines_fraction(num_designers, ...
    designers, ...
    errors_mean_v1,...
    ind_surfacing_lines)

    table(1,8)          = {'cor_surfacing'};
    table(2:end-1,8)    = num2cell(round(cor_surfacing, 3));
    table(end,8)        = num2cell(round(cor_surfacing_all, 3));
    table(1,9)          = {'p_surfacing'};
    table(2:end-1,9)    = num2cell(round(p_surfacing, 3));
    table(end,9)        = num2cell(round(p_surfacing_all, 3));
    
% accuracy_vs_lines_fraction(num_sketches, num_designers, designers, accuracy, ind_surfacing_lines, [2 3 4], ...
%     'Inaccuracies vs fraction of surfacing lines', 'fraction of surfacing lines');


%% Inaccuracies vs fraction of descriptive lines:
ind_descriptive_lines = [0:5]+1;

[fraction_of_descriptive_lines, ...
    cor_descriptive, p_descriptive,...
    cor_descriptive_all, p_descriptive_all] = accuracy_vs_lines_fraction(num_designers, ...
    designers, ...
    errors_mean_v1,...
    ind_descriptive_lines)

    table(1,10)          = {'cor_descriptive'};
    table(2:end-1,10)    = num2cell(round(cor_descriptive, 3));
    table(end,10)        = num2cell(round(cor_descriptive_all, 3));
    table(1,11)          = {'p_descriptive'};
    table(2:end-1,11)    = num2cell(round(p_descriptive, 3));
    table(end,11)        = num2cell(round(p_descriptive_all, 3));

% accuracy_vs_lines_fraction(num_sketches, num_designers, designers, accuracy, ind_descriptive_lines, [2 3 5], ...
%     'Inaccuracies vs fraction of descriptive lines', 'fraction of descriptive lines');
% 
% folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
% saveas(h, [folder 'inaccuracy_vs_lines_fracrtions.pdf']);


%% Averages per designer:

% close(figure(3));
% h = figure(3);
% hold on
% set(gca, 'FontSize', 6);
% set(gca, 'FontName', 'Myriad Pro');
% 
% accuracy_vs_lines_fraction_average (num_designers, designers, accuracy_average_designer', ind_guide_lines, [2 3 1], ...
%     'Inaccuracies vs fraction of guide lines average per designer', 'fraction of guide lines');
% accuracy_vs_lines_fraction_average (num_designers, designers, accuracy_average_designer', ind_context_lines, [2 3 2], ...
%     'Inaccuracies vs fraction of context lines average per designer', 'fraction of context lines');
% accuracy_vs_lines_fraction_average (num_designers, designers, accuracy_average_designer', ind_propotions_lines, [2 3 3], ...
%     'Inaccuracies vs fraction of proportion lines average per designer', 'fraction of proportion lines');
% accuracy_vs_lines_fraction_average (num_designers, designers, accuracy_average_designer', ind_surfacing_lines, [2 3 4], ...
%     'Inaccuracies vs fraction of surfacing lines average per designer', 'fraction of surfacing lines');
% accuracy_vs_lines_fraction_average (num_designers, designers, accuracy_average_designer', ind_descriptive_lines, [2 3 5], ...
%     'Inaccuracies vs fraction of descriptive lines average per designer', 'fraction of descriptive lines');
% 
% saveas(h, [folder 'average_per_designer.pdf']);
%  


%% Averages per object:

% close(figure(4));
% h = figure(4);
% hold on
% set(gca, 'FontSize', 6);
% set(gca, 'FontName', 'Myriad Pro');
% 
% 
% folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
% view = 'view1';
% filename= [folder view '_average_over_designers_per_object.mat'];
% errors_per_object_view1 = load(filename);
% 
% ind = find(errors_per_object_view1.mean_general_error_per_designer > 0);
% accuracy_average_object  = errors_per_object_view1.mean_general_error_per_designer(ind);
% 
% 
% 
% accuracy_vs_lines_fraction_average_object (objects, accuracy_average_object, ind_guide_lines, [2 3 1], ...
%     'Inaccuracies vs fraction of guide lines average per designer', 'fraction of guide lines');
% accuracy_vs_lines_fraction_average_object (objects, accuracy_average_object, ind_context_lines, [2 3 2], ...
%     'Inaccuracies vs fraction of context lines average per designer', 'fraction of context lines');
% accuracy_vs_lines_fraction_average_object (objects, accuracy_average_object, ind_propotions_lines, [2 3 3], ...
%     'Inaccuracies vs fraction of proportion lines average per designer', 'fraction of proportion lines');
% accuracy_vs_lines_fraction_average_object (objects, accuracy_average_object, ind_surfacing_lines, [2 3 4], ...
%     'Inaccuracies vs fraction of surfacing lines average per designer', 'fraction of surfacing lines');
% accuracy_vs_lines_fraction_average_object (objects, accuracy_average_object, ind_descriptive_lines, [2 3 5], ...
%     'Inaccuracies vs fraction of descriptive lines average per designer', 'fraction of descriptive lines');
% 
% saveas(h, [folder 'average_per_object.pdf']);



% folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
% saveas(h, [folder 'correlation_innacuracy_time_strokes_number.pdf']);
% 
% 

end


function accuracy_vs_lines_fraction_average_object(objects, accuracy, ind_line_types, fig_dim, title_str, ylabel_str)
    num_objects = length(objects);
    
    fraction_of_type_lines = zeros(num_objects, 1);
    
    for num_object = 1:num_objects   
        fraction_of_type_lines(num_object)...
            = sum(objects(num_object).lines_frequencies(ind_line_types));
    end

    correlation_plot(accuracy, fraction_of_type_lines, fig_dim, ...
        title_str, ylabel_str);
end


function accuracy_vs_lines_fraction_average (num_designers, designers, accuracy, ind_line_types, fig_dim, title_str, ylabel_str)
    
    fraction_of_type_lines = zeros(num_designers, 1);
    
    for num_designer = 1:num_designers   
            fraction_of_type_lines(num_designer)...
                = sum(designers(num_designer).lines_frequencies(ind_line_types));
    end

    correlation_plot(accuracy, fraction_of_type_lines, fig_dim, ...
        title_str, ylabel_str);

end

function correlation_plot(accuracy, ammount_of_different_lines, fig_dim, title_str, ylabel_str)
close(figure(1));
h   = figure(1);
hold on
set(gca,'FontSize', 8);

dx = fig_dim(1); 
dy = fig_dim(2);
fn = fig_dim(3);

% %% Remove outliers:
% %In accuracy
% v2 = prctile(accuracy, 95);
% v1 = prctile(accuracy, 5);
% ind = accuracy > v1 & accuracy < v2;
% accuracy = accuracy(ind);
% ammount_of_different_lines = ammount_of_different_lines(ind);
% 
% 
% %In ammount_of_different_lines
% v2 = prctile(ammount_of_different_lines, 95);
% v1 = prctile(ammount_of_different_lines, 5);
% ind = ammount_of_different_lines > v1 & ammount_of_different_lines < v2;
% accuracy = accuracy(ind);
% ammount_of_different_lines = ammount_of_different_lines(ind);


%% 
[accuracy, ind] = sort(accuracy);
ammount_of_different_lines = ammount_of_different_lines(ind);

cor_lines_accuracy = corr2(accuracy',ammount_of_different_lines');%corrcoef(accuracy',ammount_of_different_lines');


subplot(dx,dy,fn);
plot(accuracy, ammount_of_different_lines);
ylabel(ylabel_str);
xlabel('inaccuracy');
title(title_str)
legend(sprintf('correlation coefficient: %.3f', cor_lines_accuracy));%(1,2)));

end


