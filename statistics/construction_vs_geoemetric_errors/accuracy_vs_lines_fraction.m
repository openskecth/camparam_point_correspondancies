function [fraction_of_type_lines, cor, p_value,...
        cor_all, p_value_all] = accuracy_vs_lines_fraction(num_designers, ...
    designers, ...
    inaccuracy,...
    ind_line_types)
    
    fraction_of_type_lines = zeros(12, num_designers);
    
    for num_designer = 1:num_designers   
         for j = 1:length(designers(num_designer).objects)
            obj_num = designers(num_designer).objects(j);
            fraction_of_type_lines(obj_num, num_designer)...
                = sum(designers(num_designer).object(j).lines_frequencies(ind_line_types));
         end
    end
    cor = zeros(12,1);
    p_value = zeros(12,1);
    for ind_obj = 1:12
        ind_drawn = inaccuracy(ind_obj, :) >0;
        vals1 = inaccuracy(ind_obj, ind_drawn);
        vals2 = fraction_of_type_lines(ind_obj, ind_drawn);
        [cor(ind_obj), p_value(ind_obj)] = corr(vals1', vals2', 'type', 'Spearman');
    %     figure(1);
    %     [vals1, ind] = sort(vals1);
    %     vals2 = vals2(ind);
    %     plot(vals1, vals2);
    %     title([objects_id(ind_obj) ' correlation: ' sprintf('%.3f', cor_lines_presence_inaccuracy_v1(ind_obj)) ]);
    end 
    
    ind_drawn = inaccuracy >0;
    inaccuracy = inaccuracy(ind_drawn);
    fraction_of_type_lines = fraction_of_type_lines(ind_drawn);
    
    [cor_all, p_value_all] = corr(inaccuracy, fraction_of_type_lines, 'type', 'Spearman');
    
    
%     correlation_plot(accuracy, fraction_of_type_lines, fig_dim, ...
%         title_str, ylabel_str);
end