function [  ind_points] = recomputeMeanErrorsConsistentPointsSets(  )

%% Designers:
[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();
num_designers = length(designers);

%% Objects:
objects = objectsData();

%% Common point inidces between different objects:

% Initialise:
ind_points = cell(12, 1);
for i = 1:12
    ind_points{i} = 1:40;
end

%Compute
 for i = 1:num_designers 
    designer_id = designers(i).id;

    objects_indices = designers(i).objects;        
    ind_points = readPointsForEachObject(objects_indices, objects,designer_id, ind_points, 'v1');
    
    objects_indices = designers(i).objects_v2;
    ind_points = readPointsForEachObject(objects_indices, objects,designer_id, ind_points, 'v2');

         
 end
 
 %% Recompute mean errors on the specific points only
 folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters';
 
for i = 1:num_designers 
    
    objects_indices = designers(i).objects;        
    recomputeTheError(objects_indices, objects, designers(i), ind_points, folder, 'v1', 'view1');
    objects_indices = designers(i).objects_v2;
    recomputeTheError(objects_indices, objects, designers(i), ind_points, folder, 'v2', 'view2');
    
end

end

function ind_points = readPointsForEachObject(objects_indices, objects,designer_id, ind_points, view_short)
    for i_obj = 1:length(objects_indices)
        obj_ind = objects_indices(i_obj);
        object_id = objects(obj_ind).name;
        close all;
        eval([object_id '_3D']);
        designer_id = lower(designer_id);
        eval([designer_id '_' object_id '_' view_short '_points']);
%         t = ind_points{obj_ind}
%         indices_opt
        ind_points{obj_ind} = intersect(ind_points{obj_ind}, indices_opt);
    end
end


function recomputeTheError(objects_indices, objects, designer, ind_points, folder, view_short, view)
   designer_type = designer.designer_type;
   designer_id = designer.id;
    
    for i_obj = 1:length(objects_indices)
        
        obj_ind = objects_indices(i_obj);
        object_id = objects(obj_ind).name;
        
        close all;
        eval([object_id '_3D']);
        designer_id = lower(designer_id);
        eval([designer_id '_' object_id '_' view_short '_points']);
 
        x_m(:,1:2) = x_m(:,1:2)/width;
        x_m(:,1:2) = x_m(:,1:2)*2 - 1; 
        x_m(:,2) = x_m(:,2)*-1; %cordinated in webgl -1 (bottom left) matlab 1 bottom left

        
        
        x_all = x_m(indices_opt, :);
        X_all = H(indices_opt, :);
        [x_all, T_x] = normalise2dpts(x_all');
        x_all = x_all';
        
        
        x = x_m(ind_points{obj_ind}, :);
        X = H(ind_points{obj_ind}, :);
%         [x, ~] = normalise2dpts(x');
        x = T_x*x';
        x = x';
        
        folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters';
        folder_save = fullfile(folder, designer_type, designer_id, object_id, view);
        save_mat_file = fullfile(folder_save, 'errors.mat');
        
        
        load(save_mat_file, 't_general', 't_restricted');
        [error_median_general_common, error_mean_general_common, error_std_general_common,...
          error_median_restricted_common, error_mean_restricted_common, error_std_restricted_common] = ...
            evaluate_projection(t_general, t_restricted, x, X);
        
           [error_median_general_all, error_mean_general_all, error_std_general_all,...
          error_median_restricted_all, error_mean_restricted_all, error_std_restricted_all] = ...
            evaluate_projection(t_general, t_restricted, x_all, X_all);
%         
        
        ind_points{obj_ind}
        indices_opt

        disp(error_mean_general_common)
        disp(error_mean_general_all)
        
        save(save_mat_file, ...
            'error_median_general_common', 'error_mean_general_common', 'error_std_general_common',...
            'error_median_restricted_common', 'error_mean_restricted_common', 'error_std_restricted_common',...
            '-append');
    end
end

