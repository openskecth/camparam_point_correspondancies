close all;
clear all;
[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

view = 'view2';

designers = readCameraParametersForEachDesigner(designers, view);


sketch_num = 0;
azimuths    = [];
elevations   = [];
radiuses    = [];
principal_points_u = [];
principal_points_v = [];
focal_lengths = [];

for di = 1:length(designers)
     if strcmp(view, 'view1')
        designer_objects = designers(di).objects; 
     else
        designer_objects = designers(di).objects_v2; 
     end
     
    for oj = 1:length(designer_objects)
        if strcmp(view, 'view1')
            cam_params = designers(di).camera_parameters_v1(oj);
        else
            cam_params = designers(di).camera_parameters_v2(oj);
        end
        sketch_num = sketch_num+1;
        
         C = cam_params.restricted.C;
        [azimuths(sketch_num),...
         elevations(sketch_num),...
         radiuses(sketch_num)] = cart2sph(C(1),-C(3),C(2));          
       
            C(1),-C(3),C(2)
            
          [x,y,z] = sph2cart(azimuths(sketch_num),...
                             elevations(sketch_num),...
                             radiuses(sketch_num))
      
          principal_points_u(sketch_num) = (cam_params.restricted.u);
          principal_points_v(sketch_num) = (cam_params.restricted.v);
          focal_lengths(sketch_num) = cam_params.restricted.f;    
          
          
          R_ = rotationMatrixFromView(C, cam_params.restricted.focal_point, cam_params.restricted.up);
            
%           R_ = rotationMatrixFromView(C', [0.0 0.0 0.0], [0 1.0 0.0]);
          
          [theta_x(sketch_num), theta_y(sketch_num), theta_z(sketch_num),~,~,~,~] = rotm2eulr( R_ );
          
          view_dir = (C - cam_params.restricted.focal_point)';
          view_dir = view_dir/norm(view_dir);
          dir_object = C';
          
          %% XZ plane
          proj_xz = view_dir - dot(view_dir, [0.0 1.0 0.0])*[0.0 1.0 0.0];
          proj_xz = proj_xz/norm(proj_xz);
                 
          proj_xz_dir_object = dir_object - dot(dir_object, [0.0 1.0 0.0])*[0.0 1.0 0.0];
          proj_xz_dir_object = proj_xz_dir_object/norm(proj_xz_dir_object);
          
          aplha_h(sketch_num) = acos(dot(proj_xz_dir_object, proj_xz))*180.0/pi;
          
          %% YX plane
          proj_yx = view_dir - dot(view_dir, [0.0 0.0 1.0])*[0.0 0.0 1.0];
          proj_yx = proj_yx/norm(proj_yx);

          proj_yx_dir_object = dir_object - dot(dir_object, [0.0 0.0 1.0])*[0.0 0.0 1.0];
          proj_yx_dir_object = proj_yx_dir_object/norm(proj_yx_dir_object);
          
          aplha_v(sketch_num) = acos(dot(proj_yx_dir_object, proj_yx))*180.0/pi;
          
    end
end
principal_points_u = principal_points_u((principal_points_u < prctile(principal_points_u, 95)) & (principal_points_u > prctile(principal_points_u, 5)));
principal_points_v = principal_points_v((principal_points_v < prctile(principal_points_v, 95)) & (principal_points_v > prctile(principal_points_v, 5)) );

figure(11);
histogram(principal_points_u, 25, 'Normalization','probability');
hold on;
histogram(principal_points_v, 25, 'Normalization','probability');
legend('horizonatal', 'vertical');
figure(10);
histogram(principal_points_v, int32(double(sketch_num)/2.0));

figure(8);
histogram(aplha_h, int32(double(sketch_num)/2.0));

figure(9);
histogram(aplha_v, int32(double(sketch_num)/2.0));

figure(6); 
subplot(3,1,1);
histogram(theta_x*180/pi, int32(double(sketch_num)/2.0));
title('theta x')
subplot(3,1,2);
histogram(theta_y*180/pi, int32(double(sketch_num)/2.0));
title('theta y')
subplot(3,1,3);
histogram(theta_z*180/pi, int32(double(sketch_num)/2.0));
title('theta z')

figure(7);
[~,ind_sort_theta_z] = sort(theta_z*180/pi);
hold on;
plot(1:sketch_num, theta_z(ind_sort_theta_z)*180/pi, 'g');
plot(1:sketch_num, azimuths(ind_sort_theta_z)*180/pi, 'b');
legend('theta_z', 'azimuths');
figure(5); 

[theta_x,ind_sort_theta_x] = sort(theta_x);
theta_y = theta_y(ind_sort_theta_x);
theta_z = theta_z(ind_sort_theta_x);
hold on;
plot(1:sketch_num, theta_x, 'r');
plot(1:sketch_num, theta_y, 'b');
plot(1:sketch_num, theta_z, 'g');

legend('theta_x', 'theta_y', 'theta_z');

figure(4); 

[theta_z,ind_sort_theta_z] = sort(theta_z*180/pi);
theta_y = theta_y(ind_sort_theta_z)*180/pi;
theta_x = theta_x(ind_sort_theta_z)*180/pi;
hold on;
plot(1:sketch_num, theta_x, 'r');
plot(1:sketch_num, theta_y, 'b');
plot(1:sketch_num, theta_z, 'g');


legend('theta_x', 'theta_y', 'theta_z');

mean(theta_x*180/pi)
std(theta_x*180/pi)
mean(theta_y*180/pi)
std(theta_y*180/pi)
mean(theta_z*180/pi)
std(theta_z*180/pi)



hf = figure(3); 
set(hf, 'Name', 'elevations');
histogram(elevations*180/pi, sketch_num);

mean_elevations = mean(elevations)*180/pi
std_elevations = std(elevations)*180/pi

hf = figure(2); 
set(hf, 'Name', 'azimuths');
histogram(azimuths*180/pi, sketch_num);

azimuths = abs(azimuths);
azimuths(azimuths > pi/2) = azimuths(azimuths > pi/2) - pi/2;
mean_azimuths = mean(azimuths)*180/pi
std_azimuths = std(azimuths)*180/pi



% hf = figure; 
% set(hf, 'Name', azimuths);
% 
% histogram(principal_points_u, sketch_num);
% mean_u = mean(principal_points_u)
% mean_v = mean(principal_points_v)
% std_u = std(principal_points_u)
% std_v = std(principal_points_v)
% std_v = std(principal_points_v)
% figure(4); histogram(principal_points_v, sketch_num);
% figure(5); histogram(radiuses, sketch_num);

% mean_radiuses = mean(radiuses)
% std_radiuses = std(radiuses)
% [c,p] = corr(focal_lengths', radiuses')



hf = figure(1); 
set(hf, 'Name', 'focal_lengths');
focal_lengths = focal_lengths(focal_lengths < prctile(focal_lengths, 95));
histogram(focal_lengths, sketch_num);
mean_focal_lengths = mean(focal_lengths)
std_focal_lengths = std(focal_lengths)
% [focal_lengths, ind_s] = sort(focal_lengths);
% plot(focal_lengths/max(focal_lengths), 'r'); 
% hold on;
% plot(radiuses(ind_s)/max(radiuses), 'b');

view_angles = 2.0 * atan(1.0./focal_lengths) * 180.0 / pi;




% 
% figure(7); 
% histogram(focal_lengths, sketch_num);
% mean(focal_lengths)
% std(focal_lengths)

