% function [obj_designer_fx2fy, ...
%           obj_designer_f_restricted, ...
%           obj_designer_dist_fxfy2f] = ...
%           computeFocalLengthStatisticsForEachSkecth(designers)
% 
% Output:
%   obj_designer_fx2fy - num_objects-by-2*num_desginers (includes the data 
%       for view1 and view2). Contains the rations between fx and fy
%       computes with general projection matrix.
% 
%   obj_designer_f_restricted - num_objects-by-2*num_desginers (includes the data 
%       for view1 and view2). Contains the values of f
%       computes with restricted projection matrix.
% 
%   obj_designer_dist_fxfy2f - num_objects-by-2*num_desginers (includes the data 
%       for view1 and view2). Contains the distance from fx and fy to f.


function [obj_designer_fx, obj_designer_fy,...
          obj_designer_f_restricted, ...
          obj_designer_fx2fy, ...
          obj_designer_dist_fxfy2f] = ...
          computeFocalLengthStatisticsForEachSkecth(designers)

num_designers = length(designers);

%Initialise:
obj_designer_fx             = zeros(12, num_designers*2);      
obj_designer_fy             = zeros(12, num_designers*2);      
obj_designer_f_restricted   = zeros(12, num_designers*2);      
    
  

%Fill in:
for i = 1:num_designers
    
    [obj_designer_fx(:, i),...
     obj_designer_fy(:, i)] = ...
            fill_designer_fxfy( designers(i).objects,  ...
                                designers(i).camera_parameters_v1);
    [obj_designer_fx(:, i+num_designers),...
     obj_designer_fy(:, i+num_designers)] = ...
            fill_designer_fxfy( designers(i).objects_v2,  ...
                                designers(i).camera_parameters_v2);
                             
                             
    obj_designer_f_restricted(:, i) = ...
            fill_designer_f( designers(i).objects,  ...
                                 designers(i).camera_parameters_v1);
    obj_designer_f_restricted(:, i+num_designers) = ...
            fill_designer_f(designers(i).objects_v2,  ...
                                 designers(i).camera_parameters_v2);
                             

end

obj_designer_fx2fy = obj_designer_fx./obj_designer_fy;
obj_designer_dist_fxfy2f = ...
    sqrt(   (obj_designer_fx - obj_designer_f_restricted).^2 +...
            (obj_designer_fy - obj_designer_f_restricted).^2);
end


function [obj_designer_fx, obj_designer_fy] = ...
            fill_designer_fxfy(designer_objects, designer_camera_parameters)
        
     obj_designer_fx = zeros(12, 1);   
     obj_designer_fy = zeros(12, 1);   
     for j = 1:length(designer_objects)
            obj_num = designer_objects(j);
%             obj_designer_fx2fy(obj_num) = ...
%                 designer_camera_parameters(j).general.fx/...
%                 designer_camera_parameters(j).general.fy;
        obj_designer_fx(obj_num) = ...
            designer_camera_parameters(j).general.fx;
        obj_designer_fy(obj_num) = ...
            designer_camera_parameters(j).general.fy;
     end
end

function obj_designer_f = ...
            fill_designer_f(designer_objects, designer_camera_parameters)
        
     obj_designer_f = zeros(12, 1);   
     for j = 1:length(designer_objects)
            obj_num = designer_objects(j);
            obj_designer_f(obj_num) = ...
                designer_camera_parameters(j).restricted.f;
     end
end
