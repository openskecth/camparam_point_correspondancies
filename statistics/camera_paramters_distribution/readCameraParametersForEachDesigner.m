% function designers = readCameraParametersForEachDesigner(view)
% 
% Read the camera parameters for each designer into the structure
%   'designers(i).camera_parameters_v1(j)' or
%   'designers(i).camera_parameters_v2(j)',
%   where 'i' is the designer index and 'j' is the object index object 
%   designer draw in the list 'designers(i).objects'.

function designers = readCameraParametersForEachDesigner(designers, view)

% folder_camera_parameters = ...
%     'D:\Projects\SketchItProject\CollectedSketches\camera_parameters\camera_parameters_new\';

folder_camera_parameters = ...
    'D:\Projects\SketchItProject\CollectedSketches\camera-parameters\';


% folder_camera_parameters = ...
%     'D:\Projects\SketchItProject\CollectedSketches\camera-parameters\';

% folder_camera_parameters = ...
%     'D:\Projects\SketchItProject\CollectedSketches\camera_parameters\camera_parameters_v3\';

objects = objectsData();

num_designers = length(designers);

if ~exist('view', 'var')
    view = 'view1';
end

for i = 1:num_designers
    designer_type   = designers(i).designer_type;
    designer_id     = designers(i).id;
    
    
    if (strcmp(view, 'view1'))
        designer_objects = designers(i).objects;
    else
        designer_objects = designers(i).objects_v2;
    end
    
    
    for j = 1:length(designer_objects)
        
        obj_num = designer_objects(j);
        
        object_id    = objects(obj_num).name;
        
        path_save = fullfile(folder_camera_parameters, ...
                             designer_type, ...
                             designer_id, ...
                             object_id, ...
                             view, ...
                             'camera_parameters.json');

        
        fileID = fopen(path_save, 'r');
        text = fscanf(fileID, '%s');
        fclose(fileID);
        
        if (strcmp(view, 'view1'))
            designers(i).camera_parameters_v1(j) =...
                jsondecode(text);
        else
            designers(i).camera_parameters_v2(j) =...
                jsondecode(text);
        end
    end
end



end