function cameraParametersDistribution()

[~, designers(1, 1:9), ~]   = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

% fills in  designers(i).camera_parameters_v1(j):
designers = readCameraParametersForEachDesigner(designers, 'view1');
% fills in  designers(i).camera_parameters_v2(j):
designers = readCameraParametersForEachDesigner(designers, 'view2');

%Compute average focal length x - y:

[obj_designer_fx, obj_designer_fy,...
          obj_designer_f_restricted, ...
          obj_designer_fx2fy, ...
          obj_designer_dist_fxfy2f] = ...
          computeFocalLengthStatisticsForEachSkecth(designers);
      
%Crude approxiamtion of parameters:      
mask = obj_designer_f_restricted >0;
f_vals = obj_designer_f_restricted(mask);
mean_f_vals = mean(f_vals)
std_f_vals = std(f_vals)

computeFocalLengthMeanStdPerEachDesigner(designers,...
                obj_designer_fx, obj_designer_fy,...
                obj_designer_f_restricted, ...
                obj_designer_fx2fy, ...
                obj_designer_dist_fxfy2f)

            
ind_less_1  = obj_designer_fx2fy < 1;
obj_designer_fx2fy_max = obj_designer_fx2fy;
obj_designer_fx2fy_max(ind_less_1) = 1.0./obj_designer_fx2fy(ind_less_1);            
            

[corr_perspective, corr_proportions, corr_line_execution] = ...
            computeCorrelationWithDesignersHouseRanking(obj_designer_fx2fy_max(4,1:length(designers)))
        
skewValues = skewToMatrixForm(designers);
skewAngles = acot(-skewValues./obj_designer_fx);
% indLessZero = skewAngles < 0;
% skewAngles(indLessZero) = pi + skewAngles(indLessZero);


obj_designer_fy = abs(obj_designer_fy./sin(skewAngles));
obj_designer_fx2fy_max = max(obj_designer_fy./obj_designer_fx, obj_designer_fx./obj_designer_fy)-1;
[corr_perspective, corr_proportions, corr_line_execution] = ...
            computeCorrelationWithDesignersHouseRanking(obj_designer_fx2fy_max(4,1:length(designers)))



skewAngles = (pi/2- abs(skewAngles))/pi*180;

[corr_perspective, corr_proportions, corr_line_execution] = ...
            computeCorrelationWithDesignersHouseRanking(abs(skewAngles(4,1:length(designers))))


        
%   Convert focal length to field of view

%   Camera disatancies

%   Rotation angles with respect to the object

end