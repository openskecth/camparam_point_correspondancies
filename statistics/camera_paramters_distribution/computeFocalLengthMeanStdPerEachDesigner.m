function computeFocalLengthMeanStdPerEachDesigner(designers,...
                obj_designer_fx, ...
                obj_designer_fy,...
                obj_designer_f_restricted, ...
                obj_designer_fx2fy, ...
                obj_designer_dist_fxfy2f)

    num_designers = length(designers);

    mean_desginer_fx_v1     = zeros(1, num_designers);
    mean_desginer_fx_v2     = zeros(1, num_designers );
    mean_desginer_fx_v1v2   = zeros(1, num_designers );
    
    std_desginer_fx_v1      = zeros(1, num_designers);
    std_desginer_fx_v2      = zeros(1, num_designers );
    std_desginer_fx_v1v2    = zeros(1, num_designers );
    
    
    mean_desginer_fy_v1     = zeros(1, num_designers);
    mean_desginer_fy_v2     = zeros(1, num_designers );
    mean_desginer_fy_v1v2   = zeros(1, num_designers );
    
    std_desginer_fy_v1      = zeros(1, num_designers);
    std_desginer_fy_v2      = zeros(1, num_designers );
    std_desginer_fy_v1v2    = zeros(1, num_designers );
    
    
    mean_desginer_f_v1     = zeros(1, num_designers);
    mean_desginer_f_v2     = zeros(1, num_designers );
    mean_desginer_f_v1v2   = zeros(1, num_designers );
    
    std_desginer_f_v1      = zeros(1, num_designers);
    std_desginer_f_v2      = zeros(1, num_designers );
    std_desginer_f_v1v2    = zeros(1, num_designers );
    
    
    mean_desginer_fx2fy_v1     = zeros(1, num_designers);
    mean_desginer_fx2fy_v2     = zeros(1, num_designers );
    mean_desginer_fx2fy_v1v2   = zeros(1, num_designers );
    
    std_desginer_fx2fy_diff1_v1      = zeros(1, num_designers);
    std_desginer_fx2fy_diff1_v2      = zeros(1, num_designers );
    std_desginer_fx2fy_diff1_v1v2    = zeros(1, num_designers );
    
    std_desginer_fx2fy_v1      = zeros(1, num_designers);
    std_desginer_fx2fy_v2      = zeros(1, num_designers );
    std_desginer_fx2fy_v1v2    = zeros(1, num_designers );
    
    mean_desginer_dist_fxfy2f_v1     = zeros(1, num_designers);
    mean_desginer_dist_fxfy2f_v2     = zeros(1, num_designers );
    mean_desginer_dist_fxfy2f_v1v2   = zeros(1, num_designers );
    
    std_desginer_dist_fxfy2f_v1      = zeros(1, num_designers);
    std_desginer_dist_fxfy2f_v2      = zeros(1, num_designers );
    std_desginer_dist_fxfy2f_v1v2    = zeros(1, num_designers );
    
    
    ind_less_1  = obj_designer_fx2fy < 1;
    obj_designer_fx2fy_max = obj_designer_fx2fy;
    obj_designer_fx2fy_max(ind_less_1) = 1.0./obj_designer_fx2fy(ind_less_1);
            
    designers_names= cell(num_designers, 1);
    for i = 1:num_designers
        
        [   mean_desginer_fx_v1(i), std_desginer_fx_v1(i),...
            mean_desginer_fx_v2(i), std_desginer_fx_v2(i),...
            mean_desginer_fx_v1v2(i), std_desginer_fx_v1v2(i)]...
                = computeMeansAndStds(obj_designer_fx, i, num_designers);

       [    mean_desginer_fy_v1(i), std_desginer_fy_v1(i),...
            mean_desginer_fy_v2(i), std_desginer_fy_v2(i),...
            mean_desginer_fy_v1v2(i), std_desginer_fy_v1v2(i)]...
                = computeMeansAndStds(obj_designer_fy, i, num_designers);

        [   mean_desginer_f_v1(i), std_desginer_f_v1(i),...
            mean_desginer_f_v2(i), std_desginer_f_v2(i),...
            mean_desginer_f_v1v2(i), std_desginer_f_v1v2(i)]...
                = computeMeansAndStds(obj_designer_f_restricted, i, num_designers);
        
        [   mean_desginer_fx2fy_v1(i), std_desginer_fx2fy_v1(i),...
            mean_desginer_fx2fy_v2(i), std_desginer_fx2fy_v2(i),...
            mean_desginer_fx2fy_v1v2(i), std_desginer_fx2fy_v1v2(i)]...
                = computeMeansAndStds(obj_designer_fx2fy, i, num_designers);
            
          
            
          
            
        [   mean_desginer_fx2fy_diff1_v1(i), std_desginer_fx2fy_diff1_v1(i),...
            mean_desginer_fx2fy_diff1_v2(i), std_desginer_fx2fy_diff1_v2(i),...
            mean_desginer_fx2fy_diff1_v1v2(i), std_desginer_fx2fy_diff1_v1v2(i)]...
                = computeMeansAndStds(abs(obj_designer_fx2fy_max-1.0), i, num_designers);
            
            
           
            
            
        [   mean_desginer_dist_fxfy2f_v1(i), std_desginer_dist_fxfy2f_v1(i),...
            mean_desginer_dist_fxfy2f_v2(i), std_desginer_dist_fxfy2f_v2(i),...
            mean_desginer_dist_fxfy2f_v1v2(i), std_desginer_dist_fxfy2f_v1v2(i)]...
                = computeMeansAndStds(obj_designer_dist_fxfy2f, i, num_designers);  
            
        designers_names{i} = designers(i).id;  
    end
     close all;
%      h = figure;
%             errorbar(1:num_designers, mean_desginer_fx2fy_v1v2, std_desginer_fx2fy_v1v2, '-*'); 



    plotFocalLengthRationsGeneralProjection(num_designers, ...
            designers_names,...
            mean_desginer_fx2fy_diff1_v1v2,...
            std_desginer_fx2fy_diff1_v1v2);
    
    
    plotFocalLengthRationsGeneralvsRestrictedProjection(num_designers,...
            designers_names,...
            mean_desginer_dist_fxfy2f_v1v2,...
            std_desginer_dist_fxfy2f_v1v2)


    
%     [axis_y_min, axis_y_max] = compute_axis_limits(mean_desginer_fx2fy_diff1_v1v2, ...
%                                  std_desginer_fx2fy_diff1_v1v2);
%     axis([-inf inf axis_y_min axis_y_max]);      
    
    
    table = zeros(15, num_designers);
    table(1, :) = mean_desginer_fx_v1;
    table(2, :) = mean_desginer_fx_v2;
    table(3, :) = mean_desginer_fx_v1v2;
    
    table(4, :) = mean_desginer_fy_v1;
    table(5, :) = mean_desginer_fy_v2;
    table(6, :) = mean_desginer_fy_v1v2;
    
    table(7, :) = mean_desginer_f_v1;
    table(8, :) = mean_desginer_f_v2;
    table(9, :) = mean_desginer_f_v1v2;
    
    table(10, :) = mean_desginer_fx2fy_v1;
    table(11, :) = mean_desginer_fx2fy_v2;
    table(12, :) = mean_desginer_fx2fy_v1v2;
    
    table(13, :) = mean_desginer_dist_fxfy2f_v1;
    table(14, :) = mean_desginer_dist_fxfy2f_v2;
    table(15, :) = mean_desginer_dist_fxfy2f_v1v2;
    
    matrix_stds = zeros(15, num_designers);
    matrix_stds(1, :) = std_desginer_fx_v1;
    matrix_stds(2, :) = std_desginer_fx_v2;
    matrix_stds(3, :) = std_desginer_fx_v1v2;
    
    matrix_stds(4, :) = std_desginer_fy_v1;
    matrix_stds(5, :) = std_desginer_fy_v2;
    matrix_stds(6, :) = std_desginer_fy_v1v2;
    
    matrix_stds(7, :) = std_desginer_f_v1;
    matrix_stds(8, :) = std_desginer_f_v2;
    matrix_stds(9, :) = std_desginer_f_v1v2;
    
    matrix_stds(10, :) = std_desginer_fx2fy_v1;
    matrix_stds(11, :) = std_desginer_fx2fy_v2;
    matrix_stds(12, :) = std_desginer_fx2fy_v1v2;
    
    matrix_stds(13, :) = std_desginer_dist_fxfy2f_v1;
    matrix_stds(14, :) = std_desginer_dist_fxfy2f_v2;
    matrix_stds(15, :) = std_desginer_dist_fxfy2f_v1v2;
    
    
%     plot_focal_lengths(table(1:9,:), matrix_stds(1:9,:),num_designers)
end

function [mean_desginer, std_desginer] = computeMeanPerDesigner(matrix)
    ind_sketched = ~isnan(matrix)& matrix >0;
    mean_desginer = mean(matrix(ind_sketched));
    std_desginer = std(matrix(ind_sketched));
end


function [mean_desginer_v1, std_desginer_v1,...
    mean_desginer_v2, std_desginer_v2,...
    mean_desginer_v1v2, std_desginer_v1v2]...
    = computeMeansAndStds(obj_designer_data, i, num_designers)

        [mean_desginer_v1, std_desginer_v1] = ...
            computeMeanPerDesigner(obj_designer_data(:,i));
        [mean_desginer_v2, std_desginer_v2] = ...
            computeMeanPerDesigner(obj_designer_data(:,i+num_designers));
        [mean_desginer_v1v2, std_desginer_v1v2]  = ...
            computeMeanPerDesigner([obj_designer_data(:,i); obj_designer_data(:,i+num_designers)]);
end


function plot_focal_lengths(matrix_vals, matrix_stds, num_designers)
   if (~exist('axis_y_min', 'var'))
      [axis_y_min, axis_y_max] = compute_axis_limits(matrix_vals(:), ...
                                 matrix_stds(:));
   end 
   
   h = figure;
   num_rows = size(matrix_vals, 1);
   hold on;
   
   colors = lines(num_rows/3.0);
   
   for i = 1:num_rows/3.0
      errorbar(1:num_designers, matrix_vals(3*(i-1)+1,:), matrix_stds(3*(i-1)+1,:), '-.', 'Color', colors(i,:));
%       errorbar(1:num_designers, matrix_vals(3*(i-1)+2,:), matrix_stds(3*(i-1)+2,:), '--', 'Color', colors(i,:));
%       errorbar(1:num_designers, matrix_vals(3*(i-1)+3,:), matrix_stds(3*(i-1)+3,:), '-',  'Color', colors(i,:));
   end
end


function plotFocalLengthRationsGeneralProjection(num_designers,...
    designers_names,...
    mean_desginer_fx2fy_diff1_v1v2,...
    std_desginer_fx2fy_diff1_v1v2)

    h = figure;
    h.Units = 'centimeters';
    paper_sz_w = 16;
    paper_sz_h = 8;
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
    errorbar(1:num_designers, mean_desginer_fx2fy_diff1_v1v2, std_desginer_fx2fy_diff1_v1v2, '-*'); 
    set(gca,'xtick',1:num_designers,'xticklabel',designers_names(:,1), 'FontSize', 8);    
    xtickangle(90);
    ylabel('max(fx/fy, fy/fx) - 1');
    title('Analysis of focal length ratios');
    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
    subfolder = fullfile(folder, 'camera_parameters');
    if ~exist(subfolder, 'dir')
        mkdir(subfolder);
    end
    name = 'focal_length_rations_general_projection';    
    saveas(h, fullfile(subfolder, [name '.png']));
    print('-bestfit', fullfile(subfolder, [name, '.pdf']), '-dpdf');
end


function plotFocalLengthRationsGeneralvsRestrictedProjection(num_designers,...
            designers_names,...
            mean_desginer_dist_fxfy2f_v1v2,...
            std_desginer_dist_fxfy2f_v1v2)

    h = figure;
    h.Units = 'centimeters';
    paper_sz_w = 16;
    paper_sz_h = 8;
    h.PaperSize  = [paper_sz_w paper_sz_h];
    h.Position(3)  = paper_sz_w;
    h.Position(4)  = paper_sz_h;
    
    errorbar(1:num_designers, mean_desginer_dist_fxfy2f_v1v2, std_desginer_dist_fxfy2f_v1v2, '-*'); 
    set(gca, 'xtick', 1:num_designers, 'xticklabel', designers_names(:,1), 'FontSize', 8);    
    xtickangle(90);
    ylabel('sqrt((fx-f)^2 + (fy-f)^2)');
    title('Analysis of focal length estimations with general and restricted camera projection matrix');
    folder = 'D:\users\ygryadit\Data\SketchItProject\CollectedSketches\camera-parameters\statistics_plots\';
    subfolder = fullfile(folder, 'camera_parameters');
    if ~exist(subfolder, 'dir')
        mkdir(subfolder);
    end
    name = 'focal_length_rations_general_vs_restricted_projection';
    saveas(h, fullfile(subfolder, [name '.png']));
    print('-bestfit', fullfile(subfolder, [name, '.pdf']), '-dpdf');
end