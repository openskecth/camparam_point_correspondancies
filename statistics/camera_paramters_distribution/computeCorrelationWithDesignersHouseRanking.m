function [corr_perspective, corr_proportions, corr_line_execution] = ...
            computeCorrelationWithDesignersHouseRanking(values)

[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

[  ~, ind_designers, ...
            perspective_ranking,...
            proportions_ranking,...
            line_execution_ranking] = MJ_house_ranking();
        
corr_perspective    = corr2(perspective_ranking, values(ind_designers));      
corr_proportions    = corr2(proportions_ranking, values(ind_designers));      
corr_line_execution = corr2(line_execution_ranking, values(ind_designers)); 

figure;

values = values(ind_designers);
[values, ind_sorted] = sort(values);
% plot(line_execution_ranking(ind_sorted),  values);

plot(values, perspective_ranking(ind_sorted), ':');
hold on;
plot(values, proportions_ranking(ind_sorted));

end