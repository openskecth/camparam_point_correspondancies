function skewValues = skewToMatrixForm(designers)
num_designers = length(designers);
skewValues = zeros(12, num_designers*2);      

for i = 1:num_designers
    
    [skewValues(:, i)] = ...
            fill_skew_row( designers(i).objects,  ...
                                designers(i).camera_parameters_v1);
end

for i = 1:num_designers    
    [skewValues(:, num_designers+i)] = ...
            fill_skew_row( designers(i).objects_v2,  ...
                                designers(i).camera_parameters_v2);
end


end


function [skew_row] = ...
            fill_skew_row(designer_objects, designer_camera_parameters)
        
     skew_row = zeros(12, 1);         
     for j = 1:length(designer_objects)
        obj_num = designer_objects(j);

        skew_row(obj_num) = ...
            designer_camera_parameters(j).general.skew;
     end
end