%AVERAGE_TASK_COMPLEXITY_PER_DESIGNER Summary of this function goes here
%   

function [ task_difficulty ] = average_task_complexity_per_designer( )

% Load designers information:
[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();


scores = zeros(12, 1);
num_votes = zeros(12, 1);
for i = 1:length(designers)
   for j = 1:length(designers(i).objects)
       ind_obj = designers(i).objects(j);
       score = designers(i).objects_complexity(j);
       
       if ~isnan(score)
            scores(ind_obj) = scores(ind_obj) + score;
            num_votes(ind_obj) = num_votes(ind_obj) + 1; 
       end
   end
end

scores =  scores./num_votes;

task_difficulty = zeros(15, 1);
for i = 1:length(designers)
    
   for j = 1:length(designers(i).objects)
        obj_num = designers(i).objects(j);
        task_difficulty(i) = task_difficulty(i)+ scores(obj_num);
   end
    task_difficulty(i) =  task_difficulty(i)/length(designers(i).objects);
end


end

