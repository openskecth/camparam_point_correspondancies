[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

objects = objectsData();
tasks_objects = {[5 12],[3 6], [1 9], [8 10], [2 7], [1:12]};
tasks_designers = {[3],[6 8], [2 9], [1 7], [4 5], 10:15};
task_complexity  = zeros(length(tasks_objects), 1);

%evaluate complexity of the task given by the designer itself:

% for tn = 1:length(tasks_objects)
%    num_grades = num_grades + 1;
%    for obj = tasks_objects{tn}
%         %%Average complexity of the object by designers who draw that
%         %%object:
%         
%         for designer = tasks_designers{tn}
%             task_complexity(tn) = 
%             num_designer = num_designer + 1;
%         end
%    end
% end
obj_complexity_prof = zeros(12,1);
obj_rates_count = zeros(12,1);

for obj = 1:12
    for dn = 10:15
        [in, idx] = ismember(obj, designers(dn).objects);
        if (in)
           if  ~isnan(designers(dn).objects_complexity(idx))
               obj_complexity_prof(obj) = ...
                   obj_complexity_prof(obj) + ...
                   designers(dn).objects_complexity(idx);
               obj_rates_count(obj) = obj_rates_count(obj) + 1;
           end
        end
    end
end

for obj = 1:12
     obj_complexity_prof(obj) =  obj_complexity_prof(obj)/ obj_rates_count(obj);
end

mean(obj_complexity_prof)
%% ----

obj_complexity_all = zeros(12,1);
obj_rates_count = zeros(12,1);

for obj = 1:12
    for dn = 1:15
        [in, idx] = ismember(obj, designers(dn).objects);
        if (in)
           if  ~isnan(designers(dn).objects_complexity(idx))
               obj_complexity_all(obj) = ...
                   obj_complexity_all(obj) + ...
                   designers(dn).objects_complexity(idx);
               obj_rates_count(obj) = obj_rates_count(obj) + 1;
           end
        end
    end
end

for obj = 1:12
     obj_complexity_all(obj) =  obj_complexity_all(obj)/ obj_rates_count(obj);
end

mean(obj_complexity_all)