% Load designers information:
[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();

scores = zeros(12, 1);
num_votes = zeros(12, 1);
for i = 1:length(designers)
   for j = 1:length(designers(i).objects)
       ind_obj = designers(i).objects(j);
       score = designers(i).objects_complexity(j);
       
       if ~isnan(score)
            scores(ind_obj) = scores(ind_obj) + score;
            num_votes(ind_obj) = num_votes(ind_obj) + 1; 
       end
   end
end

 