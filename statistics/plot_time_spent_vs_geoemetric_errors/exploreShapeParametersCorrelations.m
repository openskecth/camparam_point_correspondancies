function [table, vals] = exploreShapeParametersCorrelations(errors_mean_inliers_both,...
    errors_mean_inliers_v1,...
    errors_mean_inliers_v2,...
    mean_error_per_object_v1,...
    mean_error_per_object_v2,...
    mean_error_per_object_both)

close all;
%% Load intial data about the designers
% Load line types information:
[line_label, line_types_names, line_types_colors, labels_order] = lineTypesData();

% Load objects information:
objects = objectsData();
scores = zeros(12,1);
for object_num = 1:12
    scores(object_num) = objects(object_num).avg_score;
    
    objects(object_num).num_students_sketched = 0.0;
    objects(object_num).num_designers_sketched = 0.0;
    for l = 1:(line_label.shadow_construction+1)
        objects(object_num).num_students_used_line(l) = 0.0;
        objects(object_num).num_designers_used_line(l) = 0.0;
    end
end    

% Load designers information:
[~, designers(1, 1:9), ~] = studentsData();
[~, designers(1, 10:15), ~] = professionalsData();


%% Load the time and number of strokes statistics about sketch:

num_designers = length(designers);
[time_full_sketch_v1, num_strokes_v1] = loadTimeStrokeDataView('view1', designers, objects);
[time_full_sketch_v2, num_strokes_v2] = loadTimeStrokeDataView('view2', designers, objects);

%% Exclude 2% from top outliers, rarrange to vectors and remove points with no data:

[time_full_sketch_v1, time_full_sketch_v2] = clean_tails(time_full_sketch_v1, time_full_sketch_v2);
[errors_mean_inliers_v1, errors_mean_inliers_v2] = clean_tails(errors_mean_inliers_v1, errors_mean_inliers_v2);


%% Sketch shape score
shape_scores = zeros(12, num_designers);
shape_scores_avg = zeros(12, num_designers);

for i = 1:num_designers
   for j = 1:length(designers(i).objects)
       object_num = designers(i).objects(j);
       shape_scores(object_num, i) = designers(i).objects_complexity(j);
       shape_scores_avg(object_num, i) = scores(object_num);
   end
end

%% Convert to vectors keeping only data points with the information:
ind_v1 = errors_mean_inliers_v1 > 0 & num_strokes_v1 > 0 & time_full_sketch_v1 > 0 & shape_scores > 0 & ~isnan(shape_scores);
ind_v2 = errors_mean_inliers_v2 > 0 & num_strokes_v2 > 0 & time_full_sketch_v2 > 0 & shape_scores > 0 & ~isnan(shape_scores);

%% Compute correlation time & number strokes:    
[vals.time_stroke_v1_pearson,   vals.time_stroke_v1_pearson_p ]     = corr(time_full_sketch_v1(ind_v1),num_strokes_v1(ind_v1), 'Type', 'Pearson');
[vals.time_stroke_v2_pearson,   vals.time_stroke_v2_pearson_p ]     = corr(time_full_sketch_v2(ind_v2),num_strokes_v2(ind_v2), 'Type', 'Pearson');
[vals.time_stroke_v1v2_pearson, vals.time_stroke_v1v2_pearson_p ]   = corr([time_full_sketch_v1(ind_v1); time_full_sketch_v2(ind_v2)],...
                                                                            [num_strokes_v1(ind_v1); num_strokes_v2(ind_v2)], 'Type', 'Pearson');
disp(vals);


%% Compute means per object

dim = 2;
[data_mean.mean_sketch_time_per_object_v1, ...
 data_mean.mean_sketch_time_per_object_v2,...
 data_mean.mean_sketch_time_per_object] =  compute_means(time_full_sketch_v1, time_full_sketch_v2, dim);

[data_mean.mean_num_strokes_per_object_v1, ...
 data_mean.mean_num_strokes_per_object_v2,...
 data_mean.mean_num_strokes_per_object] =  compute_means(num_strokes_v1, num_strokes_v2, dim);
                                      
                                        
                                    
    %% Correlation score-time on average per object shape
    table = cell(37, 3);
    
    innacuracies.mean_error_per_object_v1   = mean_error_per_object_v1;
    innacuracies.mean_error_per_object_v2   = mean_error_per_object_v2;
    innacuracies.mean_error_per_object_both = mean_error_per_object_both;
    
    table_column = 2;
    table = computeCorreltaions('Pearson', table_column, table, data_mean, scores, innacuracies);
    table{37, 2} = 'Pearson avg. object';
    
    table_column = 3;
    table = computeCorreltaions('Spearman', table_column, table, data_mean, scores, innacuracies);
    table{37, 3} = 'Spearman avg. object';
    
    
    %% Correlation on per sketch basis with the initial scores:


    num_sketches_v1 = sum(ind_v1(:));
    num_sketches_v2 = sum(ind_v2(:));
    num_sketches = num_sketches_v1 + num_sketches_v2;

   [        data_sketch.inaccuracies_per_sketch,...
            data_sketch.times_per_sketch,...
            data_sketch.num_strokes_per_sketch,...
            shape_scores_vec] = ...
                convert_to_vector(num_sketches, num_sketches_v1, num_sketches_v2, ...
                               ind_v1, ind_v2, ...
                               errors_mean_inliers_v1, errors_mean_inliers_v2,...
                               time_full_sketch_v1, time_full_sketch_v2,...
                               num_strokes_v1, num_strokes_v2,...
                               shape_scores);
    data_sketch.num_sketches_v1 = num_sketches_v1;
    data_sketch.num_sketches_v2 = num_sketches_v2;
    
    table_column = 4;
    table = computeCorreltaionsSketch('Pearson', table_column, table, data_sketch, shape_scores_vec, innacuracies);
    table{37, 4} = 'Pearson sketch original score';
    
    table_column = 5;
    table = computeCorreltaionsSketch('Spearman', table_column, table, data_sketch, shape_scores_vec, innacuracies);
    table{37, 5} = 'Spearman sketch original score';

%% Shapes score avg:
ind_v1 = errors_mean_inliers_v1 > 0 & num_strokes_v1 > 0 & time_full_sketch_v1 > 0 & shape_scores > 0 & ~isnan(shape_scores);
ind_v2 = errors_mean_inliers_v2 > 0 & num_strokes_v2 > 0 & time_full_sketch_v2 > 0 & shape_scores > 0 & ~isnan(shape_scores);


%     ind_v1 = errors_mean_inliers_v1 > 0 & num_strokes_v1 > 0 & time_full_sketch_v1 > 0 ;
%     ind_v2 = errors_mean_inliers_v2 > 0 & num_strokes_v2 > 0 & time_full_sketch_v2 > 0 ;

    num_sketches_v1 = sum(ind_v1(:));
    num_sketches_v2 = sum(ind_v2(:));
    num_sketches = num_sketches_v1 + num_sketches_v2;

    data_sketch.inaccuracies_per_sketch = zeros(num_sketches, 1);
    data_sketch.times_per_sketch = zeros(num_sketches, 1);
    data_sketch.num_strokes_per_sketch = zeros(num_sketches, 1);

    shape_scores_vec = zeros(num_sketches, 1);

    data_sketch.inaccuracies_per_sketch(1:num_sketches_v1) = errors_mean_inliers_v1(ind_v1);
    data_sketch.num_strokes_per_sketch(1:num_sketches_v1) = num_strokes_v1(ind_v1);
    data_sketch.times_per_sketch(1:num_sketches_v1) = time_full_sketch_v1(ind_v1);
    shape_scores_vec(1:num_sketches_v1) = shape_scores_avg(ind_v1);

    data_sketch.inaccuracies_per_sketch(num_sketches_v1+[1:num_sketches_v2]) = errors_mean_inliers_v2(ind_v2);
    data_sketch.num_strokes_per_sketch(num_sketches_v1+[1:num_sketches_v2]) = num_strokes_v2(ind_v2);
    data_sketch.times_per_sketch(num_sketches_v1+[1:num_sketches_v2]) = time_full_sketch_v2(ind_v2);
    shape_scores_vec(num_sketches_v1+[1:num_sketches_v2])  = shape_scores_avg(ind_v2);

    table_column = 6;
    table = computeCorreltaionsSketch('Pearson', table_column, table, data_sketch, shape_scores_vec, innacuracies);
    table{37, table_column} = 'Pearson sketch avg score';
     
    table_column = 7;
    table = computeCorreltaionsSketch('Spearman', table_column, table,data_sketch, shape_scores_vec, innacuracies);
    table{37, table_column} = 'Spearman sketch avg score';
    

end


function [values_view1, values_view2] = clean_tails(values_view1, values_view2)
    per_low = 0;
    per_up = 98;
    
    %Concatenate data for both views:
    values = [values_view1, values_view2];
    %Remove points where the shape was not sketched by a designer:
    values = values(values > 0.0);
    values = sort(values);
    
    %Thresholds on both views:
    thr_low    = prctile(values(:), per_low);
    thr_up     = prctile(values(:), per_up);
    
    
    ind_low    = values_view1(:) <= thr_low;
    ind_up     = values_view1(:) >= thr_up;
    values_view1(ind_low|ind_up) = 0.0;

 
    ind_low     = values_view2(:) <= thr_low;
    ind_up      = values_view2(:) >= thr_up;
    values_view2(ind_low|ind_up) = 0.0;
end



function [time_full_sketch, num_strokes] = loadTimeStrokeDataView(view_str, designers, objects)
% Output:
%     time_full_sketch: num_objects by num_designers matrix of time in minute spent per one sketch
%     num_strokes
 
    num_designers = length(designers);
    time_full_sketch = zeros(12, num_designers);
    num_strokes = zeros(12, num_designers);

    for i = 1:num_designers   
        if strcmp(view_str, 'view1')
            objects_designer = designers(i).objects;
        else
            objects_designer = designers(i).objects_v2;
        end
       for j = 1:length(objects_designer)
            fprintf('i=%d/%d, j = %d/%d\n', i, num_designers, j, length(objects_designer));
            object_num = objects_designer(j);

            filepath = fullfile(designers(i).folder_home,designers(i).id, ...
                                objects(object_num).name, ...
                                ['drawings_' objects(object_num).name '\' view_str '_concept_strokesStatistics.json']);
            fid = fopen(filepath);
            raw = fread(fid,inf);
            str = char(raw');
            fclose(fid);      
            data = JSON.parse(str);    
            time_full_sketch(object_num, i) = data.time_full_sketch/60;
            num_strokes(object_num, i) = length(data.speed);
       end
    end

end


function [mean_vals_v1, mean_vals_v2, mean_vals] = compute_means(vals_v1, vals_v2, dim)

mean_vals_v1     = sum(vals_v1,dim)./sum(vals_v1>0,dim);
mean_vals_v2     = sum(vals_v2,dim)./sum(vals_v2>0,dim);
mean_vals        = (sum(vals_v1,dim)+sum(vals_v2,dim))./(sum(vals_v1>0,dim)+sum(vals_v2>0,dim));                                 
                                      
end


function [  inaccuracies_per_sketch,...
            times_per_sketch,...
            num_strokes_per_sketch,...
            shape_scores_vec] = ...
                convert_to_vector(num_sketches, num_sketches_v1, num_sketches_v2, ...
                               ind_v1, ind_v2, ...
                               errors_mean_inliers_v1, errors_mean_inliers_v2,...
                               time_full_sketch_v1, time_full_sketch_v2,...
                               num_strokes_v1, num_strokes_v2,...
                               shape_scores)
                           
    inaccuracies_per_sketch = zeros(num_sketches, 1);
    times_per_sketch = zeros(num_sketches, 1);
    num_strokes_per_sketch = zeros(num_sketches, 1);

    shape_scores_vec = zeros(num_sketches, 1);

    inaccuracies_per_sketch(1:num_sketches_v1) = errors_mean_inliers_v1(ind_v1);
    num_strokes_per_sketch(1:num_sketches_v1) = num_strokes_v1(ind_v1);
    times_per_sketch(1:num_sketches_v1) = time_full_sketch_v1(ind_v1);
    shape_scores_vec(1:num_sketches_v1) = shape_scores(ind_v1);

    inaccuracies_per_sketch(num_sketches_v1+[1:num_sketches_v2]) = errors_mean_inliers_v2(ind_v2);
    num_strokes_per_sketch(num_sketches_v1+[1:num_sketches_v2]) = num_strokes_v2(ind_v2);
    times_per_sketch(num_sketches_v1+[1:num_sketches_v2]) = time_full_sketch_v2(ind_v2);
    shape_scores_vec(num_sketches_v1+[1:num_sketches_v2])  = shape_scores(ind_v2);

end