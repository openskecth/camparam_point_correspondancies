function table = computeCorreltaions(correlation_type_string, table_column, table, data_mean, scores, innacuracies)

    mean_sketch_time_per_object_v1  = data_mean.mean_sketch_time_per_object_v1;
    mean_sketch_time_per_object_v2  = data_mean.mean_sketch_time_per_object_v2;
    mean_sketch_time_per_object     = data_mean.mean_sketch_time_per_object;
    mean_num_strokes_per_object_v1  = data_mean.mean_num_strokes_per_object_v1;
    mean_num_strokes_per_object_v2  = data_mean.mean_num_strokes_per_object_v2;
    mean_num_strokes_per_object     = data_mean.mean_num_strokes_per_object;
    
    mean_error_per_object_v1   = innacuracies.mean_error_per_object_v1;
    mean_error_per_object_v2   = innacuracies.mean_error_per_object_v2;
    mean_error_per_object_both = innacuracies.mean_error_per_object_both;
    
    %% Score -- time
    [cor_score_time_v1, p_cor_score_time_v1] = corr(scores,mean_sketch_time_per_object_v1, 'type', correlation_type_string);
    [cor_score_time_v2, p_cor_score_time_v2] = corr(scores,mean_sketch_time_per_object_v2, 'type', correlation_type_string);
    [cor_score_time,    p_cor_score_time]    = corr(scores,mean_sketch_time_per_object, 'type', correlation_type_string);

    %% Inncacuracies -- time
    [cor_error_time_v1, p_cor_error_time_v1] = corr(mean_error_per_object_v1',mean_sketch_time_per_object_v1, 'type', correlation_type_string);
    [cor_error_time_v2, p_cor_error_time_v2] = corr(mean_error_per_object_v2',mean_sketch_time_per_object_v2, 'type', correlation_type_string);
    [cor_error_time,    p_cor_error_time]    = corr(mean_error_per_object_both',mean_sketch_time_per_object, 'type', correlation_type_string);

    %% Score -- num_strokes
    [cor_score_num_strokes_v1, p_cor_score_num_strokes_v1] = corr(scores,mean_num_strokes_per_object_v1, 'type', correlation_type_string);
    [cor_score_num_strokes_v2, p_cor_score_num_strokes_v2] = corr(scores,mean_num_strokes_per_object_v2, 'type', correlation_type_string);
    [cor_score_num_strokes, p_cor_score_num_strokes]       = corr(scores,mean_num_strokes_per_object, 'type', correlation_type_string);

    %% Inncacuracies -- num_strokes
    [cor_error_num_strokes_v1, p_cor_error_num_strokes_v1] = corr(mean_error_per_object_v1', mean_num_strokes_per_object_v1, 'type', correlation_type_string);
    [cor_error_num_strokes_v2, p_cor_error_num_strokes_v2] = corr(mean_error_per_object_v2', mean_num_strokes_per_object_v2, 'type', correlation_type_string);
    [cor_error_num_strokes, p_cor_error_num_strokes]       = corr(mean_error_per_object_both', mean_num_strokes_per_object, 'type', correlation_type_string);

    %% Plot error -- num strokes (v1 and both) 
    h = figure;
    
    [~, ind_sort] = sort(mean_error_per_object_both);

    plotCorrealtion(mean_error_per_object_both(ind_sort)', mean_sketch_time_per_object(ind_sort), ...
        'error both', 'time', correlation_type_string, ...
        cor_error_time, p_cor_error_time, ...
        h, 1);

    % Both views:
    
    plotCorrealtion(mean_error_per_object_both(ind_sort)',mean_num_strokes_per_object(ind_sort), ...
        'errors both', 'num strokes per sketch both', correlation_type_string, ...
        cor_error_num_strokes, p_cor_error_num_strokes, ...
        h, 2);


    %% Score - inncacuracies
    [cor_score_error_v1, p_cor_score_error_v1]  = corr(scores, mean_error_per_object_v1', 'type', correlation_type_string)
    [cor_score_error_v2, p_cor_score_error_v2]  = corr(scores, mean_error_per_object_v2', 'type', correlation_type_string)
    [cor_score_error, p_cor_score_error]        = corr(scores, mean_error_per_object_both', 'type', correlation_type_string)


    %% Time - num. strokes
    [cor_time_num_strokes_v1, p_cor_time_num_strokes_v1] = corr(mean_sketch_time_per_object_v1,mean_num_strokes_per_object_v1, 'type', correlation_type_string);
    [cor_time_num_strokes_v2, p_cor_time_num_strokes_v2] = corr(mean_sketch_time_per_object_v2,mean_num_strokes_per_object_v2, 'type', correlation_type_string);
    [cor_time_num_strokes, p_cor_time_num_strokes] = corr(mean_sketch_time_per_object,mean_num_strokes_per_object, 'type', correlation_type_string);
    %% Table

    
    table(1,1) = {'cor_score_time_v1'}'; table(1,table_column) = num2cell(cor_score_time_v1);
    table(2,1) = {'p_cor_score_time_v1'}'; table(2, table_column) = num2cell(p_cor_score_time_v1);
    table(3,1) = {'cor_score_time_v2'}'; table(3, table_column) = num2cell(cor_score_time_v2);
    table(4,1) = {'p_cor_score_time_v2'}'; table(4, table_column) = num2cell(p_cor_score_time_v2);
    table(5,1) = {'cor_score_time'}'; table(5, table_column) = num2cell(cor_score_time);
    table(6,1) = {'p_cor_score_time'}'; table(6, table_column) = num2cell(p_cor_score_time);
    
    
    table(7,1) = {'cor_error_time_v1'}';    table(7, table_column) = num2cell(cor_error_time_v1);
    table(8,1) = {'p_cor_error_time_v1'}';  table(8, table_column) = num2cell(p_cor_error_time_v1);
    table(9,1) = {'cor_error_time_v2'}';    table(9, table_column) = num2cell(cor_error_time_v2);
    table(10,1) = {'p_cor_error_time_v2'}'; table(10, table_column) = num2cell(p_cor_error_time_v2);
    table(11,1) = {'cor_error_time'}';      table(11, table_column) = num2cell(cor_error_time);
    table(12,1) = {'p_cor_error_time'}';    table(12, table_column) = num2cell(p_cor_error_time);
    
    
    
    table(13,1) = {'cor_score_num_strokes_v1'}';    table(13, table_column) = num2cell(cor_score_num_strokes_v1);
    table(14,1) = {'p_cor_score_num_strokes_v1'}';  table(14, table_column) = num2cell(p_cor_score_num_strokes_v1);
    table(15,1) = {'cor_score_num_strokes_v2'}';    table(15, table_column) = num2cell(cor_score_num_strokes_v2);
    table(16,1) = {'p_cor_score_num_strokes_v2'}';  table(16, table_column) = num2cell(p_cor_score_num_strokes_v2);
    table(17,1) = {'cor_score_num_strokes'}';       table(17, table_column) = num2cell(cor_score_num_strokes);
    table(18,1) = {'p_cor_score_num_strokes'}';     table(18, table_column) = num2cell(p_cor_score_num_strokes);
    
    
    table(19,1) = {'cor_error_num_strokes_v1'}';    table(19, table_column) = num2cell(cor_error_num_strokes_v1);
    table(20,1) = {'p_cor_error_num_strokes_v1'}';  table(20, table_column) = num2cell(p_cor_error_num_strokes_v1);
    table(21,1) = {'cor_error_num_strokes_v2'}';    table(21, table_column) = num2cell(cor_error_num_strokes_v2);
    table(22,1) = {'p_cor_error_num_strokes_v2'}';  table(22, table_column) = num2cell(p_cor_error_num_strokes_v2);
    table(23,1) = {'cor_error_num_strokes'}';       table(23, table_column) = num2cell(cor_error_num_strokes);
    table(24,1) = {'p_cor_error_num_strokes'}';     table(24, table_column) = num2cell(p_cor_error_num_strokes);
    
    
    table(25,1) = {'cor_score_error_v1'}';    table(25, table_column) = num2cell(cor_score_error_v1);
    table(26,1) = {'p_cor_score_error_v1'}';  table(26, table_column) = num2cell(p_cor_score_error_v1);
    table(27,1) = {'cor_score_error_v2'}';    table(27, table_column) = num2cell(cor_score_error_v2);
    table(28,1) = {'p_cor_score_error_v2'}';  table(28, table_column) = num2cell(p_cor_score_error_v2);
    table(29,1) = {'cor_score_error'}';       table(29, table_column) = num2cell(cor_score_error);
    table(30,1) = {'p_cor_score_error'}';     table(30, table_column) = num2cell(p_cor_score_error);
    
    
    table(31,1) = {'cor_time_num_strokes_v1'}';    table(31, table_column) = num2cell(cor_time_num_strokes_v1);
    table(32,1) = {'p_cor_time_num_strokes_v1'}';  table(32, table_column) = num2cell(p_cor_time_num_strokes_v1);
    table(33,1) = {'cor_time_num_strokes_v2'}';    table(33, table_column) = num2cell(cor_time_num_strokes_v2);
    table(34,1) = {'p_cor_time_num_strokes_v2'}';  table(34, table_column) = num2cell(p_cor_time_num_strokes_v2);
    table(35,1) = {'cor_time_num_strokes'}';       table(35, table_column) = num2cell(cor_time_num_strokes);
    table(36,1) = {'p_cor_time_num_strokes'}';     table(36, table_column) = num2cell(p_cor_time_num_strokes);
    end