    function plotCorrealtion(vals1, vals2, xlabel_str, ylabel_str, correlation_type_str, r, p, h, subplot_plot_num)
        figure(h.Number); 
        subplot(2,1,subplot_plot_num);
        plot(vals1, vals2);
        hold on;
        if (strcmp('Pearson', correlation_type_str))
            f=polyfit(vals1,vals2,1);
        else
            f=polyfit(vals1,vals2,2);
        end
        
        x=vals1;
        y=polyval(f,x);
        plot(x,y);
        
        xlabel(xlabel_str);
        ylabel(ylabel_str);
        legend(sprintf('r = %.3f, p = %.3f', r, p));
        title(correlation_type_str);
        set(h, 'Name', [correlation_type_str ' ' xlabel_str ]);
    end