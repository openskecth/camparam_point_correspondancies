function [cor_time_accuracy, cor_num_strokes_accuracy] = correlation_plot(inaccuracy, times_per_sketch, num_strokes_per_sketch, f1, f2, title_str)
dx=2; dy=2;
%% Remove outliers:
%In inaccuracy
v2 = prctile(inaccuracy, 95);
v1 = prctile(inaccuracy, 5);
ind = inaccuracy > v1 & inaccuracy < v2;
inaccuracy = inaccuracy(ind);
times_per_sketch = times_per_sketch(ind);
num_strokes_per_sketch = num_strokes_per_sketch(ind);

%In time
v2 = prctile(times_per_sketch, 95);
v1 = prctile(times_per_sketch, 5);
ind = times_per_sketch > v1 & times_per_sketch < v2;
inaccuracy = inaccuracy(ind);
times_per_sketch = times_per_sketch(ind);
num_strokes_per_sketch = num_strokes_per_sketch(ind);

%In num_strokes_per_sketch
v2 = prctile(num_strokes_per_sketch, 95);
v1 = prctile(num_strokes_per_sketch, 5);
ind = num_strokes_per_sketch > v1 & num_strokes_per_sketch < v2;
inaccuracy = inaccuracy(ind);
times_per_sketch = times_per_sketch(ind);
num_strokes_per_sketch = num_strokes_per_sketch(ind);

%% 
[accuracy_sorted, ind] = sort(inaccuracy);
time_spent_sorted = times_per_sketch(ind);
num_strokes_sorted = num_strokes_per_sketch(ind);
cor_time_accuracy = corr2(accuracy_sorted',time_spent_sorted');
cor_num_strokes_accuracy = corr2(accuracy_sorted',num_strokes_sorted');

subplot(dx,dy,f1);
plot(accuracy_sorted, time_spent_sorted/60);
ylabel('time');
xlabel('inaccuracy');
title(title_str)
legend(sprintf('correlation coefficient: %.3f', cor_time_accuracy));

subplot(dx,dy,f2);
plot(accuracy_sorted, num_strokes_sorted);
ylabel('number of strokes');
xlabel('inaccuracy');
title(title_str)
legend(sprintf('correlation coefficient: %.3f', cor_num_strokes_accuracy));

end