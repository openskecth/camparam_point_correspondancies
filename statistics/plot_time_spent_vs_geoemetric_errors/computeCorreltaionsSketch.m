 function table = computeCorreltaionsSketch(correlation_type_str, table_column, table,...
                            data_sketch, shape_scores, innacuracies)
    
    inaccuracies_per_sketch = data_sketch.inaccuracies_per_sketch;
    times_per_sketch = data_sketch.times_per_sketch;
    num_strokes_per_sketch = data_sketch.num_strokes_per_sketch;
    num_sketches_v1 = data_sketch.num_sketches_v1;
    num_sketches_v2 = data_sketch.num_sketches_v2;
                        
%     mean_error_per_object_v1   = innacuracies.mean_error_per_object_v1;
%     mean_error_per_object_v2   = innacuracies.mean_error_per_object_v2;
%     mean_error_per_object_both = innacuracies.mean_error_per_object_both;

        %% Score -- time
        [cor_score_time_v1, p_cor_score_time_v1] = corr(shape_scores(1:num_sketches_v1),...
                                                        times_per_sketch(1:num_sketches_v1), ...
                                                        'type', correlation_type_str);
        [cor_score_time_v2, p_cor_score_time_v2] = corr(shape_scores(num_sketches_v1+[1:num_sketches_v2]),...
                                                        times_per_sketch(num_sketches_v1+[1:num_sketches_v2]),...
                                                        'type', correlation_type_str);
        [cor_score_time,    p_cor_score_time]    = corr(shape_scores,...
                                                        times_per_sketch, 'type', correlation_type_str);

  
        %% Inncacuracies -- time
        [cor_error_time_v1, p_cor_error_time_v1] = corr(inaccuracies_per_sketch(1:num_sketches_v1),...
                                                        times_per_sketch(1:num_sketches_v1),...
                                                        'type', correlation_type_str);
        [cor_error_time_v2, p_cor_error_time_v2] = corr(inaccuracies_per_sketch(num_sketches_v1+[1:num_sketches_v2]),...
                                                        times_per_sketch(num_sketches_v1+[1:num_sketches_v2]),...
                                                        'type', correlation_type_str);
        [cor_error_time,    p_cor_error_time]    = corr(inaccuracies_per_sketch,times_per_sketch, 'type', correlation_type_str);
        
         %% Plot Score -- time
        h = figure;
        [~, ind_sort] = sort(times_per_sketch);
        plotCorrealtion(times_per_sketch(ind_sort), shape_scores(ind_sort), ...
            'times per sketch', 'shape scores',  correlation_type_str, ...
            cor_score_time, p_cor_score_time, ...
            h, 1);
        %% Plot Inncacuracies -- time
        plotCorrealtion(times_per_sketch(ind_sort), inaccuracies_per_sketch(ind_sort),...
            'times per sketch', 'inaccuracies_per_sketch', correlation_type_str, ...
             cor_error_time,    p_cor_error_time, ...
             h, 2);
             
        %% Score -- num_strokes
        
        [cor_score_num_strokes_v1, ...
         p_cor_score_num_strokes_v1]     = corr(shape_scores(1:num_sketches_v1), ...
                                            num_strokes_per_sketch(1:num_sketches_v1), ...
                                            'type', correlation_type_str);
        [cor_score_num_strokes_v2,...
         p_cor_score_num_strokes_v2]    = corr(shape_scores(num_sketches_v1+[1:num_sketches_v2]), ...
                                            num_strokes_per_sketch(num_sketches_v1+[1:num_sketches_v2]),...
                                            'type', correlation_type_str);
        [cor_score_num_strokes, ...
         p_cor_score_num_strokes]       = corr(shape_scores,...
                                                num_strokes_per_sketch, ...
                                                'type', correlation_type_str);
       
        %% Errors -- num_strokes
        [cor_error_num_strokes_v1,...
            p_cor_error_num_strokes_v1] = corr(inaccuracies_per_sketch(1:num_sketches_v1),...
                                                num_strokes_per_sketch(1:num_sketches_v1),...
                                                'type', correlation_type_str);
        [cor_error_num_strokes_v2,...
            p_cor_error_num_strokes_v2] = corr(inaccuracies_per_sketch(num_sketches_v1+[1:num_sketches_v2]),...
                                                num_strokes_per_sketch(num_sketches_v1+[1:num_sketches_v2]),...
                                                'type', correlation_type_str);
        [cor_error_num_strokes,...
            p_cor_error_num_strokes]    = corr(inaccuracies_per_sketch, ...
                                                num_strokes_per_sketch, ...
                                                'type', correlation_type_str);
         %% Plot Score -- num_strokes, Errors -- num_strokes
        h = figure;
        [~, ind_sort] = sort(num_strokes_per_sketch);
        
        plotCorrealtion(num_strokes_per_sketch(ind_sort),shape_scores(ind_sort),...
            'num strokes per sketch', 'shape scores',  correlation_type_str, ...
             cor_score_num_strokes, p_cor_score_num_strokes, h, 1);
               
        plotCorrealtion(num_strokes_per_sketch(ind_sort), inaccuracies_per_sketch(ind_sort),...
            'num strokes per sketch', 'inaccuracies_per_sketch',  correlation_type_str, ...
             cor_error_num_strokes, p_cor_error_num_strokes, h, 2);
         
        %% Correlation score-inncacuracies
        [cor_score_error_v1, p_cor_score_error_v1]  = corr(shape_scores(1:num_sketches_v1),inaccuracies_per_sketch(1:num_sketches_v1), 'type', correlation_type_str)
        [cor_score_error_v2, p_cor_score_error_v2]  = corr(shape_scores(num_sketches_v1+[1:num_sketches_v2]),inaccuracies_per_sketch(num_sketches_v1+[1:num_sketches_v2]), 'type', correlation_type_str)
        [cor_score_error, p_cor_score_error]        = corr(shape_scores,inaccuracies_per_sketch, 'type', correlation_type_str)

        [~, ind_sort] = sort(shape_scores);
        h= figure; plot(shape_scores(ind_sort), inaccuracies_per_sketch(ind_sort));
        xlabel('shape_scores');
        ylabel('inaccuracies_per_sketch');
        legend(sprintf('r = %.3f, p = %.3f', cor_score_error, p_cor_score_error));
        title(correlation_type_str);
        set(h, 'Name', correlation_type_str);
       
        %% Time - num. strokes
        [cor_time_num_strokes_v1, p_cor_time_num_strokes_v1] = corr(times_per_sketch(1:num_sketches_v1),num_strokes_per_sketch(1:num_sketches_v1), 'type', correlation_type_str)
        [cor_time_num_strokes_v2, p_cor_time_num_strokes_v2] = corr(times_per_sketch(num_sketches_v1+[1:num_sketches_v2]),num_strokes_per_sketch(num_sketches_v1+[1:num_sketches_v2]), 'type', correlation_type_str)
        [cor_time_num_strokes, p_cor_time_num_strokes] = corr(times_per_sketch,num_strokes_per_sketch, 'type', correlation_type_str)
        %% Table

        table(1,table_column)  = num2cell(cor_score_time_v1);
        table(2, table_column) = num2cell(p_cor_score_time_v1);
        table(3, table_column) = num2cell(cor_score_time_v2);
        table(4, table_column) = num2cell(p_cor_score_time_v2);
        table(5, table_column) = num2cell(cor_score_time);
        table(6, table_column) = num2cell(p_cor_score_time);


        table(7,1) = {'cor_error_time_v1'}';    table(7, table_column) = num2cell(cor_error_time_v1);
        table(8,1) = {'p_cor_error_time_v1'}';  table(8, table_column) = num2cell(p_cor_error_time_v1);
        table(9,1) = {'cor_error_time_v2'}';    table(9, table_column) = num2cell(cor_error_time_v2);
        table(10,1) = {'p_cor_error_time_v2'}'; table(10, table_column) = num2cell(p_cor_error_time_v2);
        table(11,1) = {'cor_error_time'}';      table(11, table_column) = num2cell(cor_error_time);
        table(12,1) = {'p_cor_error_time'}';    table(12, table_column) = num2cell(p_cor_error_time);



        table(13,1) = {'cor_score_num_strokes_v1'}';    table(13, table_column) = num2cell(cor_score_num_strokes_v1);
        table(14,1) = {'p_cor_score_num_strokes_v1'}';  table(14, table_column) = num2cell(p_cor_score_num_strokes_v1);
        table(15,1) = {'cor_score_num_strokes_v2'}';    table(15, table_column) = num2cell(cor_score_num_strokes_v2);
        table(16,1) = {'p_cor_score_num_strokes_v2'}';  table(16, table_column) = num2cell(p_cor_score_num_strokes_v2);
        table(17,1) = {'cor_score_num_strokes'}';       table(17, table_column) = num2cell(cor_score_num_strokes);
        table(18,1) = {'p_cor_score_num_strokes'}';     table(18, table_column) = num2cell(p_cor_score_num_strokes);


        table(19,1) = {'cor_error_num_strokes_v1'}';    table(19, table_column) = num2cell(cor_error_num_strokes_v1);
        table(20,1) = {'p_cor_error_num_strokes_v1'}';  table(20, table_column) = num2cell(p_cor_error_num_strokes_v1);
        table(21,1) = {'cor_error_num_strokes_v2'}';    table(21, table_column) = num2cell(cor_error_num_strokes_v2);
        table(22,1) = {'p_cor_error_num_strokes_v2'}';  table(22, table_column) = num2cell(p_cor_error_num_strokes_v2);
        table(23,1) = {'cor_error_num_strokes'}';       table(23, table_column) = num2cell(cor_error_num_strokes);
        table(24,1) = {'p_cor_error_num_strokes'}';     table(24, table_column) = num2cell(p_cor_error_num_strokes);


        table(25,1) = {'cor_score_error_v1'}';    table(25, table_column) = num2cell(cor_score_error_v1);
        table(26,1) = {'p_cor_score_error_v1'}';  table(26, table_column) = num2cell(p_cor_score_error_v1);
        table(27,1) = {'cor_score_error_v2'}';    table(27, table_column) = num2cell(cor_score_error_v2);
        table(28,1) = {'p_cor_score_error_v2'}';  table(28, table_column) = num2cell(p_cor_score_error_v2);
        table(29,1) = {'cor_score_error'}';       table(29, table_column) = num2cell(cor_score_error);
        table(30,1) = {'p_cor_score_error'}';     table(30, table_column) = num2cell(p_cor_score_error);
        
        table(31,1) = {'cor_time_num_strokes_v1'}';    table(31, table_column) = num2cell(cor_time_num_strokes_v1);
        table(32,1) = {'p_cor_time_num_strokes_v1'}';  table(32, table_column) = num2cell(p_cor_time_num_strokes_v1);
        table(33,1) = {'cor_time_num_strokes_v2'}';    table(33, table_column) = num2cell(cor_time_num_strokes_v2);
        table(34,1) = {'p_cor_time_num_strokes_v2'}';  table(34, table_column) = num2cell(p_cor_time_num_strokes_v2);
        table(35,1) = {'cor_time_num_strokes'}';       table(35, table_column) = num2cell(cor_time_num_strokes);
        table(36,1) = {'p_cor_time_num_strokes'}';     table(36, table_column) = num2cell(p_cor_time_num_strokes);
    end