num_pixels = 200;
mean_general_error_per_object_v1_pixels = zeros(1, 12);
std_general_error_per_object_v1_pixels = zeros(1, 12);


mean_restricted_error_per_object_v1_pixels = zeros(1, 12);
std_restricted_error_per_object_v1_pixels = zeros(1, 12);
bounding_box_v1_scaled = bounding_box_v1;
for i = 1:12
%    j = find(bounding_box_v1(i, :) == max(bounding_box_v1(i, :)));
   area = (bounding_box_v1(i, 2)-bounding_box_v1(i, 1))*...
            (bounding_box_v1(i, 4)-bounding_box_v1(i, 3))
   f = sqrt(400.0*400.0/area);
   bounding_box_v1_scaled(i, :) = bounding_box_v1(i, :)*f;
   
   mean_general_error_per_object_v1_pixels(:, i) = ...
       round(mean_general_error_per_object_v1(:, i)*f, 3);
   std_general_error_per_object_v1_pixels(:, i) = ...
       round(mean_std_general_per_object_v1(:, i)*f, 3);

   mean_restricted_error_per_object_v1_pixels(:, i) = ...
       round(mean_restricted_error_per_object_v1(:, i)*f, 3);
   std_restricted_error_per_object_v1_pixels(:, i) = ...
       round(mean_std_restricted_per_object_v1(:, i)*f, 3);

end

pixels = zeros(4, 12);
pixels(1, :) = mean_general_error_per_object_v1_pixels;
pixels(2, :) = std_general_error_per_object_v1_pixels;
pixels(3, :) = mean_restricted_error_per_object_v1_pixels;
pixels(4, :) = std_restricted_error_per_object_v1_pixels;


figure; for i = 1:12
plot([bounding_box_v1(i,1) bounding_box_v1(i,2) bounding_box_v1(i,2) bounding_box_v1(i,1) bounding_box_v1(i,1)],...
[bounding_box_v1(i,3) bounding_box_v1(i,3) bounding_box_v1(i,4) bounding_box_v1(i,4) bounding_box_v1(i,3)]);
hold on
end

figure; for i = 1:12
plot([bounding_box_v1_scaled(i,1) bounding_box_v1_scaled(i,2) bounding_box_v1_scaled(i,2) bounding_box_v1_scaled(i,1) bounding_box_v1_scaled(i,1)],...
[bounding_box_v1_scaled(i,3) bounding_box_v1_scaled(i,3) bounding_box_v1_scaled(i,4) bounding_box_v1_scaled(i,4) bounding_box_v1_scaled(i,3)]);
hold on
end