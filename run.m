folder = fileparts(which('run.m')); 
addpath(genpath(folder));
global folder_sketches;
folder_sketches = '.\sketches'; % the folder with all the sketches

global obj_files_folder;
obj_files_folder = '.\objects_obj_files'; % the folder with all the obj files

% global folder_centered_sketches;
% folder_centered_sketches = '.\centered_sketches'; 

global folder_camera_parameters;
folder_camera_parameters = '.\camera_parameters'; % the folder to save camera parameters


bumps_data;
flange_data;
hairdryer_data;
house_data;
mixer_data;
mouse_data;
waffle_data;
wobble_surface_data;
potato_chip_data;
shampoo_bottle_data;
tubes_data;
vacuum_cleaner_data;


