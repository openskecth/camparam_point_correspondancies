function obj = visualize_obj(filepath, fig_num, obj)
    addpath('D:\users\ygryadit\Data\SketchItProject\Code\Matlab\external');
    if (~exist('filepath', 'var'))
        filepath = 'D:\users\ygryadit\Data\SketchItProject\SketchItPaper\supplemental\objects_obj_files\wobble.obj';
    end
    
    if (~exist('obj', 'var'))
        obj = readObj(filepath);   %use appropriate file
    end

    if (~exist('fig_num', 'var'))
        fig_num = 1;
    end
    
    
%     close(figure(fig_num))
    figure(fig_num);
    xlabel('x')
    ylabel('y')
    zlabel('z')
%     set(gca, 'Units', 'pixels');
%     set(gca, 'XLim', [0 972]);
%     set(gca, 'YLim', [0 972]);
    %patch('vertices', obj.v, 'faces', obj.f.v, 'FaceVertexCData', 0.7*ones(length(obj.v), 3), 'FaceColor','flat', 'FaceLighting', 'gouraud');
%     set(gca, 'Units', 'pixels', 'Position', [1 1 972 972]);
    patch('vertices', obj.v, 'faces', obj.f.v, 'FaceVertexCData', 0.7*ones(length(obj.v), 3), 'FaceColor','flat', 'FaceLighting', 'flat', 'EdgeColor', [0 1 0]);
    %shading interp %// flat // faceted 
    % colormap(gray(256));
    % light('Position',[-1 0 0],'Style','local')
    % lighting gouraud
    camproj('perspective');
    camup([0 1 0])
    axis square
    axis equal
    grid on;
    cameratoolbar
    hold on;
end